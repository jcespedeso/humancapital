//package co.mil.imi.sigop.sigopbackend.security;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import co.mil.imi.sigop.ldap.object.Respuesta;
//import co.mil.imi.sigop.sigopbackend.constants.Constants;
//import co.mil.imi.sigop.sigopbackend.constants.Mensajes;
//import co.mil.imi.sigop.sigopbackend.dto.AuthenticationRequest;
//import co.mil.imi.sigop.sigopbackend.dto.AuthenticationResponse;
//import co.mil.imi.sigop.sigopbackend.dto.ModuloInputDTO;
//import co.mil.imi.sigop.sigopbackend.dto.RelacionUsuarioPerfilDTO;
//import co.mil.imi.sigop.sigopbackend.dto.UserDetailsSigop;
//import co.mil.imi.sigop.sigopbackend.dto.UsuariosDTO;
//import co.mil.imi.sigop.sigopbackend.security.util.JwtUtil;
//import co.mil.imi.sigop.sigopbackend.services.FuncionalidadesService;
//import co.mil.imi.sigop.sigopbackend.services.RelacionUsuarioPerfilService;
//import co.mil.imi.sigop.sigopbackend.services.UsuariosService;
//import co.mil.imi.sigop.sigopbackend.services.impl.LdapServiceImpl;
//import co.mil.imi.sigop.sigopbackend.services.impl.UserDetailsServiceSigop;
//
//@Service
//public class AutenticacionService {
//
//	@Autowired
//	private JwtUtil jwtTokenUtil;
//
//	@Autowired
//	private UserDetailsServiceSigop userDetailsService;
//
//	@Autowired
//	private LdapServiceImpl ldapService;
//
//	@Autowired
//	private UsuariosService usuariosService;
//
//	@Autowired
//	private RelacionUsuarioPerfilService usuarioPerfilService;
//
//	@Autowired
//	FuncionalidadesService funcionalidadesService;
//
//	public AuthenticationResponse procesarAutenticacion(AuthenticationRequest authenticationRequest) throws Exception {
//
//		AuthenticationResponse authenticationResponse = new AuthenticationResponse();
//		UsuariosDTO usuarioApp = usuariosService.findByUsuario(authenticationRequest.getUsername());
//
//		if (usuarioApp == null) {
//			authenticationResponse.setCodRespuesta(Constants.CODIGO_AUTH_NO_REG);
//			authenticationResponse.setMsjRespuesta(Mensajes.USUARIO_NO_REGISTRADO);
//			return authenticationResponse;
//		} else if (usuarioApp.getEstado() != (Constants.ACTIVO)) {
//			authenticationResponse.setCodRespuesta(Constants.CODIGO_AUTH_NO_ACT);
//			authenticationResponse.setMsjRespuesta(Mensajes.USUARIO_NO_ACTIVO);
//			return authenticationResponse;
//		}
//
//		Respuesta respuestaLdap = ldapService.autenticar(authenticationRequest.getUsername(),
//				authenticationRequest.getPassword());
//
//		if (respuestaLdap.getCodigo().intValue() == Constants.CODIGO_LOGIN_FALLO.intValue()) {
//			authenticationResponse.setCodRespuesta(respuestaLdap.getCodigo());
//			authenticationResponse.setMsjRespuesta(Mensajes.LOGIN_FALLIDO);
//			return authenticationResponse;
//		} else if (respuestaLdap.getCodigo().intValue() == Constants.CODIGO_AUTH_EXITOSO.intValue()) {
//
//			List<RelacionUsuarioPerfilDTO> perfilesUsuario = usuarioPerfilService
//					.findByIdUsuario(usuarioApp.getIdUsuario());
//
//			if (perfilesUsuario == null || perfilesUsuario.size() < 1) {
//				authenticationResponse.setCodRespuesta(respuestaLdap.getCodigo());
//				authenticationResponse.setMsjRespuesta(Mensajes.USUARIO_NO_PERFIL);
//				return authenticationResponse;
//			} else {
//				// TODO comparar y actualizar usuario en bd sigop, en proceso proceso asincrono
//				// TODO validar clasificacion en el servico sicdi, comparar y actualizar si es
//				// necesario
//				// Usuario uldap = respuestaLdap.getUsuario();
//
//				authenticationResponse.setUsuario(usuarioApp);
//				final UserDetailsSigop userDetails = userDetailsService
//						.loadUserByUsername(authenticationRequest.getUsername());
//				userDetails.setPassword(authenticationRequest.getPassword());
//				authenticationResponse.setJwt(jwtTokenUtil.generateToken(userDetails));
//				authenticationResponse.setModulosDTO(getModulosPerfiles(perfilesUsuario));
//				authenticationResponse.setCodRespuesta(respuestaLdap.getCodigo());
//				authenticationResponse.setMsjRespuesta(Mensajes.LOGIN_EXITOSO);
//				return authenticationResponse;
//			}
//		}
//		authenticationResponse.setCodRespuesta(respuestaLdap.getCodigo());
//		authenticationResponse.setMsjRespuesta(respuestaLdap.getDescripcion());
//		return authenticationResponse;
//	}
//
//	private List<ModuloInputDTO> getModulosPerfiles(List<RelacionUsuarioPerfilDTO> perfiles) {
//		return funcionalidadesService
//				.getModulosByPerfilUsuario(perfiles.stream().map(up -> up.getIdPerfil()).collect(Collectors.toList()));
//	}
//
//}
