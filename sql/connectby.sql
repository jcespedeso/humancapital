-- Crear extension primero
CREATE EXTENSION tablefunc;

SELECT * FROM "humanCapitalSch"."connectby"('"humanCapitalSch"."DEPARTAMENTO"', '"DEPARTAMENTO"."ID_DEPARTAMENTO"', '"DEPARTAMENTO"."DEPTO_PADRE"','"DEPARTAMENTO"."PESO_ORDEN"', '8', 0, '-')
    AS ouput(DEPARTAMENTO bigint, PADRE bigint, NIVEL int, branch text, POSICION int);

SELECT * FROM "humanCapitalSch"."connectby"('"humanCapitalSch"."CARGO"', '"CARGO"."ID_DEPARTAMENTO"', '"CARGO"."CARGO_PADRE"','"CARGO"."PESO_ORDEN"', '8', 0, '-')
    AS ouput(CARGO bigint, PADRE bigint, NIVEL int, branch text, POSICION int);