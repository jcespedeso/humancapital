-- Sql de Actualizacion Servidores Modulo GET--

-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- PostgreSQL version: 9.2
-- Project Site: pgmodeler.com.br
-- Model Author: Alejandro Herrera Montilla

-- object: "humanCapitalSch"."COMPETENCIA" | type: TABLE --
CREATE TABLE "humanCapitalSch"."COMPETENCIA"(
	"ID_COMPETENCIA" bigserial NOT NULL,
	"NOMBRE" varchar(150) NOT NULL,
	"DESCRIPCION" varchar(255) NOT NULL,
	"TIPO" varchar(30) NOT NULL,
	"ORDEN" numeric,
	"MINIMO_ESPERADO" numeric,
	"ACTIVO" boolean NOT NULL,
	"FECHA_CREACION" timestamp NOT NULL,
	"USUARIO_CREA" bigint NOT NULL,
	"FECHA_MODIFICA" timestamp NOT NULL,
	"USUARIO_MODIFICA" bigint NOT NULL,
	"FECHA_INACTIVACION" timestamp,
	"USUARIO_INACTIVA" bigint,
	"CLIENTE" bigint NOT NULL,
	"GRUPO" bigint NOT NULL,
	CONSTRAINT "PK_COMPETENCIA" PRIMARY KEY ("ID_COMPETENCIA")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
-- object: "IDX_ACTIVO_COMPETENCIA" | type: INDEX --
CREATE INDEX "IDX_ACTIVO_COMPETENCIA" ON "humanCapitalSch"."COMPETENCIA"
	USING btree
	(
	  "ACTIVO" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "IDX_ACTIVO_COMPETENCIA" IS 'Estado de la Competencia';
-- ddl-end --
-- object: "IDX_NOMBRE_COMPETENCIA" | type: INDEX --
CREATE INDEX "IDX_NOMBRE_COMPETENCIA" ON "humanCapitalSch"."COMPETENCIA"
	USING btree
	(
	  "NOMBRE" ASC NULLS LAST
	);
-- ddl-end --

COMMENT ON INDEX "IDX_NOMBRE_COMPETENCIA" IS 'Nombre de la Compentencia.';
-- ddl-end --

COMMENT ON TABLE "humanCapitalSch"."COMPETENCIA" IS 'Relacion de competencias del cliente.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."ID_COMPETENCIA" IS 'Identificador unico de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."NOMBRE" IS 'Nombre de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."DESCRIPCION" IS 'Descripción de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."TIPO" IS 'Tipologia de la competencia. Ej: Cargo, Departamento, Jefe, Colaborador o Organizacional';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."ORDEN" IS 'Define el orden de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."MINIMO_ESPERADO" IS 'Define el minimo esperado para la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."ACTIVO" IS 'Define el estado de la competencia en true= Activo false=Inactivo';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."FECHA_CREACION" IS 'Fecha de auditoria que identifica cuando se crea la competencia.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."USUARIO_CREA" IS 'Id que identifica el usuario que realizo la acción de creación de la competencia.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."FECHA_MODIFICA" IS 'Fecha de auditoria que identifica cuando se modifica la competencia.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."USUARIO_MODIFICA" IS 'Id que identifica el usuario que realiza la ultima modificación la competencia.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."FECHA_INACTIVACION" IS 'Fecha de auditoria que identifica cuando se inactiva la competencia.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."USUARIO_INACTIVA" IS 'Id que identifica el usuario que realizo la acción de inactivación de la competencia.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."CLIENTE" IS 'Id de identificacion principal de los clientes de HumanCapital.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA"."GRUPO" IS 'Identificador unico del grupo de competencias.';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_COMPETENCIA" ON "humanCapitalSch"."COMPETENCIA" IS 'Identificador unico. ';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."COMPETENCIA" OWNER TO admindbhc;
-- ddl-end --

-- object: "humanCapitalSch"."COMPORTAMIENTO" | type: TABLE --
CREATE TABLE "humanCapitalSch"."COMPORTAMIENTO"(
	"ID_COMPORTAMIENTO" bigserial NOT NULL,
	"DESCRIPCION" varchar(255) NOT NULL,
	"ORDEN" numeric,
	"ACTIVO" boolean NOT NULL,
	"FECHA_CREACION" timestamp NOT NULL,
	"USUARIO_CREA" bigint NOT NULL,
	"FECHA_MODIFICA" timestamp NOT NULL,
	"USUARIO_MODIFICA" bigint NOT NULL,
	"FECHA_INACTIVACION" timestamp,
	"USUARIO_INACTIVA" bigint,
	"COMPETENCIA" bigint NOT NULL,
	"CATEGORIA" bigint NOT NULL,
	CONSTRAINT "PK_COMPORTAMIENTO" PRIMARY KEY ("ID_COMPORTAMIENTO")
	WITH (FILLFACTOR = 10)
	USING INDEX TABLESPACE "humanCapitalTB"

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
-- object: "IDX_ACTIVO_COMPORTAMIENTO" | type: INDEX --
CREATE INDEX "IDX_ACTIVO_COMPORTAMIENTO" ON "humanCapitalSch"."COMPORTAMIENTO"
	USING btree
	(
	  "ACTIVO" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "IDX_ACTIVO_COMPORTAMIENTO" IS 'Estado del Comportamiento.';
-- ddl-end --

COMMENT ON TABLE "humanCapitalSch"."COMPORTAMIENTO" IS 'Comportamientos de las competencias.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."ID_COMPORTAMIENTO" IS 'Identifcador unico del comportamiento. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."DESCRIPCION" IS 'Descripción del comportamiento. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."ORDEN" IS 'Define el orden del comportamiento dentro de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."ACTIVO" IS 'Define el estado del comportamiento en true= Activo false=Inactivo';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."FECHA_CREACION" IS 'Fecha de auditoria que identifica cuando se crea el comportamiento.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."USUARIO_CREA" IS 'Id que identifica el usuario que realizo la acción de creación del comportamiento.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."FECHA_MODIFICA" IS 'Fecha de auditoria que identifica cuando se modifica el comportamiento.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."USUARIO_MODIFICA" IS 'Id que identifica el usuario que realiza la ultima modificación del comportamiento.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."FECHA_INACTIVACION" IS 'Fecha de auditoria que identifica cuando se inactiva el comportamiento.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."USUARIO_INACTIVA" IS 'Id que identifica el usuario que realizo la acción de inactivación del comportamiento.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."COMPETENCIA" IS 'Identificador unico de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPORTAMIENTO"."CATEGORIA" IS 'Identificador unico de la categoria';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_COMPORTAMIENTO" ON "humanCapitalSch"."COMPORTAMIENTO" IS 'Clave primaria del comportamiento.';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."COMPORTAMIENTO" OWNER TO admindbhc;
-- ddl-end --

-- object: "FK_CLIENTE" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPETENCIA" ADD CONSTRAINT "FK_CLIENTE" FOREIGN KEY ("CLIENTE")
REFERENCES "humanCapitalSch"."CLIENTE" ("ID_CLIENTE") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_COMPETENCIA" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPORTAMIENTO" ADD CONSTRAINT "FK_COMPETENCIA" FOREIGN KEY ("COMPETENCIA")
REFERENCES "humanCapitalSch"."COMPETENCIA" ("ID_COMPETENCIA") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --

-- object: "humanCapitalSch"."COMPETENCIA_X_CARGO" | type: TABLE --
CREATE TABLE "humanCapitalSch"."COMPETENCIA_X_CARGO"(
	"COMPETENCIA" bigint,
	"CARGO" bigint,
	CONSTRAINT "PK_COMPETENCIA_X_CARGO" PRIMARY KEY ("COMPETENCIA","CARGO")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA_X_CARGO"."COMPETENCIA" IS 'Identificador unico de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA_X_CARGO"."CARGO" IS 'Identificador del cargo.';
-- ddl-end --
-- ddl-end --

-- object: "FK_COMPETENCIA" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPETENCIA_X_CARGO" ADD CONSTRAINT "FK_COMPETENCIA" FOREIGN KEY ("COMPETENCIA")
REFERENCES "humanCapitalSch"."COMPETENCIA" ("ID_COMPETENCIA") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_CARGO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPETENCIA_X_CARGO" ADD CONSTRAINT "FK_CARGO" FOREIGN KEY ("CARGO")
REFERENCES "humanCapitalSch"."CARGO" ("ID_CARGO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "humanCapitalSch"."COMPETENCIA_X_DEPARTAMENTO" | type: TABLE --
CREATE TABLE "humanCapitalSch"."COMPETENCIA_X_DEPARTAMENTO"(
	"COMPETENCIA" bigint,
	"DEPARTAMENTO" bigint,
	CONSTRAINT "PK_COMPETENCIA_X_DEPARTAMENTO" PRIMARY KEY ("COMPETENCIA","DEPARTAMENTO")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA_X_DEPARTAMENTO"."COMPETENCIA" IS 'Identificador unico de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA_X_DEPARTAMENTO"."DEPARTAMENTO" IS 'Identificador del departamento. ';
-- ddl-end --
-- ddl-end --

-- object: "FK_COMPETENCIA" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPETENCIA_X_DEPARTAMENTO" ADD CONSTRAINT "FK_COMPETENCIA" FOREIGN KEY ("COMPETENCIA")
REFERENCES "humanCapitalSch"."COMPETENCIA" ("ID_COMPETENCIA") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_DEPARTAMENTO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPETENCIA_X_DEPARTAMENTO" ADD CONSTRAINT "FK_DEPARTAMENTO" FOREIGN KEY ("DEPARTAMENTO")
REFERENCES "humanCapitalSch"."DEPARTAMENTO" ("ID_DEPARTAMENTO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "humanCapitalSch"."GRUPO" | type: TABLE --
CREATE TABLE "humanCapitalSch"."GRUPO"(
	"ID_GRUPO" bigserial NOT NULL,
	"NOMBRE" varchar(50) NOT NULL,
	"ACTIVO" boolean NOT NULL,
	CONSTRAINT "PK_GRUPO" PRIMARY KEY ("ID_GRUPO")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON TABLE "humanCapitalSch"."GRUPO" IS 'Agrupación de competencias. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."GRUPO"."ID_GRUPO" IS 'Identificador unico del grupo de competencias.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."GRUPO"."NOMBRE" IS 'Nombre del Grupo.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."GRUPO"."ACTIVO" IS 'Define el estado del grupo en true= Activo false=Inactivo';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."GRUPO" OWNER TO admindbhc;
-- ddl-end --

-- object: "FK_GRUPO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPETENCIA" ADD CONSTRAINT "FK_GRUPO" FOREIGN KEY ("GRUPO")
REFERENCES "humanCapitalSch"."GRUPO" ("ID_GRUPO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "humanCapitalSch"."EVALUACION" | type: TABLE --
CREATE TABLE "humanCapitalSch"."EVALUACION"(
	"ID_EVALUACION" bigserial NOT NULL,
	"NOMBRE" varchar(75) NOT NULL,
	"GRADO" varchar(100) NOT NULL,
	"OBS_AFIRMACION" boolean NOT NULL,
	"OBS_AFIRMACION_OBLIGATORIO" boolean NOT NULL,
	"OBS_COMPETENCIA" boolean NOT NULL,
	"OBS_COMPETENCIA_OBLIGATORIO" boolean NOT NULL,
	"OBS_EVALUACION" boolean NOT NULL,
	"OBS_EVALUACION_OBLIGATORIO" boolean NOT NULL,
	"TIPO_SEMAFORO" varchar(30) NOT NULL,
	"TIPO_RESPUESTA" varchar(30) NOT NULL,
	"CONFIGURACION" varchar(30) NOT NULL,
	"NO_SE" boolean NOT NULL,
	"CABECERA" text,
	"HABILITAR_RESULTADO" boolean NOT NULL,
	"NUMERO_INVALIDA" numeric,
	"FECHA_INICIO" date NOT NULL,
	"FECHA_CIERRE" date NOT NULL,
	"HORA_ENVIO" time NOT NULL,
	"ASUNTO_INVITACION" varchar(80),
	"ASUNTO_RECORDATORIO" varchar(80),
	"COMENTARIO_COMPETENCIA" varchar(255),
	"MENSAJE_FIN_EVALUACION" varchar(255),
	"ACTIVO" boolean NOT NULL,
	"FECHA_CREACION" timestamp NOT NULL,
	"USUARIO_CREA" bigint NOT NULL,
	"FECHA_MODIFICA" timestamp NOT NULL,
	"USUARIO_MODIFICA" bigint NOT NULL,
	"FECHA_INACTIVACION" timestamp,
	"USUARIO_INACTIVA" bigint,
	CONSTRAINT "PK_ID_EVALUACION" PRIMARY KEY ("ID_EVALUACION")
	WITH (FILLFACTOR = 10)
	--USING INDEX TABLESPACE "humanCapitalTB"

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
-- object: "IDX_ACTIVO_EVALUACION" | type: INDEX --
CREATE INDEX "IDX_ACTIVO_EVALUACION" ON "humanCapitalSch"."EVALUACION"
	USING btree
	(
	  "ACTIVO" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "IDX_ACTIVO_EVALUACION" IS 'Estado del Registro.';
-- ddl-end --

COMMENT ON TABLE "humanCapitalSch"."EVALUACION" IS 'Datos de evaluación realizados sobre las Competencias. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."ID_EVALUACION" IS 'Identificador unico de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."NOMBRE" IS 'Nombre de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."GRADO" IS 'Define el grado o los grados de evaluación de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."OBS_AFIRMACION" IS 'Define si por cada pregunta contestada se debe habiliatar argumentación textual. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."OBS_AFIRMACION_OBLIGATORIO" IS 'Se especifica si la observación es de caracter obligatorio.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."OBS_COMPETENCIA" IS 'Define si por cada competencia se debe permitir argumentación textual. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."OBS_COMPETENCIA_OBLIGATORIO" IS 'Se especifica si la observación es de caracter obligatorio.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."OBS_EVALUACION" IS 'Define si por cada evaluación se debe permitir una argumentación textual. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."OBS_EVALUACION_OBLIGATORIO" IS 'Se especifica si la observación es de caracter obligatorio.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."TIPO_SEMAFORO" IS 'Tipo de semaforo a presentar.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."TIPO_RESPUESTA" IS 'Define el Tipo de Respuesta que se puede ofrecer a los usuarios.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."CONFIGURACION" IS 'Define la configuracion de las preguntas.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."NO_SE" IS 'Permite establecer una opción de No se.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."CABECERA" IS 'Se define el texto que sera mostrado como cabecera de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."HABILITAR_RESULTADO" IS 'Permite definir si habilitan los resultados para los evaluadores.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."NUMERO_INVALIDA" IS 'Define el numero de opciones invalidas.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."FECHA_INICIO" IS 'Define la fecha de inicio de la Evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."FECHA_CIERRE" IS 'Determina la fecha maxima para ser resuelta la evaluación. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."HORA_ENVIO" IS 'Define la hora exacta en la que se envia la evaluacion a los empleados.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."ASUNTO_INVITACION" IS 'Texto de asunto para el correo de invitacion a contestar la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."ASUNTO_RECORDATORIO" IS 'Texto de asunto para recordar contestar la  evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."COMENTARIO_COMPETENCIA" IS 'Texto que se utiliza como comentario para en todas las Competencias.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."MENSAJE_FIN_EVALUACION" IS 'Texto como mensaje final de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."ACTIVO" IS 'Define el estado de la evaluacion en true= Activo false=Inactivo';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."FECHA_CREACION" IS 'Fecha de auditoria que identifica cuando se crea la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."USUARIO_CREA" IS 'Id que identifica el usuario que realizo la acción de creación de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."FECHA_MODIFICA" IS 'Fecha de auditoria que identifica cuando se modifica la evaluacion.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."USUARIO_MODIFICA" IS 'Id que identifica el usuario que realiza la ultima modificación de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."FECHA_INACTIVACION" IS 'Fecha de auditoria que identifica cuando se inactiva la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."USUARIO_INACTIVA" IS 'Id que identifica el usuario que realizo la acción de inactivación de la evaluación..';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_ID_EVALUACION" ON "humanCapitalSch"."EVALUACION" IS 'Clave primaria de la evaluacion. ';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."EVALUACION" OWNER TO admindbhc;
-- ddl-end --

-- object: "humanCapitalSch"."SEMAFORO" | type: TABLE --
CREATE TABLE "humanCapitalSch"."SEMAFORO"(
	"ID_SEMAFORO" bigint NOT NULL,
	"COLOR" varchar(50) NOT NULL,
	"ETIQUETA" varchar(50) NOT NULL,
	"PORCENTAJE" numeric NOT NULL,
	"EVALUACION" bigint NOT NULL,
	CONSTRAINT "PK_SEMAFORO" PRIMARY KEY ("ID_SEMAFORO")
	WITH (FILLFACTOR = 10)
	--USING INDEX TABLESPACE "humanCapitalTB"

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON TABLE "humanCapitalSch"."SEMAFORO" IS 'Configuración del Semaforo para el resultado de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."SEMAFORO"."ID_SEMAFORO" IS 'Identificador unico del semaforo a configurar.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."SEMAFORO"."COLOR" IS 'Color a mostrar en el semaforo.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."SEMAFORO"."ETIQUETA" IS 'Etiqueta para el nivel del semaforo.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."SEMAFORO"."PORCENTAJE" IS 'Valor porcentual del color determinado.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."SEMAFORO"."EVALUACION" IS 'Identificador unico de la evaluación.';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_SEMAFORO" ON "humanCapitalSch"."SEMAFORO" IS 'Clave primaria del semaforo.';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."SEMAFORO" OWNER TO admindbhc;
-- ddl-end --

-- object: "humanCapitalSch"."OPCION_RESPUESTA" | type: TABLE --
CREATE TABLE "humanCapitalSch"."OPCION_RESPUESTA"(
	"ID_OPCION_RESPUESTA" bigserial NOT NULL,
	"NOMBRE" varchar(50) NOT NULL,
	"LEYENDA" varchar(50) NOT NULL,
	"VALOR" numeric NOT NULL,
	"OPCION_INVALIDA" boolean NOT NULL,
	"EVALUACION" bigint NOT NULL,
	CONSTRAINT "PK_OPCION_RESPUESTA" PRIMARY KEY ("ID_OPCION_RESPUESTA")
	WITH (FILLFACTOR = 10)
	--USING INDEX TABLESPACE "humanCapitalTB"

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON TABLE "humanCapitalSch"."OPCION_RESPUESTA" IS 'Relación de opciones de respuesta de las diferentes evaluaciones.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OPCION_RESPUESTA"."ID_OPCION_RESPUESTA" IS 'Id de la respuesta.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OPCION_RESPUESTA"."NOMBRE" IS 'Nombre de la opción.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OPCION_RESPUESTA"."LEYENDA" IS 'Texto de leyenda de la opción.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OPCION_RESPUESTA"."VALOR" IS 'Valor de la opción. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OPCION_RESPUESTA"."EVALUACION" IS 'Identificador unico de la evaluación.';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_OPCION_RESPUESTA" ON "humanCapitalSch"."OPCION_RESPUESTA" IS 'Clave primaria de la respuesta. ';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."OPCION_RESPUESTA" OWNER TO admindbhc;
-- ddl-end --

-- object: "FK_EVALUACION" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."OPCION_RESPUESTA" ADD CONSTRAINT "FK_EVALUACION" FOREIGN KEY ("EVALUACION")
REFERENCES "humanCapitalSch"."EVALUACION" ("ID_EVALUACION") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_EVALUACION" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."SEMAFORO" ADD CONSTRAINT "FK_EVALUACION" FOREIGN KEY ("EVALUACION")
REFERENCES "humanCapitalSch"."EVALUACION" ("ID_EVALUACION") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO" | type: TABLE --
CREATE TABLE "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO"(
	"ID_RESPUESTA" bigserial NOT NULL,
	"EVALUACION_EMPLEADO" bigint NOT NULL,
	"OBS_AFIRMACION" varchar(200),
	"EVALUADOR" bigint NOT NULL,
	"RESPUESTA" varchar(50) NOT NULL,
	"COMPORTAMIENTO" bigint NOT NULL,
	CONSTRAINT "PK_RESPUESTA_EVALUACION" PRIMARY KEY ("ID_RESPUESTA")
	WITH (FILLFACTOR = 10)
	--USING INDEX TABLESPACE "humanCapitalTB"

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
-- object: "IDX_COMPORTAMIENTO" | type: INDEX --
CREATE INDEX "IDX_COMPORTAMIENTO" ON "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO"
	USING btree
	(
	  "COMPORTAMIENTO" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "IDX_COMPORTAMIENTO" IS 'Id del comportamiento evaluado y observado.';
-- ddl-end --

COMMENT ON TABLE "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO" IS 'Registro de las respuestas dadas al comportamiento.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO"."ID_RESPUESTA" IS 'Identificador unico de la respuesta. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO"."EVALUACION_EMPLEADO" IS 'Identificador unico de los resultados.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO"."OBS_AFIRMACION" IS 'Observación realizada por el Evaluado a la pregunta.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO"."EVALUADOR" IS 'Id Empleado evaluador';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO"."RESPUESTA" IS 'Valor de respuesta dada por el evaluado.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO"."COMPORTAMIENTO" IS 'Id del comportamiento evaluado.';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_RESPUESTA_EVALUACION" ON "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO" IS 'Clave primaria de la respuesta.';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO" OWNER TO admindbhc;
-- ddl-end --

-- object: "humanCapitalSch"."CATEGORIA" | type: TABLE --
CREATE TABLE "humanCapitalSch"."CATEGORIA"(
	"ID_CATEGORIA" bigserial NOT NULL,
	"NOMBRE" varchar(50) NOT NULL,
	"ACTIVO" boolean NOT NULL,
	CONSTRAINT "PK_CATEGORIA" PRIMARY KEY ("ID_CATEGORIA")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
-- object: "IDX_ACTIVO_CATEGORIA" | type: INDEX --
CREATE INDEX "IDX_ACTIVO_CATEGORIA" ON "humanCapitalSch"."CATEGORIA"
	USING btree
	(
	  "ACTIVO" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "IDX_ACTIVO_CATEGORIA" IS 'Estado de la Categoria';
-- ddl-end --

COMMENT ON TABLE "humanCapitalSch"."CATEGORIA" IS 'Categoria de los Cargos y Competencias';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."CATEGORIA"."ID_CATEGORIA" IS 'Identificador unico de la categoria';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."CATEGORIA"."NOMBRE" IS 'Nombre de la categoria ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."CATEGORIA"."ACTIVO" IS 'Define el estado de la Categoria en true= Activo false=Inactivo';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_CATEGORIA" ON "humanCapitalSch"."CATEGORIA" IS 'Clave primaria de la categoria.';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."CATEGORIA" OWNER TO admindbhc;
-- ddl-end --

-- object: "FK_CATEGORIA" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPORTAMIENTO" ADD CONSTRAINT "FK_CATEGORIA" FOREIGN KEY ("CATEGORIA")
REFERENCES "humanCapitalSch"."CATEGORIA" ("ID_CATEGORIA") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_CATEGORIA" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."CARGO" ADD CONSTRAINT "FK_CATEGORIA" FOREIGN KEY ("CATEGORIA")
REFERENCES "humanCapitalSch"."CATEGORIA" ("ID_CATEGORIA") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "humanCapitalSch"."OBS_EVALUACION" | type: TABLE --
CREATE TABLE "humanCapitalSch"."OBS_EVALUACION"(
	"ID_OBS_EVALUACION" bigserial NOT NULL,
	"EVALUACION_EMPLEADO" bigint,
	"EVALUADOR" bigint NOT NULL,
	"OBS_EVALUACION" text NOT NULL,
	CONSTRAINT "PK_ID_OBS_EVALUACION" PRIMARY KEY ("ID_OBS_EVALUACION")
	WITH (FILLFACTOR = 10)
	--USING INDEX TABLESPACE "humanCapitalTB"

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON TABLE "humanCapitalSch"."OBS_EVALUACION" IS 'Observaciones de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_EVALUACION"."ID_OBS_EVALUACION" IS 'Identificador unico de la observacion a la evaluación. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_EVALUACION"."EVALUACION_EMPLEADO" IS 'Identificador unico de los resultados.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_EVALUACION"."EVALUADOR" IS 'Id del evaluador que genera la observación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_EVALUACION"."OBS_EVALUACION" IS 'Observacion a la evaluación.';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_ID_OBS_EVALUACION" ON "humanCapitalSch"."OBS_EVALUACION" IS 'Clave primaria de la observación.';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."OBS_EVALUACION" OWNER TO admindbhc;
-- ddl-end --

-- object: "humanCapitalSch"."OBS_COMPETENCIA" | type: TABLE --
CREATE TABLE "humanCapitalSch"."OBS_COMPETENCIA"(
	"ID_OBS_COMPETENCIA" bigserial NOT NULL,
	"EVALUACION_EMPLEADO" bigint NOT NULL,
	"OBS_COMPETENCIA" text NOT NULL,
	"EVALUADOR" bigint NOT NULL,
	"COMPETENCIA" bigint NOT NULL,
	CONSTRAINT "PK_ID_OBS_COMPETENCIA" PRIMARY KEY ("ID_OBS_COMPETENCIA")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
-- object: "IDX_COMPETENCIA" | type: INDEX --
CREATE INDEX "IDX_COMPETENCIA" ON "humanCapitalSch"."OBS_COMPETENCIA"
	USING btree
	(
	  "COMPETENCIA" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "IDX_COMPETENCIA" IS 'Id de la competencia observada.';
-- ddl-end --

COMMENT ON TABLE "humanCapitalSch"."OBS_COMPETENCIA" IS 'Observaciones de las competencias';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_COMPETENCIA"."ID_OBS_COMPETENCIA" IS 'Identificador de la observación';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_COMPETENCIA"."EVALUACION_EMPLEADO" IS 'Identificador unico de los resultados.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_COMPETENCIA"."OBS_COMPETENCIA" IS 'Observacion de la Competencia';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_COMPETENCIA"."EVALUADOR" IS 'Id del evaluador que genera la observación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OBS_COMPETENCIA"."COMPETENCIA" IS 'Id de la competencia evaluada.';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_ID_OBS_COMPETENCIA" ON "humanCapitalSch"."OBS_COMPETENCIA" IS 'Clave primaria de la observación.';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."OBS_COMPETENCIA" OWNER TO admindbhc;
-- ddl-end --

-- object: "humanCapitalSch"."OTRA_RELACION" | type: TABLE --
CREATE TABLE "humanCapitalSch"."OTRA_RELACION"(
	"ID_RELACION" bigserial NOT NULL,
	"EVALUACION_EMPLEADO" bigint NOT NULL,
	"EVALUADOR" bigint NOT NULL,
	"EMPLEADO" bigint NOT NULL,
	"RELACION" varchar(2) NOT NULL,
	CONSTRAINT "PK_ID_RELACION" PRIMARY KEY ("ID_RELACION")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON TABLE "humanCapitalSch"."OTRA_RELACION" IS 'Definición de otras relaciones entre empleados y evaluadores.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OTRA_RELACION"."ID_RELACION" IS 'Identificador unico de la relación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OTRA_RELACION"."EVALUACION_EMPLEADO" IS 'Identificador unico de los resultados.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OTRA_RELACION"."EVALUADOR" IS 'Id del evaluador';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OTRA_RELACION"."EMPLEADO" IS 'Id del empleado evaluado.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."OTRA_RELACION"."RELACION" IS 'Definición de la relación entre el Evaluador y el Empleado.';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_ID_RELACION" ON "humanCapitalSch"."OTRA_RELACION" IS 'Clave primaria de la relación.';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."OTRA_RELACION" OWNER TO admindbhc;
-- ddl-end --

-- object: "humanCapitalSch"."EVALUACION_EMPLEADO" | type: TABLE --
CREATE TABLE "humanCapitalSch"."EVALUACION_EMPLEADO"(
	"ID_EVA_EMPLEADO" bigserial NOT NULL,
	"ACTIVO" boolean NOT NULL,
	"EMPLEADO" bigint NOT NULL,
	CONSTRAINT "PK_EVALUACION_EMPLEADO" PRIMARY KEY ("ID_EVA_EMPLEADO")
	WITH (FILLFACTOR = 10)
	--USING INDEX TABLESPACE "humanCapitalTB"

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
-- object: "IDX_ACTIVO_EVA_EMPLEADO" | type: INDEX --
CREATE INDEX "IDX_ACTIVO_EVA_EMPLEADO" ON "humanCapitalSch"."EVALUACION_EMPLEADO"
	USING btree
	(
	  "ACTIVO" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "IDX_ACTIVO_EVA_EMPLEADO" IS 'Estado del registro.';
-- ddl-end --

COMMENT ON TABLE "humanCapitalSch"."EVALUACION_EMPLEADO" IS 'Resume los datos de la evaluacion del Empleado.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION_EMPLEADO"."ID_EVA_EMPLEADO" IS 'Identificador unico de los resultados.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION_EMPLEADO"."ACTIVO" IS 'Define el estado de la evaluación en true= Activo false=Inactivo';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION_EMPLEADO"."EMPLEADO" IS 'Identificador unico para los empleados de los diferentes Clientes. ';
-- ddl-end --
COMMENT ON CONSTRAINT "PK_EVALUACION_EMPLEADO" ON "humanCapitalSch"."EVALUACION_EMPLEADO" IS 'Clave unica que identifica el resultado.';
-- ddl-end --
ALTER TABLE "humanCapitalSch"."EVALUACION_EMPLEADO" OWNER TO admindbhc;
-- ddl-end --

-- object: "FK_EMPLEADO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."EVALUACION_EMPLEADO" ADD CONSTRAINT "FK_EMPLEADO" FOREIGN KEY ("EMPLEADO")
REFERENCES "humanCapitalSch"."EMPLEADO" ("ID_EMPLEADO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_EVALUACION_EMPLEADO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."OTRA_RELACION" ADD CONSTRAINT "FK_EVALUACION_EMPLEADO" FOREIGN KEY ("EVALUACION_EMPLEADO")
REFERENCES "humanCapitalSch"."EVALUACION_EMPLEADO" ("ID_EVA_EMPLEADO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_EVALUACION_EMPLEADO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."OBS_COMPETENCIA" ADD CONSTRAINT "FK_EVALUACION_EMPLEADO" FOREIGN KEY ("EVALUACION_EMPLEADO")
REFERENCES "humanCapitalSch"."EVALUACION_EMPLEADO" ("ID_EVA_EMPLEADO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_EVALUACION_EMPLEADO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."RESPUESTA_COMPORTAMIENTO" ADD CONSTRAINT "FK_EVALUACION_EMPLEADO" FOREIGN KEY ("EVALUACION_EMPLEADO")
REFERENCES "humanCapitalSch"."EVALUACION_EMPLEADO" ("ID_EVA_EMPLEADO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_EVALUACION_EMPLEADO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."OBS_EVALUACION" ADD CONSTRAINT "FK_EVALUACION_EMPLEADO" FOREIGN KEY ("EVALUACION_EMPLEADO")
REFERENCES "humanCapitalSch"."EVALUACION_EMPLEADO" ("ID_EVA_EMPLEADO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "UQ_OBS_EVALUACION" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."OBS_EVALUACION" ADD CONSTRAINT "UQ_OBS_EVALUACION" UNIQUE ("EVALUACION_EMPLEADO");
-- ddl-end --


-- object: "humanCapitalSch"."COMPETENCIA_X_EVALUACION" | type: TABLE --
CREATE TABLE "humanCapitalSch"."COMPETENCIA_X_EVALUACION"(
	"COMPETENCIA" bigint,
	"EVALUACION" bigint,
	CONSTRAINT "PK_COMPETENCIA_X_EVALUACION" PRIMARY KEY ("COMPETENCIA","EVALUACION")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA_X_EVALUACION"."COMPETENCIA" IS 'Identificador unico de la competencia. ';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."COMPETENCIA_X_EVALUACION"."EVALUACION" IS 'Identificador unico de la evaluación.';
-- ddl-end --
-- ddl-end --

-- object: "FK_COMPETENCIA" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPETENCIA_X_EVALUACION" ADD CONSTRAINT "FK_COMPETENCIA" FOREIGN KEY ("COMPETENCIA")
REFERENCES "humanCapitalSch"."COMPETENCIA" ("ID_COMPETENCIA") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "PK_EVALUACION" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."COMPETENCIA_X_EVALUACION" ADD CONSTRAINT "PK_EVALUACION" FOREIGN KEY ("EVALUACION")
REFERENCES "humanCapitalSch"."EVALUACION" ("ID_EVALUACION") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "humanCapitalSch"."EVALUACION_X_EVAL_EMPLEADO" | type: TABLE --
CREATE TABLE "humanCapitalSch"."EVALUACION_X_EVAL_EMPLEADO"(
	"EVALUACION" bigint,
	"EVALUACION_EMPLEADO" bigint,
	CONSTRAINT "PK_EVALUACION_X_EVAL_EMPLEADO" PRIMARY KEY ("EVALUACION","EVALUACION_EMPLEADO")

)
TABLESPACE "humanCapitalTB";
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION_X_EVAL_EMPLEADO"."EVALUACION" IS 'Identificador unico de la evaluación.';
-- ddl-end --
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION_X_EVAL_EMPLEADO"."EVALUACION_EMPLEADO" IS 'Identificador unico de los resultados.';
-- ddl-end --
-- ddl-end --

-- object: "FK_EVALUACION" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."EVALUACION_X_EVAL_EMPLEADO" ADD CONSTRAINT "FK_EVALUACION" FOREIGN KEY ("EVALUACION")
REFERENCES "humanCapitalSch"."EVALUACION" ("ID_EVALUACION") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: "FK_EVALUACION_EMPLEADO" | type: CONSTRAINT --
ALTER TABLE "humanCapitalSch"."EVALUACION_X_EVAL_EMPLEADO" ADD CONSTRAINT "FK_EVALUACION_EMPLEADO" FOREIGN KEY ("EVALUACION_EMPLEADO")
REFERENCES "humanCapitalSch"."EVALUACION_EMPLEADO" ("ID_EVA_EMPLEADO") MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --

-- object: "IDX_NOMBRE_EVALUACION" | type: INDEX --
CREATE INDEX "IDX_NOMBRE_EVALUACION" ON "humanCapitalSch"."EVALUACION"
	USING btree
	(
	  "NOMBRE" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "humanCapitalSch"."IDX_NOMBRE_EVALUACION" IS 'Criterio busque reiterativo.';
-- ddl-end --
-- object: "IDX_GRADO_EVALUACION" | type: INDEX --
CREATE INDEX "IDX_GRADO_EVALUACION" ON "humanCapitalSch"."EVALUACION"
	USING btree
	(
	  "GRADO" ASC NULLS LAST
	)
	TABLESPACE "humanCapitalTB";
-- ddl-end --

COMMENT ON INDEX "humanCapitalSch"."IDX_GRADO_EVALUACION" IS 'Criterio de busqueda recurrente.';
-- ddl-end --

-- ADD CLIENTE A EVALUACION --
ALTER TABLE "humanCapitalSch"."EVALUACION" ADD COLUMN "CLIENTE" bigint;
COMMENT ON COLUMN "humanCapitalSch"."EVALUACION"."CLIENTE" IS 'Id del Cliente al que pertenece la evaluación.';
-- ddl-end --

-- ADD CLIENTE A CATEGORIA --
ALTER TABLE "humanCapitalSch"."CATEGORIA" ADD COLUMN "CLIENTE" bigint;
COMMENT ON COLUMN "humanCapitalSch"."CATEGORIA"."CLIENTE" IS 'Id del cliente al cual pertenece la categoria. ';
-- ddl-end --

-- ADD CLIENTE A GRUPO --
ALTER TABLE "humanCapitalSch"."GRUPO" ADD COLUMN "CLIENTE" bigint;
COMMENT ON COLUMN "humanCapitalSch"."GRUPO"."CLIENTE" IS 'Id del Cliente al que pertenece el grupo.';
-- ddl-end --

-- Table: "humanCapitalSch"."EVALUATION_NOTIFICATION"
-- DROP TABLE "humanCapitalSch"."EVALUATION_NOTIFICATION";
CREATE TABLE "humanCapitalSch"."EVALUATION_NOTIFICATION"
(
    "ID" bigserial NOT NULL,
    "FROM_ADDRESS" character varying(255) COLLATE pg_catalog."default",
    "FROM_NAME" character varying(255) COLLATE pg_catalog."default",
    "HTML_MESSAGE" text COLLATE pg_catalog."default",
    "NOTIFICATION_DATE" timestamp without time zone,
    "SUBJECT" character varying(255) COLLATE pg_catalog."default",
    "TEST_FROM_ADDRESS" character varying(255) COLLATE pg_catalog."default",
    "TEST_FROM_NAME" character varying(255) COLLATE pg_catalog."default",
    "TEST_TO_ADDRESS" character varying(255) COLLATE pg_catalog."default",
    "TEST_TO_NAME" character varying(255) COLLATE pg_catalog."default",
    "EVALUACION_ID_EVALUACION" bigint NOT NULL,
    CONSTRAINT "EVALUATION_NOTIFICATION_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT "EVALUATION_NOTIFICATION_FK" FOREIGN KEY ("EVALUACION_ID_EVALUACION")
        REFERENCES "humanCapitalSch"."EVALUACION" ("ID_EVALUACION") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE "humanCapitalTB";
ALTER TABLE "humanCapitalSch"."EVALUATION_NOTIFICATION" OWNER TO admindbhc;