-- Table: "humanCapitalSch"."QUESTION_GROUP"
-- DROP TABLE "humanCapitalSch"."QUESTION_GROUP";
CREATE TABLE "humanCapitalSch"."QUESTION_GROUP"
(
    "ID" bigserial NOT NULL,
    "CLIENT_ID" bigint NOT NULL,
    "NAME" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "QUESTION_GROUP_pkey" PRIMARY KEY ("ID")
)
TABLESPACE "humanCapitalTB";
ALTER TABLE "humanCapitalSch"."QUESTION_GROUP" OWNER to admindbhc;

-- Table: "humanCapitalSch"."QUESTION_CATEGORY"
-- DROP TABLE "humanCapitalSch"."QUESTION_CATEGORY";
CREATE TABLE "humanCapitalSch"."QUESTION_CATEGORY"
(
    "ID" bigserial NOT NULL,
    "CLIENT_ID" bigint NOT NULL,
    "NAME" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "QUESTION_GROUP_ID" bigint,
    CONSTRAINT "QUESTION_CATEGORY_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT "FK_QUESTION_CATEGORY_QUESTION_GROUP_ID" FOREIGN KEY ("QUESTION_GROUP_ID")
        REFERENCES "humanCapitalSch"."QUESTION_GROUP" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE "humanCapitalTB";
ALTER TABLE "humanCapitalSch"."QUESTION_CATEGORY" OWNER to admindbhc;

-- Table: "humanCapitalSch"."SURVEY"
-- DROP TABLE "humanCapitalSch"."SURVEY";
CREATE TABLE "humanCapitalSch"."SURVEY"
(
    "ID" bigserial NOT NULL,
    "CLIENT_ID" bigint,
    "NAME" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "DISABLED_MESSAGE" character varying(255) COLLATE pg_catalog."default",
    "EMAIL_INVITATION_MESSAGE" text COLLATE pg_catalog."default",
    "EMAIL_INVITATION_SUBJECT" character varying(255) COLLATE pg_catalog."default",
    "EMAIL_REMINDER_MESSAGE" text COLLATE pg_catalog."default",
    "EMAIL_REMINDER_SUBJECT" character varying(255) COLLATE pg_catalog."default",
    "ENABLED" boolean NOT NULL,
    "END_DATE" date NOT NULL,
    "START_DATE" date NOT NULL,
    "TIMEOUT_ENABLED" boolean NOT NULL,
    "TIMEOUT_SECONDS" integer NOT NULL,
    "ACTIVO" boolean NOT NULL,
    "FECHA_CREACION" date NOT NULL,
    "FECHA_INACTIVACION" date,
    "FECHA_MODIFICA" date,
    "USUARIO_CREA" bigint NOT NULL,
    "USUARIO_INACTIVA" bigint,
    "USUARIO_MODIFICA" bigint NOT NULL,
    CONSTRAINT "SURVEY_pkey" PRIMARY KEY ("ID")
)
TABLESPACE "humanCapitalTB";
ALTER TABLE "humanCapitalSch"."SURVEY" OWNER to admindbhc;

-- Table: "humanCapitalSch"."QUESTION"
-- DROP TABLE "humanCapitalSch"."QUESTION";
CREATE TABLE "humanCapitalSch"."QUESTION"
(
    "ID" bigserial NOT NULL,
    "DTYPE" character varying(31) COLLATE pg_catalog."default" NOT NULL,
    "NAME" character varying(255) COLLATE pg_catalog."default",
    "SURVEY_ID" bigint NOT NULL,
    "QUESTION_TYPE" integer,
    "REQUIRED" boolean NOT NULL,
    "TITLE" character varying(255) COLLATE pg_catalog."default",
    "COLUMNS_COUNT" integer,
    "AMOUNT_OPTIONS" integer,
    "MAX_RATE_DESCRIPTION" character varying(255) COLLATE pg_catalog."default",
    "MIN_RATE_DESCRIPTION" character varying(255) COLLATE pg_catalog."default",
    "PLACE_HOLDER" character varying(255) COLLATE pg_catalog."default",
    "QUESTION_TEXT_TYPE" integer,
    "QUESTION_CATEGORY_ID" bigint NOT NULL,
    "ACTIVO" boolean NOT NULL,
    "FECHA_CREACION" date NOT NULL,
    "FECHA_INACTIVACION" date,
    "FECHA_MODIFICA" date,
    "USUARIO_CREA" bigint NOT NULL,
    "USUARIO_INACTIVA" bigint,
    "USUARIO_MODIFICA" bigint NOT NULL,
    CONSTRAINT "QUESTION_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT "FK_QUESTION_SURVEY_ID" FOREIGN KEY ("SURVEY_ID")
        REFERENCES "humanCapitalSch"."SURVEY" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "FK_QUESTION_QUESTION_CATEGORY_ID" FOREIGN KEY ("QUESTION_CATEGORY_ID")
        REFERENCES "humanCapitalSch"."QUESTION_CATEGORY" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE "humanCapitalTB";
ALTER TABLE "humanCapitalSch"."QUESTION" OWNER to admindbhc;


-- Table: "humanCapitalSch"."QUESTION_CHOICES"
-- DROP TABLE "humanCapitalSch"."QUESTION_CHOICES";
CREATE TABLE "humanCapitalSch"."QUESTION_CHOICES"
(
    "QUESTION_ID" bigint NOT NULL,
    "CHOICE_ID" integer NOT NULL,
    "CHOICE_VALUE" text COLLATE pg_catalog."default",
    CONSTRAINT "QUESTION_CHOICES_pkey" PRIMARY KEY ("QUESTION_ID", "CHOICE_ID"),
    CONSTRAINT "FK_QUESTION_CHOICES_QUESTION_ID" FOREIGN KEY ("QUESTION_ID")
        REFERENCES "humanCapitalSch"."QUESTION" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE "humanCapitalTB";
ALTER TABLE "humanCapitalSch"."QUESTION_CHOICES" OWNER to admindbhc;

-- Table: "humanCapitalSch"."QUESTIONNAIRE"
-- DROP TABLE "humanCapitalSch"."QUESTIONNAIRE";
CREATE TABLE "humanCapitalSch"."QUESTIONNAIRE"
(
    "ID" bigserial NOT NULL,
    "ACTIVO" boolean NOT NULL,
    "FECHA_CREACION" date NOT NULL,
    "FECHA_INACTIVACION" date,
    "FECHA_MODIFICA" date,
    "USUARIO_CREA" bigint NOT NULL,
    "USUARIO_INACTIVA" bigint,
    "USUARIO_MODIFICA" bigint NOT NULL,
    "EVALUATOR_ID" bigint NOT NULL,
    "SURVEY_ID" bigint,
    CONSTRAINT "QUESTIONNAIRE_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT "FK_QUESTIONNAIRE_SURVEY_ID" FOREIGN KEY ("SURVEY_ID")
        REFERENCES "humanCapitalSch"."SURVEY" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE "humanCapitalTB";
ALTER TABLE "humanCapitalSch"."QUESTIONNAIRE" OWNER to admindbhc;

-- Table: "humanCapitalSch"."QUESTIONNAIRE_ANSWERS"
-- DROP TABLE "humanCapitalSch"."QUESTIONNAIRE_ANSWERS";
CREATE TABLE "humanCapitalSch"."QUESTIONNAIRE_ANSWERS"
(
    "QUESTIONNAIRE_ID" bigint NOT NULL,
    "QUESTION_ID" bigint NOT NULL,
    "ANSWER" character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT "QUESTIONNAIRE_ANSWERS_pkey" PRIMARY KEY ("QUESTIONNAIRE_ID", "QUESTION_ID"),
    CONSTRAINT "FK_QUESTIONNAIRE_ANSWERS_QUESTIONNAIRE_ID" FOREIGN KEY ("QUESTIONNAIRE_ID")
        REFERENCES "humanCapitalSch"."QUESTIONNAIRE" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
TABLESPACE "humanCapitalTB";
ALTER TABLE "humanCapitalSch"."QUESTIONNAIRE_ANSWERS" OWNER to admindbhc;
