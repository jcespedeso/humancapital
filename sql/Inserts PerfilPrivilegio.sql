-- Perfil Privilegio.

-- Perfil SUPERADMIN HumanCapital
INSERT INTO "humanCapitalSch"."PERFIL_X_PRIVILEGIO"(
	"PERFIL", "PRIVILEGIO")
	--Cambie las XX por el id del Perfil SUPERADMIN
	VALUES (XX, 'ALL');
---------------------//----------------------------------

-- Perfil ADMINCLIENTE HumanCapital
INSERT INTO "humanCapitalSch"."PERFIL_X_PRIVILEGIO"(
	"PERFIL", "PRIVILEGIO")
	--Cambie las XX por el id del Perfil ADMINCLIENTE
	VALUES (8, 'EMPRESA_VER');
	VALUES (8, 'EMPRESA_EDITAR');
	VALUES (8, 'ASIGNAR_MODULO');
	VALUES (8, 'ASIGNAR_PRODUCTO');

	VALUES (8, 'DEPTO_CREAR');
	VALUES (8, 'DEPTO_EDITAR');
	VALUES (8, 'DEPTO_VER_X_CLIENTE');
	VALUES (8, 'DEPTO_ELIMINAR');

	VALUES (8, 'CARGO_CREAR');
	VALUES (8, 'CARGO_EDITAR');
	VALUES (8, 'CARGO_VER_X_CLIENTE');
	VALUES (8, 'CARGO_ELIMINAR');

	VALUES (8, 'USUARIO_VER');
	VALUES (8, 'USUARIO_EDITAR');
	VALUES (8, 'USUARIO_CREAR');

	VALUES (8, 'EMPLEADO_CREAR');
	VALUES (8, 'EMPLEADO_EDITAR');
	VALUES (8, 'EMPLEADO_ELIMINAR');
	VALUES (8, 'EMPLEADO_VER_X_CARGO');
	VALUES (8, 'EMPLEADO_VER_X_DEPTO');
	VALUES (8, 'EMPLEADO_VER_X_CLIENTE');

	VALUES (8, 'DATO_ADICIONAL_CREAR');
	VALUES (8, 'DATO_ADICIONAL_EDITAR');
	VALUES (8, 'DATO_ADICIONAL_ELIMINAR');
	VALUES (8, 'DATO_ADICIONAL_VER');

	VALUES (8, 'PARAMETRO_VER');
	VALUES (8, 'PARAMETRO_ASIGNAR');
	VALUES (8, 'ASIGNAR_PARAMETRO_EDITAR');
	VALUES (8, 'ASIGNAR_PARAMETRO_VER');
	VALUES (8, 'ASIGNAR_PARAMETRO_ELIMINAR');

	VALUES (8, 'OTROINGRESO_CREAR');
	VALUES (8, 'OTROINGRESO_EDITAR');
	VALUES (8, 'OTROINGRESO_ELIMINAR');
	VALUES (8, 'OTROINGRESO_VER_CLIENTE');

	VALUES (8, 'DESCUENTO_CREAR');
	VALUES (8, 'DESCUENTO_EDITAR');
	VALUES (8, 'DESCUENTO_ELIMINAR');
	VALUES (8, 'DESCUENTO_VER_CLIENTE');

	VALUES (8, 'CONTRATO_CREAR');
	VALUES (8, 'CONTRATO_EDITAR');
	VALUES (8, 'CONTRATO_ELIMINAR');

	VALUES (8, 'RFI_EDITAR');
	VALUES (8, 'RFI_ELIMINAR');
	VALUES (8, 'RFI_VER');

	VALUES (8, 'CONTEMERGENCIA_CREAR');
	VALUES (8, 'CONTEMERGENCIA_EDITAR');
	VALUES (8, 'CONTEMERGENCIA_ELIMINAR');
	VALUES (8, 'CONTEMERGENCIA_VER');

	VALUES (8, 'COMPETENCIA_CREAR');
	VALUES (8, 'COMPETENCIA_EDITAR');
	VALUES (8, 'COMPETENCIA_ELIMINAR');
	VALUES (8, 'COMPETENCIA_VER_CLIENTE');

--JSON INSERT http://52.15.200.56:8090/perfil/update
{
	"idPerfil":"8",
    "nombre": "ADMINCLIENTE",
    "descripcion": "Usuario Admin del Cliente.",
    "privilegioCollection":[
    	"EMPRESA_VER",
    	"EMPRESA_EDITAR",
    	"ASIGNAR_MODULO",
    	"ASIGNAR_PRODUCTO",
    	"DEPTO_CREAR",
    	"DEPTO_EDITAR",
    	"DEPTO_VER_X_CLIENTE",
    	"DEPTO_ELIMINAR",
    	"CARGO_CREAR",
    	"CARGO_EDITAR",
    	"CARGO_VER",
    	"CARGO_ELIMINAR",
    	"USUARIO_VER",
    	"USUARIO_EDITAR",
    	"USUARIO_CREAR",
    	"EMPLEADO_CREAR",
    	"EMPLEADO_EDITAR",
    	"EMPLEADO_ELIMINAR",
    	"EMPLEADO_VER",
    	"EMPLEADO_VER_X_CARGO",
    	"EMPLEADO_VER_X_DEPTO",
    	"EMPLEADO_VER_X_CLIENTE",
    	"DATO_ADICIONAL_CREAR",
    	"DATO_ADICIONAL_EDITAR",
    	"DATO_ADICIONAL_ELIMINAR",
    	"DATO_ADICIONAL_VER",
    	"PARAMETRO_VER",
    	"PARAMETRO_ASIGNAR",
    	"ASIGNAR_PARAMETRO_EDITAR",
    	"ASIGNAR_PARAMETRO_VER",
    	"ASIGNAR_PARAMETRO_ELIMINAR",
    	"OTROINGRESO_CREAR",
    	"OTROINGRESO_EDITAR",
    	"OTROINGRESO_ELIMINAR",
    	"OTROINGRESO_VER_CLIENTE",
    	"DESCUENTO_CREAR",
    	"DESCUENTO_EDITAR",
    	"DESCUENTO_ELIMINAR",
    	"DESCUENTO_VER_CLIENTE",
    	"CONTRATO_CREAR",
    	"CONTRATO_EDITAR",
    	"CONTRATO_ELIMINAR",
    	"RFI_EDITAR",
    	"RFI_ELIMINAR",
    	"RFI_VER",
    	"CONTEMERGENCIA_CREAR",
    	"CONTEMERGENCIA_EDITAR",
    	"CONTEMERGENCIA_ELIMINAR",
    	"CONTEMERGENCIA_VER",
    	"COMPETENCIA_CREAR",
    	"COMPETENCIA_EDITAR",
    	"COMPETENCIA_ELIMINAR",
    	"COMPETENCIA_VER_CLIENTE"
    ]
}

-------------------//-----------------------------

-- Perfil EMPLEADO HumanCapital
INSERT INTO "humanCapitalSch"."PERFIL_X_PRIVILEGIO"(
	"PERFIL", "PRIVILEGIO")
	--Cambie las XX por el id del Perfil EMPLEADO
	VALUES (9, 'DEPTO_VER_X_CLIENTE');
	VALUES (9, 'CARGO_VER_X_DEPTO');

	VALUES (9, 'USUARIO_VER');
	VALUES (9, 'USUARIO_EDITAR');
	VALUES (9, 'RESTPASSWORD');

	VALUES (9, 'EMPLEADO_EDITAR');
	VALUES (9, 'EMPLEADO_VER');

	VALUES (9, 'OTROINGRESO_VER');

	VALUES (9, 'DESCUENTO_VER');

	VALUES (9, 'CONTRATO_VER');

	VALUES (9, 'RFI_CREAR');
	VALUES (9, 'RFI_EDITAR');
	VALUES (9, 'RFI_VER');
	VALUES (9, 'RFI_ELIMINAR');

	VALUES (9, 'CONTEMERGENCIA_VER');
	VALUES (9, 'CONTEMERGENCIA_CREAR');
	VALUES (9, 'CONTEMERGENCIA_EDITAR');
	VALUES (9, 'CONTEMERGENCIA_ELIMINAR');

--JSON INSERT http://52.15.200.56:8090/perfil/update
{
	"idPerfil":"6",
    "nombre": "EMPLEADO",
    "descripcion": "Perfil por defecto para los Empleados de los Clientes.",
    "privilegioCollection":[
    	"DEPTO_VER_X_CLIENTE",
    	"CARGO_VER_X_DEPTO",
    	"USUARIO_VER",
    	"USUARIO_EDITAR",
    	"RESTPASSWORD",
    	"EMPLEADO_EDITAR",
    	"EMPLEADO_VER",
    	"OTROINGRESO_VER",
    	"DESCUENTO_VER",
    	"CONTRATO_VER",
    	"RFI_CREAR",
    	"RFI_EDITAR",
    	"RFI_VER",
    	"RFI_ELIMINAR",
    	"CONTEMERGENCIA_VER",
    	"CONTEMERGENCIA_CREAR",
    	"CONTEMERGENCIA_EDITAR",
    	"CONTEMERGENCIA_ELIMINAR"
    ]
}
---------------------------//---------------------






