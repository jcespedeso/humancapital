--Alejandro Herrera Montilla

-- Data usuario Admin Defaul.
INSERT INTO "humanCapitalSch"."USUARIO"(
	"USERNAME", "PASSWORD", "EMPLEADO", "ACTIVO", "FECHA_CREACION", "USUARIO_CREA", "FECHA_MODIFICA", "USUARIO_MODIFICA", "FECHA_INACTIVACION", "USUARIO_INACTIVA")
	VALUES ('admin', '$2a$10$i6Wbz2qRIcZGsWKSeOE80Oqx1YxUdIA6OLnpPPJodmOXC.cOOMnwq', null, true, '2019/12/15', 0, '2019/12/15', 0, null, null);
-- -----------------------//----------------------------

-- Data predeterminada NIVEL_ARL.
INSERT INTO "humanCapitalSch"."NIVEL_ARL"(
	"CODIGO", "NIVEL_RIESGO", "PORCENTAJE_APORTE", "VALOR_APORTE")
	VALUES (1, 'Riesgo I', 0.522, 0);
INSERT INTO "humanCapitalSch"."NIVEL_ARL"(
	"CODIGO", "NIVEL_RIESGO", "PORCENTAJE_APORTE", "VALOR_APORTE")
	VALUES (2, 'Riesgo II', 1.044, 0);
INSERT INTO "humanCapitalSch"."NIVEL_ARL"(
	"CODIGO", "NIVEL_RIESGO", "PORCENTAJE_APORTE", "VALOR_APORTE")
	VALUES (3, 'Riesgo III', 2.436, 0);
INSERT INTO "humanCapitalSch"."NIVEL_ARL"(
	"CODIGO", "NIVEL_RIESGO", "PORCENTAJE_APORTE", "VALOR_APORTE")
	VALUES (4, 'Riesgo IV', 4.35, 0);

INSERT INTO "humanCapitalSch"."NIVEL_ARL"(
	"CODIGO", "NIVEL_RIESGO", "PORCENTAJE_APORTE", "VALOR_APORTE")
	VALUES (5, 'Riesgo V', 6.96, 0);
-- -----------------------//----------------------------

-- Data predeterminada CLASIFICACION_CLIENTE.
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sin Clasificacion');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Persona Natural');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sociedad Limitada');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sociedad Anonima');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sociedad Colectiva');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sociedad en Comandita Simple');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sociedad en Comandita por Acciones');
	INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sociedad Extranjera Sucursal');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Empresas Asociativas para el Trabajo');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Empresas Unipersonales');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sociedad Agraria de Transformación');
INSERT INTO "humanCapitalSch"."CLASIFICACION_CLIENTE"("TIPO_ESPECIFICO")
	VALUES ('Sociedades por Acciones Simplificadas');
-- -----------------------//----------------------------

-- predeterminada TIPO_OTRO_INGRESO.
INSERT INTO "humanCapitalSch"."TIPO_OTRO_INGRESO"(
	"TIPO_INGRESO", "SALARIAL", "GRAVADO", "RECOMPONE", "FLEXIBILIZA", "ACTIVO", "FECHA_CREACION", "USUARIO_CREA", "FECHA_MODIFICA", "USUARIO_MODIFICA", "FECHA_INACTIVACION", "USUARIO_INACTIVA")
	VALUES ( 'Salarial, Recompone y Flexibiliza', true, null, true, true, true, '2019/12/15', 1, '2019/12/15', 1, null, null);
INSERT INTO "humanCapitalSch"."TIPO_OTRO_INGRESO"(
	"TIPO_INGRESO", "SALARIAL", "GRAVADO", "RECOMPONE", "FLEXIBILIZA", "ACTIVO", "FECHA_CREACION", "USUARIO_CREA", "FECHA_MODIFICA", "USUARIO_MODIFICA", "FECHA_INACTIVACION", "USUARIO_INACTIVA")
	VALUES ( 'Salarial, No Recompone y No Flexibiliza', true, null, false, false, true, '2019/12/15', 1, '2019/12/15', 1, null, null);
INSERT INTO "humanCapitalSch"."TIPO_OTRO_INGRESO"(
	"TIPO_INGRESO", "SALARIAL", "GRAVADO", "RECOMPONE", "FLEXIBILIZA", "ACTIVO", "FECHA_CREACION", "USUARIO_CREA", "FECHA_MODIFICA", "USUARIO_MODIFICA", "FECHA_INACTIVACION", "USUARIO_INACTIVA")
	VALUES ( 'No Salarial, Recompone y Flexibiliza', false, null, true, true, true, '2019/12/15', 1, '2019/12/15', 1, null, null);
INSERT INTO "humanCapitalSch"."TIPO_OTRO_INGRESO"(
	"TIPO_INGRESO", "SALARIAL", "GRAVADO", "RECOMPONE", "FLEXIBILIZA", "ACTIVO", "FECHA_CREACION", "USUARIO_CREA", "FECHA_MODIFICA", "USUARIO_MODIFICA", "FECHA_INACTIVACION", "USUARIO_INACTIVA")
	VALUES ( 'No Salarial, Gravado, No Recompone y No Flexibiliza', false, true, false, false, true, '2019/12/15', 1, '2019/12/15', 1, null, null);
INSERT INTO "humanCapitalSch"."TIPO_OTRO_INGRESO"(
	"TIPO_INGRESO", "SALARIAL", "GRAVADO", "RECOMPONE", "FLEXIBILIZA", "ACTIVO", "FECHA_CREACION", "USUARIO_CREA", "FECHA_MODIFICA", "USUARIO_MODIFICA", "FECHA_INACTIVACION", "USUARIO_INACTIVA")
	VALUES ( 'No Salarial, No Gravado, No Recompone y No Flexibiliza', false, false, false, false, true, '2019/12/15', 1, '2019/12/15', 1, null, null);
-- -----------------------//----------------------------

-- predeterminada CATEGORIAS.
INSERT INTO "humanCapitalSch"."CATEGORIA"("ID_CATEGORIA", "NOMBRE", "ACTIVO")
	VALUES ('ESTRATEGICO', true);
INSERT INTO "humanCapitalSch"."CATEGORIA"("ID_CATEGORIA", "NOMBRE", "ACTIVO")
	VALUES ('OPERACIONAL', true);
INSERT INTO "humanCapitalSch"."CATEGORIA"("ID_CATEGORIA", "NOMBRE", "ACTIVO")
	VALUES ('TÁCTICO', true);
-- -----------------------//----------------------------