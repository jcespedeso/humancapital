package co.com.humancapital.portal.rfi.type;

public enum ExcelColumnType {
    TEXT,
    DATE,
    NUMBER,
    NUMBER_INTEGER,
    NUMBER_AGE,
    MONEY,
    EMAIL,
    BOOLEAN,
    ITEMS
}
