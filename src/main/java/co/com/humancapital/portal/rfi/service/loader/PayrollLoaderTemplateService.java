package co.com.humancapital.portal.rfi.service.loader;

import co.com.humancapital.portal.config.rfi.loader.AbstractTemplateConfig;
import co.com.humancapital.portal.config.rfi.loader.PayrollTemplateConfig;
import java.io.InputStream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PayrollLoaderTemplateService extends AbstractLoaderTemplateService {
    
    @Override
    protected AbstractTemplateConfig getTemplateConfig() {
        return PayrollTemplateConfig.getInstance();
    }
    
    @Override
    public byte[] buildTemplate() {
        throw new NotImplementedException("::: Method buildTemplate not implemented in PayrollLoaderTemplateService");
    }
    
    @Override
    public byte[] loadTemplate(InputStream inputStream, Long clientIds) {
        throw new NotImplementedException("::: Method loadTemplate not implemented in PayrollLoaderTemplateService");
    }
}
