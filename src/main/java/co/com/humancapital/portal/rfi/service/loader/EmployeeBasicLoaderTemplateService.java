package co.com.humancapital.portal.rfi.service.loader;

import co.com.humancapital.portal.config.rfi.loader.AbstractTemplateConfig;
import co.com.humancapital.portal.config.rfi.loader.EmployeeBasicTemplateConfig;
import co.com.humancapital.portal.config.rfi.loader.ExcelColumnHeaderDefinition;
import co.com.humancapital.portal.dto.*;
import co.com.humancapital.portal.entity.*;
import co.com.humancapital.portal.get.service.CategoriaService;
import co.com.humancapital.portal.service.*;
import co.com.humancapital.portal.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static co.com.humancapital.portal.util.PoiUtil.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class EmployeeBasicLoaderTemplateService extends AbstractLoaderTemplateService {
    
    private final DepartamentoService departamentoService;
    private final CargoService cargoService;
    private final EmpleadoService empleadoService;
    private final CompensacionService compensacionService;
    private final DatoTributarioService datoTributarioService;
    private final ClienteService clienteService;
    private final TransactionTemplate transactionTemplate;
    private final CategoriaService categoriaService;

    @Override
    public byte[] buildTemplate() {
        String sheetName = WorkbookUtil.createSafeSheetName(getTemplateConfig().getSheetName());
        Map<Class<?>, List<ExcelColumnHeaderDefinition>> columnHeadersDefinition = getTemplateConfig().getFields();
        
        XSSFWorkbook template = new XSSFWorkbook();
        XSSFSheet firstSheet = template.createSheet(sheetName);
        CellStyle headerStyle = getHeaderStyle(template);
        
        columnHeadersDefinition.forEach((clazz, headerDefinitions) ->
                createHeaderGroup(firstSheet, headerStyle, clazz, headerDefinitions));
        
        return writeWorkbook(template);
    }
    
    @Override
    public byte[] loadTemplate(InputStream inputStream, Long clientId) {
        try {
            EmployeeBasicTemplateConfig templateConfig = EmployeeBasicTemplateConfig.getInstance();
            XSSFWorkbook workbook = XSSFWorkbookFactory.createWorkbook(inputStream);
            String sheetName = templateConfig.getSheetName();
            Cliente cliente = getCliente(clientId);
            Map<String, Map<String, Cargo>> jobsPerDepartment = buildJobsPerDepartment(cliente);
            Map<String, Departamento> departmentMap = buildDepartmentsMap(cliente);
            
            if (isValidWorkbook(workbook, sheetName)) {
                XSSFSheet sheet = workbook.getSheet(sheetName);
                int startInfoRow = getDataRowPosition();
                int endInfoRow = sheet.getLastRowNum();
                CellStyle errorStyle = getCellErrorStyle(workbook);
                CellStyle successStyle = getCellSuccessStyle(workbook);
                
                for (int i = startInfoRow; i <= endInfoRow; i++) {
                    XSSFRow currentRow = sheet.getRow(i);
                    loadEmployee(currentRow, cliente, jobsPerDepartment, departmentMap, successStyle, errorStyle);
                }
            }
    
            return writeWorkbook(workbook);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private void loadEmployee(XSSFRow currentRow, Cliente cliente,
            Map<String, Map<String, Cargo>> jobsPerDepartment,
            Map<String, Departamento> departmentMap, CellStyle successStyle, CellStyle errorStyle) {
        try {
            transactionTemplate.executeWithoutResult(status -> {
                Empleado empleado = buildEmployeeWithDependencies(
                        currentRow, jobsPerDepartment, departmentMap, cliente.getIdCliente());
                log.info("::: Empleado cargado [{}]", empleado.getIdEmpleado());
            });
            
            getOrCreateCell(currentRow, 0).setCellStyle(successStyle);
        } catch (Exception e) {
            log.error("::: Error cargando empleado", e);
            XSSFCell errorCell = getOrCreateCell(currentRow, 0);
            errorCell.setCellStyle(errorStyle);
            addComment(errorCell, e.getMessage());
        }
    }
    
    private Cliente getCliente(Long clienteId) {
        return clienteService.findId(clienteId);
    }
    
    private Map<String, Departamento> buildDepartmentsMap(Cliente cliente) {
        return departamentoService.departamentoXCliente(cliente.getIdCliente(), Pageable.unpaged()).stream()
                .collect(Collectors.toMap(departamento -> departamento.getNombre(), Function.identity()));
        
    }
    
    private Map<String, Map<String, Cargo>> buildJobsPerDepartment(Cliente cliente) {
        return cargoService.cargosXCliente(cliente.getIdCliente(), Pageable.unpaged()).stream()
                .collect(Collectors.groupingBy(cargo -> cargo.getDepartamento().getNombre(),
                        Collectors.toMap(cargo -> cargo.getNombre(), Function.identity(),
                                (jobName1, jobName2) -> jobName1, LinkedHashMap::new)));
    }
    
    private Empleado buildEmployeeWithDependencies(XSSFRow currentRow,
            Map<String, Map<String, Cargo>> jobsPerDepartment, Map<String, Departamento> departmentMap, Long clientId) {
        Departamento rowDepartment = getObjectFromRow(Departamento.class, currentRow);
        rowDepartment.setNombre(rowDepartment.getNombre().toUpperCase());
        Departamento persistedDepartment = getDepartment(rowDepartment.getNombre(), departmentMap, clientId);
        Cargo cargo = getObjectFromRow(Cargo.class, currentRow);
        cargo.setNombre(cargo.getNombre().toUpperCase());
        cargo = getJob(cargo.getNombre(), cargo.getCategoria().getIdCategoria(), persistedDepartment, jobsPerDepartment);
        
        Empleado empleado = getObjectFromRow(Empleado.class, currentRow);
        empleado.setCargo(cargo);
        empleado.setUsuarioCrea(-1L);
        empleado.setUsuarioModifica(-1L);
        empleado.setTipoDocumento(EnumConstantUtil.getIntStringIdFromValue(empleado.getTipoDocumento(), TipoDocumento.listar()));
        empleado.setGenero(EnumConstantUtil.getStringIdFromValue(empleado.getGenero(), Genero.listar()));
        empleado.setEstadoCivil(EnumConstantUtil.getIntStringIdFromValue(empleado.getEstadoCivil(), EstadoCivil.listar()));
        empleado.setRegimenPension(EnumConstantUtil.getIntStringIdFromValue(empleado.getRegimenPension(), RegimenPension.listar()));
        EmpleadoInputDTO employeeInput = EmpleadoInputDTO.getDTO(empleado);
        try {
            empleado = empleadoService.create(employeeInput);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Compensacion compensacion = buildCompensacion(currentRow, empleado);
        empleado.addCompensacion(compensacion);
        
        DatoTributario datoTributario = buildDatoTributario(currentRow, empleado);
        empleado.setDatoTributario(datoTributario);
        log.debug("::: Empleado cargado -> {}", empleado);
        return empleado;
    }
    
    private Compensacion buildCompensacion(XSSFRow currentRow, Empleado empleado) {
        Compensacion compensacion = getObjectFromRow(Compensacion.class, currentRow);
        compensacion.setEmpleado(empleado);
        compensacion.setUsuarioCrea(-1L);
        compensacion.setUsuarioModifica(-1L);
        compensacion.setTipoContrato(EnumConstantUtil
                .getIntStringIdFromValue(compensacion.getTipoContrato(), TipoContrato.listar()));
        compensacion.setRegimenLaboral(EnumConstantUtil
                .getIntStringIdFromValue(compensacion.getRegimenLaboral(), TipoRegimen.listar()));
        compensacion.setCondicionLaboral(EnumConstantUtil
                .getIntStringIdFromValue(compensacion.getCondicionLaboral(), CondicionEmpleado.listar()));
        return compensacionService.create(CompensacionInputDTO.getDTO(compensacion));
    }
    
    private DatoTributario buildDatoTributario(XSSFRow currentRow, Empleado empleado) {
        DatoTributario datoTributario = getObjectFromRow(DatoTributario.class, currentRow);
        datoTributario.setEmpleado(empleado);
        return datoTributarioService.create(DatoTributarioInputDTO.getDTO(datoTributario));
    }
    
    private Departamento getDepartment(String departmentName, Map<String, Departamento> departmentMap, Long clientId) {
        if (!departmentMap.containsKey(departmentName)) {
            DepartamentoInputDTO departamentoInputDTO = DepartamentoInputDTO.builder()
                    .nombre(departmentName)
                    .usuarioCrea(-1L)
                    .usuarioModifica(-1L)
                    .cliente(clientId)
                    .build();
            Departamento departamento = departamentoService.create(departamentoInputDTO);
            departmentMap.put(departamento.getNombre(), departamento);
        }
        
        return departmentMap.get(departmentName);
    }
    
    private Cargo getJob(String jobName, Long jobCategory, Departamento department,
            Map<String, Map<String, Cargo>> jobsPerDepartment) {
        String deptoName = department.getNombre();
        Long categoryId = categoriaService.findId(jobCategory).getIdCategoria();

        if (!jobsPerDepartment.containsKey(deptoName)) {
            jobsPerDepartment.put(deptoName, new LinkedHashMap<>());
        }
        
        if (!jobsPerDepartment.get(deptoName).containsKey(jobName)) {
            CargoInputDTO jobInput = CargoInputDTO.builder()
                    .departamento(department.getIdDepartamento())
                    .nombre(jobName)
                    .categoria(categoryId)
                    .usuarioCrea(-1L)
                    .usuarioModifica(-1L)
                    .build();
            
            Cargo cargo = cargoService.create(jobInput);
            jobsPerDepartment.get(deptoName).put(cargo.getNombre(), cargo);
        }
        
        return jobsPerDepartment.get(department.getNombre()).get(jobName);
    }
    
    
    @Override
    protected AbstractTemplateConfig getTemplateConfig() {
        return EmployeeBasicTemplateConfig.getInstance();
    }
}
