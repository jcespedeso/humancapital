package co.com.humancapital.portal.rfi.service.loader;

import java.io.IOException;
import java.io.InputStream;

public interface LoaderTemplateService {
    byte[] buildTemplate();
    byte[] loadTemplate(InputStream inputStream, Long clientIds) throws IOException;
}
