package co.com.humancapital.portal.rfi.service.loader;

import co.com.humancapital.portal.config.rfi.loader.AbstractTemplateConfig;
import co.com.humancapital.portal.config.rfi.loader.ExcelColumnHeaderDefinition;
import co.com.humancapital.portal.util.ReflectionUtil;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.BeanUtils;

import static co.com.humancapital.portal.constant.LoaderConstant.DEFAULT_ROW_HEADER_OFFSET;
import static co.com.humancapital.portal.util.PoiUtil.createHeaderCell;
import static co.com.humancapital.portal.util.PoiUtil.getCellValue;
import static co.com.humancapital.portal.util.PoiUtil.getOrCreateRow;

@Slf4j
public abstract class AbstractLoaderTemplateService implements LoaderTemplateService {
    
    protected abstract AbstractTemplateConfig getTemplateConfig();
    
    //=========================================================================================
    // When the template configuration is ready, groups and columns should come from a service
    //=========================================================================================
    protected static void createHeaderGroup(XSSFSheet sheet, CellStyle headerStyle,
            Class<?> entity, List<ExcelColumnHeaderDefinition> columns) {
        
        String columnGroupName = entity.getSimpleName();
        int firstColumn = columns.get(0).getColumnIndex();
        int lastColumn = columns.get(columns.size() - 1).getColumnIndex();
        int groupRowIndex = getHeaderGroupRowPosition();
        
        Row groupRow = getOrCreateRow(sheet, groupRowIndex);
        Cell groupCell = groupRow.createCell(firstColumn);
        groupCell.setCellValue(columnGroupName);
        groupCell.setCellStyle(headerStyle);
        
        if (firstColumn < lastColumn) { //Merge areas are allowed for 2 or more cells
            sheet.addMergedRegion(new CellRangeAddress(groupRowIndex, groupRowIndex, firstColumn, lastColumn));
        }
        
        XSSFRow headerRow = getOrCreateRow(sheet, getHeaderRowPosition());
        columns.forEach(columnHeader -> createHeaderCell(headerRow, columnHeader, headerStyle));
    }
    
    protected static int getHeaderGroupRowPosition() {
        return DEFAULT_ROW_HEADER_OFFSET;
    }
    
    protected static int getHeaderRowPosition() {
        return getHeaderGroupRowPosition() + 1;
    }
    
    protected static int getDataRowPosition() {
        return getHeaderRowPosition() + 1;
    }
    
    protected <T> T getObjectFromRow(Class<T> clazz, XSSFRow row) {
        T bean = BeanUtils.instantiateClass(clazz);
        List<ExcelColumnHeaderDefinition> headersConfig = getTemplateConfig()
                .getFields()
                .get(clazz);
        
        headersConfig.forEach(headerConfig -> {
            XSSFCell currentCell = row.getCell(headerConfig.getColumnIndex());
            
            if (Objects.nonNull(currentCell)) {
                Object cellValue = getCellValue(currentCell, headerConfig);
                ReflectionUtil.setField(clazz, bean, headerConfig.getName(), cellValue);
            }
        });
        
        return bean;
    }
}
