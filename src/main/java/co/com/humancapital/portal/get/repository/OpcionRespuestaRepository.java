package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.OpcionRespuesta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020
 */
@Repository
public interface OpcionRespuestaRepository extends JpaRepository<OpcionRespuesta, Long> {

    OpcionRespuesta findByIdOpcionRespuesta(Long idRelacion);
    Page<OpcionRespuesta> findAll(Pageable pageable);

    @Query("select opcR from OpcionRespuesta opcR " +
            "left join Evaluacion eva on eva.idEvaluacion = opcR.evaluacion " +
            "where eva.idEvaluacion=:idEvaluacion")
    Page<OpcionRespuesta> findAllByEvaluacion(@Param("idEvaluacion") Long idEmpleado, Pageable pageable);

}
