package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.EvaluacionEmpleado;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de salida de la entidad EvaluacionEmpleado.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EvaluacionEmpleadoOutputDTO {

    private Long idEvaEmpleado;
    private boolean activo;

    //Relaciones
    private Long empleado;
    private Long evaluacion;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * EvaluacionEmpleado.
     * @param evaluacionEmpleado Objeto EvaluacionEmpleado.
     * @return Objeto EvaluacionEmpleadoOutputDTO.
     */
    public static EvaluacionEmpleadoOutputDTO getDTO(EvaluacionEmpleado evaluacionEmpleado) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper.map(evaluacionEmpleado, EvaluacionEmpleadoOutputDTO.class);
    }

}
