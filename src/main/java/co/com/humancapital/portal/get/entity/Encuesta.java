/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.get.entity;

import co.com.humancapital.portal.entity.Empleado;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"ENCUESTA\"")
@NoArgsConstructor
@Data
public class Encuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ENCUESTA")
    private Long idEncuesta;
    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Column(name = "FECHA_FINAL")
    @Temporal(TemporalType.DATE)
    private Date fechaFinal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;

    //Relaciones
    @ManyToMany(mappedBy = "encuestaCollection")
    private Collection<Empleado> empleadoCollection;

    //Relaciones Omitidas
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "encuesta")
//    private Collection<Pregunta> preguntaCollection;

    
}
