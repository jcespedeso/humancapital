package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.get.entity.Competencia;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/01/2020
 */
@Repository
public interface CompetenciaRepository extends JpaRepository<Competencia, Long> {

    Competencia findByIdCompetenciaAndActivoTrue(Long idCompetencia);
    Page<Competencia> findAllByActivoTrue(Pageable pageable);
    List<Competencia> findAllByActivoTrue();
    Page<Competencia> findAllByClienteAndActivoTrue(Cliente cliente, Pageable pageable);
    List<Competencia> findAllByClienteAndActivoTrue(Cliente cliente);

}
