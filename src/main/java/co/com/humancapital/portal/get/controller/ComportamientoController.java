package co.com.humancapital.portal.get.controller;

import co.com.humancapital.portal.controller.AbstractController;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.get.dto.ComportamientoInputDTO;
import co.com.humancapital.portal.get.service.ComportamientoService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/03/2020
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Comportamiento.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/comportamiento")
public class ComportamientoController extends AbstractController<ComportamientoInputDTO, GeneralOutput, Long> {

    private final String CREATE = "COMPORTAMIENTO_CREAR";
    private final String UPDATE = "COMPORTAMIENTO_EDITAR";
    private final String VIEW = "COMPORTAMIENTO_VER";
    private final String DELETE = "COMPORTAMIENTO_ELIMINAR";

    private final ComportamientoService comportamientoService;

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), comportamientoService.findId(id));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"orden"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), comportamientoService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody ComportamientoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, CREATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        comportamientoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody ComportamientoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, UPDATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        comportamientoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, DELETE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        comportamientoService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/competencia/{idCompetencia}")
    public GeneralOutput getAllbyCompetencia(@PathVariable Long idCompetencia, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"orden"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), comportamientoService.comportamientoXCompetencia(idCompetencia, pageable));
    }

    @Override
    public GeneralOutput create(ComportamientoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(ComportamientoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
