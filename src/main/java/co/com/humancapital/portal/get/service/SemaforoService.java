package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.SemaforoInputDTO;
import co.com.humancapital.portal.get.dto.SemaforoOutputDTO;
import co.com.humancapital.portal.get.entity.Semaforo;
import co.com.humancapital.portal.get.repository.EvaluacionRepository;
import co.com.humancapital.portal.get.repository.SemaforoRepository;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

import static co.com.humancapital.portal.util.ObjectMapperUtils.map;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/03/2020
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de la entidad Semaforo.
 */
@Service
@RequiredArgsConstructor
public class SemaforoService implements ServiceInterfaceDto<SemaforoOutputDTO, Long, SemaforoInputDTO> {

    private final SemaforoRepository semaforoRepository;
    private final EvaluacionService evaluacionService;
    private final EvaluacionRepository evaluacionRepository;

    @Override
    public SemaforoOutputDTO findId(Long key) {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Semaforo con id "+key+" no existe.");
        }
        return SemaforoOutputDTO.getDTO(semaforoRepository.findByIdSemaforo(key));
    }

    @Override
    public SemaforoOutputDTO create(SemaforoInputDTO entity) throws Exception{
        if(entity.getEvaluacion() == null || !evaluacionService.existeById(entity.getEvaluacion())) {
            throw new ResourceNotFoundException("La Evaluación con id "+entity.getEvaluacion()+" no existe o no se envio el dato.");
        }
        Semaforo semaforo = new Semaforo();
        semaforo.setColor(entity.getColor().trim());
        semaforo.setEtiqueta(entity.getEtiqueta().trim());
        semaforo.setPorcentaje(entity.getPorcentaje());
        semaforo.setEvaluacion(evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getEvaluacion()));
        return SemaforoOutputDTO.getDTO(semaforoRepository.saveAndFlush(semaforo));
    }

    @Override
    public SemaforoOutputDTO update(SemaforoInputDTO entity) throws Exception{
        if (entity.getIdSemaforo() == null || !existeById(entity.getIdSemaforo())) {
            throw new ResourceNotFoundException("El Semaforo con id "+entity.getIdSemaforo()+" no existe o esta vacio.");
        }
        if(entity.getEvaluacion() == null || !evaluacionService.existeById(entity.getEvaluacion())) {
            throw new ResourceNotFoundException("La Evaluación con id "+entity.getEvaluacion()+" no existe o no se envio el dato.");
        }
        Semaforo semaforo = semaforoRepository.findByIdSemaforo(entity.getIdSemaforo());
        semaforo.setColor(entity.getColor().trim());
        semaforo.setEtiqueta(entity.getEtiqueta().trim());
        semaforo.setPorcentaje(entity.getPorcentaje());
        semaforo.setEvaluacion(evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getEvaluacion()));
        return SemaforoOutputDTO.getDTO(semaforoRepository.saveAndFlush(semaforo));
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite eliminar un semaforo asociado a una Evaluación.
     * @param key Id del semaforo a eliminar.
     */
    public void delete(Long key) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Semaforo con id "+key+" no existe o esta vacio.");
        }
        semaforoRepository.delete(semaforoRepository.findByIdSemaforo(key));
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(semaforoRepository.findByIdSemaforo(key)==null){
            return false;
        }
        return semaforoRepository.existsById(key);
    }

    /**
     * Metodo que permite obtener las configuración del semaforo personalizada de una Evaluación.
     * @param idEvaluacion Id de la Evaluacion a validar.
     * @return Lista de OpcionRespuestaOutputDTO.
     */
    public Page<SemaforoOutputDTO> findAllEvaluacion(Long idEvaluacion, Pageable pageable) throws Exception{
        if(idEvaluacion == null || !evaluacionService.existeById(idEvaluacion)) {
            throw new ResourceNotFoundException("La Evaluación con id "+ idEvaluacion +" no existe o no se envio en dato.");
        }
        List<SemaforoOutputDTO> semaforoOutputDTOS = new ArrayList<>();
        for (Semaforo semaforo : semaforoRepository.findAllByEvaluacion(idEvaluacion, pageable)) {
            SemaforoOutputDTO semaforoOutputDTO = SemaforoOutputDTO.getDTO(semaforo);
            semaforoOutputDTOS.add(semaforoOutputDTO);
        }
        return new PageImpl<>(semaforoOutputDTOS);
    }

    @Override
    public Page<SemaforoOutputDTO> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public SemaforoOutputDTO create(SemaforoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public SemaforoOutputDTO update(SemaforoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}