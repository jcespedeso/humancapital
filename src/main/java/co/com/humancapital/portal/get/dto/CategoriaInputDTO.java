package co.com.humancapital.portal.get.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 08703/2020
 * Clase que permite modelar los datos de entrada de la entidad Categoria.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoriaInputDTO {

    private Long idCategoria;
    @NotEmpty(message = "El nombre de la Categoria no puede estar vacio.")
    @Size(max = 50, message = "El nombre debe no puede tener mas de 50 caracteres")
    private String nombre;
    @NotNull(message = "El Cliente no puede estar vacio.")
    private Long cliente;

}
