/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.get.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"COMPORTAMIENTO\"")
@NoArgsConstructor
@Data
public class Comportamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COMPORTAMIENTO")
    private Long idComportamiento;
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ORDEN")
    @Range(min = 1)
    private BigInteger orden;

    // Datos de Auditoria
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    private LocalDate fechaCreacion;
    @Basic(optional = false)
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;
    @Basic(optional = false)
    @Column(name = "FECHA_MODIFICA")
    private LocalDate fechaModifica;
    @Basic(optional = false)
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;
    @Column(name = "FECHA_INACTIVACION")
    private LocalDate fechaInactivacion;
    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

    // Relaciones
    @JoinColumn(name = "COMPETENCIA", referencedColumnName = "ID_COMPETENCIA")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Competencia competencia;

    @JoinColumn(name = "CATEGORIA", referencedColumnName = "ID_CATEGORIA")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Categoria categoria;

}
