package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.CategoriaInputDTO;
import co.com.humancapital.portal.get.dto.CategoriaOutputDTO;
import co.com.humancapital.portal.get.entity.Categoria;
import co.com.humancapital.portal.get.repository.CategoriaRepository;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 07/03/2020
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de las Categorias.
 */
@Service
@RequiredArgsConstructor
public class CategoriaService implements ServiceInterfaceDto<CategoriaOutputDTO, Long, CategoriaInputDTO> {

    private final CategoriaRepository categoriaRepository;
    private final ClienteRepository clienteRepository;

    @Override
    public CategoriaOutputDTO findId(Long key) {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Categoria con id "+key+" no existe.");
        }
        return CategoriaOutputDTO.getDTO(categoriaRepository.findByIdCategoriaAndActivoTrue(key));
    }

    @Override
    public Page<CategoriaOutputDTO> findAll(Pageable pageable) {
        if(categoriaRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Categorias creadas en el sistema.");
        }
        List<CategoriaOutputDTO> categoriaOutputDTOS = new ArrayList<>();
        for (Categoria categoria : categoriaRepository.findAllByActivoTrue(pageable)) {
            CategoriaOutputDTO categoriaOutputDTO = CategoriaOutputDTO.getDTO(categoria);
            categoriaOutputDTOS.add(categoriaOutputDTO);
        }
        return new PageImpl<>(categoriaOutputDTOS);
    }

    @Override
    public CategoriaOutputDTO create(CategoriaInputDTO entity) {
        if (existeByNombre(entity.getNombre())) {
            throw new ResourceNotFoundException("El nombre de la Categoria a crear ya existe.");
        }
        Categoria categoria = new Categoria();
        categoria.setNombre(entity.getNombre().trim().toUpperCase());
        categoria.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        categoria.setActivo(true);
        return CategoriaOutputDTO.getDTO(categoriaRepository.saveAndFlush(categoria));
    }

    @Override
    public CategoriaOutputDTO update(CategoriaInputDTO entity) {
        if (entity.getIdCategoria() == null || !existeById(entity.getIdCategoria())) {
            throw new ResourceNotFoundException("La Categoria con id "+entity.getIdCategoria()+" no existe o esta vacia.");
        }
        Categoria categoria = categoriaRepository.findByIdCategoriaAndActivoTrue(entity.getIdCategoria());
        categoria.setNombre(entity.getNombre().trim().toUpperCase());
        categoria.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        return CategoriaOutputDTO.getDTO(categoriaRepository.saveAndFlush(categoria));
    }

    public void delete(Long key) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La Categoria con id "+key+" no existe o esta vacio.");
        }
        Categoria categoria = categoriaRepository.findByIdCategoriaAndActivoTrue(key);
        categoria.setActivo(false);
        categoriaRepository.save(categoria);
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(categoriaRepository.findByIdCategoriaAndActivoTrue(key)==null){
            return false;
        }
        return categoriaRepository.existsById(key);
    }

    /**
     * Metodo que permite evaluar si una Categoria ya existe a partir de su nombre.
     * @param nombre Nombre de la Categoria a validar.
     * @return {@code true} si existe algun registro con el nombre suministrado de lo contrario {@code false}
     */
    public Boolean existeByNombre(String nombre) {
        if(nombre==null){
            throw new ResourceNotFoundException("El Nombre a validar, no puede estar vacio.");
        }
        if(categoriaRepository.findByNombreAndActivoTrue(nombre.trim().toUpperCase())==null){
            return false;
        }
        return true;
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 02/04/2020
     * Metodo que permite obtener las Categorias de un Cliente Especifico.
     * @param idCliente Id del Cliente a consultar.
     * @return Listado de CategoriaOutputDTO con la data de Categorias.
     */
    public List<CategoriaOutputDTO> findCategoriaByCliente(Long idCliente){
        if (idCliente == null || !clienteRepository.existsByIdClienteAndActivoTrue(idCliente)) {
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        List<CategoriaOutputDTO> categoriaOutputDTOS = new ArrayList<>();
        for (Categoria categoria:categoriaRepository.findAllByClienteAndActivoTrueOrderByNombre(cliente)) {
            categoriaOutputDTOS.add(CategoriaOutputDTO.getDTO(categoria));
        }
        return categoriaOutputDTOS;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }

    @Override
    public CategoriaOutputDTO create(CategoriaInputDTO entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public CategoriaOutputDTO update(CategoriaInputDTO entity, List<MultipartFile> files) {
        return null;
    }
}