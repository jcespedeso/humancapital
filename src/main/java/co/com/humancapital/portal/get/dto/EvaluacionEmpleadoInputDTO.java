package co.com.humancapital.portal.get.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de entrada de la entidad EvaluacionEmpleado.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EvaluacionEmpleadoInputDTO {

    private Long idEvaEmpleado;
    private boolean activo;

    //Relaciones
    private Long empleado;
    private Long evaluacion;

}
