package co.com.humancapital.portal.get.dto;

import java.time.LocalDateTime;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EvaluationNotificationDTO {
    private Long id;
    @NotEmpty
    private String htmlMessage;
    @NotEmpty
    private String subject;
    @NotNull
    private LocalDateTime notificationDate;
    @NotEmpty
    private String fromAddress;
    @NotEmpty
    private String fromName;
    @NotEmpty
    private String testFromAddress;
    @NotEmpty
    private String testFromName;
    @NotEmpty
    private String testToAddress;
    @NotEmpty
    private String testToName;
    @NotNull
    private Long evaluacionId;
}