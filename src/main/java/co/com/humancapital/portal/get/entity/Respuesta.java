
package co.com.humancapital.portal.get.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"RESPUESTA\"")
@NoArgsConstructor
@Data
public class Respuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_RESPUESTA")
    private Long idRespuesta;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION_RESPUESTA")
    private String descripcionRespuesta;
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private boolean activo;

    //Relaciones
    @JoinColumn(name = "PREGUNTA", referencedColumnName = "ID_PREGUNTA")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Pregunta pregunta;

}
