
package co.com.humancapital.portal.get.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"OPCION_RESPUESTA\"")
@NoArgsConstructor
@Data
public class OpcionRespuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_OPCION_RESPUESTA")
    private Long idOpcionRespuesta;
    @NotEmpty
    @Size(min=1,max = 50)
    @Column(name = "ETIQUETA")
    private String etiqueta;
    @NotEmpty
    @Size(min=1,max = 50)
    @Column(name = "LEYENDA")
    private String leyenda;
    @NotNull
    @PositiveOrZero
    @Column(name = "VALOR")
    private BigInteger valor;
    @Basic(optional = false)
    @Column(name = "OPCION_INVALIDA")
    private boolean opcionInvalida;

    //Relaciones
    @JoinColumn(name = "EVALUACION", referencedColumnName = "ID_EVALUACION")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Evaluacion evaluacion;

    public OpcionRespuesta(Long idOpcionRespuesta) {
        this.idOpcionRespuesta=idOpcionRespuesta;
    }

}
