package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.Evaluacion;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 14/03/2020
 * Clase que permite modelar los datos de salida de la entidad Evaluacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EvaluacionOutputDTO {

    private Long idEvaluacion;
    private String nombre;
    private String grado;
    private boolean obsAfirmacion;
    private boolean obsAfirmacionObligatorio;
    private boolean obsCompetencia;
    private boolean obsCompetenciaObligatorio;
    private boolean obsEvaluacion;
    private boolean obsEvaluacionObligatorio;
    private String tipoSemaforo;
    private String tipoRespuesta;
    private String configuracion;
    private boolean noSe;
    private String cabecera;
    private boolean habilitarResultado;
    private BigInteger numeroInvalida;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate fechaInicio;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate fechaCierre;
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private LocalDateTime horaEnvio;
    private String asuntoInvitacion;
    private String asuntoRecordatorio;
    private String comentarioCompetencia;
    private String mensajeFinEvaluacion;

    // Datos de Auditoria
    private boolean activo;

    //Relaciones
    private Map<String, Object> cliente;
//    @ManyToMany(mappedBy = "evaluacionCollection")
//    @ToString.Exclude
//    private Collection<Competencia> competenciaCollection;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * Evaluacion.
     * @param evaluacion Objeto Evaluacion.
     * @return Objeto EvaluacionOutputDTO.
     */
    public static EvaluacionOutputDTO getDTO(Evaluacion evaluacion) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        EvaluacionOutputDTO evaluacionOutputDTO = modelMapper.map(evaluacion, EvaluacionOutputDTO.class);
        if(evaluacion.getCliente() != null){
            Map<String, Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",evaluacion.getCliente().getIdCliente());
            clienteData.put("nit", evaluacion.getCliente().getNit());
            clienteData.put("razonSocial",evaluacion.getCliente().getRazonSocial());
            clienteData.put("Activo",evaluacion.getCliente().isActivo());
            evaluacionOutputDTO.setCliente(clienteData);
        }
        return evaluacionOutputDTO;
    }

}
