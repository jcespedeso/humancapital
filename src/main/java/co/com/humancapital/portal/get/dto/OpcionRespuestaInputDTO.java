package co.com.humancapital.portal.get.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.math.BigInteger;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de entrada de la entidad OpcionRespuesta.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OpcionRespuestaInputDTO {

    private Long idOpcionRespuesta;
    @NotEmpty(message = "El nombre no puede estar vacio.")
    @Size(max = 50, message = "El nombre no puede tener mas de 50 caracteres.")
    private String etiqueta;
    @NotEmpty(message = "La Leyenda no puede estar vacia.")
    @Size(max = 50, message = "La Leyenda no puede tener mas de 50 caracteres.")
    private String leyenda;
    @NotNull(message = "El valor no puede estar vacio.")
    @PositiveOrZero(message = "El valo no puede ser un numero negativo.")
    private BigInteger valor;
    @Basic(optional = false)
    private boolean opcionInvalida;

    //Relaciones
    @Basic(optional = false)
    private Long evaluacion;

}
