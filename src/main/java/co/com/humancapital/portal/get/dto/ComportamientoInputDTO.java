package co.com.humancapital.portal.get.dto;

import lombok.*;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.math.BigInteger;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 15/03/2020
 * Clase que permite modelar los datos de entrada de la entidad Comportamiento.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ComportamientoInputDTO {

    private Long idComportamiento;
    @NotEmpty(message = "La descripción del comportamiento no puede estar vacia.")
    @Size(max = 255, message = "La descripción no puede tener mas de 255 caracteres.")
    private String descripcion;
    @Range(min = 1, message = "El Orden no permite numeros negativos y su valor debe ser un entero mayor que cero.")
    private BigInteger orden;

    // Datos de Auditoria
    private Long usuarioCrea;
    private Long usuarioModifica;
    private Long usuarioInactiva;

    // Relaciones
    @ToString.Exclude
    //Todo validar existencia en servicio
    private Long competencia;

    @ToString.Exclude
    //Todo validar existencia en servicio
    private Long categoria;

}
