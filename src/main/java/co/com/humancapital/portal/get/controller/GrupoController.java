package co.com.humancapital.portal.get.controller;

import co.com.humancapital.portal.controller.AbstractController;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.get.dto.GrupoInputDTO;
import co.com.humancapital.portal.get.service.GrupoService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/03/2020
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Grupo.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/grupoCompetencia")
public class GrupoController extends AbstractController<GrupoInputDTO, GeneralOutput, Long> {

    private final String CREATE = "CATEGORIA_CREAR";
    private final String UPDATE = "CATEGORIA_EDITAR";
    private final String VIEW = "CATEGORIA_VER";
    private final String DELETE = "CATEGORIA_ELIMINAR";

    private final GrupoService grupoService;

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), grupoService.findId(id));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),grupoService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody GrupoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, CREATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        grupoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody GrupoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, UPDATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        grupoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, DELETE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        grupoService.delete(id);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/cliente/{idCliente}")
    public GeneralOutput getGrupoByCliente(@PathVariable Long idCliente,@RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),grupoService.findGrupoByCliente(idCliente));
    }

    @Override
    public GeneralOutput create(GrupoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(GrupoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
