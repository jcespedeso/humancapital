package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.Categoria;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 11/01/2020
 * Clase que permite modelar los datos de salida de la entidad Categoria.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoriaOutputDTO {

    private Long idCategoria;
    private String nombre;
    //Datos Auditoria
    private boolean activo;
    private Map<String, Object> cliente;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Categoria.
     * @param categoria Objeto Categoria.
     * @return Objeto CategoriaOutputDTO
     */
    public static CategoriaOutputDTO getDTO(Categoria categoria){
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        CategoriaOutputDTO categoriaOutputDTO = modelMapper.map(categoria, CategoriaOutputDTO.class);
        if(categoria.getCliente() != null){
            Map<String, Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",categoria.getCliente().getIdCliente());
            clienteData.put("nit", categoria.getCliente().getNit());
            clienteData.put("razonSocial",categoria.getCliente().getRazonSocial());
            clienteData.put("Activo",categoria.getCliente().isActivo());
            categoriaOutputDTO.setCliente(clienteData);
        }
        return categoriaOutputDTO;
    }

}
