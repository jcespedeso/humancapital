package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.ComportamientoInputDTO;
import co.com.humancapital.portal.get.dto.ComportamientoOutputDTO;
import co.com.humancapital.portal.get.entity.Competencia;
import co.com.humancapital.portal.get.entity.Comportamiento;
import co.com.humancapital.portal.get.repository.CategoriaRepository;
import co.com.humancapital.portal.get.repository.CompetenciaRepository;
import co.com.humancapital.portal.get.repository.ComportamientoRepository;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/03/2020
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de las Comportamiento.
 */
@Service
@RequiredArgsConstructor
public class ComportamientoService implements ServiceInterfaceDto<ComportamientoOutputDTO, Long, ComportamientoInputDTO> {

    private static LocalDate fechaActual = LocalDate.now();
    private final ComportamientoRepository comportamientoRepository;
    private final CompetenciaRepository competenciaRepository;
    private final CompetenciaService competenciaService;
    private final CategoriaRepository categoriaRepository;
    private final CategoriaService categoriaService;


    @Override
    public ComportamientoOutputDTO findId(Long key) {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Comportamiento con id "+key+" no existe.");
        }
        return ComportamientoOutputDTO.getDTO(comportamientoRepository.findByIdComportamientoAndActivoTrue(key));
    }

    @Override
    public Page<ComportamientoOutputDTO> findAll(Pageable pageable) {
        if(comportamientoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Comportamientos creados en el sistema.");
        }
        List<ComportamientoOutputDTO> comportamientoOutputDTOS = new ArrayList<>();
        for (Comportamiento comportamiento : comportamientoRepository.findAllByActivoTrue(pageable)) {
            ComportamientoOutputDTO comportamientoOutputDTO = ComportamientoOutputDTO.getDTO(comportamiento);
            comportamientoOutputDTOS.add(comportamientoOutputDTO);
        }
        return new PageImpl<>(comportamientoOutputDTOS);
    }

    @Override
    public ComportamientoOutputDTO create(ComportamientoInputDTO entity) throws Exception{
        if(entity.getCategoria() == null || !categoriaService.existeById(entity.getCategoria())){
            throw new ResourceNotFoundException("La Categoria no existe o no es un valor valido para el sistema.");
        }
        if(entity.getCompetencia() == null || !competenciaService.existeById(entity.getCompetencia())){
            throw new ResourceNotFoundException("La Competencia no existe o no es un valor valido para el sistema.");
        }

        Comportamiento comportamiento = new Comportamiento();
        comportamiento.setDescripcion(entity.getDescripcion().trim());
        comportamiento.setOrden(entity.getOrden());
        comportamiento.setCategoria(categoriaRepository.findByIdCategoriaAndActivoTrue(entity.getCategoria()));
        comportamiento.setCompetencia(competenciaRepository.findByIdCompetenciaAndActivoTrue(entity.getCompetencia()));

        // Datos de Auditoria
        comportamiento.setActivo(true);
        comportamiento.setFechaCreacion(fechaActual);
        comportamiento.setUsuarioCrea(entity.getUsuarioCrea());
        comportamiento.setFechaModifica(fechaActual);
        comportamiento.setUsuarioModifica(entity.getUsuarioCrea());
        return ComportamientoOutputDTO.getDTO(comportamientoRepository.saveAndFlush(comportamiento));
    }

    @Override
    public ComportamientoOutputDTO update(ComportamientoInputDTO entity) throws Exception {
        if (entity.getIdComportamiento() == null || !existeById(entity.getIdComportamiento())) {
            throw new ResourceNotFoundException("El Comportamiento con id "+entity.getIdComportamiento()+" no existe o esta vacio.");
        }
        if(entity.getCategoria() == null || !categoriaService.existeById(entity.getCategoria())){
            throw new ResourceNotFoundException("La Categoria no existe o no es un valor valido para el sistema.");
        }
        if(entity.getCompetencia() == null || !competenciaService.existeById(entity.getCompetencia())){
            throw new ResourceNotFoundException("La Competencia no existe o no es un valor valido para el sistema.");
        }

        Comportamiento comportamiento = comportamientoRepository.findByIdComportamientoAndActivoTrue(entity.getIdComportamiento());
        comportamiento.setDescripcion(entity.getDescripcion().trim());
        comportamiento.setOrden(entity.getOrden());
        comportamiento.setCategoria(categoriaRepository.findByIdCategoriaAndActivoTrue(entity.getCategoria()));
        comportamiento.setCompetencia(competenciaRepository.findByIdCompetenciaAndActivoTrue(entity.getCompetencia()));

        //Datos auditoria
        comportamiento.setFechaModifica(fechaActual);
        comportamiento.setUsuarioModifica(entity.getUsuarioModifica());
        return ComportamientoOutputDTO.getDTO(comportamientoRepository.saveAndFlush(comportamiento));
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Comportamiento con id "+key+" no existe o esta vacio.");
        }
        Comportamiento comportamiento = comportamientoRepository.findByIdComportamientoAndActivoTrue(key);
        comportamiento.setActivo(false);
        comportamiento.setUsuarioInactiva(usuarioInactiva);
        comportamiento.setFechaInactivacion(fechaActual);
        comportamientoRepository.save(comportamiento);
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(comportamientoRepository.findByIdComportamientoAndActivoTrue(key)==null){
            return false;
        }
        return comportamientoRepository.existsById(key);
    }

    /**
     * Metodo que permite obtener los comportamientos de una competencia especifica.
     * @param idCompetencia Id de la Competencia a validar.
     * @return Page de objeto ComportamientoOutputDTO con los comportamientos asociados a la competencia.
     */
    public Page<ComportamientoOutputDTO> comportamientoXCompetencia(Long idCompetencia, Pageable pageable) throws Exception{
        if(idCompetencia == null  || !competenciaService.existeById(idCompetencia)) {
            throw new ResourceNotFoundException("La Competencia con id+"+idCompetencia+" no existe o esta vacia.");
        }
        Competencia competencia = competenciaRepository.findByIdCompetenciaAndActivoTrue(idCompetencia);
        if(comportamientoRepository.findAllByCompetenciaAndActivoTrue(competencia,pageable).isEmpty()) {
            throw new ResourceNotFoundException("La Competencia "+competencia.getNombre()+" no tiene comportamientos asociados.");
        }
        List<ComportamientoOutputDTO> comportamientoOutputDTOS = new ArrayList<>();
        for (Comportamiento comportamiento : comportamientoRepository.findAllByCompetenciaAndActivoTrue(competencia,pageable)) {
            ComportamientoOutputDTO comportamientoOutputDTO = ComportamientoOutputDTO.getDTO(comportamiento);
            comportamientoOutputDTOS.add(comportamientoOutputDTO);
        }
        return new PageImpl<>(comportamientoOutputDTOS);
    }

    @Override
    public ComportamientoOutputDTO create(ComportamientoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public ComportamientoOutputDTO update(ComportamientoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

}