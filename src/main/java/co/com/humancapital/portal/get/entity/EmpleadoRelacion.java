
package co.com.humancapital.portal.get.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"EMPLEADO_RELACION\"")
@Data
@NoArgsConstructor
public class EmpleadoRelacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_RELACION")
    private Long idRelacion;
    @NotEmpty
    @Size(min = 1,max = 30)
    @Column(name = "RELACION")
    private String relacion;
    @NotNull
    @Column(name = "EVALUADOR")
    private Long evaluador;
    @Column(name = "ACTIVO")
    private boolean activo;

    //Relaciones
    @JoinColumn(name = "EVALUACION_EMPLEADO", referencedColumnName = "ID_EVA_EMPLEADO")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private EvaluacionEmpleado evaluacionEmpleado;

    public EmpleadoRelacion(Long idRelacion) {
        this.idRelacion=idRelacion;
    }
}
