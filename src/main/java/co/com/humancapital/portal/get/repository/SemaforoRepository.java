package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.Semaforo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020
 */
@Repository
public interface SemaforoRepository extends JpaRepository<Semaforo, Long> {

    Semaforo findByIdSemaforo(Long idSemaforo);
    Page<Semaforo> findAll(Pageable pageable);

    @Query("select sem from Semaforo sem " +
            "left join Evaluacion eva on eva.idEvaluacion = sem.evaluacion " +
            "where eva.idEvaluacion=:idEvaluacion")
    Page<Semaforo> findAllByEvaluacion(@Param("idEvaluacion") Long idEvaluacion, Pageable pageable);

}
