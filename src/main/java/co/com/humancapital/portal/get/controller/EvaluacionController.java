package co.com.humancapital.portal.get.controller;

import co.com.humancapital.portal.controller.AbstractController;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.get.dto.EvaCompAsignarInputDTO;
import co.com.humancapital.portal.get.dto.EvaEmpleadoAsignarInputDTO;
import co.com.humancapital.portal.get.dto.EvaluacionInputDTO;
import co.com.humancapital.portal.get.service.EvaluacionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/03/2020
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Evaluacion.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/evaluacion")
public class EvaluacionController extends AbstractController<EvaluacionInputDTO, GeneralOutput, Long> {

    private final String CREATE = "EVALUACION_CREAR";
    private final String UPDATE = "EVALUACION_EDITAR";
    private final String VIEW = "EVALUACION_VER";
    private final String DELETE = "EVALUACION_ELIMINAR";

    private final EvaluacionService evaluacionService;

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), evaluacionService.findId(id));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), evaluacionService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody EvaluacionInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, CREATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        evaluacionService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody EvaluacionInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, UPDATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        evaluacionService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, DELETE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        evaluacionService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @PutMapping("/asigCompetencia")
    public GeneralOutput asignarCompetencia(@Valid @RequestBody EvaCompAsignarInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        evaluacionService.asignarCompetencia(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(), "Asignación de competencias realizadas satisfactoriamente...");
    }

    @PutMapping("/asigEmpleado")
    public GeneralOutput asignarEmpleado(@Valid @RequestBody EvaEmpleadoAsignarInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        evaluacionService.asignarEmpleado(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(), "Asignación de competencias realizadas satisfactoriamente...");
    }

    @GetMapping("/cliente/{idCliente}")
    public GeneralOutput getEvaluacionByCliente(@PathVariable Long idCliente,@RequestHeader("Authorization") String token, Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),evaluacionService.findEvaluacionByCliente(idCliente, pageable));
    }

    @GetMapping("/evaEmpleado/{idEvaluacion}")
    public GeneralOutput getEmpleadosByEvaluacion(@PathVariable Long idEvaluacion,@RequestHeader("Authorization") String token, Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),evaluacionService.obtenerEmpleadoByEvaluacion(idEvaluacion, pageable));
    }

    @GetMapping("/empleado/{idEmpleado}")
    public GeneralOutput getEvaluacionByEmpleado(@PathVariable Long idEmpleado,@RequestHeader("Authorization") String token, Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),evaluacionService.obtenerEvaluacionesByEmpleado(idEmpleado, pageable));
    }

    @GetMapping("/evaEvaluador/{idEvaluacion}")
    public GeneralOutput getEvaluadoresByEvaluacion(@PathVariable Long idEvaluacion,@RequestHeader("Authorization") String token, Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),evaluacionService.obtenerEvaluadorByEvaluacion(idEvaluacion, pageable));
    }

    @GetMapping("/evaluador/{idEvaluador}")
    public GeneralOutput getEvaluacionByEvaluador(@PathVariable Long idEvaluador,@RequestHeader("Authorization") String token, Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),evaluacionService.obtenerEvaluacionesByEvaluador(idEvaluador, pageable));
    }

    @Override
    public GeneralOutput create(EvaluacionInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(EvaluacionInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
