package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.RespuestaComportamiento;
import lombok.*;
import org.modelmapper.ModelMapper;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 20/03/2020
 * Clase que permite modelar los datos de salida de la entidad RespuestaComportamiento.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RespComportamientoOutputDTO {

    private Long idRespuesta;
    private Long evaluador;
    private Long comportamiento;
    private String respuesta;
    private String obsAfirmacion;

    //Relaciones
    @ToString.Exclude
    private Long evaluacionEmpleado;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * RespuestaComportamiento.
     * @param respComportamiento Objeto obsEvaluacion.
     * @return Objeto ObsEvaluacionOutputDTO
     */
    public static RespComportamientoOutputDTO getDTO(RespuestaComportamiento respComportamiento) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        RespComportamientoOutputDTO respComportamientoOutputDTO = modelMapper.map(respComportamiento, RespComportamientoOutputDTO.class);
        respComportamientoOutputDTO.setEvaluacionEmpleado(respComportamiento.getEvaluacionEmpleado().getIdEvaEmpleado());
        return respComportamientoOutputDTO;
    }

}
