package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.EvaluationNotification;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EvaluationNotificationRepository extends PagingAndSortingRepository<EvaluationNotification, Long> {
    List<EvaluationNotification> findAllByNotificationDateBetween(
            LocalDateTime notificationDateStart,
            LocalDateTime notificationDateEnd);
}
