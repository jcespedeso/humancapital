package co.com.humancapital.portal.get.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 18/03/2020
 * Clase que permite modelar los datos de entrada de la entidad Semaforo.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SemaforoInputDTO {

    private Long idSemaforo;
    @NotEmpty(message = "El rango de color no puede estar vacio.")
    @Size(max = 50, message = "El color no puede tener mas de 50 caracteres.")
    private String color;
    @NotEmpty(message = "La etiqueta no puede estar vacia.")
    @Size(max = 50, message = "La etiqueta no puede tener mas de 50 caracteres.")
    private String etiqueta;
    @NotNull(message = "El porcentaje asignado al color no puede estar vacio.")
    @PositiveOrZero(message = "El valor porcentual no puede ser un valor negativo.")
    @Range(max = 100, message = "El Valor porcentual no puede ser menor a cero, ni mayor a 100.")
    private Integer porcentaje;

    //Relaciones
    private Long evaluacion;

}
