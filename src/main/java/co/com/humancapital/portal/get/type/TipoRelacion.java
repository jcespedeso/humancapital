package co.com.humancapital.portal.get.type;

public enum TipoRelacion {
    AUTOEVALUACION,
    COLABORADOR,
    JEFE,
    PAR,
    CLIENTE_INTERNO,
    CLIENTE_EXTERNO,
    SUPERVISOR,
    MEDICION_MITAD_CICLO,
    VP,
    ENCUESTAS;

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del enum.
     * @param key Codigo del atributo a validar.
     * @return {@code true} si el enum contiene el valor de la variable de lo contrario {@code false}.
     */
    public static Boolean existeKey(String key){
        for(TipoRelacion tipoRelacion: TipoRelacion.values()){
            if(tipoRelacion.name().equals(key.replaceAll("^\\s*","").toUpperCase())){
                return true;
            }
        }
        return false;
    }
}