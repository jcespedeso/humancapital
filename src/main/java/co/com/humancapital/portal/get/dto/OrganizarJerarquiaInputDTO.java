package co.com.humancapital.portal.get.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 29/03/2020
 * Clase que permite modelar los datos de entrada para la actualización gerarquica de los cargos.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizarJerarquiaInputDTO {
    @NotNull(message = "El idEntity no puede estar vacio.")
    private Long idEntiy;
    @NotNull(message = "El id Padre no puede estar vacio.")
    private Long idEntityPadre;
    @PositiveOrZero(message = "El valor de Peso y Orden no puede ser un numero negativo.")
    private Integer pesoOrden;
    private Long usuarioModifica;
}
