package co.com.humancapital.portal.get.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de entrada de la entidad EmpleadoRelacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmpleadoRelacionInputDTO {

    private Long idRelacion;
    @NotEmpty(message = "La Relacion no puede estar vacio.")
    @Size(max = 30, message = "La relación no puede tener mas de 30 caracteres.")
    private String relacion;
    @NotNull(message = "El evaluador no puede estar vacio.")
    private Long evaluador;
    private boolean activo;

    //Relaciones
    @NotNull(message = "La Evaluacion del Empleado no puede estar vacio.")
    private Long evaluacionEmpleado;

}
