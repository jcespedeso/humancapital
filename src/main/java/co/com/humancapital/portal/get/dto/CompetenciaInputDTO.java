package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Departamento;
import lombok.*;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.util.Collection;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 14/03/2020
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompetenciaInputDTO {

    private Long idCompetencia;
    @NotEmpty(message = "El nombre de la competencia no puede estar vacio.")
    @Size(max = 150, message = "El nombre de la competencia no puede tener mas de 150 caracteres.")
    private String nombre;
    @NotEmpty(message = "La descripción de la competencia no puede estar vacia.")
    @Size(max = 255, message = "La descripción de la competencia no puede tener mas de 225 caracteres.")
    private String descripcion;
    @NotEmpty(message = "El tipo de competencia no puede estar vacio.")
    @Size(max = 30, message = "El tipo de competencia no puede tener mas de 30 caracteres.")
    private String tipo;
    @Range(min = 1, message = "El Orden no permite numeros negativos y su valor debe ser un entero mayor que cero.")
    private BigInteger orden;
    @Range(min = 1, max = 100, message = "El porcentaje minimo experado debe ser un valor entero mayor que cero y menor que 100.")
    private BigInteger minimoEsperado;

    //Auditoria
    private Long usuarioCrea;
    private Long usuarioModifica;
    private Long usuarioInactiva;

    //Relaciones
    @NotNull(message = "El cliente al cual pertenece la competencia, no puede estar vacio.")
    private Long cliente;
    @NotNull(message = "El grupo al cual pertenece la competencia, no puede estar vacio.")
    private Long grupo;

    @ToString.Exclude
    private Collection<Departamento> departamentoCollection;
    @ToString.Exclude
    private Collection<Cargo> cargoCollection;

    public CompetenciaInputDTO(Collection<Cargo> cargoCollection) {
        this.cargoCollection = cargoCollection;
    }
}
