package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.RespuestaComportamiento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Alejandro Herrera Montilla
 * @date 20/03/2020
 */
@Repository
public interface RespComportamientoRepository extends JpaRepository<RespuestaComportamiento, Long> {

    RespuestaComportamiento findByIdRespuesta(Long idRespuesta);

    @Query("select resp from RespuestaComportamiento resp " +
            "left join EvaluacionEmpleado evEmp on evEmp.idEvaEmpleado = resp.evaluacionEmpleado " +
            "left join Empleado emp on emp.idEmpleado = evEmp.empleado " +
            "where emp.activo=true and emp.idEmpleado=:idEmpleado")
    Page<RespuestaComportamiento> findAllByEmpleadoAndActivoTrue(@Param("idEmpleado") Long idEmpleado, Pageable pageable);

}
