package co.com.humancapital.portal.get.controller;

import co.com.humancapital.portal.controller.AbstractController;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.get.dto.CompetenciaInputDTO;
import co.com.humancapital.portal.get.service.CompetenciaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/01/2020 Clase para la implementación de los diferentes microservicios provistos para la entidad
 * Competencia.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/competencia")
public class CompetenciaController extends AbstractController<CompetenciaInputDTO, GeneralOutput, Long> {
    
    private final String CREATE = "COMPETENCIA_CREAR";
    private final String UPDATE = "COMPETENCIA_EDITAR";
    private final String VIEW = "COMPETENCIA_VER";
    private final String VIEW_CLIENTE = "COMPETENCIA_VER_CLIENTE";
    private final String DELETE = "COMPETENCIA_ELIMINAR";
    
    private final CompetenciaService competenciaService;
    
    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        System.out.println("Controller id = " + id);
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), competenciaService.findId(id));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),competenciaService.findAll(pageable));
    }
    
    @Override
    public GeneralOutput create(@Valid @RequestBody CompetenciaInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, CREATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        competenciaService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(), HttpStatus.CREATED.getReasonPhrase(), "Registro Creado Exitosamente...");
    }
    
    @Override
    public GeneralOutput update(@Valid @RequestBody CompetenciaInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, UPDATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        competenciaService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(), HttpStatus.ACCEPTED.getReasonPhrase(), "Registro Actualizado Exitosamente...");
    }
    
    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, DELETE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        competenciaService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(), HttpStatus.ACCEPTED.getReasonPhrase(), "Registro Eliminado Exitosamente...");
    }
    
    @GetMapping("/listCompetCliente/{idCliente}")
    public GeneralOutput getAll(@PathVariable Long idCliente, @RequestHeader("Authorization") String token,
            @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW_CLIENTE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), competenciaService.findAllByCliente(idCliente, pageable));
    }
    
    @Override
    public GeneralOutput create(CompetenciaInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
    
    @Override
    public GeneralOutput update(CompetenciaInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
