
package co.com.humancapital.portal.get.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"SEMAFORO\"")
@Data
@NoArgsConstructor
public class Semaforo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SEMAFORO")
    private Long idSemaforo;
    @NotEmpty
    @Size(min = 1,max = 50)
    @Column(name = "COLOR")
    private String color;
    @NotEmpty
    @Size(min = 1,max = 50)
    @Column(name = "ETIQUETA")
    private String etiqueta;
    @NotNull
    @PositiveOrZero
    @Range(max = 100)
    @Column(name = "PORCENTAJE")
    private Integer porcentaje;

    //Relaciones
    @JoinColumn(name = "EVALUACION", referencedColumnName = "ID_EVALUACION")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Evaluacion evaluacion;

    public Semaforo(Long idSemaforo) {
        this.idSemaforo=idSemaforo;
    }
}
