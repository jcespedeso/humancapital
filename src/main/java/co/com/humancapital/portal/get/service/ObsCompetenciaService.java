package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.ObsCompetenciaInputDTO;
import co.com.humancapital.portal.get.dto.ObsCompetenciaOutputDTO;
import co.com.humancapital.portal.get.entity.EvaluacionEmpleado;
import co.com.humancapital.portal.get.entity.ObsCompetencia;
import co.com.humancapital.portal.get.repository.EvaluacionEmpleadoRepository;
import co.com.humancapital.portal.get.repository.EvaluacionRepository;
import co.com.humancapital.portal.get.repository.ObsCompetenciaRepository;
import co.com.humancapital.portal.get.type.ConfigPregunta;
import co.com.humancapital.portal.service.EmpleadoService;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ObsCompetenciaService implements ServiceInterfaceDto<ObsCompetenciaOutputDTO, Long, ObsCompetenciaInputDTO> {

    private final ObsCompetenciaRepository obsCompetenciaRepository;
    private final EmpleadoService empleadoService;
    private final CompetenciaService competenciaService;
    private final EvaluacionEmpleadoService evaluacionEmpleadoService;
    private final EvaluacionEmpleadoRepository evaluacionEmpleadoRepository;
    private final EvaluacionRepository evaluacionRepository;

    @Override
    public ObsCompetenciaOutputDTO findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Observación de la Competencia con id "+key+" no existe.");
        }
        return ObsCompetenciaOutputDTO.getDTO(obsCompetenciaRepository.findByIdObsCompetencia(key));
    }

    @Override
    public ObsCompetenciaOutputDTO create(ObsCompetenciaInputDTO entity) throws Exception {

        if (entity.getEvaluador()==null || !empleadoService.existeById(entity.getEvaluador())) {
            throw new ResourceNotFoundException("El Evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if(entity.getCompetencia() == null || !competenciaService.existeById(entity.getCompetencia())){
            throw new ResourceNotFoundException("La Competencia con id "+ entity.getCompetencia() +" no existe o no se envio el dato.");
        }
        if(entity.getEvaluacionEmpleado() == null || !evaluacionEmpleadoService.existeById(entity.getEvaluacionEmpleado())){
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+ entity.getEvaluacionEmpleado() +" no existe o no se envio el dato.");
        }
        EvaluacionEmpleado evaluacionEmpleado = evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado());
        if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(evaluacionEmpleado.getEvaluacion().getIdEvaluacion()).isObsCompetencia()){
            throw new ResourceNotFoundException("La Evaluación no esta configurada para permitir observaciones a las Competencia evaluadas.");
        }
        ObsCompetencia obsCompetencia = new ObsCompetencia();
        obsCompetencia.setEvaluacionEmpleado(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado()));
        obsCompetencia.setEvaluador(entity.getEvaluador());
        obsCompetencia.setCompetencia(entity.getCompetencia());
        obsCompetencia.setObsCompetencia(entity.getObsCompetencia().trim());
        return ObsCompetenciaOutputDTO.getDTO(obsCompetenciaRepository.saveAndFlush(obsCompetencia));
    }

    @Override
    public ObsCompetenciaOutputDTO update(ObsCompetenciaInputDTO entity) throws Exception {
        if (entity.getIdObsCompetencia()==null || !empleadoService.existeById(entity.getIdObsCompetencia())) {
            throw new ResourceNotFoundException("La Observación por competencia con id "+ entity.getIdObsCompetencia() +" no existe o no se envio el dato.");
        }
        if (entity.getEvaluador()==null || !empleadoService.existeById(entity.getEvaluador())) {
            throw new ResourceNotFoundException("El Evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if(entity.getCompetencia() == null || !competenciaService.existeById(entity.getCompetencia())){
            throw new ResourceNotFoundException("La Competencia con id "+ entity.getCompetencia() +" no existe o no se envio el dato.");
        }
        if(entity.getEvaluacionEmpleado() == null || !evaluacionEmpleadoService.existeById(entity.getEvaluacionEmpleado())){
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+ entity.getEvaluacionEmpleado() +" no existe o no se envio el dato.");
        }
        EvaluacionEmpleado evaluacionEmpleado = evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado());
        if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(evaluacionEmpleado.getEvaluacion().getIdEvaluacion()).isObsCompetencia()){
            throw new ResourceNotFoundException("La Evaluación no esta configurada para permitir observaciones a las Competencia evaluadas.");
        }
        ObsCompetencia obsCompetencia = obsCompetenciaRepository.findByIdObsCompetencia(entity.getIdObsCompetencia());
        obsCompetencia.setEvaluacionEmpleado(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado()));
        obsCompetencia.setCompetencia(entity.getCompetencia());
        obsCompetencia.setEvaluador(entity.getEvaluador());
        obsCompetencia.setObsCompetencia(entity.getObsCompetencia().trim());
        return ObsCompetenciaOutputDTO.getDTO(obsCompetenciaRepository.saveAndFlush(obsCompetencia));
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que elimina la observación de una competencia asociada a la evaluación de un empleado.
     * @param key Id de la Observación.
     */
    public void delete(Long key) throws Exception {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La Observación por competencia con id "+key+" no existe o esta vacio.");
        }
        obsCompetenciaRepository.delete(obsCompetenciaRepository.findByIdObsCompetencia(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(obsCompetenciaRepository.findByIdObsCompetencia(key)==null){
            return false;
        }
        return obsCompetenciaRepository.existsById(key);
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite obtener las Observaciones realizadas a las diferentes competencias dentro de la
     * Evaluación de un Empleado especifico.
     * @param idEmpleado Id el empleado a validar.
     * @return Lista de ObsCompetenciaOutputDTO.
     */
    public Page<ObsCompetenciaOutputDTO> findAllEmpleado(Long idEmpleado, Pageable pageable) {
        if(idEmpleado == null || !empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con "+ idEmpleado +" no existe o no se envio en dato.");
        }
        List<ObsCompetenciaOutputDTO> obsCompetenciaOutputDTOS = new ArrayList<>();
        for (ObsCompetencia obsCompetencia : obsCompetenciaRepository.findAllByEmpleadoAndActivoTrue(idEmpleado, pageable)) {
            ObsCompetenciaOutputDTO obsCompetenciaOutputDTO = ObsCompetenciaOutputDTO.getDTO(obsCompetencia);
            obsCompetenciaOutputDTOS.add(obsCompetenciaOutputDTO);
        }
        return new PageImpl<>(obsCompetenciaOutputDTOS);
    }

    @Override
    public Page<ObsCompetenciaOutputDTO> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public ObsCompetenciaOutputDTO update(ObsCompetenciaInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public ObsCompetenciaOutputDTO create(ObsCompetenciaInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}
