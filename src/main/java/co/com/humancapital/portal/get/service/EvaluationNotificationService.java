package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.EvaluationNotificationDTO;
import co.com.humancapital.portal.get.entity.Evaluacion;
import co.com.humancapital.portal.get.entity.EvaluationNotification;
import co.com.humancapital.portal.get.repository.EvaluationNotificationRepository;
import co.com.humancapital.portal.service.EmailNotificationService;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static co.com.humancapital.portal.util.ObjectMapperUtils.map;
import static org.apache.commons.lang3.StringUtils.EMPTY;

@Service
@RequiredArgsConstructor
@Slf4j
public class EvaluationNotificationService {
    
    private static final String EMPLOYEE_NAME_TAG = "${EMPLOYEE_NAME}";
    private static final String EMAIL_ADDRESS_DELIMITER = ";";
    private final EmailNotificationService emailNotificationService;
    private final EvaluationNotificationRepository notificationRepository;
    private final EvaluacionService evaluacionService;
    
    public EvaluationNotificationDTO saveUpdate(EvaluationNotificationDTO notificationDTO) {
        log.info("::: Persistiendo EvaluationNotification");
        EvaluationNotification evaluationNotification = map(notificationDTO, EvaluationNotification.class);
        evaluationNotification = notificationRepository.save(evaluationNotification);
        notificationDTO = map(evaluationNotification, EvaluationNotificationDTO.class);
        notificationDTO.setEvaluacionId(evaluationNotification.getEvaluacion().getIdEvaluacion());
        log.info("::: EvaluationNotification persistida con ID[{}]", notificationDTO.getId());
        return notificationDTO;
    }
    
    public void delete(Long notificationId) {
        log.info("::: Eliminando EvaluationNotification {}", notificationId);
        notificationRepository.deleteById(notificationId);
        log.info("::: EvaluationNotification eliminada {}", notificationId);
    }
    
    public EvaluationNotification findById(Long notificationId) {
        return notificationRepository.findById(notificationId)
                .orElseThrow(() -> new ResourceNotFoundException());
    }
    
    public void sendTestNotification(Long notificationId) {
        log.info("::: Ejecutando envio de notificaciones por email");
        EvaluationNotification config = findById(notificationId);
        String htmlMessage = StringUtils.replace(config.getHtmlMessage(), EMPLOYEE_NAME_TAG, config.getTestToName());
        List<String> emails = getEmailAddressesAsList(config.getTestToAddress());
        emailNotificationService.sendHtmlMessage(emails, config.getSubject(), htmlMessage,
                config.getTestFromAddress(), config.getTestFromName());
        log.info("::: Notificaciones en proceso de envio...");
    }
    
    public void sendRealNotification(Long notificationId) {
        EvaluationNotification config = findById(notificationId);
        sendRealNotification(config);
    }
    
    private void sendRealNotification(EvaluationNotification config) {
        List<Empleado> evaluadores = new ArrayList<>();
        evaluadores.forEach(evaluador -> {
            String htmlMessage = StringUtils.replace(config.getHtmlMessage(), EMPLOYEE_NAME_TAG,
                    evaluador.getNombre() + EMPTY + evaluador.getApellido());
            emailNotificationService.sendHtmlMessage(evaluador.getEmail(), config.getSubject(), htmlMessage,
                    config.getFromAddress(), config.getFromName());
        });
    }
    
    private List<String> getEmailAddressesAsList(String emailAddresses) {
        if (StringUtils.contains(emailAddresses, EMAIL_ADDRESS_DELIMITER)) {
            return Arrays.stream(StringUtils.split(emailAddresses, EMAIL_ADDRESS_DELIMITER))
                    .collect(Collectors.toList());
        } else {
            return Collections.singletonList(emailAddresses);
        }
    }
    
    @Scheduled(cron = "${human.mail.notifications.job.cron}")
    public void sendScheduledNotifications() {
        log.info("::: Ejecutando envio de notificaciones por email");
        LocalDateTime startDate = LocalDateTime.now();
        LocalDateTime endDate = startDate.plus(15, ChronoUnit.MINUTES);
        notificationRepository.findAllByNotificationDateBetween(startDate, endDate)
                .forEach(notificationConfig -> sendRealNotification(notificationConfig));
        log.info("::: Notificaciones en proceso de envio...");
    }
    
    private Evaluacion findEvaluacion(Long evaluacionId) {
        return evaluacionService.findModel(evaluacionId)
                .orElseThrow(
                        () -> new ResourceNotFoundException("::: Evaluacion sno encontrada con ID " + evaluacionId));
    }
}
