
package co.com.humancapital.portal.get.entity;

import co.com.humancapital.portal.entity.Empleado;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"EVALUACION_EMPLEADO\"")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EvaluacionEmpleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_EVA_EMPLEADO")
    private Long idEvaEmpleado;
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private boolean activo;

    //Relaciones
    @JoinColumn(name = "EMPLEADO", referencedColumnName = "ID_EMPLEADO")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Empleado empleado;

    @JoinColumn(name = "EVALUACION", referencedColumnName = "ID_EVALUACION")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Evaluacion evaluacion;

    //Relaciones Omitidas
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionEmpleado")
//    private Collection<RespuestaComportamiento> respuestaComportamientoCollection;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionEmpleado")
//    private Collection<ObsCompetencia> obsCompetenciaCollection;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionEmpleado")
//    private Collection<OtraRelacion> otraRelacionCollection;

    public EvaluacionEmpleado(Long idEvaEmpleado) {
        this.idEvaEmpleado=idEvaEmpleado;
    }
}
