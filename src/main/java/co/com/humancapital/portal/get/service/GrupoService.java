package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.CategoriaOutputDTO;
import co.com.humancapital.portal.get.dto.GrupoInputDTO;
import co.com.humancapital.portal.get.dto.GrupoOutputDTO;
import co.com.humancapital.portal.get.entity.Categoria;
import co.com.humancapital.portal.get.entity.Grupo;
import co.com.humancapital.portal.get.repository.GrupoRepository;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/03/2020
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de las Grupo.
 */
@Service
@RequiredArgsConstructor
public class GrupoService implements ServiceInterfaceDto<GrupoOutputDTO, Long, GrupoInputDTO> {

    private final GrupoRepository grupoRepository;
    private final ClienteRepository clienteRepository;

    @Override
    public GrupoOutputDTO findId(Long key) {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Grupo con id "+key+" no existe.");
        }
        return GrupoOutputDTO.getDTO(grupoRepository.findByIdGrupoAndActivoTrue(key));
    }

    @Override
    public Page<GrupoOutputDTO> findAll(Pageable pageable) {
        if(grupoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Grupos creados en el sistema.");
        }
        List<GrupoOutputDTO> grupoOutputDTOS = new ArrayList<>();
        for (Grupo grupo : grupoRepository.findAllByActivoTrue(pageable)) {
            GrupoOutputDTO grupoOutputDTO = GrupoOutputDTO.getDTO(grupo);
            grupoOutputDTOS.add(grupoOutputDTO);
        }
        return new PageImpl<>(grupoOutputDTOS);
    }

    @Override
    public GrupoOutputDTO create(GrupoInputDTO entity) {
        if (existeByNombre(entity.getNombre())) {
            throw new ResourceNotFoundException("El nombre del Grupo a crear ya existe.");
        }
        Grupo grupo = new Grupo();
        grupo.setNombre(entity.getNombre().trim().toUpperCase());
        grupo.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        grupo.setActivo(true);
        return GrupoOutputDTO.getDTO(grupoRepository.saveAndFlush(grupo));
    }

    @Override
    public GrupoOutputDTO update(GrupoInputDTO entity) {
        if (entity.getIdGrupo() == null || !existeById(entity.getIdGrupo())) {
            throw new ResourceNotFoundException("El Grupo con id "+entity.getIdGrupo()+" no existe o esta vacio.");
        }
        Grupo grupo = grupoRepository.findByIdGrupoAndActivoTrue(entity.getIdGrupo());
        grupo.setNombre(entity.getNombre().trim().toUpperCase());
        grupo.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        return GrupoOutputDTO.getDTO(grupoRepository.saveAndFlush(grupo));
    }

    public void delete(Long key) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Grupo con id "+key+" no existe o esta vacio.");
        }
        Grupo grupo = grupoRepository.findByIdGrupoAndActivoTrue(key);
        grupo.setActivo(false);
        grupoRepository.save(grupo);
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(grupoRepository.findByIdGrupoAndActivoTrue(key)==null){
            return false;
        }
        return grupoRepository.existsById(key);
    }

    /**
     * Metodo que permite evaluar si un Grupo ya existe a partir de su nombre.
     * @param nombre Nombre del Grupo a validar.
     * @return {@code true} si existe algun registro con el nombre suministrado de lo contrario {@code false}
     */
    public Boolean existeByNombre(String nombre) {
        if(nombre.isEmpty()){
            throw new ResourceNotFoundException("El Nombre a validar, no puede estar vacio.");
        }
        if(grupoRepository.findByNombreAndActivoTrue(nombre.toUpperCase())==null){
            return false;
        }
        return true;
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 02/04/2020
     * Metodo que permite obtener los Grupos de un Cliente Especifico.
     * @param idCliente Id del Cliente a consultar.
     * @return Listado de GrupoOutputDTO con la data de Grupos.
     */
    public List<GrupoOutputDTO> findGrupoByCliente(Long idCliente){
        if (idCliente == null || !clienteRepository.existsByIdClienteAndActivoTrue(idCliente)) {
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        List<GrupoOutputDTO> grupoOutputDTOS = new ArrayList<>();
        for (Grupo grupo:grupoRepository.findAllByClienteAndActivoTrueOrderByNombre(cliente)) {
            grupoOutputDTOS.add(GrupoOutputDTO.getDTO(grupo));
        }
        return grupoOutputDTOS;
    }

    @Override
    public GrupoOutputDTO create(GrupoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public GrupoOutputDTO update(GrupoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}