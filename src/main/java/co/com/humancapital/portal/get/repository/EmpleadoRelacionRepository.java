package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.EmpleadoRelacion;
import co.com.humancapital.portal.get.entity.Evaluacion;
import co.com.humancapital.portal.get.entity.EvaluacionEmpleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020
 */
@Repository
public interface EmpleadoRelacionRepository extends JpaRepository<EmpleadoRelacion, Long> {

    EmpleadoRelacion findByIdRelacionAndActivoTrue(Long idRelacion);

    @Query(value = "select empRel from EmpleadoRelacion empRel " +
            "left join EvaluacionEmpleado evEmp on evEmp.idEvaEmpleado = empRel.evaluacionEmpleado " +
            "left join Empleado emp on emp.idEmpleado = evEmp.empleado " +
            "where empRel.activo=true and evEmp.activo=true and emp.activo=true and emp.idEmpleado=:idEmpleado")
    Page<EmpleadoRelacion> findAllByEmpleadoAndActivoTrue(@Param("idEmpleado") Long idEmpleado, Pageable pageable);

}
