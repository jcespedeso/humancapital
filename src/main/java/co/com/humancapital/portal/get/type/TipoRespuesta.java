package co.com.humancapital.portal.get.type;

public enum TipoRespuesta {
    ESCALA,
    CALIFICACION;

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del enum.
     * @param key Codigo del atributo a validar.
     * @return {@code true} si el enum contiene el valor de la variable de lo contrario {@code false}.
     */
    public static Boolean existeKey(String key){
        for(TipoRespuesta tipoRespuesta: TipoRespuesta.values()){
            if(tipoRespuesta.name().equals(key.replaceAll("^\\s*","").toUpperCase())){
                return true;
            }
        }
        return false;
    }
}
