package co.com.humancapital.portal.get.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de entrada de la entidad ObsEvaluacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ObsEvaluacionInputDTO {

    private Long idObsEvaluacion;
    @NotNull(message = "El id del Evaluador no puede estar vacio.")
    private Long evaluador;
    @NotEmpty(message = "La Observación de la Evaluación no puede estar vacia.")
    @Size(max =255, message = "La Observación no puede tener mas de 255 caracteres.")
    private String obsEvaluacion;

    //Relaciones
    private Long evaluacionEmpleado;

}
