package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.OpcionRespuesta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import java.math.BigInteger;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de entrada de la entidad OpcionRespuesta.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OpcionRespuestaOutputDTO {

    private Long idOpcionRespuesta;
    private String etiqueta;
    private String leyenda;
    private BigInteger valor;
    private boolean opcionInvalida;

    //Relaciones
    private Long evaluacion;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * OpcionRespuesta.
     * @param opcionRespuesta Objeto OpcionRespuesta.
     * @return Objeto OpcionRespuestaOutputDTO.
     */
    public static OpcionRespuestaOutputDTO getDTO(OpcionRespuesta opcionRespuesta) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        OpcionRespuestaOutputDTO opcionRespuestaOutputDTO = modelMapper.map(opcionRespuesta, OpcionRespuestaOutputDTO.class);
        opcionRespuestaOutputDTO.setEvaluacion(opcionRespuesta.getEvaluacion().getIdEvaluacion());
        return opcionRespuestaOutputDTO;
    }

}
