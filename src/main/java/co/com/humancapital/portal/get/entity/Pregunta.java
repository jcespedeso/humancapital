
package co.com.humancapital.portal.get.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"PREGUNTA\"")
@NoArgsConstructor
@Data
public class Pregunta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PREGUNTA")
    private Long idPregunta;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION_PREGUNTA")
    private String descripcionPregunta;
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private boolean activo;

    //Relaciones
    @JoinColumn(name = "ENCUESTA", referencedColumnName = "ID_ENCUESTA")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Encuesta encuesta;

    //Relaciones Omitidas
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pregunta")
//    private Collection<Respuesta> respuestaCollection;

}
