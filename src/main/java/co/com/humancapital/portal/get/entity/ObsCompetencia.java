
package co.com.humancapital.portal.get.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"OBS_COMPETENCIA\"")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObsCompetencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_OBS_COMPETENCIA")
    private Long idObsCompetencia;
    @NotNull
    @Column(name = "EVALUADOR")
    private Long evaluador;
    @NotNull
    @Column(name = "COMPETENCIA")
    private Long competencia;
    @NotNull
    @Size(min =1 ,max =255)
    @Column(name = "OBS_COMPETENCIA")
    private String obsCompetencia;

    //Relaciones
    @JoinColumn(name = "EVALUACION_EMPLEADO", referencedColumnName = "ID_EVA_EMPLEADO")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private EvaluacionEmpleado evaluacionEmpleado;

    public ObsCompetencia(Long idObsCompetencia) {
        this.idObsCompetencia=idObsCompetencia;
    }
}
