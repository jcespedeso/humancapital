package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.EmpleadoXDatoAdicional;
import co.com.humancapital.portal.util.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class EmpEvaluadorOutputDTO {

    private Long idEmpleado;
    private String numeroDocumento;
    private String nombreCompleto;
    private String email;
    private String telefono;
    //Datos de Auditoria
    private boolean activo;

    //    Relaciones
    private Map<String, Object> cargo;

    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * Empleado que hagan sus veces de Evaluadores.
     * @param empleado Objeto Empleado.
     * @return Objeto EmpEvaluadorOutputDTO
     */
    public static EmpEvaluadorOutputDTO getDTO(Empleado empleado) {
        EmpEvaluadorOutputDTO empleadoOutputDTO = new EmpEvaluadorOutputDTO();
        empleadoOutputDTO.setIdEmpleado(empleado.getIdEmpleado());
        empleadoOutputDTO.setNumeroDocumento(empleado.getNumeroDocumento());
        empleadoOutputDTO.setNombreCompleto(empleado.getNombre()+" "+empleado.getApellido());
        empleadoOutputDTO.setEmail(empleado.getEmail());
        empleadoOutputDTO.setTelefono(empleado.getTelefono());
        if (empleado.getCargo() != null) {
            Map<String, Object> cargoData = new LinkedHashMap<>();
            cargoData.put("idCargo", empleado.getCargo().getIdCargo());
            cargoData.put("nombre", empleado.getCargo().getNombre());
            cargoData.put("Activo", empleado.getCargo().isActivo());
            empleadoOutputDTO.setCargo(cargoData);
        }
        //Datos Auditoria
        empleadoOutputDTO.setActivo(empleado.isActivo());
        return empleadoOutputDTO;
    }
}
