package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.ObsEvaluacionInputDTO;
import co.com.humancapital.portal.get.dto.ObsEvaluacionOutputDTO;
import co.com.humancapital.portal.get.entity.EvaluacionEmpleado;
import co.com.humancapital.portal.get.entity.ObsEvaluacion;
import co.com.humancapital.portal.get.repository.EvaluacionEmpleadoRepository;
import co.com.humancapital.portal.get.repository.EvaluacionRepository;
import co.com.humancapital.portal.get.repository.ObsEvaluacionRepository;
import co.com.humancapital.portal.service.EmpleadoService;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ObsEvaluacionService implements ServiceInterfaceDto<ObsEvaluacionOutputDTO, Long, ObsEvaluacionInputDTO> {

    private final ObsEvaluacionRepository obsEvaluacionRepository;
    private final EmpleadoService empleadoService;
    private final EvaluacionEmpleadoService evaluacionEmpleadoService;
    private final EvaluacionEmpleadoRepository evaluacionEmpleadoRepository;
    private final EvaluacionRepository evaluacionRepository;

    @Override
    public ObsEvaluacionOutputDTO findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Observación de la Evaluación con id "+key+" no existe.");
        }
        return ObsEvaluacionOutputDTO.getDTO(obsEvaluacionRepository.findByIdObsEvaluacion(key));
    }

    @Override
    public ObsEvaluacionOutputDTO create(ObsEvaluacionInputDTO entity) throws Exception {

        if (entity.getEvaluador()==null || !empleadoService.existeById(entity.getEvaluador())) {
            throw new ResourceNotFoundException("El Evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if(entity.getEvaluacionEmpleado() == null || !evaluacionEmpleadoService.existeById(entity.getEvaluacionEmpleado())){
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+ entity.getEvaluacionEmpleado() +" no existe o no se envio el dato.");
        }
        EvaluacionEmpleado evaluacionEmpleado = evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado());
        if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(evaluacionEmpleado.getEvaluacion().getIdEvaluacion()).isObsEvaluacion()){
            throw new ResourceNotFoundException("La Evaluación no esta configurada para permitir observaciones a la Evaluación.");
        }
        ObsEvaluacion obsEvaluacion = new ObsEvaluacion();
        obsEvaluacion.setEvaluacionEmpleado(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado()));
        obsEvaluacion.setEvaluador(entity.getEvaluador());
        obsEvaluacion.setObsEvaluacion(entity.getObsEvaluacion().trim());
        return ObsEvaluacionOutputDTO.getDTO(obsEvaluacionRepository.saveAndFlush(obsEvaluacion));
    }

    @Override
    public ObsEvaluacionOutputDTO update(ObsEvaluacionInputDTO entity) throws Exception {
        if (entity.getIdObsEvaluacion()==null || !empleadoService.existeById(entity.getIdObsEvaluacion())) {
            throw new ResourceNotFoundException("La Observación por evaluación con id "+ entity.getIdObsEvaluacion() +" no existe o no se envio el dato.");
        }
        if (entity.getEvaluador()==null || !empleadoService.existeById(entity.getEvaluador())) {
            throw new ResourceNotFoundException("El Evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if(entity.getEvaluacionEmpleado() == null || !evaluacionEmpleadoService.existeById(entity.getEvaluacionEmpleado())){
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+ entity.getEvaluacionEmpleado() +" no existe o no se envio el dato.");
        }
        EvaluacionEmpleado evaluacionEmpleado = evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado());
        if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(evaluacionEmpleado.getEvaluacion().getIdEvaluacion()).isObsEvaluacion()){
            throw new ResourceNotFoundException("La Evaluación no esta configurada para permitir observaciones a la Evaluación.");
        }
        ObsEvaluacion obsEvaluacion = obsEvaluacionRepository.findByIdObsEvaluacion(entity.getIdObsEvaluacion());
        obsEvaluacion.setEvaluacionEmpleado(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado()));
        obsEvaluacion.setEvaluador(entity.getEvaluador());
        obsEvaluacion.setObsEvaluacion(entity.getObsEvaluacion().trim());
        return ObsEvaluacionOutputDTO.getDTO(obsEvaluacionRepository.saveAndFlush(obsEvaluacion));
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite eliminar una observación a la evaluación de un empleado.
     * @param key Id de la observación a eliminar.
     */
    public void delete(Long key) throws Exception {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La Observación por evaluación con id "+key+" no existe o esta vacio.");
        }
        obsEvaluacionRepository.delete(obsEvaluacionRepository.findByIdObsEvaluacion(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(obsEvaluacionRepository.findByIdObsEvaluacion(key)==null){
            return false;
        }
        return obsEvaluacionRepository.existsById(key);
    }

    /**
     * Metodo que permite obtener las Observaciones realizadas a la evaluación de un Empleado especifico.
     * @param idEmpleado Id el empleado a validar.
     * @return Lista de ObsEvaluacionOutputDTO.
     */
    public Page<ObsEvaluacionOutputDTO> findAllEmpleado(Long idEmpleado, Pageable pageable) {
        if(idEmpleado == null || !empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con "+ idEmpleado +" no existe o no se envio en dato.");
        }
        List<ObsEvaluacionOutputDTO> obsEvaluacionOutputDTOS = new LinkedList<>();
        for (ObsEvaluacion obsEvaluacion : obsEvaluacionRepository.findAllByEmpleadoAndActivoTrue(idEmpleado, pageable)) {
            ObsEvaluacionOutputDTO obsEvaluacionOutputDTO = ObsEvaluacionOutputDTO.getDTO(obsEvaluacion);
            obsEvaluacionOutputDTOS.add(obsEvaluacionOutputDTO);
        }
        return new PageImpl<>(obsEvaluacionOutputDTOS);
    }

    @Override
    public Page<ObsEvaluacionOutputDTO> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public ObsEvaluacionOutputDTO update(ObsEvaluacionInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public ObsEvaluacionOutputDTO create(ObsEvaluacionInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}
