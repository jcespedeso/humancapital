package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.Encuesta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/03/2020
 */
@Repository
public interface EncuestaRepository extends JpaRepository<Encuesta, Long> {

    Encuesta findByIdEncuestaAndActivoTrue(Long idEncuesta);
    Page<Encuesta> findAllByActivoTrue(Pageable pageable);
    List<Encuesta> findAllByActivoTrue();

}
