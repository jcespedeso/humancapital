package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.EmpleadoRelacionInputDTO;
import co.com.humancapital.portal.get.dto.EvaluacionEmpleadoInputDTO;
import co.com.humancapital.portal.get.dto.EvaluacionEmpleadoOutputDTO;
import co.com.humancapital.portal.get.entity.EvaluacionEmpleado;
import co.com.humancapital.portal.get.repository.EvaluacionEmpleadoRepository;
import co.com.humancapital.portal.get.repository.EvaluacionRepository;
import co.com.humancapital.portal.get.type.GradoEvaluacion;
import co.com.humancapital.portal.get.type.TipoRelacion;
import co.com.humancapital.portal.repository.CargoRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import co.com.humancapital.portal.service.EmpleadoService;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020 Clase para la implementación de las diferentes reglas de negocio que se generen alrededor del
 * EvaluacionEmpleado.
 */
@Service
@Slf4j
public class EvaluacionEmpleadoService implements ServiceInterfaceDto<EvaluacionEmpleadoOutputDTO, Long, EvaluacionEmpleadoInputDTO> {

    private final EvaluacionEmpleadoRepository evaluacionEmpleadoRepository;
    private final EmpleadoService empleadoService;
    private final EvaluacionService evaluacionService;
    private final EmpleadoRepository empleadoRepository;
    private final EvaluacionRepository evaluacionRepository;
    private final EmpleadoRelacionService empleadoRelacionService;
    private final CargoRepository cargoRepository;

    public EvaluacionEmpleadoService(EvaluacionEmpleadoRepository evaluacionEmpleadoRepository, EmpleadoService empleadoService, @Lazy EvaluacionService evaluacionService, EmpleadoRepository empleadoRepository, EvaluacionRepository evaluacionRepository, @Lazy EmpleadoRelacionService empleadoRelacionService, CargoRepository cargoRepository) {
        this.evaluacionEmpleadoRepository = evaluacionEmpleadoRepository;
        this.empleadoService = empleadoService;
        this.evaluacionService = evaluacionService;
        this.empleadoRepository = empleadoRepository;
        this.evaluacionRepository = evaluacionRepository;
        this.empleadoRelacionService = empleadoRelacionService;
        this.cargoRepository = cargoRepository;
    }

    @Override
    public EvaluacionEmpleadoOutputDTO findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+key+" no existe.");
        }
        return EvaluacionEmpleadoOutputDTO.getDTO(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(key));
    }

    @Override
    public EvaluacionEmpleadoOutputDTO create(EvaluacionEmpleadoInputDTO entity) throws Exception {
        log.info(":: Creando Evaluacion Empleado",entity.getEmpleado());
        if (entity.getEmpleado()==null || !empleadoService.existeById(entity.getEmpleado())) {
            throw new ResourceNotFoundException("El Empleado a evaluar con id "+ entity.getEmpleado() +" no existe o no se envio el dato.");
        }
        if(entity.getEvaluacion() == null || !evaluacionService.existeById(entity.getEvaluacion())){
            throw new ResourceNotFoundException("La Evaluación con id "+ entity.getEvaluacion() +" no existe o no se envio el dato.");
        }
        EvaluacionEmpleado evaluacionEmpleado = new EvaluacionEmpleado();
        evaluacionEmpleado.setEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()));
        evaluacionEmpleado.setEvaluacion(evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getEvaluacion()));
        evaluacionEmpleado.setActivo(true);
        evaluacionEmpleadoRepository.saveAndFlush(evaluacionEmpleado);
        crearRelacionesPrimarias(evaluacionEmpleado);
        return EvaluacionEmpleadoOutputDTO.getDTO(evaluacionEmpleado);
    }

    @Override
    public EvaluacionEmpleadoOutputDTO update(EvaluacionEmpleadoInputDTO entity) throws Exception {
        if (entity.getIdEvaEmpleado()==null || !existeById(entity.getIdEvaEmpleado())) {
            throw new ResourceNotFoundException("La evaluación del empleado con id "+ entity.getIdEvaEmpleado() +" no existe o no se envio el dato.");
        }
        if (entity.getEmpleado()==null || !empleadoService.existeById(entity.getEmpleado())) {
            throw new ResourceNotFoundException("El Empleado a evaluar con id "+ entity.getEmpleado() +" no existe o no se envio el dato.");
        }
        if(entity.getEvaluacion() == null || !evaluacionService.existeById(entity.getEvaluacion())){
            throw new ResourceNotFoundException("La Evaluación con id "+ entity.getEvaluacion() +" no existe o no se envio el dato.");
        }
        EvaluacionEmpleado evaluacionEmpleado = evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getIdEvaEmpleado());
        evaluacionEmpleado.setEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()));
        evaluacionEmpleado.setEvaluacion(evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getEvaluacion()));
        return EvaluacionEmpleadoOutputDTO.getDTO(evaluacionEmpleadoRepository.saveAndFlush(evaluacionEmpleado));
    }

    public void delete(Long key) throws Exception {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+key+" no existe o esta vacio.");
        }
        EvaluacionEmpleado evaluacionEmpleado =evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(key);
        evaluacionEmpleado.setActivo(false);
        evaluacionEmpleadoRepository.save(evaluacionEmpleado);
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(key)==null){
            return false;
        }
        return evaluacionEmpleadoRepository.existsById(key);
    }

    /**
     * Metodo que permite obtener las Evaluaciones realizadas a un Empleado especifico.
     * @param idEmpleado Id el empleado a validar.
     * @return Lista de EvaluacionEmpleadoOutputDTO.
     */
    public Page<EvaluacionEmpleadoOutputDTO> findAllEmpleado(Long idEmpleado, Pageable pageable) {
        if(idEmpleado == null || empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id "+ idEmpleado +" no existe o no se envio en dato.");
        }
        List<EvaluacionEmpleadoOutputDTO> evaluacionEmpleadoOutputDTOS = new ArrayList<>();
        for (EvaluacionEmpleado evaluacionEmpleado : evaluacionEmpleadoRepository.findAllByEmpleadoAndActivoTrue(idEmpleado, pageable)) {
            EvaluacionEmpleadoOutputDTO evaluacionEmpleadoOutputDTO = EvaluacionEmpleadoOutputDTO.getDTO(evaluacionEmpleado);
            evaluacionEmpleadoOutputDTOS.add(evaluacionEmpleadoOutputDTO);
        }
        return new PageImpl<>(evaluacionEmpleadoOutputDTOS);
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 30/03/2020
     * Metodo que permite crear las Relaciones primarias o basicas de un Empleado, permitiendo
     * identificar los evaluadores del Empleado.
     * @param evaluacionEmpleado  Objeto con la información de evaluación del Empleado.
     */
    public void crearRelacionesPrimarias(EvaluacionEmpleado evaluacionEmpleado) throws Exception{
        log.info("::: Creando Relaciones primarias.");
        String gradoEvaluacion = evaluacionEmpleado.getEvaluacion().getGrado();
        for (String grado:gradoEvaluacion.split("\\s")) {
            System.out.println("::: grado crear relaciones primarias = " + grado);
            switch (grado){
                case "AUTOEVALUACION":
                    log.info("::: Definiendo Autoevaluador.");
                    EmpleadoRelacionInputDTO entityAuto = new EmpleadoRelacionInputDTO();
                    entityAuto.setEvaluacionEmpleado(evaluacionEmpleado.getIdEvaEmpleado());
                    entityAuto.setRelacion(TipoRelacion.AUTOEVALUACION.toString());
                    entityAuto.setEvaluador(evaluacionEmpleado.getEmpleado().getIdEmpleado());
                    empleadoRelacionService.create(entityAuto);
                    break;
                case "JEFE":
                    log.info("::: Definiendo Evaluador Jefe.");
                    crearRelacionJefe(evaluacionEmpleado);
                    break;
                case "PAR":
                    log.info("::: Definiendo Evaluadores Pares.");
                    crearRelacionPares(evaluacionEmpleado);
                    break;
                case "COLABORADOR":
                    log.info("::: Definiendo Evaluadores Colaboradores.");
                    crearRelacionColaboradores(evaluacionEmpleado);
                    break;
            }
        }
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 30/03/2020
     * Metodo que permite crear las Relacion primarias o basicas de un Empleado con su Jefe,
     * para que este le evaluen.
     * @param evaluacionEmpleado  Objeto con la información de evaluación del Empleado.
     */
    public void crearRelacionJefe(EvaluacionEmpleado evaluacionEmpleado) throws Exception{
        Empleado empleado = evaluacionEmpleado.getEmpleado();
        Cargo cargoJefe = cargoRepository.findByCargoPadreAndActivoTrue(empleado.getCargo());
        if(cargoJefe != null){
            Empleado empleadoJefe = cargoJefe.getEmpleadoCollection().iterator().next();
            EmpleadoRelacionInputDTO entityJefe = new EmpleadoRelacionInputDTO();
            entityJefe.setEvaluacionEmpleado(evaluacionEmpleado.getIdEvaEmpleado());
            entityJefe.setRelacion(TipoRelacion.JEFE.toString());
            entityJefe.setEvaluador(empleadoJefe.getIdEmpleado());
            empleadoRelacionService.create(entityJefe);
        }
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 30/03/2020
     * Metodo que permite crear las Relacion primarias o basicas de un Empleado con sus Pares,
     * para que estos le evaluen.
     * @param evaluacionEmpleado  Objeto con la información de evaluación del Empleado.
     */
    public void crearRelacionPares(EvaluacionEmpleado evaluacionEmpleado) throws Exception{
        Empleado empleado = evaluacionEmpleado.getEmpleado();
        Cargo cargoJefe = cargoRepository.findByCargoPadreAndActivoTrue(empleado.getCargo());
        if(cargoJefe != null){
            List<Cargo> cargosPares = cargoRepository.obtenerCargoHijo(cargoJefe.getIdCargo());
            if(cargosPares != null){
                List<Empleado> pares = new LinkedList<>();
                for (Cargo cargo:cargosPares) {
                    if (cargo.getEmpleadoCollection() != null){
                        pares.addAll(cargo.getEmpleadoCollection());
                    }
                }
                pares.remove(empleado);
                if (empleado.getCargo().getEmpleadoCollection() != null) {
                    pares.addAll(empleado.getCargo().getEmpleadoCollection());
                }
                pares.remove(empleado);
                for (Empleado empleadoPar:pares) {
                    EmpleadoRelacionInputDTO entityPar = new EmpleadoRelacionInputDTO();
                    entityPar.setEvaluacionEmpleado(evaluacionEmpleado.getIdEvaEmpleado());
                    entityPar.setRelacion(TipoRelacion.PAR.toString());
                    entityPar.setEvaluador(empleadoPar.getIdEmpleado());
                    empleadoRelacionService.create(entityPar);
                }
            }
        }else{
            List<Empleado> pares = new LinkedList<>();
            if (empleado.getCargo().getEmpleadoCollection() != null) {
                pares.addAll(empleado.getCargo().getEmpleadoCollection());
            }
            pares.remove(empleado);
            for (Empleado empleadoPar:pares) {
                EmpleadoRelacionInputDTO entityPar = new EmpleadoRelacionInputDTO();
                entityPar.setEvaluacionEmpleado(evaluacionEmpleado.getIdEvaEmpleado());
                entityPar.setRelacion(TipoRelacion.PAR.toString());
                entityPar.setEvaluador(empleadoPar.getIdEmpleado());
                empleadoRelacionService.create(entityPar);
            }
        }
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 30/03/2020
     * Metodo que permite crear las Relacion primarias o basicas de un Empleado con sus Colaboradores,
     * para que estos le evaluen.
     * @param evaluacionEmpleado  Objeto con la información de evaluación del Empleado.
     */
    public void crearRelacionColaboradores(EvaluacionEmpleado evaluacionEmpleado) throws Exception{
        Empleado empleado = evaluacionEmpleado.getEmpleado();
        List<Cargo> cargosColaboradores = cargoRepository.obtenerCargoHijo(empleado.getCargo().getIdCargo());
        if(cargosColaboradores != null){
            List<Empleado> colaboradores = new LinkedList<>();
            for (Cargo cargo:cargosColaboradores) {
                if(cargo.getEmpleadoCollection() != null){
                    colaboradores.addAll(cargo.getEmpleadoCollection());
                }
            }
            for (Empleado empleadoColaborador:colaboradores) {
                EmpleadoRelacionInputDTO entityColaborador = new EmpleadoRelacionInputDTO();
                entityColaborador.setEvaluacionEmpleado(evaluacionEmpleado.getIdEvaEmpleado());
                entityColaborador.setRelacion(TipoRelacion.COLABORADOR.toString());
                entityColaborador.setEvaluador(empleadoColaborador.getIdEmpleado());
                empleadoRelacionService.create(entityColaborador);
            }
        }
    }

    @Override
    public Page<EvaluacionEmpleadoOutputDTO> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public EvaluacionEmpleadoOutputDTO update(EvaluacionEmpleadoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public EvaluacionEmpleadoOutputDTO create(EvaluacionEmpleadoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}
