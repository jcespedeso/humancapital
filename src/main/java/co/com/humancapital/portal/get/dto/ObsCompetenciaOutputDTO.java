package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.ObsCompetencia;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de salida de la entidad ObsCompetencia.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ObsCompetenciaOutputDTO {

    private Long idObsCompetencia;
    private Long evaluador;
    private Long competencia;
    private String obsCompetencia;

    //Relaciones
    private Long evaluacionEmpleado;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * ObsCompetencia.
     * @param obsCompetencia Objeto ObsCompetencia.
     * @return Objeto ObsCompetenciaOutputDTO
     */
    public static ObsCompetenciaOutputDTO getDTO(ObsCompetencia obsCompetencia) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        ObsCompetenciaOutputDTO obsCompetenciaOutputDTO = modelMapper.map(obsCompetencia, ObsCompetenciaOutputDTO.class);
        if(obsCompetencia.getEvaluacionEmpleado() != null){
            obsCompetenciaOutputDTO.setEvaluacionEmpleado(obsCompetencia.getEvaluacionEmpleado().getIdEvaEmpleado());
        }
        return obsCompetenciaOutputDTO;
    }

}
