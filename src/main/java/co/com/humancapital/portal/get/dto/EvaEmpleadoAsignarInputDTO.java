package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.entity.Empleado;
import lombok.*;

import java.util.Collection;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 14/03/2020
 * Clase que permite modelar los datos de entrada de la entidad Evaluacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EvaEmpleadoAsignarInputDTO {

    private Long idEvaluacion;
    private Long cliente;

    //Relaciones
    @ToString.Exclude
    private Collection<Empleado> empleadoCollection;

}
