package co.com.humancapital.portal.get.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Entity
@Data
public class EvaluationNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty
    @Column(columnDefinition="text")
    private String htmlMessage;
    @NotEmpty
    private String subject;
    private LocalDateTime notificationDate;
    @NotEmpty
    private String fromAddress;
    @NotEmpty
    private String fromName;
    @NotEmpty
    private String testFromAddress;
    @NotEmpty
    private String testFromName;
    @NotEmpty
    private String testToAddress;
    @NotEmpty
    private String testToName;
    @ManyToOne(optional = false)
    private Evaluacion evaluacion;
}
