/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.get.entity;

import co.com.humancapital.portal.entity.Cliente;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"GRUPO\"")
@NoArgsConstructor
@Data
public class Grupo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_GRUPO")
    private Long idGrupo;
    @NotEmpty
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private boolean activo;

    @JoinColumn(name = "CLIENTE", referencedColumnName = "ID_CLIENTE")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Cliente cliente;

    //Relaciones Omitidas
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupo")
//    private Collection<Competencia> competenciaCollection;
    
}
