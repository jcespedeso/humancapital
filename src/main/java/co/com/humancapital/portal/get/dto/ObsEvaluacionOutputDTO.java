package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.ObsEvaluacion;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de salida de la entidad ObsEvaluacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ObsEvaluacionOutputDTO {

    private Long idObsEvaluacion;
    private Long evaluador;
    private String obsEvaluacion;

    //Relaciones
    private Long evaluacionEmpleado;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * ObsEvaluacion.
     * @param obsEvaluacion Objeto obsEvaluacion.
     * @return Objeto ObsEvaluacionOutputDTO
     */
    public static ObsEvaluacionOutputDTO getDTO(ObsEvaluacion obsEvaluacion) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        ObsEvaluacionOutputDTO obsEvaluacionOutputDTO = modelMapper.map(obsEvaluacion, ObsEvaluacionOutputDTO.class);
        obsEvaluacionOutputDTO.setEvaluacionEmpleado(obsEvaluacion.getEvaluacionEmpleado().getIdEvaEmpleado());
        return obsEvaluacionOutputDTO;
    }

}
