package co.com.humancapital.portal.get.type;

public enum TipoPregunta {
    PERSONALIZADA,
    ESCALA_1_5
}
