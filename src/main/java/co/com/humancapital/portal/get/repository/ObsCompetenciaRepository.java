package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.ObsCompetencia;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020
 */
@Repository
public interface ObsCompetenciaRepository extends JpaRepository<ObsCompetencia, Long> {

    ObsCompetencia findByIdObsCompetencia(Long idObsCompetencia);

    @Query("select obsComp from ObsCompetencia obsComp " +
            "left join EvaluacionEmpleado evEmp on evEmp.idEvaEmpleado = obsComp.evaluacionEmpleado " +
            "left join Empleado emp on emp.idEmpleado = evEmp.empleado " +
            "where emp.activo=true and emp.idEmpleado=:idEmpleado")
    Page<ObsCompetencia> findAllByEmpleadoAndActivoTrue(@Param("idEmpleado") Long idEmpleado, Pageable pageable);

}
