package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.EmpleadoRelacionInputDTO;
import co.com.humancapital.portal.get.dto.EmpleadoRelacionOutputDTO;
import co.com.humancapital.portal.get.entity.EmpleadoRelacion;
import co.com.humancapital.portal.get.entity.Evaluacion;
import co.com.humancapital.portal.get.entity.EvaluacionEmpleado;
import co.com.humancapital.portal.get.repository.EmpleadoRelacionRepository;
import co.com.humancapital.portal.get.repository.EvaluacionEmpleadoRepository;
import co.com.humancapital.portal.get.repository.EvaluacionRepository;
import co.com.humancapital.portal.get.type.TipoRelacion;
import co.com.humancapital.portal.service.EmpleadoService;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020
 * Clase para la implementación de las diferentes reglas de negocio que se generen alrededor del
 * EmpleadoRelacion.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EmpleadoRelacionService implements ServiceInterfaceDto<EmpleadoRelacionOutputDTO, Long, EmpleadoRelacionInputDTO> {

    private final EmpleadoRelacionRepository empleadoRelacionRepository;
    private final EmpleadoService empleadoService;
    private final EvaluacionEmpleadoService evaluacionEmpleadoService;
    private final EvaluacionEmpleadoRepository evaluacionEmpleadoRepository;
    private final EvaluacionRepository evaluacionRepository;

    @Override
    public EmpleadoRelacionOutputDTO findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El registro de relacion del Empleado con id "+key+" no existe.");
        }
        return EmpleadoRelacionOutputDTO.getDTO(empleadoRelacionRepository.findByIdRelacionAndActivoTrue(key));
    }

    @Override
    public EmpleadoRelacionOutputDTO create(EmpleadoRelacionInputDTO entity) throws Exception {
        log.info("::: Creando la relacion");
        if (entity.getEvaluador()==null || !empleadoService.existeById(entity.getEvaluador())) {
            throw new ResourceNotFoundException("El Empleado evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if (entity.getEvaluacionEmpleado()==null || !evaluacionEmpleadoService.existeById(entity.getEvaluacionEmpleado())) {
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if(entity.getRelacion() == null || !TipoRelacion.existeKey(entity.getRelacion())){
            throw new ResourceNotFoundException("El tipo de Relación "+ entity.getRelacion() +" no existe o no es un valor permitido por el sistema.");
        }
        EmpleadoRelacion empleadoRelacion = new EmpleadoRelacion();
        empleadoRelacion.setRelacion(entity.getRelacion());
        empleadoRelacion.setEvaluador(entity.getEvaluador());
        empleadoRelacion.setActivo(true);
        empleadoRelacion.setEvaluacionEmpleado(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado()));
        return EmpleadoRelacionOutputDTO.getDTO(empleadoRelacionRepository.saveAndFlush(empleadoRelacion));
    }

    @Override
    public EmpleadoRelacionOutputDTO update(EmpleadoRelacionInputDTO entity) throws Exception {
        if (entity.getIdRelacion()==null || !existeById(entity.getIdRelacion())) {
            throw new ResourceNotFoundException("La Relación Empleado-Evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if (entity.getEvaluador()==null || !empleadoService.existeById(entity.getEvaluador())) {
            throw new ResourceNotFoundException("El Empleado evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if (entity.getEvaluacionEmpleado()==null || !evaluacionEmpleadoService.existeById(entity.getEvaluacionEmpleado())) {
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if(entity.getRelacion() == null || !TipoRelacion.existeKey(entity.getRelacion())){
            throw new ResourceNotFoundException("El tipo de Relación "+ entity.getRelacion() +" no existe o no es un valor permitido por el sistema.");
        }
        EmpleadoRelacion empleadoRelacion = empleadoRelacionRepository.findByIdRelacionAndActivoTrue(entity.getIdRelacion());
        empleadoRelacion.setRelacion(entity.getRelacion());
        empleadoRelacion.setEvaluador(entity.getEvaluador());
        empleadoRelacion.setActivo(entity.isActivo());
        empleadoRelacion.setEvaluacionEmpleado(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado()));
        return EmpleadoRelacionOutputDTO.getDTO(empleadoRelacionRepository.saveAndFlush(empleadoRelacion));
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite eliminar una relacion Empleado - Evaluador.
     * @param key Id de la Relación a eliminar.
     */
    public void delete(Long key) throws Exception {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La relación del Empleado con id "+key+" no existe o esta vacio.");
        }
        empleadoRelacionRepository.delete(empleadoRelacionRepository.findByIdRelacionAndActivoTrue(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(empleadoRelacionRepository.findByIdRelacionAndActivoTrue(key)==null){
            return false;
        }
        return empleadoRelacionRepository.existsById(key);
    }

    /**
     * Metodo que permite obtener las Relaciones especiales de un Empleado especifico.
     * @param idEmpleado Id el empleado a validar.
     * @return Lista de EmpleadoRelacionOutputDTO.
     */
    public Page<EmpleadoRelacionOutputDTO> findAllEmpleado(Long idEmpleado, Pageable pageable) {
        if(idEmpleado == null || !empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id "+ idEmpleado +" no existe o no se envio en dato.");
        }
        List<EmpleadoRelacionOutputDTO> empleadoRelacionOutputDTOS = new ArrayList<>();
        for (EmpleadoRelacion empleadoRelacion : empleadoRelacionRepository.findAllByEmpleadoAndActivoTrue(idEmpleado, pageable)) {
            EmpleadoRelacionOutputDTO empleadoRelacionOutputDTO = EmpleadoRelacionOutputDTO.getDTO(empleadoRelacion);
            empleadoRelacionOutputDTOS.add(empleadoRelacionOutputDTO);
        }
        return new PageImpl<>(empleadoRelacionOutputDTOS);
    }

//    /**
//     * @author Alejandro Herrera Montilla
//     * @date 01/04/2020
//     * Metodo que permite modelar la estructura jerarquica de la organizacion a nivel de los Cargos.
//     * @param idEvaluador Id del Cargo desde el cual se desea desplegar la organización jerarquica.
//     * @param pageable Determina el nivel maximo al caul se desea visualizar donde cero permite visualizar todo.
//     * @return Lista de datos de los cargos organizados jerarquicamente.
//     */
    public void verEvaluados(Long idEvaluador){
        if (idEvaluador == null || !empleadoService.existeById(idEvaluador)) {
            throw new ResourceNotFoundException("El Empleado evaluador con id " + idEvaluador + " no exite.");
        }


    }



    @Override
    public Page<EmpleadoRelacionOutputDTO> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public EmpleadoRelacionOutputDTO update(EmpleadoRelacionInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public EmpleadoRelacionOutputDTO create(EmpleadoRelacionInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}
