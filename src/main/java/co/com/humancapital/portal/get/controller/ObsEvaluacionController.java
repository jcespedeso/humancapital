package co.com.humancapital.portal.get.controller;

import co.com.humancapital.portal.controller.AbstractController;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.get.dto.ObsEvaluacionInputDTO;
import co.com.humancapital.portal.get.service.ObsEvaluacionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad ObsEvaluacion.
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/obsEvaluacion")
public class ObsEvaluacionController extends AbstractController<ObsEvaluacionInputDTO, GeneralOutput, Long> {

    private final String CREATE = "EVALUACION_CREAR";
    private final String UPDATE = "EVALUACION_EDITAR";
    private final String VIEW = "EVALUACION_VER";
    private final String DELETE = "EVALUACION_ELIMINAR";

    private final ObsEvaluacionService obsEvaluacionService;

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), obsEvaluacionService.findId(id));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody ObsEvaluacionInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, CREATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        obsEvaluacionService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody ObsEvaluacionInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, UPDATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        obsEvaluacionService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, DELETE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        obsEvaluacionService.delete(id);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/empleado/{idEmpleado}")
    public GeneralOutput getAllbyCompetencia(@PathVariable Long idEmpleado, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"obsEvaluacion"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), obsEvaluacionService.findAllEmpleado(idEmpleado, pageable));
    }

    @Override
    public GeneralOutput getAll(String token, Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(ObsEvaluacionInputDTO entity, String token, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput update(ObsEvaluacionInputDTO entity, String token, List<MultipartFile> files) throws Exception {
        return null;
    }
}
