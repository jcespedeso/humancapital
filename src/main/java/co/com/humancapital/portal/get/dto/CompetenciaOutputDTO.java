package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.Competencia;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 14/03/2020
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompetenciaOutputDTO {

    private Long idCompetencia;
    private String nombre;
    private String descripcion;
    private String tipo;
    private BigInteger orden;
    private BigInteger minimoEsperado;

    //Auditoria
    private boolean activo;

    //Relaciones
    private Map<String, Object> cliente;
    private Map<String, Object> grupo;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Compentencia.
     * @param competencia Objeto Competencia.
     * @return Objeto CompetenciaOutputDTO
     */
    public static CompetenciaOutputDTO getDTO(Competencia competencia) {
        ModelMapper modelMapper = new ModelMapper();
        CompetenciaOutputDTO competenciaOutputDTO = modelMapper.map(competencia, CompetenciaOutputDTO.class);

        //Relaciones
        if(competencia.getCliente() !=null){
            Map<String, Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",competencia.getCliente().getIdCliente());
            clienteData.put("nit", competencia.getCliente().getNit());
            clienteData.put("razonSocial",competencia.getCliente().getRazonSocial());
            clienteData.put("Activo",competencia.getCliente().isActivo());
            competenciaOutputDTO.setCliente(clienteData);
        }
        if(competencia.getGrupo() !=null){
            Map<String, Object> grupoData = new LinkedHashMap<>();
            grupoData.put("idGrupo",competencia.getGrupo().getIdGrupo());
            grupoData.put("nombre", competencia.getGrupo().getNombre());
            grupoData.put("activo", competencia.getGrupo().isActivo());
            competenciaOutputDTO.setGrupo(grupoData);
        }
        return competenciaOutputDTO;
    }

}
