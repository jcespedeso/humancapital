package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.RespComportamientoInputDTO;
import co.com.humancapital.portal.get.dto.RespComportamientoOutputDTO;
import co.com.humancapital.portal.get.entity.Evaluacion;
import co.com.humancapital.portal.get.entity.EvaluacionEmpleado;
import co.com.humancapital.portal.get.entity.RespuestaComportamiento;
import co.com.humancapital.portal.get.repository.EvaluacionEmpleadoRepository;
import co.com.humancapital.portal.get.repository.EvaluacionRepository;
import co.com.humancapital.portal.get.repository.RespComportamientoRepository;
import co.com.humancapital.portal.service.EmpleadoService;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class RespComportamientoService implements ServiceInterfaceDto<RespComportamientoOutputDTO, Long, RespComportamientoInputDTO> {

    private final RespComportamientoRepository respComportamientoRepository;
    private final EmpleadoService empleadoService;
    private final EvaluacionEmpleadoService evaluacionEmpleadoService;
    private final EvaluacionEmpleadoRepository evaluacionEmpleadoRepository;
    private final ComportamientoService comportamientoService;
    private final EvaluacionRepository evaluacionRepository;

    @Override
    public RespComportamientoOutputDTO findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Respuesta con id "+key+" no existe.");
        }
        return RespComportamientoOutputDTO.getDTO(respComportamientoRepository.findByIdRespuesta(key));
    }

    @Override
    public RespComportamientoOutputDTO create(RespComportamientoInputDTO entity) throws Exception {

        if (entity.getEvaluador()==null || !empleadoService.existeById(entity.getEvaluador())) {
            throw new ResourceNotFoundException("El Evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if(entity.getEvaluacionEmpleado() == null || !evaluacionEmpleadoService.existeById(entity.getEvaluacionEmpleado())){
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+ entity.getEvaluacionEmpleado() +" no existe o no se envio el dato.");
        }
        if(entity.getComportamiento() == null || !comportamientoService.existeById(entity.getComportamiento())){
            throw new ResourceNotFoundException("El Comportamiento con id "+ entity.getComportamiento() +" no existe o no se envio el dato.");
        }
        RespuestaComportamiento respuestaComportamiento = new RespuestaComportamiento();
        respuestaComportamiento.setEvaluacionEmpleado(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado()));
        respuestaComportamiento.setEvaluador(entity.getEvaluador());
        respuestaComportamiento.setComportamiento(entity.getComportamiento());
        respuestaComportamiento.setRespuesta(entity.getRespuesta().trim());
        EvaluacionEmpleado evaluacionEmpleado = evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado());
        if(entity.getObsAfirmacion()!=null){
            if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(evaluacionEmpleado.getEvaluacion().getIdEvaluacion()).isObsAfirmacion()){
                throw new ResourceNotFoundException("La Evaluación no esta configurada para permitir observaciones a las afirmaciones.");
            }
            respuestaComportamiento.setObsAfirmacion(entity.getObsAfirmacion().trim());
        } else {
            if(evaluacionRepository.findByIdEvaluacionAndActivoTrue(evaluacionEmpleado.getEvaluacion().getIdEvaluacion()).isObsAfirmacionObligatorio()){
                throw new ResourceNotFoundException("La Evaluación esta configurada para permitir observaciones a las afirmaciones de forma obligatoria.");
            }
        }
        return RespComportamientoOutputDTO.getDTO(respComportamientoRepository.saveAndFlush(respuestaComportamiento));
    }

    @Override
    public RespComportamientoOutputDTO update(RespComportamientoInputDTO entity) throws Exception {
        if (entity.getIdRespuesta()==null || !existeById(entity.getIdRespuesta())) {
            throw new ResourceNotFoundException("La respuesta con id "+ entity.getIdRespuesta() +" no existe o no se envio el dato.");
        }
        if (entity.getEvaluador()==null || !empleadoService.existeById(entity.getEvaluador())) {
            throw new ResourceNotFoundException("El Evaluador con id "+ entity.getEvaluador() +" no existe o no se envio el dato.");
        }
        if(entity.getEvaluacionEmpleado() == null || !evaluacionEmpleadoService.existeById(entity.getEvaluacionEmpleado())){
            throw new ResourceNotFoundException("La Evaluación del Empleado con id "+ entity.getEvaluacionEmpleado() +" no existe o no se envio el dato.");
        }
        if(entity.getComportamiento() == null || !comportamientoService.existeById(entity.getComportamiento())){
            throw new ResourceNotFoundException("El Comportamiento con id "+ entity.getComportamiento() +" no existe o no se envio el dato.");
        }
        RespuestaComportamiento respuestaComportamiento = respComportamientoRepository.findByIdRespuesta(entity.getIdRespuesta());
        respuestaComportamiento.setEvaluacionEmpleado(evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado()));
        respuestaComportamiento.setEvaluador(entity.getEvaluador());
        respuestaComportamiento.setComportamiento(entity.getComportamiento());
        respuestaComportamiento.setRespuesta(entity.getRespuesta().trim());
        EvaluacionEmpleado evaluacionEmpleado = evaluacionEmpleadoRepository.findByIdEvaEmpleadoAndActivoTrue(entity.getEvaluacionEmpleado());
        if(entity.getObsAfirmacion()!=null){
            if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(evaluacionEmpleado.getEvaluacion().getIdEvaluacion()).isObsAfirmacion()){
                throw new ResourceNotFoundException("La Evaluación no esta configurada para permitir observaciones a las afirmaciones.");
            }
            respuestaComportamiento.setObsAfirmacion(entity.getObsAfirmacion().trim());
        } else {
            if(evaluacionRepository.findByIdEvaluacionAndActivoTrue(evaluacionEmpleado.getEvaluacion().getIdEvaluacion()).isObsAfirmacionObligatorio()){
                throw new ResourceNotFoundException("La Evaluación esta configurada para permitir observaciones a las afirmaciones de forma obligatoria.");
            }
        }
        return RespComportamientoOutputDTO.getDTO(respComportamientoRepository.saveAndFlush(respuestaComportamiento));
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite eliminar una respuesta dada a un comportamiento de la evaluación de un empleado.
     * @param key Id de la respuesta a eliminar.
     */
    public void delete(Long key) throws Exception {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La respuesta con id "+key+" no existe o esta vacio.");
        }
        respComportamientoRepository.delete(respComportamientoRepository.findByIdRespuesta(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(respComportamientoRepository.findByIdRespuesta(key)==null){
            return false;
        }
        return respComportamientoRepository.existsById(key);
    }

    /**
     * Metodo que permite obtener las respuestas y las observaciones realizadas a la evaluación de un Empleado especifico.
     * @param idEmpleado Id el empleado a validar.
     * @return Lista de RespComportamientoOutputDTO.
     */
    public Page<RespComportamientoOutputDTO> findAllEmpleado(Long idEmpleado, Pageable pageable) {
        if(idEmpleado == null || !empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con "+ idEmpleado +" no existe o no se envio en dato.");
        }
        List<RespComportamientoOutputDTO> obsEvaluacionOutputDTOS = new LinkedList<>();
        for (RespuestaComportamiento respuestaComportamiento : respComportamientoRepository.findAllByEmpleadoAndActivoTrue(idEmpleado, pageable)) {
            RespComportamientoOutputDTO respComportamientoOutputDTO = RespComportamientoOutputDTO.getDTO(respuestaComportamiento);
            obsEvaluacionOutputDTOS.add(respComportamientoOutputDTO);
        }
        return new PageImpl<>(obsEvaluacionOutputDTOS);
    }

    @Override
    public Page<RespComportamientoOutputDTO> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public RespComportamientoOutputDTO update(RespComportamientoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public RespComportamientoOutputDTO create(RespComportamientoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}
