package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.Categoria;
import co.com.humancapital.portal.get.entity.Competencia;
import co.com.humancapital.portal.get.entity.Comportamiento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/03/2020
 */
@Repository
public interface ComportamientoRepository extends JpaRepository<Comportamiento, Long> {

    Comportamiento findByIdComportamientoAndActivoTrue(Long idComportamiento);
    Page<Comportamiento> findAllByActivoTrue(Pageable pageable);
    List<Comportamiento> findAllByActivoTrue();
    Page<Comportamiento> findAllByCompetenciaAndActivoTrue(Competencia competencia, Pageable pageable);
    List<Comportamiento> findAllByCompetenciaAndActivoTrue(Competencia competencia);
    Page<Comportamiento> findAllByCategoriaAndActivoTrue(Categoria categoria, Pageable pageable);
    List<Comportamiento> findAllByCategoriaAndActivoTrue(Categoria categoria);

}
