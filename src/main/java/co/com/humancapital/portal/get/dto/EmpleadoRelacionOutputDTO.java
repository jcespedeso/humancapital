package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.EmpleadoRelacion;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de salida de la entidad EmpleadoRelacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmpleadoRelacionOutputDTO {

    private Long idRelacion;
    private String relacion;
    private Long evaluador;
    private boolean activo;

    //Relaciones
    private Long evaluacionEmpleado;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * EmpleadoRelacion.
     * @param empleadoRelacion Objeto EmpleadoRelacion.
     * @return Objeto EmpleadoRelacionOutputDTO.
     */
    public static EmpleadoRelacionOutputDTO getDTO(EmpleadoRelacion empleadoRelacion) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        EmpleadoRelacionOutputDTO empleadoRelacionOutputDTO = modelMapper.map(empleadoRelacion, EmpleadoRelacionOutputDTO.class);
        empleadoRelacionOutputDTO.setEvaluacionEmpleado(empleadoRelacion.getEvaluacionEmpleado().getIdEvaEmpleado());
        return  empleadoRelacionOutputDTO;
    }

}
