
package co.com.humancapital.portal.get.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"OBS_EVALUACION\"")
@Data
@NoArgsConstructor
public class ObsEvaluacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_OBS_EVALUACION")
    private Long idObsEvaluacion;
    @NotNull
    @Column(name = "EVALUADOR")
    private Long evaluador;
    @NotEmpty
    @Size(min =1 ,max =255)
    @Column(name = "OBS_EVALUACION")
    private String obsEvaluacion;

    //Relaciones
    @JoinColumn(name = "EVALUACION_EMPLEADO", referencedColumnName = "ID_EVA_EMPLEADO")
    @OneToOne
    @ToString.Exclude
    private EvaluacionEmpleado evaluacionEmpleado;

    public ObsEvaluacion(Long idObsEvaluacion) {
        this.idObsEvaluacion=idObsEvaluacion;
    }
}
