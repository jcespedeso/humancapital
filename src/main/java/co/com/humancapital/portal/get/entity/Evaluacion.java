/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.get.entity;

import co.com.humancapital.portal.entity.Cliente;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"EVALUACION\"")
@NoArgsConstructor
@Data
public class Evaluacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_EVALUACION")
    private Long idEvaluacion;
    @NotEmpty
    @Size(min = 1, max = 75)
    @Column(name = "NOMBRE")
    private String nombre;
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(name = "GRADO")
    private String grado;
    @Basic(optional = false)
    @Column(name = "OBS_AFIRMACION")
    private boolean obsAfirmacion;
    @Basic(optional = false)
    @Column(name = "OBS_AFIRMACION_OBLIGATORIO")
    private boolean obsAfirmacionObligatorio;
    @Basic(optional = false)
    @Column(name = "OBS_COMPETENCIA")
    private boolean obsCompetencia;
    @Basic(optional = false)
    @Column(name = "OBS_COMPETENCIA_OBLIGATORIO")
    private boolean obsCompetenciaObligatorio;
    @Basic(optional = false)
    @Column(name = "OBS_EVALUACION")
    private boolean obsEvaluacion;
    @Basic(optional = false)
    @Column(name = "OBS_EVALUACION_OBLIGATORIO")
    private boolean obsEvaluacionObligatorio;
    @NotEmpty
    @Size(min = 1, max = 30)
    @Column(name = "TIPO_SEMAFORO")
    private String tipoSemaforo;
    @NotEmpty
    @Size(min = 1, max = 30)
    @Column(name = "TIPO_RESPUESTA")
    private String tipoRespuesta;
    @NotEmpty
    @Size(min = 1, max = 30)
    @Column(name = "CONFIGURACION")
    private String configuracion;
    @Basic(optional = false)
    @Column(name = "NO_SE")
    private boolean noSe;
    @Size(max = 255)
    @Column(name = "CABECERA")
    private String cabecera;
    @Basic(optional = false)
    @Column(name = "HABILITAR_RESULTADO")
    private boolean habilitarResultado;
    @PositiveOrZero
    @Column(name = "NUMERO_INVALIDA")
    private BigInteger numeroInvalida;
    @Column(name = "FECHA_INICIO")
    private LocalDate fechaInicio;
    @Column(name = "FECHA_CIERRE")
    private LocalDate fechaCierre;
    @Column(name = "FECHA_HORA_ENVIO")
    private LocalDateTime fechaHoraEnvio;
    @Size(max = 80)
    @Column(name = "ASUNTO_INVITACION")
    private String asuntoInvitacion;
    @Size(max = 80)
    @Column(name = "ASUNTO_RECORDATORIO")
    private String asuntoRecordatorio;
    @Size(max = 255)
    @Column(name = "COMENTARIO_COMPETENCIA")
    private String comentarioCompetencia;
    @Size(max = 255)
    @Column(name = "MENSAJE_FIN_EVALUACION")
    private String mensajeFinEvaluacion;

    // Datos de Auditoria
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private boolean activo;
    @Basic(optional = false)
    @Column(name = "FECHA_CREACION")
    private LocalDate fechaCreacion;
    @Basic(optional = false)
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;
    @Basic(optional = false)
    @Column(name = "FECHA_MODIFICA")
    private LocalDate fechaModifica;
    @Basic(optional = false)
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;
    @Column(name = "FECHA_INACTIVACION")
    private LocalDate fechaInactivacion;
    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;
    
    //Relaciones
    @JoinTable(name = "\"COMPETENCIA_X_EVALUACION\"", joinColumns = {
            @JoinColumn(name = "EVALUACION", referencedColumnName = "ID_EVALUACION")}, inverseJoinColumns = {
            @JoinColumn(name = "COMPETENCIA", referencedColumnName = "ID_COMPETENCIA")})
    @ManyToMany
    @ToString.Exclude
    private Collection<Competencia> competenciaCollection;

    @JoinColumn(name = "CLIENTE", referencedColumnName = "ID_CLIENTE")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Cliente cliente;

    public Evaluacion(Long idEvaluacion) {
        this.idEvaluacion=idEvaluacion;
    }

    //Relaciones Omitidas
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacion")
//    private Collection<Semaforo> semaforoCollection;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacion")
//    private Collection<EvaluacionEmpleado> evaluacionEmpleadoCollection;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacion")
//    private Collection<OpcionRespuesta> opcionRespuestaCollection;

}
