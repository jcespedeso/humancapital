package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.get.entity.EvaluacionEmpleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020
 */
@Repository
public interface EvaluacionEmpleadoRepository extends JpaRepository<EvaluacionEmpleado, Long> {

    EvaluacionEmpleado findByIdEvaEmpleadoAndActivoTrue(Long idEvEmpleado);
    Page<EvaluacionEmpleado> findAllByActivoTrue(Pageable pageable);

    @Query("select evEmp from EvaluacionEmpleado evEmp " +
            "left join Empleado emp on emp.idEmpleado = evEmp.empleado " +
            "where emp.activo=true and emp.idEmpleado=:idEmpleado")
    Page<EvaluacionEmpleado> findAllByEmpleadoAndActivoTrue(@Param("idEmpleado") Long idEmpleado, Pageable pageable);

}
