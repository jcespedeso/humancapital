package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.Competencia;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.validation.constraints.*;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 14/03/2020
 * Clase que permite modelar los datos de entrada de la entidad Evaluacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EvaluacionInputDTO {

    private Long idEvaluacion;
    @NotEmpty(message = "El nombre de la evaluación no puede estar vacio.")
    @Size(max = 75, message = "El nombre de la evaluación no puede tener mad de 75 caracteres.")
    private String nombre;
    @NotEmpty(message = "El grado de evaluación no puede estar vacio.")
    @Size(max = 100, message = "El grado de Evaluador no puede tener mas de 100 caracteres.")
    private String[] grado;
    private boolean obsAfirmacion;
    private boolean obsAfirmacionObligatorio;
    private boolean obsCompetencia;
    private boolean obsCompetenciaObligatorio;
    private boolean obsEvaluacion;
    private boolean obsEvaluacionObligatorio;
    @NotEmpty(message = "El tipo de semaforo a utilizar en la visualización de los datos no puede estar vacio.")
    @Size(max = 30, message = "El tipo de semaforo no puede tener mas de 30 caracteres.")
    private String tipoSemaforo;
    @NotEmpty(message = "El tipo de respuesta a utilizar en la visualización de los datos no puede estar vacio.")
    @Size(max = 30, message = "El tipo de respuesta no puede tener mas de 30 caracteres.")
    private String tipoRespuesta;
    @NotEmpty(message = "Defina el tipo de configuracion para el rango de respuestas, este valor no puede estar vacio.")
    @Size(max = 30, message = "El valor de configuración no puede tener mas de 30 caracteres.")
    private String configuracion;
    private boolean noSe;
    @Size(max = 255, message = "El encabezado no puede tener mas de 255 Caracteres.")
    private String cabecera;
    private boolean habilitarResultado;
    @PositiveOrZero(message = "El numero invalida no permite numeros negativos y su valor debe ser un entero mayor o igual que cero.")
    private BigInteger numeroInvalida;
    @NotNull(message = "Defina la fecha de Inicio de la evaluación, esta no puede estar vacia.")
//    @FutureOrPresent(message = "La fecha de inicio de la evaluación no puede estar en el pasado.")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate fechaInicio;
    @NotNull(message = "Defina la fecha de Cierre de la evaluación, esta no puede estar vacia.")
//    @FutureOrPresent(message = "La fecha de cierre de la evaluación no puede estar en el pasado.")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate fechaCierre;
    @NotNull(message = "Defina la hora de envio de la notificación de apertura de la evaluación, esta no puede estar vacia.")
    @FutureOrPresent(message = "La fecha y hora del envio de la notificación, no puede estar en el pasado.")
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private LocalDateTime fechaHoraEnvio;
    @Size(max = 80, message = "El asunto de mensaje de la invitación no puede tener mas de 80 caracteres.")
    private String asuntoInvitacion;
    @Size(max = 80, message = "El asunto del mensaje de recordatorio de la evaluación no puede tener mas de 80 caracteres.")
    private String asuntoRecordatorio;
    @Size(max = 255, message = "El comentario general de las competencia no puede tener mas de 255 caracteres.")
    private String comentarioCompetencia;
    @Size(max = 255, message = "El mensaje general de la evaluación no puede tener mas de 255 caracteres.")
    private String mensajeFinEvaluacion;

    //Auditoria
    private Long usuarioCrea;
    private Long usuarioModifica;
    private Long usuarioInactiva;

    //Relaciones
    @NotNull(message = "El Cliente no puede estar vacio.")
    private Long cliente;
    @ToString.Exclude
    private Collection<Competencia> competenciaCollection;

}
