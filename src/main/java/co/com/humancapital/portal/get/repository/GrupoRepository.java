package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.get.entity.Categoria;
import co.com.humancapital.portal.get.entity.Grupo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/03/2020
 */
@Repository
public interface GrupoRepository extends JpaRepository<Grupo, Long> {

    Grupo findByIdGrupoAndActivoTrue(Long idGrupo);
    Grupo findByNombreAndActivoTrue(String nombre);
    Page<Grupo> findAllByActivoTrue(Pageable pageable);
    List<Grupo> findAllByActivoTrue();
    List<Grupo> findAllByClienteAndActivoTrueOrderByNombre(Cliente cliente);

}
