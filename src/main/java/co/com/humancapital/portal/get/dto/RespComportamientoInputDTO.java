package co.com.humancapital.portal.get.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 20/03/2020
 * Clase que permite modelar los datos de entrada de la entidad RespuestaComportamiento.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RespComportamientoInputDTO {

    private Long idRespuesta;
    @NotNull(message = "El Evaluador no puede estar vacio.")
    private Long evaluador;
    @NotNull(message = "El Comportamiento no puede estar vacio.")
    private Long comportamiento;
    @NotEmpty(message = "La respuesta no puede estar vacio.")
    @Size(max = 50, message = "La respuesta no puede tener mas de 50 caracteres.")
    private String respuesta;
    @Size(max = 200, message = "La observación para el comportamiento no puede tener mas de 200 caracteres.")
    private String obsAfirmacion;

    //Relaciones
    @ToString.Exclude
    private Long evaluacionEmpleado;

}
