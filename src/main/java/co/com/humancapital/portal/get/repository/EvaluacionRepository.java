package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.get.entity.Categoria;
import co.com.humancapital.portal.get.entity.Competencia;
import co.com.humancapital.portal.get.entity.EmpleadoRelacion;
import co.com.humancapital.portal.get.entity.Evaluacion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 12/03/2020
 */
@Repository
public interface EvaluacionRepository extends JpaRepository<Evaluacion, Long> {

    Evaluacion findByIdEvaluacionAndActivoTrue(Long idEvaluacion);
    Boolean existsByIdEvaluacionAndActivoTrue(Long idEvaluacion);
    Page<Evaluacion> findAllByActivoTrue(Pageable pageable);
    Page<Evaluacion> findAllByCompetenciaCollectionContainsAndActivoTrue(Competencia competencia, Pageable pageable);
    List<Evaluacion> findAllByCompetenciaCollectionContainsAndActivoTrue(Competencia competencia);
    Page<Evaluacion> findAllByClienteAndActivoTrueOrderByNombre(Cliente cliente, Pageable pageable);

    @Query("select emp from Empleado emp " +
            "left join EvaluacionEmpleado evEmp on evEmp.empleado = emp.idEmpleado " +
            "left join Evaluacion eva on eva.idEvaluacion = evEmp.evaluacion " +
            "where emp.activo=true and evEmp.activo=true and eva.activo=true and eva.idEvaluacion=:idEvaluacion ")
    Page<Empleado> obtenerEmpleadosByEvaluacion(@Param("idEvaluacion") Long idEvaluacion, Pageable pageable);

    @Query("select eva from Evaluacion eva " +
            "left join EvaluacionEmpleado evEmp on evEmp.evaluacion=eva.idEvaluacion " +
            "where evEmp.activo=true and eva.activo=true and evEmp.empleado=:idEmpledo ")
    Page<Evaluacion> obtenerEvaluacionByEmpleado(@Param("idEmpledo") Empleado idEmpledo, Pageable pageable);

    @Query("select eva from Evaluacion eva " +
            "left join EvaluacionEmpleado evEmp on evEmp.evaluacion = eva.idEvaluacion " +
            "left join EmpleadoRelacion empRel on empRel.evaluacionEmpleado = evEmp.idEvaEmpleado " +
            "where empRel.activo=true and evEmp.activo=true and eva.activo=true and empRel.evaluador=:idEvaluador ")
    Page<Evaluacion> obtenerEvaluacionByEvaluador(@Param("idEvaluador") Long idEvaluador, Pageable pageable);

    @Query("select empR from EmpleadoRelacion empR " +
            "left join EvaluacionEmpleado evEmp on evEmp.idEvaEmpleado = empR.evaluacionEmpleado " +
            "left join Evaluacion eva on eva.idEvaluacion = evEmp.evaluacion " +
            "where empR.activo=true and evEmp.activo=true and eva.activo=true and eva.idEvaluacion=:idEvaluacion ")
    Page<EmpleadoRelacion> obtenerEvaluadoresByEvaluacion(@Param("idEvaluacion") Long idEvaluacion, Pageable pageable);

}
