
package co.com.humancapital.portal.get.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"RESPUESTA_COMPORTAMIENTO\"")
@Data
@NoArgsConstructor
public class RespuestaComportamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_RESPUESTA")
    private Long idRespuesta;
    @NotNull
    @Column(name = "EVALUADOR")
    private Long evaluador;
    @NotNull
    @Column(name = "COMPORTAMIENTO")
    private Long comportamiento;
    @NotEmpty
    @Size(min = 1, max = 50)
    @Column(name = "RESPUESTA")
    private String respuesta;
    @Size(max = 200)
    @Column(name = "OBS_AFIRMACION")
    private String obsAfirmacion;

    //Relaciones
    @JoinColumn(name = "EVALUACION_EMPLEADO", referencedColumnName = "ID_EVA_EMPLEADO")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private EvaluacionEmpleado evaluacionEmpleado;

    public RespuestaComportamiento(Long idRespuesta) {
        this.idRespuesta=idRespuesta;
    }
}
