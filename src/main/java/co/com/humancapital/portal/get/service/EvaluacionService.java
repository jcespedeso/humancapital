package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.dto.EmpleadoOutputDTO;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.*;
import co.com.humancapital.portal.get.entity.Categoria;
import co.com.humancapital.portal.get.entity.Competencia;
import co.com.humancapital.portal.get.entity.EmpleadoRelacion;
import co.com.humancapital.portal.get.entity.Evaluacion;
import co.com.humancapital.portal.get.repository.CompetenciaRepository;
import co.com.humancapital.portal.get.repository.EvaluacionRepository;
import co.com.humancapital.portal.get.type.ConfigPregunta;
import co.com.humancapital.portal.get.type.GradoEvaluacion;
import co.com.humancapital.portal.get.type.TipoRespuesta;
import co.com.humancapital.portal.get.type.TipoSemaforo;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import co.com.humancapital.portal.service.ClienteService;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import co.com.humancapital.portal.util.TransformarString;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static co.com.humancapital.portal.util.ObjectMapperUtils.map;

@Service
@RequiredArgsConstructor
@Slf4j
public class EvaluacionService implements ServiceInterfaceDto<EvaluacionOutputDTO, Long, EvaluacionInputDTO> {
    private static LocalDate fechaActual = LocalDate.now();
    private final EvaluacionRepository evaluacionRepository;
    private final CompetenciaRepository competenciaRepository;
    private final ClienteService clienteService;
    private final ClienteRepository clienteRepository;
    private final EvaluacionEmpleadoService evaluacionEmpleadoService;
    private final EmpleadoRepository empleadoRepository;

    @Override
    public EvaluacionOutputDTO findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La evaluacion con id "+key+" no existe.");
        }
        return EvaluacionOutputDTO.getDTO(evaluacionRepository.findByIdEvaluacionAndActivoTrue(key));
    }

    @Override
    public Page<EvaluacionOutputDTO> findAll(Pageable pageable) throws Exception {
        if(evaluacionRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Evaluaciones creadas en el sistema.");
        }
        List<EvaluacionOutputDTO> evaluacionOutputDTOS = new ArrayList<>();
        for (Evaluacion evaluacion : evaluacionRepository.findAllByActivoTrue(pageable)) {
            EvaluacionOutputDTO evaluacionOutputDTO = EvaluacionOutputDTO.getDTO(evaluacion);
            evaluacionOutputDTOS.add(evaluacionOutputDTO);
        }
        return new PageImpl<>(evaluacionOutputDTOS);
    }

    @Override
    public EvaluacionOutputDTO create(EvaluacionInputDTO entity) throws Exception {
        log.info("::: Persistiendo Evaluacion Crear");
        for (String grado:entity.getGrado()) {
            if(entity.getGrado() == null || !GradoEvaluacion.existeKey(grado)){
                throw new ResourceNotFoundException("El grado "+ grado +" no existe o no es un valor valido para el sistema.");
            }
        }
        if(entity.getTipoSemaforo() == null || !TipoSemaforo.existeKey(entity.getTipoSemaforo())){
            throw new ResourceNotFoundException("El tipo de semaforo no existe o no es un valor valido para el sistema.");
        }
        if(entity.getTipoRespuesta() == null || !TipoRespuesta.existeKey(entity.getTipoRespuesta())){
            throw new ResourceNotFoundException("El tipo de respuesta no existe o no es un valor valido para el sistema.");
        }
        if(entity.getConfiguracion() == null || !ConfigPregunta.existeKey(entity.getConfiguracion())){
            throw new ResourceNotFoundException("La Configuracion a plicar no existe o no es un valor valido para el sistema.");
        }
        Evaluacion evaluacion = map(entity, Evaluacion.class);
        evaluacion.setNombre(entity.getNombre().trim().toUpperCase());
        evaluacion.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        evaluacion.setGrado(TransformarString.transformarArray(entity.getGrado()));

        //Datos Auditoria
        evaluacion.setActivo(true);
        evaluacion.setFechaCreacion(fechaActual);
        evaluacion.setUsuarioCrea(entity.getUsuarioCrea());
        evaluacion.setFechaModifica(fechaActual);
        evaluacion.setUsuarioModifica(entity.getUsuarioCrea());
        evaluacionRepository.saveAndFlush(evaluacion);
        log.info("::: Evaluacion persistida creando con ID[{}]", evaluacion.getIdEvaluacion());
        return EvaluacionOutputDTO.getDTO(evaluacion);
    }

    @Override
    public EvaluacionOutputDTO update(EvaluacionInputDTO entity) throws Exception {
        log.info("::: Persistiendo Evaluacion Actualizando");
        if (entity.getIdEvaluacion() == null || !existeById(entity.getIdEvaluacion())) {
            throw new ResourceNotFoundException("La Evaluacon con id "+entity.getIdEvaluacion()+" no existe o esta vacio.");
        }
        for (String grado:entity.getGrado()) {
            System.out.println("gradodddfsdfsdfsdsd = " + grado);
            if(entity.getGrado() == null || !GradoEvaluacion.existeKey(grado)){
                throw new ResourceNotFoundException("El grado "+ grado +" no existe o no es un valor valido para el sistema.");
            }
        }
        if(entity.getTipoSemaforo() == null || !TipoSemaforo.existeKey(entity.getTipoSemaforo())){
            throw new ResourceNotFoundException("El tipo de semaforo no existe o no es un valor valido para el sistema.");
        }
        if(entity.getTipoRespuesta() == null || !TipoRespuesta.existeKey(entity.getTipoRespuesta())){
            throw new ResourceNotFoundException("El tipo de respuesta no existe o no es un valor valido para el sistema.");
        }
        if(entity.getConfiguracion() == null || !ConfigPregunta.existeKey(entity.getConfiguracion())){
            throw new ResourceNotFoundException("La Configuracion a plicar no existe o no es un valor valido para el sistema.");
        }
        Evaluacion evaluacion = evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getIdEvaluacion());
        evaluacion.setNombre(entity.getNombre().trim().toUpperCase());
        evaluacion.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        evaluacion.setGrado(TransformarString.transformarArray(entity.getGrado()));
        evaluacion.setObsAfirmacion(entity.isObsAfirmacion());
        evaluacion.setObsAfirmacionObligatorio(entity.isObsAfirmacionObligatorio());
        evaluacion.setObsCompetencia(entity.isObsCompetencia());
        evaluacion.setObsCompetenciaObligatorio(entity.isObsCompetenciaObligatorio());
        evaluacion.setObsEvaluacion(entity.isObsEvaluacion());
        evaluacion.setObsEvaluacionObligatorio(entity.isObsEvaluacionObligatorio());
        evaluacion.setTipoSemaforo(entity.getTipoSemaforo());
        evaluacion.setTipoRespuesta(entity.getTipoRespuesta());
        evaluacion.setConfiguracion(entity.getConfiguracion());
        evaluacion.setNoSe(entity.isNoSe());
        evaluacion.setCabecera(entity.getCabecera());
        evaluacion.setHabilitarResultado(entity.isHabilitarResultado());
        evaluacion.setNumeroInvalida(entity.getNumeroInvalida());
        evaluacion.setFechaInicio(entity.getFechaInicio());
        evaluacion.setFechaCierre(entity.getFechaCierre());
        evaluacion.setFechaHoraEnvio(entity.getFechaHoraEnvio());
        evaluacion.setAsuntoInvitacion(entity.getAsuntoInvitacion());
        evaluacion.setAsuntoRecordatorio(entity.getAsuntoRecordatorio());
        evaluacion.setComentarioCompetencia(entity.getComentarioCompetencia());
        evaluacion.setMensajeFinEvaluacion(entity.getMensajeFinEvaluacion());

        //Datos auditoria
        evaluacion.setFechaModifica(fechaActual);
        evaluacion.setUsuarioModifica(entity.getUsuarioModifica());
        evaluacionRepository.saveAndFlush(evaluacion);
        log.info("::: Evaluacion persistida Actualizando con ID[{}]", evaluacion.getIdEvaluacion());
        return EvaluacionOutputDTO.getDTO(evaluacion);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        log.info("::: Persistiendo Eliminando");
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La Evaluación con id "+key+" no existe o esta vacio.");
        }
        Evaluacion evaluacion = evaluacionRepository.findByIdEvaluacionAndActivoTrue(key);

        //Datos adiconales
        evaluacion.setActivo(false);
        evaluacion.setUsuarioInactiva(usuarioInactiva);
        evaluacion.setFechaInactivacion(fechaActual);
        evaluacionRepository.save(evaluacion);
        log.info("::: Evaluacion persistida Eliminando con ID[{}]", evaluacion.getIdEvaluacion());
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(evaluacionRepository.findByIdEvaluacionAndActivoTrue(key)==null){
            return false;
        }
        return evaluacionRepository.existsById(key);
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite la asignación de Competencias a una Evaluación.
     * @param entity Objeto EvaCompAsignarInputDTO
     */
    public void asignarCompetencia(EvaCompAsignarInputDTO entity) throws Exception{
        if (entity.getIdEvaluacion() == null || !existeById(entity.getIdEvaluacion())) {
            throw new ResourceNotFoundException("La Evaluacon con id "+entity.getIdEvaluacion()+" no existe o esta vacio.");
        }
        if (entity.getCliente() != null) {
            asignarMasivoCompetencia(entity.getIdEvaluacion(), entity.getCliente());
        }else{
            if (entity.getCompetenciaCollection() == null || entity.getCompetenciaCollection().isEmpty()) {
                throw new ResourceNotFoundException("No se han seleccionado Competencias para asignar.");
            }
            Evaluacion evaluacion = evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getIdEvaluacion());
            evaluacion.setCompetenciaCollection(entity.getCompetenciaCollection());
            evaluacionRepository.save(evaluacion);
        }
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite la asignación masiva de la totalidad de Competencias asignadas a un Cliente.
     * @param idEvaluacion id de la Evaluacion a la que se asignaran las competencias.
     * @param idCliente id del Cliente a validar.
     */
    public void asignarMasivoCompetencia(Long idEvaluacion, Long idCliente) throws Exception {
        if(idCliente == null ||  !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Cliente seleccionado no existe.");
        }
        if(idEvaluacion == null ||  !existeById(idEvaluacion)){
            throw new ResourceNotFoundException("La Evaluación con id "+idEvaluacion+" no existe.");
        }
        if(evaluacionRepository.findByIdEvaluacionAndActivoTrue(idEvaluacion).getCompetenciaCollection() != null){
            if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(idEvaluacion).getCompetenciaCollection().iterator().next().getCliente().getIdCliente().equals(idCliente))
            throw new ResourceNotFoundException("La Evaluación con id "+idEvaluacion+" pertenece a un Cliente diferente al seleccionado.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if(cliente.getCompetenciaCollection() == null || cliente.getCompetenciaCollection().isEmpty()){
            throw new ResourceNotFoundException("El Cliente seleccionado no tiene competencias asignadas.");
        }
        Evaluacion evaluacion = evaluacionRepository.findByIdEvaluacionAndActivoTrue(idEvaluacion);
        evaluacion.setCompetenciaCollection(competenciaRepository.findAllByClienteAndActivoTrue(cliente));
        evaluacionRepository.save(evaluacion);
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite la asignación de EvaluacionEmpleado(Empleado) a una Evaluación.
     * @param entity Objeto EvaEmpleadoAsignarInputDTO
     */
    public void asignarEmpleado(EvaEmpleadoAsignarInputDTO entity) throws Exception{
        if (entity.getIdEvaluacion() == null || !existeById(entity.getIdEvaluacion())) {
            throw new ResourceNotFoundException("La Evaluacon con id "+entity.getIdEvaluacion()+" no existe o esta vacio.");
        }
        Evaluacion evaluacion = evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getIdEvaluacion());
        if (entity.getCliente() != null) {
            asignarMasivoEmpleado(entity.getIdEvaluacion(), entity.getCliente());
        }else{
            if (entity.getEmpleadoCollection() == null || entity.getEmpleadoCollection().isEmpty()) {
                throw new ResourceNotFoundException("No se han seleccionado Empleados para asignar.");
            }
            for (Empleado empleado : entity.getEmpleadoCollection()) {
                EvaluacionEmpleadoInputDTO empleadoInputDTO = new EvaluacionEmpleadoInputDTO();
                empleadoInputDTO.setEmpleado(empleado.getIdEmpleado());
                empleadoInputDTO.setEvaluacion(evaluacion.getIdEvaluacion());
                empleadoInputDTO.setActivo(true);
                evaluacionEmpleadoService.create(empleadoInputDTO);
            }
            evaluacionRepository.save(evaluacion);
        }
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite la asignación masiva de la totalidad de EvaluacionEmpleado(Empleado) asignadas a un Cliente.
     * @param idEvaluacion id de la Evaluacion a la que se asignaran las EvaluacionEmpleado(Empleado).
     * @param idCliente id del Cliente a validar.
     */
    public void asignarMasivoEmpleado(Long idEvaluacion, Long idCliente) throws Exception {
        if(idCliente == null ||  !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Cliente seleccionado no existe.");
        }
        if(idEvaluacion == null ||  !existeById(idEvaluacion)){
            throw new ResourceNotFoundException("La Evaluación con id "+idEvaluacion+" no existe.");
        }
        Evaluacion evaluacion = evaluacionRepository.findByIdEvaluacionAndActivoTrue(idEvaluacion);
        if(evaluacionRepository.findByIdEvaluacionAndActivoTrue(idEvaluacion).getCompetenciaCollection() != null){
            if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(idEvaluacion).getCompetenciaCollection().iterator().next().getCliente().getIdCliente().equals(idCliente))
                throw new ResourceNotFoundException("La Evaluación con id "+idEvaluacion+" pertenece a un Cliente diferente al seleccionado.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if(cliente.getCompetenciaCollection() == null || cliente.getCompetenciaCollection().isEmpty()){
            throw new ResourceNotFoundException("El Cliente seleccionado no tiene competencias asignadas.");
        }
        if (empleadoRepository.obtenerEmpleadoXCliente(cliente.getIdCliente()) == null || empleadoRepository.obtenerEmpleadoXCliente(cliente.getIdCliente()).isEmpty()) {
            throw new ResourceNotFoundException("El Cliente no tiene Empleados asignados.");
        }
        for (Empleado empleado : empleadoRepository.obtenerEmpleadoXCliente(cliente.getIdCliente())) {
            EvaluacionEmpleadoInputDTO empleadoInputDTO=new EvaluacionEmpleadoInputDTO();
            empleadoInputDTO.setEmpleado(empleado.getIdEmpleado());
            empleadoInputDTO.setEvaluacion(evaluacion.getIdEvaluacion());
            empleadoInputDTO.setActivo(true);
            evaluacionEmpleadoService.create(empleadoInputDTO);
        }
    }

    public Optional<Evaluacion> findModel(Long evaluacionId) {
        return evaluacionRepository.findById(evaluacionId);
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 02/04/2020
     * Metodo que permite obtener las Evaluaciones activas de un Cliente Especifico.
     * @param idCliente Id del Cliente a consultar.
     * @return Listado de CategoriaOutputDTO con la data de Evaluaciones.
     */
    public Page<EvaluacionOutputDTO> findEvaluacionByCliente(Long idCliente, Pageable pageable){
        if (idCliente == null || !clienteRepository.existsByIdClienteAndActivoTrue(idCliente)) {
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if (evaluacionRepository.findAllByClienteAndActivoTrueOrderByNombre(cliente, pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no tiene evaluaciones creadas.");
        }
        List<EvaluacionOutputDTO> evaluacionOutputDTOS = new ArrayList<>();
        for (Evaluacion evaluacion:evaluacionRepository.findAllByClienteAndActivoTrueOrderByNombre(cliente, pageable)) {
            EvaluacionOutputDTO evaluacionOutputDTO = EvaluacionOutputDTO.getDTO(evaluacion);
            evaluacionOutputDTOS.add(evaluacionOutputDTO);
        }
        return new PageImpl<>(evaluacionOutputDTOS);
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 02/04/2020
     * Metodo que permite obtener los Empleado vinculados a una Evaluacion activas de un Cliente Especifico.
     * @param idEvaluacion Id de la evaluacion a consultar.
     * @return Listado de EmpleadoOutputDTO con la data del Empleado.
     */
    public Page<EmpleadoOutputDTO> obtenerEmpleadoByEvaluacion(Long idEvaluacion, Pageable pageable){
        if (idEvaluacion == null || !evaluacionRepository.existsByIdEvaluacionAndActivoTrue(idEvaluacion)) {
            throw new ResourceNotFoundException("La Evaluación con id "+idEvaluacion+" no existe.");
        }
        List<EmpleadoOutputDTO> empleadoOutputDTOS = new ArrayList<>();
        for (Empleado empleado:evaluacionRepository.obtenerEmpleadosByEvaluacion(idEvaluacion, pageable)) {
            EmpleadoOutputDTO empleadoOutputDTO = EmpleadoOutputDTO.getDTO(empleado);
            empleadoOutputDTOS.add(empleadoOutputDTO);
        }
        return new PageImpl<>(empleadoOutputDTOS);
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 04/04/2020
     * Metodo que permite obtener las Evaluaciones activas de un Empleado.
     * @param idEmpleado Id del empleado a consultar.
     * @return Lista de EvaluacionOutputDTO.
     */
    public Page<EvaluacionOutputDTO> obtenerEvaluacionesByEmpleado(Long idEmpleado, Pageable pageable){
        if (idEmpleado == null || !empleadoRepository.existsByIdEmpleadoAndActivoTrue(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id "+idEmpleado+" no existe.");
        }
        List<EvaluacionOutputDTO> evaluacionOutputDTOS = new ArrayList<>();
        for (Evaluacion evaluacion:evaluacionRepository.obtenerEvaluacionByEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(idEmpleado), pageable)) {
            EvaluacionOutputDTO evaluacionOutputDTO = EvaluacionOutputDTO.getDTO(evaluacion);
            evaluacionOutputDTOS.add(evaluacionOutputDTO);
        }
        return new PageImpl<>(evaluacionOutputDTOS);
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 03/04/2020
     * Metodo que permite obtener los Empleado Evaluadores vinculados a una Evaluacion activas de un Cliente Especifico.
     * @param idEvaluacion Id de la evaluacion a consultar.
     * @return Listado de EmpleadoOutputDTO con la data del Empleado evaluador.
     */
    public Page<EmpEvaluadorOutputDTO> obtenerEvaluadorByEvaluacion(Long idEvaluacion, Pageable pageable){
        if (idEvaluacion == null || !evaluacionRepository.existsByIdEvaluacionAndActivoTrue(idEvaluacion)) {
            throw new ResourceNotFoundException("La Evaluación con id "+idEvaluacion+" no existe.");
        }
        List<EmpEvaluadorOutputDTO> empleadoOutputDTOS = new ArrayList<>();
        for (EmpleadoRelacion empleadoRelacion:evaluacionRepository.obtenerEvaluadoresByEvaluacion(idEvaluacion, pageable)) {
            EmpEvaluadorOutputDTO empleadoOutputDTO = EmpEvaluadorOutputDTO.getDTO(empleadoRepository.findByIdEmpleadoAndActivoTrue(empleadoRelacion.getEvaluador()));
            empleadoOutputDTOS.add(empleadoOutputDTO);
        }
        return new PageImpl<>(empleadoOutputDTOS);
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 04/04/2020
     * Metodo que permite obtener las Evaluaciones activas de un empleado Evaluador.
     * @param idEvaluador Id del empleado evaluador a consultar.
     * @return Lista de EvaluacionOutputDTO.
     */
    public Page<EvaluacionOutputDTO> obtenerEvaluacionesByEvaluador(Long idEvaluador, Pageable pageable){
        if (idEvaluador == null || !empleadoRepository.existsByIdEmpleadoAndActivoTrue(idEvaluador)) {
            throw new ResourceNotFoundException("El Empleado con id "+idEvaluador+" no existe.");
        }
        List<EvaluacionOutputDTO> evaluacionOutputDTOS = new ArrayList<>();
        for (Evaluacion evaluacion:evaluacionRepository.obtenerEvaluacionByEvaluador(idEvaluador, pageable)) {
            EvaluacionOutputDTO evaluacionOutputDTO = EvaluacionOutputDTO.getDTO(evaluacion);
            evaluacionOutputDTOS.add(evaluacionOutputDTO);
        }
        return new PageImpl<>(evaluacionOutputDTOS);
    }

    @Override
    public EvaluacionOutputDTO update(EvaluacionInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public EvaluacionOutputDTO create(EvaluacionInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
