package co.com.humancapital.portal.get.controller;

import co.com.humancapital.portal.get.dto.EvaluationNotificationDTO;
import co.com.humancapital.portal.get.service.EvaluationNotificationService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/evaluation/notification")
@RequiredArgsConstructor
public class EvaluationNotificationController {
    private final EvaluationNotificationService evaluationNotificationService;
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EvaluationNotificationDTO save(@RequestBody @Valid EvaluationNotificationDTO notificationDTO) {
        return evaluationNotificationService.saveUpdate(notificationDTO);
    }
    
    @PostMapping("/{notificationId}/test")
    public void sendTestNotification(@PathVariable Long notificationId) {
        evaluationNotificationService.sendTestNotification(notificationId);
    }
    
    @PostMapping("/{notificationId}/send")
    public void sendRealNotification(@PathVariable Long notificationId) {
        evaluationNotificationService.sendRealNotification(notificationId);
    }
    //yvevcgntqfoxgscl
}
