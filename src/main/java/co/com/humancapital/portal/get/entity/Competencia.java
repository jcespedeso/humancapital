/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.get.entity;

import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Departamento;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Collection;

/**
 * @author Alejandro Herrera Montilla - alhemoasde@gmail.com
 */
@Entity
@Table(name = "\"COMPETENCIA\"")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Competencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COMPETENCIA")
    private Long idCompetencia;
    @NotEmpty
    @Size(min = 1, max = 150)
    @Column(name = "NOMBRE")
    private String nombre;
    @NotEmpty
    @Size(min = 1, max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @NotEmpty
    @Size(min = 1, max = 30)
    @Column(name = "TIPO")
    private String tipo;
    @Column(name = "ORDEN")
    @Range(min = 1)
    private BigInteger orden;
    @Column(name = "MINIMO_ESPERADO")
    @Range(min = 1, max = 100)
    private BigInteger minimoEsperado;

    //Datos de Auditoria
    @Basic(optional = false)
    @Column(name = "ACTIVO")
    private boolean activo;
    @Basic(optional = false)
    @Column(name = "FECHA_CREACION")
    private LocalDate fechaCreacion;
    @Basic(optional = false)
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;
    @Basic(optional = false)
    @Column(name = "FECHA_MODIFICA")
    private LocalDate fechaModifica;
    @Basic(optional = false)
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;
    @Column(name = "FECHA_INACTIVACION")
    private LocalDate fechaInactivacion;
    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

    //Relaciones
    @JoinTable(name = "\"COMPETENCIA_X_DEPARTAMENTO\"", joinColumns = {
        @JoinColumn(name = "COMPETENCIA", referencedColumnName = "ID_COMPETENCIA")}, inverseJoinColumns = {
        @JoinColumn(name = "DEPARTAMENTO", referencedColumnName = "ID_DEPARTAMENTO")})
    @ManyToMany
    @ToString.Exclude
    private Collection<Departamento> departamentoCollection;

    @JoinTable(name = "\"COMPETENCIA_X_CARGO\"", joinColumns = {
            @JoinColumn(name = "COMPETENCIA", referencedColumnName = "ID_COMPETENCIA")}, inverseJoinColumns = {
            @JoinColumn(name = "CARGO", referencedColumnName = "ID_CARGO")})
    @ManyToMany
    @ToString.Exclude
    private Collection<Cargo> cargoCollection;

    @ManyToMany(mappedBy = "competenciaCollection")
    @ToString.Exclude
    private Collection<Evaluacion> evaluacionCollection;

    // Relaciones Omitidas
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "competencia")
//    @ToString.Exclude
//    private Collection<Comportamiento> comportamientoCollection;

//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "competencia")
//    private Collection<ObsCompetencia> obsCompetenciaCollection;

    @JoinColumn(name = "CLIENTE", referencedColumnName = "ID_CLIENTE")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Cliente cliente;

    @JoinColumn(name = "GRUPO", referencedColumnName = "ID_GRUPO")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Grupo grupo;

    public Competencia(Long idCompetencia) {
        this.idCompetencia=idCompetencia;
    }

}
