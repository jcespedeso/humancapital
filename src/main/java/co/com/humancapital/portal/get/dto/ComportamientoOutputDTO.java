package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.Comportamiento;
import lombok.*;
import org.modelmapper.ModelMapper;

import java.math.BigInteger;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 15/03/2020
 * Clase que permite modelar los datos de salida de la entidad Comportamiento.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ComportamientoOutputDTO {

    private Long idComportamiento;
    private String descripcion;
    private BigInteger orden;

    // Datos de Auditoria
    private boolean activo;

    // Relaciones
    @ToString.Exclude
    private Long competencia;

    @ToString.Exclude
    private Long categoria;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Comportamiento.
     * @param comportamiento Objeto Comportamiento.
     * @return Objeto ComportamientoOutputDTO
     */
    public static ComportamientoOutputDTO getDTO(Comportamiento comportamiento) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(comportamiento, ComportamientoOutputDTO.class);
    }

}
