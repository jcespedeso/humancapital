package co.com.humancapital.portal.get.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.get.entity.Categoria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 07/03/2020
 */
@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

    Categoria findByIdCategoriaAndActivoTrue(Long idCategoria);
    Categoria findByNombreAndActivoTrue(String nombre);
    Page<Categoria> findAllByActivoTrue(Pageable pageable);
    List<Categoria> findAllByActivoTrue();
    List<Categoria> findAllByClienteAndActivoTrueOrderByNombre(Cliente cliente);

}
