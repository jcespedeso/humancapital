package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.CompetenciaInputDTO;
import co.com.humancapital.portal.get.dto.CompetenciaOutputDTO;
import co.com.humancapital.portal.get.entity.Competencia;
import co.com.humancapital.portal.get.repository.CompetenciaRepository;
import co.com.humancapital.portal.get.repository.GrupoRepository;
import co.com.humancapital.portal.get.type.TipoCompetencia;
import co.com.humancapital.portal.service.CargoService;
import co.com.humancapital.portal.service.ClienteService;
import co.com.humancapital.portal.service.DepartamentoService;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import co.com.humancapital.portal.util.TransformarString;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/01/2020
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de la entidad Competencia.
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class CompetenciaService implements ServiceInterfaceDto<CompetenciaOutputDTO, Long, CompetenciaInputDTO> {

    private static LocalDate fechaActual = LocalDate.now();
    private final CompetenciaRepository competenciaRepository;
    private final ClienteService clienteService;
    private final GrupoService grupoService;
    private final GrupoRepository grupoRepository;
    private final DepartamentoService departamentoService;
    private final CargoService cargoService;

    @Override
    public CompetenciaOutputDTO findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Competencia con id "+key+" no existe.");
        }
        return CompetenciaOutputDTO.getDTO(competenciaRepository.findByIdCompetenciaAndActivoTrue(key));
    }

    @Override
    public Page<CompetenciaOutputDTO> findAll(Pageable pageable) throws Exception {
        if(competenciaRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Competencias creadas en el sistema.");
        }
        List<CompetenciaOutputDTO> competenciaOutputDTOS = new ArrayList<>();
        for (Competencia competencia : competenciaRepository.findAllByActivoTrue(pageable)) {
            CompetenciaOutputDTO competenciaOutputDTO = CompetenciaOutputDTO.getDTO(competencia);
            competenciaOutputDTOS.add(competenciaOutputDTO);
        }
        return new PageImpl<>(competenciaOutputDTOS);
    }

    @Override
    public CompetenciaOutputDTO create(CompetenciaInputDTO entity) throws Exception{
        log.info("::: Creando competencia...");
        if(!TipoCompetencia.existeKey(entity.getTipo())) {
            throw new ResourceNotFoundException("El Tipo de Competencia indicado, no es un valor permitido por el sistema.");
        }
        if(!clienteService.existeById(entity.getCliente())) {
            throw new ResourceNotFoundException("El Cliente indicado no existe.");
        }
        if(!grupoService.existeById(entity.getGrupo())) {
            throw new ResourceNotFoundException("El Grupo indicado no existe.");
        }

        Competencia competencia = new Competencia();
        if(TipoCompetencia.DEPARTAMENTO.name().equals(TransformarString.transformar(entity.getTipo()).toUpperCase())){
            if(entity.getDepartamentoCollection() == null || entity.getDepartamentoCollection().isEmpty()){
                throw new ResourceNotFoundException("Según el tipo de Competencia seleccionada, se requiere que seleccione como minimo un Departamento.");
            }
            //Todo implementar validacion que solo permita agregar departamentos que pertenezcan al cliente.
            competencia.setDepartamentoCollection(entity.getDepartamentoCollection());
        }
        if(TipoCompetencia.CARGO.name().equals(TransformarString.transformar(entity.getTipo()).toUpperCase())){
            if(entity.getCargoCollection() == null || entity.getCargoCollection().isEmpty()){
                throw new ResourceNotFoundException("Según el tipo de Compentencia seleccionada, se requiere que seleccione como minimo un Cargo.");
            }
            //Todo implementar validacion que solo permita agregar cargos que pertenezcan al cliente.
            competencia.setCargoCollection(entity.getCargoCollection());
        }
        competencia.setNombre(entity.getNombre().toUpperCase());
        competencia.setDescripcion(entity.getDescripcion());
        competencia.setTipo(entity.getTipo().toUpperCase());
        competencia.setOrden(entity.getOrden());
        competencia.setMinimoEsperado(entity.getMinimoEsperado());
        competencia.setCliente(clienteService.findId(entity.getCliente()));
        competencia.setGrupo(grupoRepository.findByIdGrupoAndActivoTrue(entity.getGrupo()));

        competencia.setActivo(true);
        competencia.setFechaCreacion(fechaActual);
        competencia.setUsuarioCrea(entity.getUsuarioCrea());
        competencia.setFechaModifica(fechaActual);
        competencia.setUsuarioModifica(entity.getUsuarioCrea());
        competenciaRepository.saveAndFlush(competencia);
        log.info("::: Competencia persistida Creando con ID[{}]", competencia.getIdCompetencia());
        return CompetenciaOutputDTO.getDTO(competencia);
    }

    @Override
    public CompetenciaOutputDTO update(CompetenciaInputDTO entity) throws Exception {
        log.info("::: Actulizando competencia...");
        if(entity.getIdCompetencia() == null || !existeById(entity.getIdCompetencia())) {
            throw new ResourceNotFoundException("La Competencia con id "+entity.getIdCompetencia()+" a Actualizar, no existe o no se envio el dato.");
        }
        if(!TipoCompetencia.existeKey(entity.getTipo())) {
            throw new ResourceNotFoundException("El Tipo de Competencia indicado, no es un valor permitido por el sistema.");
        }
        if(!clienteService.existeById(entity.getCliente())) {
            throw new ResourceNotFoundException("El Cliente indicado no existe o no se envio el dato.");
        }
        if(!grupoService.existeById(entity.getGrupo())) {
            throw new ResourceNotFoundException("El Grupo indicado no existe.");
        }

        Competencia competencia = competenciaRepository.findByIdCompetenciaAndActivoTrue(entity.getIdCompetencia());
        if(TipoCompetencia.DEPARTAMENTO.name().equals(TransformarString.transformar(entity.getTipo()).toUpperCase())){
            if(entity.getDepartamentoCollection() == null || entity.getDepartamentoCollection().isEmpty()){
                throw new ResourceNotFoundException("Según el tipo de compensación seleccionada, se requiere que seleccione como minimo un Departamento.");
            }
            //Todo implementar validacion que solo permita agregar departamentos que pertenezcan al cliente.
            competencia.setDepartamentoCollection(entity.getDepartamentoCollection());
        }
        if(TipoCompetencia.CARGO.name().equals(TransformarString.transformar(entity.getTipo()).toUpperCase())){
            if(entity.getCargoCollection() == null || entity.getCargoCollection().isEmpty()){
                throw new ResourceNotFoundException("Según el tipo de compensación seleccionada, se requiere que seleccione como minimo un Cargo.");
            }
            //Todo implementar validacion que solo permita agregar departamentos que pertenezcan al cliente.
            competencia.setCargoCollection(entity.getCargoCollection());
        }

        competencia.setNombre(entity.getNombre().toUpperCase());
        competencia.setDescripcion(entity.getDescripcion());
        competencia.setTipo(entity.getTipo().toUpperCase());
        competencia.setOrden(entity.getOrden());
        competencia.setMinimoEsperado(entity.getMinimoEsperado());
        competencia.setCliente(clienteService.findId(entity.getCliente()));
        competencia.setGrupo(grupoRepository.findByIdGrupoAndActivoTrue(entity.getGrupo()));

        competencia.setFechaModifica(fechaActual);
        competencia.setUsuarioModifica(entity.getUsuarioModifica());
        competenciaRepository.saveAndFlush(competencia);
        log.info("::: Competencia persistida Actualizando con ID[{}]", competencia.getIdCompetencia());
        return CompetenciaOutputDTO.getDTO(competencia);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception{
        log.info("::: Eliminando competencia...");
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La Competencia con id " + key +" no existe o no se envio el dato.");
        }
        Competencia competencia = competenciaRepository.findByIdCompetenciaAndActivoTrue(key);
        competencia.setActivo(false);
        competencia.setFechaInactivacion(fechaActual);
        competencia.setUsuarioInactiva(usuarioInactiva);
        log.info("::: Competencia persistida Eliminando con ID[{}]", competencia.getIdCompetencia());
        competenciaRepository.save(competencia);
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(competenciaRepository.findByIdCompetenciaAndActivoTrue(key)==null){
            return false;
        }
        return competenciaRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar todas las Competencias laborales que registra un Cliente
     * @param idCliente Id del Cliente a validar.
     * @return Lista de Competencias que pueda tener un Cliente.
     */
    public Page<CompetenciaOutputDTO> findAllByCliente(Long idCliente, Pageable pageable) throws Exception {
        if(idCliente == null || !clienteService.existeById(idCliente)) {
            throw new ResourceNotFoundException("El Cliente indicado no existe o no se envio el dato.");
        }
        Cliente cliente = clienteService.findId(idCliente);
        if (competenciaRepository.findAllByClienteAndActivoTrue(cliente, pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Cliente "+cliente.getRazonSocial()+" no tiene Competencias registradas.");
        }
        List<CompetenciaOutputDTO> outputDTOS = new ArrayList<>();
        for (Competencia list : competenciaRepository.findAllByClienteAndActivoTrue(cliente, pageable)) {
            outputDTOS.add(CompetenciaOutputDTO.getDTO(list));
        }
        return new PageImpl<>(outputDTOS);
    }

    @Override
    public CompetenciaOutputDTO create(CompetenciaInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public CompetenciaOutputDTO update(CompetenciaInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
