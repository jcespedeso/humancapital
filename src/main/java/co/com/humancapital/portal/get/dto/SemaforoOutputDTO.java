package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.Semaforo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 18/03/2020
 * Clase que permite modelar los datos de salida de la entidad Semaforo.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SemaforoOutputDTO {

    private Long idSemaforo;
    private String color;
    private String etiqueta;
    private Integer porcentaje;

    //Relaciones
    private Long evaluacion;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * Semaforo.
     * @param semaforo Objeto Semaforo.
     * @return Objeto SemaforoOutputDTO.
     */
    public static SemaforoOutputDTO getDTO(Semaforo semaforo) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        SemaforoOutputDTO semaforoOutputDTO = modelMapper.map(semaforo, SemaforoOutputDTO.class);
        semaforoOutputDTO.setEvaluacion(semaforo.getEvaluacion().getIdEvaluacion());
        return semaforoOutputDTO;
    }

}
