package co.com.humancapital.portal.get.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 17/03/2020
 * Clase que permite modelar los datos de entrada de la entidad ObsCompetencia.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ObsCompetenciaInputDTO {

    private Long idObsCompetencia;
    @NotNull(message = "El id del Evaluador no puede estar vacio.")
    private Long evaluador;
    @NotNull(message = "El id de la Competencia no puede estar vacio.")
    private Long competencia;
    @NotEmpty(message = "La Observación de la competencia no puede estar vacia.")
    @Size(max =255, message = "La Observación no puede tener mas de 255 caracteres.")
    private String obsCompetencia;

    //Relaciones
    private Long evaluacionEmpleado;

}
