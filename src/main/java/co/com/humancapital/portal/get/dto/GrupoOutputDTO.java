package co.com.humancapital.portal.get.dto;

import co.com.humancapital.portal.get.entity.Grupo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 15/03/2020
 * Clase que permite modelar los datos de salida de la entidad Grupo.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GrupoOutputDTO {

    private Long idGrupo;
    private String nombre;
    private Map<String, Object> cliente;

    //Auditoria
    private boolean activo;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Grupo.
     * @param grupo Objeto Grupo.
     * @return Objeto GrupoOutputDTO
     */
    public static GrupoOutputDTO getDTO(Grupo grupo) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        GrupoOutputDTO grupoOutputDTO = modelMapper.map(grupo, GrupoOutputDTO.class);
        if(grupo.getCliente() != null){
            Map<String, Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",grupo.getCliente().getIdCliente());
            clienteData.put("nit", grupo.getCliente().getNit());
            clienteData.put("razonSocial",grupo.getCliente().getRazonSocial());
            clienteData.put("Activo",grupo.getCliente().isActivo());
            grupoOutputDTO.setCliente(clienteData);
        }
        return grupoOutputDTO;
    }

}
