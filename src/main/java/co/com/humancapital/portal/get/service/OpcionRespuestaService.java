package co.com.humancapital.portal.get.service;

import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.OpcionRespuestaInputDTO;
import co.com.humancapital.portal.get.dto.OpcionRespuestaOutputDTO;
import co.com.humancapital.portal.get.entity.OpcionRespuesta;
import co.com.humancapital.portal.get.repository.EvaluacionRepository;
import co.com.humancapital.portal.get.repository.OpcionRespuestaRepository;
import co.com.humancapital.portal.get.type.ConfigPregunta;
import co.com.humancapital.portal.service.ServiceInterfaceDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

import static co.com.humancapital.portal.util.ObjectMapperUtils.map;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/03/2020 Clase para la implementación de las diferentes reglas de negocio que se generen alrededor del
 * OpcionRespuesta.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class OpcionRespuestaService implements ServiceInterfaceDto<OpcionRespuestaOutputDTO, Long, OpcionRespuestaInputDTO> {

    private final OpcionRespuestaRepository opcionRespuestaRepository;
    private final EvaluacionService evaluacionService;
    private final EvaluacionRepository evaluacionRepository;

    @Override
    public OpcionRespuestaOutputDTO findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Opcion de Respuesta con id "+key+" no existe.");
        }
        return OpcionRespuestaOutputDTO.getDTO(opcionRespuestaRepository.findByIdOpcionRespuesta(key));
    }

    @Override
    public OpcionRespuestaOutputDTO create(OpcionRespuestaInputDTO entity) throws Exception {
        if (entity.getEvaluacion()==null || !evaluacionService.existeById(entity.getEvaluacion())) {
            throw new ResourceNotFoundException("La Evaluación con id "+ entity.getEvaluacion() +" no existe o no se envio el dato.");
        }
        if(!evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getEvaluacion()).getConfiguracion().equals(ConfigPregunta.PERSONALIZADA.toString())){
            throw new ResourceNotFoundException("La Evaluación no esta configurada para presentar opciones de respuesta personalizadas.");
        }
        OpcionRespuesta opcionRespuesta = new OpcionRespuesta();
        opcionRespuesta.setEtiqueta(entity.getEtiqueta().trim().toUpperCase());
        opcionRespuesta.setLeyenda(entity.getLeyenda().trim().toUpperCase());
        opcionRespuesta.setValor(entity.getValor());
        opcionRespuesta.setOpcionInvalida(entity.isOpcionInvalida());
        opcionRespuesta.setEvaluacion(evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getEvaluacion()));
        return OpcionRespuestaOutputDTO.getDTO(opcionRespuestaRepository.saveAndFlush(opcionRespuesta));
    }

    @Override
    public OpcionRespuestaOutputDTO update(OpcionRespuestaInputDTO entity) throws Exception {
        if (entity.getIdOpcionRespuesta()==null || !existeById(entity.getIdOpcionRespuesta())) {
            throw new ResourceNotFoundException("La Opcion de respuesta con id "+ entity.getIdOpcionRespuesta() +" no existe o no se envio el dato.");
        }
        if (entity.getEvaluacion()==null || !evaluacionService.existeById(entity.getEvaluacion())) {
            throw new ResourceNotFoundException("La Evaluación con id "+ entity.getEvaluacion() +" no existe o no se envio el dato.");
        }
        OpcionRespuesta opcionRespuesta = opcionRespuestaRepository.findByIdOpcionRespuesta(entity.getIdOpcionRespuesta());
        opcionRespuesta.setEtiqueta(entity.getEtiqueta().trim().toUpperCase());
        opcionRespuesta.setLeyenda(entity.getLeyenda().trim().toUpperCase());
        opcionRespuesta.setValor(entity.getValor());
        opcionRespuesta.setOpcionInvalida(entity.isOpcionInvalida());
        opcionRespuesta.setEvaluacion(evaluacionRepository.findByIdEvaluacionAndActivoTrue(entity.getEvaluacion()));
        return OpcionRespuestaOutputDTO.getDTO(opcionRespuestaRepository.saveAndFlush(opcionRespuesta));
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite eliminar una opción de respuesta asociado a una Evaluación.
     * @param key Id de la Opción de Respueta a eliminar.
     */
    public void delete(Long key) throws Exception {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La Opción de Respuesta con id "+key+" no existe o esta vacio.");
        }
        opcionRespuestaRepository.delete(opcionRespuestaRepository.findByIdOpcionRespuesta(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(opcionRespuestaRepository.findByIdOpcionRespuesta(key)==null){
            return false;
        }
        return opcionRespuestaRepository.existsById(key);
    }

    /**
     * Metodo que permite obtener las opciones de respuesta personalizadas de una Evaluación.
     * @param idEvaluacion Id de la Evaluacion a validar.
     * @return Lista de OpcionRespuestaOutputDTO.
     */
    public Page<OpcionRespuestaOutputDTO> findAllEvaluacion(Long idEvaluacion, Pageable pageable) throws Exception{
        if(idEvaluacion == null || !evaluacionService.existeById(idEvaluacion)) {
            throw new ResourceNotFoundException("La Evaluación con id "+ idEvaluacion +" no existe o no se envio en dato.");
        }
        List<OpcionRespuestaOutputDTO> opcionRespuestaOutputDTOS = new ArrayList<>();
        for (OpcionRespuesta opcionRespuesta : opcionRespuestaRepository.findAllByEvaluacion(idEvaluacion, pageable)) {
            OpcionRespuestaOutputDTO opcionRespuestaOutputDTO = OpcionRespuestaOutputDTO.getDTO(opcionRespuesta);
            opcionRespuestaOutputDTOS.add(opcionRespuestaOutputDTO);
        }
        return new PageImpl<>(opcionRespuestaOutputDTOS);
    }

    @Override
    public Page<OpcionRespuestaOutputDTO> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public OpcionRespuestaOutputDTO update(OpcionRespuestaInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public OpcionRespuestaOutputDTO create(OpcionRespuestaInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}
