package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.TipoOtroIngreso;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/12/2019
 */
@Repository
@Transactional
public interface TipoOtroIngresoRepository extends JpaRepository<TipoOtroIngreso, Long> {

    TipoOtroIngreso findAllByIdTipoOtroIngresoAndActivoTrue(Long idIngreso);
    Page<TipoOtroIngreso> findAllByActivoTrue(Pageable pageable);

}
