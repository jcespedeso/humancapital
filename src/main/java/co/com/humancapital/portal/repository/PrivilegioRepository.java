package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Privilegio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 16/12/2019
 */
@Repository
@Transactional
public interface PrivilegioRepository extends JpaRepository<Privilegio, String> {

    Privilegio findByCodigoPrivilegioEquals(String codigo);
//    Privilegio findByCodigoPrivilegioEquals(String codigo);
    Page<Privilegio> findAllByCategoriaContainingIgnoreCase(String categoria, Pageable pageable);
    Page<Privilegio> findByOrderByCategoriaAscNombrePrivilegioAsc(Pageable pageable);

    @Query(nativeQuery = true, value = "SELECT pri.* FROM \"humanCapitalSch\".\"USUARIO\" us\n" +
            "JOIN \"humanCapitalSch\".\"USUARIO_X_PERFIL\" uxp ON ( \"uxp\".\"USUARIO\" = \"us\".\"ID_USUARIO\")\n" +
            "JOIN \"humanCapitalSch\".\"PERFIL\" pro ON (\"pro\".\"ID_PERFIL\" = \"uxp\".\"PERFIL\")\n" +
            "JOIN \"humanCapitalSch\".\"PERFIL_X_PRIVILEGIO\" pxp ON ( \"pxp\".\"PERFIL\" = \"pro\".\"ID_PERFIL\" )\n" +
            "JOIN \"humanCapitalSch\".\"PRIVILEGIO\" pri ON ( \"pri\".\"CODIGO_PRIVILEGIO\" = \"pxp\".\"PRIVILEGIO\" )\n" +
            "WHERE us.\"USERNAME\" = :usuario AND pro.\"ACTIVO\" = true")
    Page<Privilegio> findPrivilegiosForUsername(@Param("usuario") String username, Pageable pageable);

//    @Query("select pri from Usuario us " +
//            "left join UsuarioXperfil uxp on uxp.usuario=us.idUsuario " +
//            "left join Perfil pro on pro.idPerfil=uxp.perfil " +
//            "left join PerfilXPrivilegio pxp on pxp.perfil=pro.idPerfil " +
//            "left join Privilegio pri on pri.codigoPrivilegio=pxp.privilegio " +
//            "where us.activo=true and pro.activo=true and us.username=:username")
//    Page<Privilegio> findPrivilegiosForUsername(@Param("usuario") String username, Pageable pageable);


    @Query(nativeQuery = true, value = "SELECT pri.* FROM \"humanCapitalSch\".\"USUARIO\" us\n" +
            "JOIN \"humanCapitalSch\".\"USUARIO_X_PERFIL\" uxp ON ( \"uxp\".\"USUARIO\" = \"us\".\"ID_USUARIO\")\n" +
            "JOIN \"humanCapitalSch\".\"PERFIL\" pro ON (\"pro\".\"ID_PERFIL\" = \"uxp\".\"PERFIL\")\n" +
            "JOIN \"humanCapitalSch\".\"PERFIL_X_PRIVILEGIO\" pxp ON ( \"pxp\".\"PERFIL\" = \"pro\".\"ID_PERFIL\" )\n" +
            "JOIN \"humanCapitalSch\".\"PRIVILEGIO\" pri ON ( \"pri\".\"CODIGO_PRIVILEGIO\" = \"pxp\".\"PRIVILEGIO\" )\n" +
            "WHERE us.\"USERNAME\" = :usuario AND pro.\"ACTIVO\" = true")
    List<Privilegio> findPrivilegiosForUsername(@Param("usuario") String username);

    @Query("select priv.categoria from Privilegio priv group by priv.categoria")
    Page<String> obtenerCategoriasPrivilegios(Pageable pageable);


}
