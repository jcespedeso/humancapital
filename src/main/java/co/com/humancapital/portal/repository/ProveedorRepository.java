package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Proveedor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 */
@Repository
@Transactional
public interface ProveedorRepository extends JpaRepository<Proveedor, Long> {

    Proveedor findByIdProveedorAndActivoTrue(Long idProveedor);
    Proveedor findByNitAndActivoTrue(String nit);
    Proveedor findByRazonSocialAndActivoTrue(String nombre);
    Page<Proveedor> findAllByActivoTrue(Pageable pageable);
    Page<Proveedor> findAll(Pageable pageable);
}
