package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 */
@Repository
@Transactional
public interface ProductoRepository extends JpaRepository<Producto, Long> {
    Producto findByIdProductoAndActivoTrue(Long idProducto);
    Boolean existsByIdProductoAndActivoTrue(Long idProducto);
    Page<Producto> findAllByActivoTrue(Pageable pageable);
    Page<Producto> findAllByProveedorAndActivoTrue(Proveedor proveedor, Pageable pageable);
    List<Producto> findAllByProveedorAndActivoTrue(Proveedor proveedor);

    Page<Producto> findAllByModuloCollectionAndActivoTrue(Modulo idModulo, Pageable pageable);

    @Query(value = "SELECT \"PRO\".* FROM \"humanCapitalSch\".\"MODULO\" AS \"MOD\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"MODULO_X_PRODUCTO\" AS \"MODPRO\" ON \"MODPRO\".\"MODULO\" = \"MOD\".\"ID_MODULO\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"PRODUCTO\" AS \"PRO\" ON \"PRO\".\"ID_PRODUCTO\" = \"MODPRO\".\"PRODUCTO\"\n" +
            "WHERE \"MOD\".\"ID_MODULO\"=:idModulo AND \"PRO\".\"ACTIVO\"=true AND \"MOD\".\"ACTIVO\"=true\n" +
            "ORDER BY \"PRO\".\"NOMBRE\" ASC",nativeQuery = true)
    List<Producto> obtenerProductoXModulo(@Param("idModulo") Long idModulo);

    Page<Producto> findAllByClienteCollectionAndActivoTrue(Cliente idCliente, Pageable pageable);

    @Query(value = "SELECT \"PRO\".* FROM \"humanCapitalSch\".\"CLIENTE\" AS \"CLI\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"CLIENTE_X_PRODUCTO\" AS \"CLIPRO\" ON \"CLIPRO\".\"CLIENTE\" = \"CLI\".\"ID_CLIENTE\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"PRODUCTO\" AS \"PRO\" ON \"PRO\".\"ID_PRODUCTO\" = \"CLIPRO\".\"PRODUCTO\"\n" +
            "WHERE \"CLI\".\"ID_CLIENTE\"=:idCliente AND \"PRO\".\"ACTIVO\"=true AND \"CLI\".\"ACTIVO\"=true\n" +
            "ORDER BY \"PRO\".\"NOMBRE\" ASC",nativeQuery = true)
    List<Producto> obtenerProductoXCliente(@Param("idCliente") Long idCliente);

    @Query(value = "SELECT \"PRO\".* FROM \"humanCapitalSch\".\"PRODUCTO\" AS \"PRO\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"SALARIO_FLEXIBLE_X_PRODUCTO\" AS \"SALFLEX_PRO\" ON \"SALFLEX_PRO\".\"PRODUCTO\" = \"PRO\".\"ID_PRODUCTO\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"SALARIO_FLEXIBLE\" AS \"SALFLEX\" ON \"SALFLEX\".\"ID_SALARIO_FLEXIBLE\" = \"SALFLEX_PRO\".\"SALARIO_FLEXIBLE\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"COMPENSACION\" AS \"COMP\" ON \"COMP\".\"ID_COMPENSACION\" = \"SALFLEX\".\"COMPENSACION\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"EMPLEADO\" AS \"EMP\" ON \"EMP\".\"ID_EMPLEADO\" = \"COMP\".\"EMPLEADO\"\n" +
            "WHERE \"EMP\".\"ID_EMPLEADO\"=:idEmpleado AND \"SALFLEX\".\"ESTADO_SIMULACION\"='3' AND \"PRO\".\"ACTIVO\"=true AND \"EMP\".\"ACTIVO\"=true\n" +
            "ORDER BY \"PRO\".\"NOMBRE\" ASC",nativeQuery = true)
    Page<Producto> obtenerProductoXEmpleado(@Param("idEmpleado") Long idEmpleado, Pageable pageable);

    Page<Producto> findAllBySalarioFlexibleXProductoCollectionAndActivoTrue(SalarioFlexibleXProducto idSalarioFlex, Pageable pageable);

    @Query(value = "SELECT \"PRO\".* FROM \"humanCapitalSch\".\"PRODUCTO\" AS \"PRO\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"SALARIO_FLEXIBLE_X_PRODUCTO\" AS \"SALFLEX_PRO\" ON \"SALFLEX_PRO\".\"PRODUCTO\" = \"PRO\".\"ID_PRODUCTO\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"SALARIO_FLEXIBLE\" AS \"SALFLEX\" ON \"SALFLEX\".\"ID_SALARIO_FLEXIBLE\" = \"SALFLEX_PRO\".\"SALARIO_FLEXIBLE\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"COMPENSACION\" AS \"COMP\" ON \"COMP\".\"ID_COMPENSACION\" = \"SALFLEX\".\"COMPENSACION\"\n" +
            "LEFT JOIN \"humanCapitalSch\".\"EMPLEADO\" AS \"EMP\" ON \"EMP\".\"ID_EMPLEADO\" = \"COMP\".\"EMPLEADO\"\n" +
            "WHERE \"EMP\".\"ID_EMPLEADO\"=:idEmpleado AND \"SALFLEX\".\"ESTADO_SIMULACION\"='3' AND \"PRO\".\"ACTIVO\"=true AND \"EMP\".\"ACTIVO\"=true\n" +
            "ORDER BY \"PRO\".\"NOMBRE\" ASC",nativeQuery = true)
    List<Producto> obtenerProductoXEmpleado(@Param("idEmpleado") Long idEmpleado);

}
