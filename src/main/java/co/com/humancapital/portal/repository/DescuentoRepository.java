package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Descuento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 13/12/2019
 */
@Repository
@Transactional
public interface DescuentoRepository extends JpaRepository<Descuento, Long> {

    Descuento findByIdDescuentoAndActivoTrue(Long id);
    Page<Descuento> findAllByClienteAndActivoTrue(Cliente cliente, Pageable pageable);
    List<Descuento> findAllByClienteAndActivoTrue(Cliente cliente);
    Page<Descuento> findAllByActivoTrue(Pageable pageable);

}
