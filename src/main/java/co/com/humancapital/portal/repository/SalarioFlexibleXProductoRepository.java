package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Producto;
import co.com.humancapital.portal.entity.SalarioFlexible;
import co.com.humancapital.portal.entity.SalarioFlexibleXProducto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 22/12/2019
 */
@Repository
@Transactional
public interface SalarioFlexibleXProductoRepository extends JpaRepository<SalarioFlexibleXProducto, Long> {

    SalarioFlexibleXProducto findByIdSalarioProducto(Long id);
    Page<SalarioFlexibleXProducto> findAllByProducto(Producto producto, Pageable pageable);
    Page<SalarioFlexibleXProducto> findAllBySalarioFlexible(SalarioFlexible salarioFlexible, Pageable pageable);
    List<SalarioFlexibleXProducto> findAllBySalarioFlexible(SalarioFlexible salarioFlexible);

    @Query("select pro.nombre, pro.idProducto, count (salflexpro.producto) from Producto pro " +
            "left join SalarioFlexibleXProducto salflexpro on salflexpro.producto = pro.idProducto " +
            "left join SalarioFlexible salflex on salflex.idSalarioFlexible = salflexpro.salarioFlexible " +
            "left join Compensacion comp on comp.idCompensacion = salflex.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "left join Cargo car on car.idCargo = emp.cargo " +
            "left join Departamento dep on dep.idDepartamento = car.departamento " +
            "left join Cliente cli on cli.idCliente = dep.cliente " +
            "where cli.activo=true and emp.activo=true and salflex.estadoSimulacion='3' and pro.activo=true and cli.idCliente=:idCliente group by pro.nombre, pro.idProducto")
    List<Object[]> contarProductosUtilizadosXCliente(@Param("idCliente") Long idCliente);

    @Query("select pro.nombre, pro.idProducto, count (salflexpro.producto) from Producto pro " +
            "left join SalarioFlexibleXProducto salflexpro on salflexpro.producto = pro.idProducto " +
            "left join SalarioFlexible salflex on salflex.idSalarioFlexible = salflexpro.salarioFlexible " +
            "left join Compensacion comp on comp.idCompensacion = salflex.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "left join Cargo car on car.idCargo = emp.cargo " +
            "left join Departamento dep on dep.idDepartamento = car.departamento " +
            "left join Cliente cli on cli.idCliente = dep.cliente " +
            "where cli.activo=true and emp.activo=true and salflex.estadoSimulacion='3' and pro.activo=true group by pro.nombre, pro.idProducto")
    List<Object[]> contarProductosUtilizados();

}
