package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.SalarioFlexible;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/12/2019
 */
@Repository
@Transactional
public interface SalarioFlexibleRepository extends JpaRepository<SalarioFlexible, Long> {

    SalarioFlexible findByIdSalarioFlexible(Long idSalarioFlexible);
    SalarioFlexible findByIdSalarioFlexibleAndEstadoSimulacion(Long idSalarioFlexible, String estadoSimulacion);
    List<SalarioFlexible> findAllByCompensacionAndEstadoSimulacion(Compensacion compensacion, String estadoSimulacion);

    @Query("select salflex from SalarioFlexible salflex " +
            "left join Compensacion comp on comp.idCompensacion = salflex.compensacion " +
            "where salflex.estadoSimulacion='1' or salflex.estadoSimulacion='2' or salflex.estadoSimulacion='3' " +
            "and comp.idCompensacion=:idCompensacion")
    SalarioFlexible findByCompensacionEstadosIniciales(@Param("idCompensacion") Long idCompensacion);

    @Query("select salflex.estadoSimulacion, count (salflex.estadoSimulacion) from SalarioFlexible salflex " +
            "left join Compensacion comp on comp.idCompensacion = salflex.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "left join Cargo car on car.idCargo = emp.cargo " +
            "left join Departamento dep on dep.idDepartamento = car.departamento " +
            "left join Cliente cli on cli.idCliente = dep.cliente " +
            "where cli.idCliente=:idCliente and salflex.estadoSimulacion=:estado and emp.activo=true group by salflex.estadoSimulacion")
    Map<String, BigInteger> contarFlexibilizacionesPorEstado(@Param("idCliente") Long idCliente, @Param("estado") String estado);

    @Query("select max (salflex.porcentajeFlexible) as Maximo, min (salflex.porcentajeFlexible) as Minimo, avg (salflex.porcentajeFlexible) as Promedio " +
            "from SalarioFlexible salflex " +
            "left join Compensacion comp on comp.idCompensacion = salflex.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "left join Cargo car on car.idCargo = emp.cargo " +
            "left join Departamento dep on dep.idDepartamento = car.departamento " +
            "left join Cliente cli on cli.idCliente = dep.cliente " +
            "where cli.idCliente=:idCliente and salflex.estadoSimulacion='3' and emp.activo=true")
    List<Object[]> obtenerMaxMinAvgPorcentajeFlex(@Param("idCliente") Long idCliente);

}
