package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.DatoTributario;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.OtroIngreso;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 16/12/2019
 */
@Repository
@Transactional
public interface DatoTributarioRepository extends JpaRepository<DatoTributario, Long> {

    Page<DatoTributario> findAll(Pageable pageable);
    DatoTributario findAllByEmpleado(Empleado empleado);
    DatoTributario findByIdDatoTributario(Long idDatoTributario);

}
