package co.com.humancapital.portal.repository;
import co.com.humancapital.portal.entity.Perfil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 16/12/2019
 */
@Repository
@Transactional
public interface PerfilRepository extends JpaRepository<Perfil, Long> {

    Perfil findByIdPerfilAndActivoTrue(Long idPerfil);
    Perfil findByNombreAndActivoTrue(String nombre);
    Page<Perfil> findAllByActivoTrue(Pageable pageable);

}
