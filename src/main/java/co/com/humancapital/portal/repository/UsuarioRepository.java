package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Alejandro Herrera Montilla
 * @date 20/11/2019
 */
@Repository
@Transactional
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    Usuario findByIdUsuarioAndActivoTrue(Long idUsuario);
    Usuario findByIdUsuarioAndActivoFalse(Long idUsuario);
    Optional<Usuario> findByUsernameAndActivoTrue (String username);
    Usuario findByEmpleadoAndActivoTrue(Empleado empleado);
    Usuario findByEmpleadoAndActivoFalse(Empleado empleado);
    Page<Usuario> findAllByActivoFalse(Pageable pageable);
    Page<Usuario> findAllByActivoTrue(Pageable pageable);
    Page<Usuario> findAllByClienteAndActivoTrue(Cliente cliente, Pageable pageable);
    List<Usuario> findAllByClienteAndActivoTrue(Cliente cliente);
    Page<Usuario> findAllByClienteAndActivoFalse(Cliente cliente, Pageable pageable);

}
