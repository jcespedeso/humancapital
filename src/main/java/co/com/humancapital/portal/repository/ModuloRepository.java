package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Modulo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 */
@Repository
@Transactional
public interface ModuloRepository extends JpaRepository<Modulo, Long> {

    Modulo findByIdModuloAndActivoTrue(Long idMedulo);
    Page<Modulo> findAllByActivoTrue(Pageable pageable);
    List<Modulo> findAllByActivoTrue();

}
