package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.OtroIngreso;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/12/2019
 */
@Repository
@Transactional
public interface OtroIngresoRepository extends JpaRepository<OtroIngreso, Long> {

    OtroIngreso findAllByIdOtroIngresoAndActivoTrue(Long idIngreso);
    Page<OtroIngreso> findAllByActivoTrue(Pageable pageable);
    Page<OtroIngreso> findAllByClienteAndActivoTrue(Cliente cliente, Pageable pageable);
    List<OtroIngreso> findAllByClienteAndActivoTrue(Cliente cliente);

}
