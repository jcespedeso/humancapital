package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.AsignacionParametro;
import co.com.humancapital.portal.entity.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 */
@Repository
@Transactional
public interface AsignacionParametroRepository extends JpaRepository<AsignacionParametro, Long> {

    AsignacionParametro findByIdAsignacionAndActivoTrue(Long idAsignacion);
    Page<AsignacionParametro> findAllByActivoTrue(Pageable pageable);
    Page<AsignacionParametro> findAllByClienteAndActivoTrue(Cliente cliente, Pageable pageable);
    List<AsignacionParametro> findAllByClienteAndActivoTrue(Cliente cliente);

}
