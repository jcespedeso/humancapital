package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.CompensacionXDescuento;
import co.com.humancapital.portal.entity.Descuento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 20/12/2019
 */
@Repository
@Transactional
public interface CompensacionXDescuentoRepository extends JpaRepository<CompensacionXDescuento, Long> {

    CompensacionXDescuento findByIdCompDescuento(Long id);
    Page<CompensacionXDescuento> findAllByCompensacion(Compensacion compensacion, Pageable pageable);
    Page<CompensacionXDescuento> findAllByDescuento(Descuento descuento, Pageable pageable);

    @Query("select compxdes from Descuento des " +
            "left join CompensacionXDescuento compxdes on compxdes.descuento = des.idDescuento " +
            "left join Compensacion comp on comp.idCompensacion = compxdes.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "where emp.activo=true and des.activo=true and comp.activo=true and emp.idEmpleado=:idEmpleado")
    Page<CompensacionXDescuento> obtenerDescuentoXEmpleado(@Param("idEmpleado") Long idEmpleado, Pageable pageable);

    @Query("select compxdes from Descuento des " +
            "left join CompensacionXDescuento compxdes on compxdes.descuento = des.idDescuento " +
            "left join Compensacion comp on comp.idCompensacion = compxdes.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "where emp.activo=true and des.activo=true and comp.activo=true and emp.idEmpleado=:idEmpleado")
    List<CompensacionXDescuento> obtenerDescuentoXEmpleado(@Param("idEmpleado") Long idEmpleado);

    @Query("select sum (compxdes.valor) as TOTAL_DESCUENTO " +
            "from CompensacionXDescuento compxdes " +
            "left join Compensacion comp on comp.idCompensacion = compxdes.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "where emp.activo=true and comp.activo=true and emp.idEmpleado=:idEmpleado")
    Double obtenerTotalDescuentoXEmpleado(@Param("idEmpleado") Long idEmpleado);

}
