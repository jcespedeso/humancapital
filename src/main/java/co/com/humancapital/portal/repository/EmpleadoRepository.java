package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Empleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 13/11/2019
 */
@Repository
@Transactional
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {

    Empleado findByIdEmpleadoAndActivoTrue(Long idEmpleado);
    Empleado findByIdEmpleadoAndActivoFalse(Long idEmpleado);
    Empleado findByCodigoAndActivoTrue(String codigo);
    Boolean existsByIdEmpleadoAndActivoTrue(Long idEmpledo);
    Page<Empleado> findAllByActivoTrue(Pageable pageable);
    List<Empleado> findAllByActivoTrue();
    Page<Empleado> findAllByActivoFalse(Pageable pageable);
    List<Empleado> findAllByNumeroDocumento(String documento);
    List<Empleado> findAllByCargoAndActivoTrue(Cargo cargo);
    Page<Empleado> findAllByCargoAndActivoTrue(Cargo cargo, Pageable pageable);

    @Query("select e from Empleado e " +
            "left join Cargo c on e.cargo = c.idCargo " +
            "left join Departamento d on c.departamento = d.idDepartamento " +
            "left join Cliente cl on d.cliente = cl.idCliente " +
            "where cl.activo=true and d.activo=true and c.activo=true and e.activo=true and cl.idCliente=:idCliente")
    Page<Empleado> obtenerEmpleadoXCliente(@Param("idCliente") long idCliente, Pageable pageable);

    @Query("select e from Empleado e " +
            "left join Cargo c on e.cargo = c.idCargo " +
            "left join Departamento d on c.departamento = d.idDepartamento " +
            "left join Cliente cl on d.cliente = cl.idCliente " +
            "where cl.activo=true and d.activo=true and c.activo=true and e.activo=true and cl.idCliente=:idCliente")
    List<Empleado> obtenerEmpleadoXCliente(@Param("idCliente") long idCliente);

    @Query("select e from Empleado e " +
            "left join Cargo c on e.cargo = c.idCargo " +
            "left join Departamento d on c.departamento = d.idDepartamento " +
            "left join Cliente cl on d.cliente = cl.idCliente " +
            "where cl.activo=true and d.activo=true and c.activo=true and e.activo=true and d.idDepartamento=:idDepto")
    Page<Empleado> obtenerEmpleadoXDepartamento(@Param("idDepto") long idDepto, Pageable pageable);

    @Query("select e from Empleado e " +
            "left join Cargo c on e.cargo = c.idCargo " +
            "left join Departamento d on c.departamento = d.idDepartamento " +
            "left join Cliente cl on d.cliente = cl.idCliente " +
            "where cl.activo=true and d.activo=true and c.activo=true and e.activo=true and cl.idCliente=:idCliente and e.idEmpleado in (select d1.jefe from Departamento d1)")
    Page<Empleado> obtenerEmpleadoJefes(@Param("idCliente") long idCliente, Pageable pageable);

    @Query("select e from Empleado e " +
            "left join Cargo c on e.cargo = c.idCargo " +
            "left join Departamento d on c.departamento = d.idDepartamento " +
            "left join Cliente cl on d.cliente = cl.idCliente " +
            "where cl.activo=true and d.activo=true and c.activo=true and e.activo=true and cl.idCliente=:idCliente and e.idEmpleado in (select d1.jefe from Departamento d1)")
    List<Empleado> obtenerEmpleadoJefes(@Param("idCliente") long idCliente);

    Long countEmpleadoByActivoTrueAndPuedeSimularTrueAndCargo(Cargo cargo);
}
