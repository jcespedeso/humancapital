package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.ClasificacionCliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 */
@Repository
@Transactional
public interface ClasificacionClienteRepository extends JpaRepository<ClasificacionCliente, Long> {
    ClasificacionCliente findByIdClasificacion(Long idClasificacion);
    Page<ClasificacionCliente> findAll(Pageable pageable);
    ClasificacionCliente findClasificacionClienteByTipoEspecifico(String tipoEspecifico);
}
