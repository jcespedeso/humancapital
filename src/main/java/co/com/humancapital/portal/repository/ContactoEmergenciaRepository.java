package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.ContactoEmergencia;
import co.com.humancapital.portal.entity.Empleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 */
@Repository
@Transactional
public interface ContactoEmergenciaRepository extends JpaRepository<ContactoEmergencia, Long> {

    ContactoEmergencia findByIdContacto(Long idContacto);
    Page<ContactoEmergencia> findAllByEmpleado(Empleado empleado, Pageable pageable);
    Page<ContactoEmergencia> findAll(Pageable pageable);

}
