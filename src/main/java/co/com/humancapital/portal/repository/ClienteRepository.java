package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 */
@Repository
@Transactional
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    Cliente findByIdClienteAndActivoTrue(Long idCliente);
    Cliente findByNitAndActivoTrue(String nit);
    Cliente findByNit(String nit);
    Cliente findByRazonSocial(String razonSocial);
    Page<Cliente> findAllByActivoTrue(Pageable pageable);
    List<Cliente> findAllByActivoTrue();
    Boolean existsByIdClienteAndActivoTrue(Long idCliente);

    @Query("select cl from Cliente cl " +
            "join Departamento d on cl.idCliente = d.cliente " +
            "join Cargo c on d.idDepartamento = c.departamento " +
            "where cl.activo=true and d.activo=true and c.activo=true and c.idCargo=:idCargo")
    Cliente obtenerClienteXCargo(@Param("idCargo") long idCargo);

    @Query("select cl from Cliente cl " +
            "join Departamento d on cl.idCliente = d.cliente " +
            "where cl.activo=true and d.activo=true and d.idDepartamento=:idDepartamento")
    Cliente obtenerClienteXDepartamento(@Param("idDepartamento") long idDepartamento);

    @Query("select cl from Cliente cl " +
            "left join Usuario u on cl.idCliente = u.cliente " +
            "where cl.activo=true and u.activo=true and u.username=:username")
    Cliente obtenerClienteXUsuario(@Param("username") String username);

}
