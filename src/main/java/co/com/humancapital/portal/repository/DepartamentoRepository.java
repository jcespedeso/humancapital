package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Departamento;
import co.com.humancapital.portal.entity.Empleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 */
@Repository
@Transactional
public interface DepartamentoRepository extends JpaRepository<Departamento, Long> {

    Departamento findByIdDepartamentoAndActivoTrue(Long idDepartamento);
    Page<Departamento> findAllByActivoTrue(Pageable pageable);
    Page<Departamento> findAllByClienteAndActivoTrue(Cliente cliente, Pageable pageable);
    List<Departamento> findAllByClienteAndActivoTrue(Cliente cliente);

    @Query(value = "SELECT * FROM \"humanCapitalSch\".\"connectby\"('\"humanCapitalSch\".\"DEPARTAMENTO\"', '\"DEPARTAMENTO\".\"ID_DEPARTAMENTO\"'," +
            " '\"DEPARTAMENTO\".\"DEPTO_PADRE\"','\"DEPARTAMENTO\".\"PESO_ORDEN\"', :inicio,  :nivel, '-')\n" +
            " AS ouput(DEPARTAMENTO bigint, PADRE bigint, NIVEL int, branch text, POSICION int)",nativeQuery = true)
    List<Object[]> organigramaDepartamentos(@Param("inicio") String inicio, @Param("nivel") int nivel);

    @Query(value = "select depto from Departamento depto " +
            "where depto.activo=true and depto.deptoPadre.idDepartamento = :idPadre")
    List<Departamento> obtenerDeptoHijo(@Param("idPadre") Long idPadre);

}
