package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.Empleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 */
@Repository
@Transactional
public interface CompensacionRepository extends JpaRepository<Compensacion, Long> {

    Compensacion findByIdCompensacionAndActivoTrue(Long idCompensacion);
    Page<Compensacion> findAllByActivoTrue(Pageable pageable);
    Page<Compensacion> findAllByEmpleado(Empleado empleado, Pageable pageable);
    List<Compensacion> findAllByEmpleado(Empleado empleado);
    Compensacion findByIdCompensacion(Long idCompensacion);
    Compensacion findByEmpleadoAndActivoTrue(Empleado empleado);

}
