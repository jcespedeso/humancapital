package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.NivelArl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/12/2019
 */
@Repository
@Transactional
public interface NivelArlRepository extends JpaRepository<NivelArl, Integer> {

    NivelArl findByCodigo(Integer codigo);

}
