package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Departamento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 */
@Repository
@Transactional
public interface CargoRepository extends JpaRepository<Cargo, Long> {

    Cargo findAllByIdCargoAndActivoTrue(Long idCargo);
    Page<Cargo> findAllByActivoTrue(Pageable pageable);
    Page<Cargo> findAllByDepartamentoAndActivoTrue(Departamento departamento,Pageable pageable);
    List<Cargo> findAllByDepartamentoAndActivoTrue(Departamento departamento);

    @Query("select c from Cargo c " +
            "left join Departamento d on c.departamento = d.idDepartamento " +
            "left join Cliente cl on d.cliente = cl.idCliente " +
            "where cl.activo=true and d.activo=true and c.activo=true and cl.idCliente=:idCliente")
    Page<Cargo> obtenerCargoXCliente(@Param("idCliente") long idCliente, Pageable pageable);

    @Query("select c from Cargo c " +
            "left join Departamento d on c.departamento = d.idDepartamento " +
            "left join Cliente cl on d.cliente = cl.idCliente " +
            "where cl.activo=true and d.activo=true and c.activo=true and cl.idCliente=:idCliente")
    List<Cargo> obtenerCargoXCliente(@Param("idCliente") long idCliente);

    @Query(value = "SELECT * FROM \"humanCapitalSch\".\"connectby\"('\"humanCapitalSch\".\"CARGO\"', '\"CARGO\".\"ID_CARGO\"'," +
            " '\"CARGO\".\"CARGO_PADRE\"','\"CARGO\".\"PESO_ORDEN\"', :inicio,  :nivel, '-')\n" +
            " AS ouput(CARGO bigint, PADRE bigint, NIVEL int, branch text, POSICION int)",nativeQuery = true)
    List<Object[]> organigramaCargos(@Param("inicio") String inicio, @Param("nivel") int nivel);

    @Query(value = "select car from Cargo car " +
            "where car.activo=true and car.cargoPadre.idCargo = :idPadre")
    List<Cargo> obtenerCargoHijo(@Param("idPadre") Long idPadre);

    Cargo findByCargoPadreAndActivoTrue(Cargo cargoPadre);

}
