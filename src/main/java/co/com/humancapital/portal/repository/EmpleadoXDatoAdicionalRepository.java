package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 04/02/2020
 */
@Repository
@Transactional
public interface EmpleadoXDatoAdicionalRepository extends JpaRepository<EmpleadoXDatoAdicional, Long> {

    EmpleadoXDatoAdicional findByIdEmpleadoDato(Long id);
    Page<EmpleadoXDatoAdicional> findAllByEmpleado(Empleado empleado, Pageable pageable);

}
