package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Multimedia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/11/2019
 */
@Repository
@Transactional
public interface MultimediaRepository extends JpaRepository<Multimedia, Long> {
    Multimedia findByIdMultimedia(Long idMultimedia);
}
