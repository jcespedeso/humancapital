package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.CompensacionXOtroIngreso;
import co.com.humancapital.portal.entity.OtroIngreso;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 19/12/2019
 */
@Repository
@Transactional
public interface CompensacionXOtroIngresoRepository extends JpaRepository<CompensacionXOtroIngreso, Long> {

    CompensacionXOtroIngreso findByIdCompOtroIngreso(Long id);
    Page<CompensacionXOtroIngreso> findAllByCompensacion(Compensacion compensacion, Pageable pageable);
    Page<CompensacionXOtroIngreso> findAllByOtroIngreso(OtroIngreso otroIngreso, Pageable pageable);

    @Query("select compxotroin from OtroIngreso otroin " +
            "left join CompensacionXOtroIngreso compxotroin on compxotroin.otroIngreso = otroin.idOtroIngreso " +
            "left join Compensacion comp on comp.idCompensacion = compxotroin.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "where emp.activo=true and otroin.activo=true and comp.activo=true and emp.idEmpleado=:idEmpleado")
    Page<CompensacionXOtroIngreso> obtenerOtroIngresoXEmpleado(@Param("idEmpleado") Long idEmpleado, Pageable pageable);


    @Query("select compxotroin from OtroIngreso otroin " +
            "left join CompensacionXOtroIngreso compxotroin on compxotroin.otroIngreso = otroin.idOtroIngreso " +
            "left join Compensacion comp on comp.idCompensacion = compxotroin.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "where emp.activo=true and otroin.activo=true and comp.activo=true and emp.idEmpleado=:idEmpleado")
    List<CompensacionXOtroIngreso> obtenerOtroIngresoXEmpleado(@Param("idEmpleado") Long idEmpleado);

    @Query("select sum (compxotroin.valor) as TOTAL_OTRO_INGRESO " +
            "from CompensacionXOtroIngreso compxotroin " +
            "left join Compensacion comp on comp.idCompensacion = compxotroin.compensacion " +
            "left join Empleado emp on emp.idEmpleado = comp.empleado " +
            "where emp.activo=true and comp.activo=true and emp.idEmpleado=:idEmpleado")
    Double obtenerTotalOtroIngresoXEmpleado(@Param("idEmpleado") Long idEmpleado);

}
