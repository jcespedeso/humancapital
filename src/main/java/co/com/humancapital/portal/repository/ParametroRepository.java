package co.com.humancapital.portal.repository;

import co.com.humancapital.portal.entity.Parametro;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 */
@Repository
@Transactional
public interface ParametroRepository extends JpaRepository<Parametro, Long> {

    Parametro findByIdParametroAndActivoTrue(Long idParametro);
    Page<Parametro> findAllByActivoTrue(Pageable pageable);

    @Query("select par from Parametro par " +
            "left join AsignacionParametro asigpar on asigpar.parametro=par.idParametro " +
            "left join Cliente cli on cli.idCliente = asigpar.cliente " +
            "where cli.activo=true and par.activo=true and cli.idCliente=:idCliente")
    Page<Parametro> obtenerParametroXCliente(@Param("idCliente") long idCliente, Pageable pageable);

}
