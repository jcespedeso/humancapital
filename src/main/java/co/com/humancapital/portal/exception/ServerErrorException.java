package co.com.humancapital.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Alejandro Herrera Montilla
 * @date 14/11/2019
 * Modela los mensajes de error que produce la aplicacion a nivel de servidor.
 */
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class ServerErrorException extends Exception {
    public static final String DESCRIPCION = "Error interno del servidor";

    public ServerErrorException(){
        super(DESCRIPCION);
    }

    public ServerErrorException(String detalleError){
        super(DESCRIPCION+" : "+detalleError);
    }


}
