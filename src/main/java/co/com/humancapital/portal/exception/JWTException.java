package co.com.humancapital.portal.exception;

/**
 * @author Alejandro Herrera Montilla
 * @project humancapital
 * @date 10/01/2020
 */
public class JWTException extends Exception {

    private static final long serialVersionUID = 2893141502868586192L;

    public JWTException(String message) {
        super(message);
    }

    public JWTException(String message, Throwable cause) {
        super(message, cause);
    }
}
