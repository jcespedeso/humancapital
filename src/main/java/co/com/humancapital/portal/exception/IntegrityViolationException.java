package co.com.humancapital.portal.exception;

/**
 * @author Alejandro Herrera Montilla
 * @date 17/02/2020
 * Modela los mensajes de error que produce la aplicacion cuando los datos indicados violan una
 * regla de integridad referencial de la base de datos.
 */
public class IntegrityViolationException extends RuntimeException {
    public static final String DESCRIPCION = "Regla de Integridad Referencial Violada";

    public IntegrityViolationException(){
        super(DESCRIPCION);
    }

    public IntegrityViolationException(String detalleError){
        super(DESCRIPCION+" : "+detalleError);
    }


}
