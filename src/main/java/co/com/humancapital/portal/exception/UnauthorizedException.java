package co.com.humancapital.portal.exception;

/**
 * @author Alejandro Herrera Montilla
 * @project humancapital
 * @date 10/01/2020
 */
public class UnauthorizedException extends RuntimeException {
    public static final String DESCRIPCION = "No autorizado";

    public UnauthorizedException(){
        super(DESCRIPCION);
    }

    public UnauthorizedException(String detalleError){
        super(DESCRIPCION+" : "+detalleError);
    }
}
