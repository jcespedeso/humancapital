package co.com.humancapital.portal.exception;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/11/2019
 * Modela los mensajes de error que produce la aplicacion cuando el archivo solicitado no existeKey.
 */
public class StorageFileNotFoundException extends Exception {
    public static final String DESCRIPCION = "El archivo solicitado no existe";

    public StorageFileNotFoundException(){
        super(DESCRIPCION);
    }

    public StorageFileNotFoundException(String detalleError){
        super(DESCRIPCION+" : "+detalleError);
    }


}
