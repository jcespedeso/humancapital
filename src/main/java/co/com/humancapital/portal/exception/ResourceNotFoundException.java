package co.com.humancapital.portal.exception;

/**
 * @author Alejandro Herrera Montilla
 * @date 14/11/2019
 * Modela los mensajes de error que produce la aplicacion cuando el dato solicitado no existeKey.
 */
public class ResourceNotFoundException extends RuntimeException {
    public static final String DESCRIPCION = "Recurso sin resultados";

    public ResourceNotFoundException(){
        super(DESCRIPCION);
    }

    public ResourceNotFoundException(String detalleError){
        super(DESCRIPCION+" : "+detalleError);
    }


}
