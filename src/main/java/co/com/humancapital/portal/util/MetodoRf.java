package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/12/2019
 * Clase utilitaria para exponer los metodos para el calculo de la retencion en la fuente a un empleado, que se pueden utilizar en el sistema.
 */
public class MetodoRf {
    public static final String METODO_1 ="Metodo 1";
    public static final String METODO_2 ="Metodo 2";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1, METODO_1);
        map.put(2, METODO_2);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(Integer key){
        if(listar().containsKey(key)){
            return true;
        }
        return false;
    }
}
