package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/12/2019
 * Clase utilitaria para exponer las condiciones de aplicacion del modelo RFI en un empleado, que se pueden utilizar en el sistema.
 */
public class CondicionEmpleado {
    public static final String NORMAL ="Normal";
    public static final String CASO_ESPECIAL ="Caso Especial";
    public static final String LIMITE_IBC ="Tiene Limite en IBC";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1, NORMAL);
        map.put(2, CASO_ESPECIAL);
        map.put(3, LIMITE_IBC);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
