package co.com.humancapital.portal.util;

import co.com.humancapital.portal.config.rfi.loader.ExcelColumnHeaderDefinition;
import co.com.humancapital.portal.rfi.type.ExcelColumnType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.function.Function;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationConstraint.OperatorType;
import org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@Slf4j
public final class PoiUtil {
    
    private static final int UNRANGED = -1;
    private static int MAX_LETTERS = 26;
    private static int MIN_ASCII_LETTER = 65;
    private static DataFormatter formatter = new DataFormatter();
    
    private PoiUtil() {
    }
    
    public static CellStyle getHeaderStyle(XSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setFillForegroundColor(IndexedColors.GREY_80_PERCENT.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFont(getHeaderFont(workbook));
        
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setLeftBorderColor(IndexedColors.WHITE.getIndex());
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
        cellStyle.setBorderTop(BorderStyle.MEDIUM_DASHED);
        cellStyle.setTopBorderColor(IndexedColors.WHITE.getIndex());
        return cellStyle;
    }
    
    public static CellStyle getCellErrorStyle(XSSFWorkbook workbook) {
        return getCellFeedbackStyle(workbook, IndexedColors.CORAL);
    }
    
    public static CellStyle getCellSuccessStyle(XSSFWorkbook workbook) {
        return getCellFeedbackStyle(workbook, IndexedColors.LIME);
    }
    
    private static CellStyle getCellFeedbackStyle(XSSFWorkbook workbook, IndexedColors color) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setFillForegroundColor(color.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFont(getHeaderFont(workbook));
        return cellStyle;
    }
    
    public static Font getHeaderFont(XSSFWorkbook workbook) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 14);
        font.setColor(IndexedColors.WHITE.getIndex());
        return font;
    }
    
    public static void addNumberRangeValidatorToColumn(XSSFSheet sheet, int columnIndex, int min, int max) {
        XSSFDataValidationHelper validatorHelper = new XSSFDataValidationHelper(sheet);
        DataValidationConstraint validator = validatorHelper.createNumericConstraint(
                ValidationType.INTEGER, OperatorType.BETWEEN, Integer.toString(min), Integer.toString(max));
        applyValidatorToColumn(sheet, columnIndex, validatorHelper, validator, "Valor numérico fuera de rango");
    }
    
    public static void addDateValidatorToColumn(XSSFSheet sheet, int columnIndex) {
        XSSFDataValidationHelper validatorHelper = new XSSFDataValidationHelper(sheet);
        DataValidationConstraint validator = validatorHelper.createDateConstraint(OperatorType.GREATER_THAN,
                Double.toString(DateUtil.getExcelDate(LocalDate.MIN)), "", "yyyy/MM/dd");
        applyValidatorToColumn(sheet, columnIndex, validatorHelper, validator,
                "La columna requiere una fecha (yyyy/MM/dd)");
    }
    
    public static void addNumberValidatorToColumn(XSSFSheet sheet, int columnIndex) {
        Function<String, String> validatorFormula = columnLetter -> getNumberValidatorFormula(columnLetter, 1);
        String errorMessage = "La columna requiere un número válido";
        addFormulaValidatorToColumn(sheet, columnIndex, validatorFormula, errorMessage);
    }
    
    public static void addEmailValidatorToColumn(XSSFSheet sheet, int columnIndex) {
        Function<String, String> validatorFormula = columnLetter -> getEmailValidatorFormula(columnLetter, 1);
        String errorMessage = "La columna requiere un email";
        addFormulaValidatorToColumn(sheet, columnIndex, validatorFormula, errorMessage);
    }
    
    private static void addFormulaValidatorToColumn(XSSFSheet sheet, int columnIndex,
            Function<String, String> formulaValidatorSupplier, String errorMessage) {
        String columnLetter = getLetterFromColumnIndex(columnIndex);
        String validatorFormula = formulaValidatorSupplier.apply(columnLetter);
        
        XSSFDataValidationHelper validatorHelper = new XSSFDataValidationHelper(sheet);
        DataValidationConstraint validator = validatorHelper.createCustomConstraint(validatorFormula);
        applyValidatorToColumn(sheet, columnIndex, validatorHelper, validator, errorMessage);
    }
    
    public static void addListValidatorToColumn(XSSFSheet sheet, int columnIndex, Collection<String> values) {
        XSSFDataValidationHelper validatorHelper = new XSSFDataValidationHelper(sheet);
        DataValidationConstraint validator = validatorHelper
                .createExplicitListConstraint(values.toArray(new String[0]));
        applyValidatorToColumn(sheet, columnIndex, validatorHelper, validator, "Valor debe ser una de las opciones");
    }
    
    private static void applyValidatorToColumn(XSSFSheet sheet, int columnIndex,
            XSSFDataValidationHelper validatorHelper, DataValidationConstraint validator, String errorMessage) {
        CellRangeAddressList cellRange = new CellRangeAddressList(UNRANGED, UNRANGED, columnIndex, columnIndex);
        DataValidation validation = validatorHelper.createValidation(validator, cellRange);
        validation.setShowErrorBox(true);
        validation.setErrorStyle(DataValidation.ErrorStyle.STOP);
        validation.createErrorBox("Error de validación", errorMessage);
        sheet.addValidationData(validation);
    }
    
    private static String getEmailValidatorFormula(String columnLetter, int row) {
        return "=AND(FIND(\"@\"," + columnLetter + row
                + "),FIND(\".\"," + columnLetter + row
                + "),ISERROR(FIND(\" \"," + columnLetter + row + ")))";
    }
    
    private static String getNumberValidatorFormula(String columnLetter, int row) {
        return "=ISNUMBER(" + columnLetter + row + ")";
    }
    
    public static String getLetterFromColumnIndex(int columnIndex) {
        String columnLetter = "";
        int div = columnIndex + 1;
        int mod;
        
        while (div > 0) {
            mod = (div - 1) % MAX_LETTERS;
            columnLetter = (char) (MIN_ASCII_LETTER + mod) + columnLetter;
            div = ((div - mod) / MAX_LETTERS);
        }
        
        return columnLetter;
    }
    
    public static boolean isValidWorkbook(Workbook workbook, String defaultSheetName) {
        return workbook != null
                && workbook.getNumberOfSheets() > 0
                && workbook.getSheetAt(0) != null
                && workbook.getSheet(defaultSheetName) != null
                && workbook.getSheetAt(0).getLastRowNum() > 0;
    }
    
    public static Object getCellValue(XSSFCell cell, ExcelColumnHeaderDefinition definition) {
        switch (definition.getFieldType()) {
            case ITEMS:
                return getCellValue(cell, definition.getItemType());
            default:
                return getCellValue(cell, definition.getFieldType());
        }
    }
    
    private static Object getCellValue(XSSFCell cell, ExcelColumnType type) {
        switch (type) {
            case TEXT:
            case EMAIL:
                return formatter.formatCellValue(cell);
            case MONEY:
            case NUMBER_AGE:
            case NUMBER:
                return cell.getNumericCellValue();
            case NUMBER_INTEGER:
                return (int) cell.getNumericCellValue();
            case BOOLEAN:
                return BooleanUtil.parse(cell.getStringCellValue());
            case DATE:
                return cell.getDateCellValue();
            default:
                throw new IllegalArgumentException("Type [" + type + "] not supported");
        }
    }
    
    public static void applyColumnValidator(XSSFSheet sheet, ExcelColumnHeaderDefinition columnHeader) {
        switch (columnHeader.getFieldType()) {
            case EMAIL:
                addEmailValidatorToColumn(sheet, columnHeader.getColumnIndex());
                break;
            case NUMBER:
            case MONEY:
            case NUMBER_INTEGER:
                addNumberValidatorToColumn(sheet, columnHeader.getColumnIndex());
                break;
            case NUMBER_AGE:
                addNumberRangeValidatorToColumn(sheet, columnHeader.getColumnIndex(), 0, 200);
                break;
            case ITEMS:
            case BOOLEAN:
                addListValidatorToColumn(sheet, columnHeader.getColumnIndex(), columnHeader.getItems());
                break;
            case DATE:
                addDateValidatorToColumn(sheet, columnHeader.getColumnIndex());
                break;
            case TEXT:
                break;
            default:
                log.warn("::: Validator not supported [{}]", columnHeader.getFieldType());
        }
    }
    
    public static XSSFRow getOrCreateRow(XSSFSheet sheet, int rowIndex) {
        return sheet.getRow(rowIndex) == null ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);
    }
    
    public static XSSFCell getOrCreateCell(XSSFRow row, int columnIndex) {
        return row.getCell(columnIndex) == null ? row.createCell(columnIndex) : row.getCell(columnIndex);
    }
    
    public static XSSFCell createHeaderCell(XSSFRow row,
            ExcelColumnHeaderDefinition headerDefinition, CellStyle headerStyle) {
        int columnIndex = headerDefinition.getColumnIndex();
        XSSFCell currentCell = row.createCell(columnIndex, CellType.STRING);
        currentCell.setCellStyle(headerStyle);
        currentCell.setCellValue(headerDefinition.getName());
        applyColumnValidator(row.getSheet(), headerDefinition);
        row.getSheet().autoSizeColumn(columnIndex);
        return currentCell;
    }
    
    public static byte[] writeWorkbook(Workbook workbook) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            workbook.write(baos);
            workbook.close();
            return baos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void addComment(XSSFCell cell, String text) {
        XSSFRow row = cell.getRow();
        XSSFSheet sheet = row.getSheet();
        CreationHelper factory = sheet.getWorkbook().getCreationHelper();
        Drawing drawing = sheet.createDrawingPatriarch();
        
        // When the comment box is visible, have it show in a 1x3 space
        ClientAnchor anchor = factory.createClientAnchor();
        anchor.setCol1(cell.getColumnIndex());
        anchor.setCol2(cell.getColumnIndex() + 3);
        anchor.setRow1(row.getRowNum());
        anchor.setRow2(row.getRowNum() + 10);
        
        // Create the comment and set the text+author
        RichTextString richText = factory.createRichTextString(text);
        Comment comment = drawing.createCellComment(anchor);
        comment.setString(richText);
        cell.setCellComment(comment);
    }
}
