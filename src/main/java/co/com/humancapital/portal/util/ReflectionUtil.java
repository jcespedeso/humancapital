package co.com.humancapital.portal.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.ReflectionUtils;

public final class ReflectionUtil {
    private ReflectionUtil() {}
    
    public static List<Field> getFieldsWithAnnotation(Class<?> clazz, Class<? extends Annotation> annotation) {
        List<Field> fieldList = new ArrayList<>();
        Class<?> currentClass = clazz;
        
        while (currentClass != null) {
            for (Field field : currentClass.getDeclaredFields()) {
                if (field.isAnnotationPresent(annotation)) {
                    fieldList.add(field);
                }
            }
            
            currentClass = currentClass.getSuperclass();
        }
        
        return fieldList;
    }
    
    public static void setField(Class<?> clazz, Object entity, String fieldName, Object fieldValue) {
        Field field = ReflectionUtils.findField(clazz, fieldName);
        field.setAccessible(true);
        ReflectionUtils.setField(field, entity, fieldValue);
    }
}
