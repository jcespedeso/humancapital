package co.com.humancapital.portal.util;

/**
 * @author Alejandro Herrera Montilla
 * @date 14/03/2020
 * Clase utilitaria para transformar cadenas de String que requieren ser validadas.
 */
public class TransformarString {

    public static String transformar(String texto){
        return texto.replaceAll("^\\s*","");
    }

    public static String transformarArray(Object[] array){
        StringBuilder value= new StringBuilder();
        for (int i = 0; i < array.length; i++){
            System.out.println("array[i].toString() = " + array[i].toString());
            value.append(array[i].toString());
            value.append(" ");
        }
        return value.toString().trim();
    }


}
