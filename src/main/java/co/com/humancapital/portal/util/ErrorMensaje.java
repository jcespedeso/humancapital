package co.com.humancapital.portal.util;

import co.com.humancapital.portal.dto.AbstractOutput;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Alejandro Herrera Montilla
 * @date 20/11/2019
 * Define los atributos que tendran las respuestas que se generan como Excepciones.
 */
public class ErrorMensaje extends AbstractOutput  {
    private String error;
    private String path;

    public ErrorMensaje(Exception exception, int status, String path) {
        this(exception.getClass().getSimpleName(), status,exception.getMessage(), path);
    }

    public ErrorMensaje(Exception exception, int status, String message,  String path) {
        this(exception.getClass().getSimpleName(), status,message, path);
    }

    public ErrorMensaje(String exception, int status, String message, String path) {
        this.error = exception;
        super.setStatus(status);
        super.setMessage(message);
        this.path = path;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
