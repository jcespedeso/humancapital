package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/12/2019 Clase utilitaria para exponer los tipos de contrato de una empresa, que se pueden utilizar en el
 * sistema.
 */
public class TipoContrato {
    
    public static final String OBRA_LABOR = "Obra o Labor";
    public static final String TERMINO_FIJO = "Término Fijo";
    public static final String TERMINO_INDEFINIDO = "Término Indefinido";
    public static final String APRENDIZAJE = "Aprendizaje";
    
    public static Map<Integer, String> listar() {
        Map<Integer, String> map = new TreeMap<>();
        map.put(1, OBRA_LABOR);
        map.put(2, TERMINO_FIJO);
        map.put(3, TERMINO_INDEFINIDO);
        map.put(4, APRENDIZAJE);
        return map;
    }
    
    /**
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     * @author Alejandro Herrera Montilla Permite validar si el parametro esta contenido dentro del mapa de elementos
     * establecidos como atributos de la clase.
     */
    public static Boolean existeKey(String key) {
        if (listar().containsKey(new Integer(key))) {
            return true;
        }
        return false;
    }
}
