package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/12/2019
 * Clase utilitaria para exponer los tipos de regimen pensional existentes, que se pueden utilizar en el sistema.
 */
public class RegimenPension {
    public static final String OBRA_LABOR ="Privado";
    public static final String COLPENSIONES ="Colpensiones";
    public static final String PENSIONADO ="Pensionado";
    public static final String EXPATRIADO ="Expatriado";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1, OBRA_LABOR);
        map.put(2, COLPENSIONES);
        map.put(3, PENSIONADO);
        map.put(4, EXPATRIADO);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
