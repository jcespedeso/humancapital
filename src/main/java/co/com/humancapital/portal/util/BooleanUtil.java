package co.com.humancapital.portal.util;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

public final class BooleanUtil {
    private static final String YES_SPANISH = "si";
    
    public static boolean parse(String booleanMultiLanguage) {
        booleanMultiLanguage = StringUtils.lowerCase(booleanMultiLanguage);
        return YES_SPANISH.equals(booleanMultiLanguage) ? Boolean.TRUE :
              BooleanUtils.toBoolean(booleanMultiLanguage);
    }
}
