package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 29/12/2019
 * Clase utilitaria para exponer los parentescos posibles entre un empleado y su contacto de emergencia, que se pueden utilizar en el sistema.
 */
public class Parentesco {
    public static final String CONYUGE ="Cónyuge";
    public static final String MADRE ="Madre";
    public static final String PADRE ="Padre";
    public static final String HERMANO ="Hermano(a)";
    public static final String PRIMO ="Primo(a)";
    public static final String TIO ="Tio(a)";
    public static final String CUNADO ="Cuñado(a)";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1,CONYUGE);
        map.put(2,MADRE);
        map.put(3,PADRE);
        map.put(4,HERMANO);
        map.put(5,PRIMO);
        map.put(6,TIO);
        map.put(7,CUNADO);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
