package co.com.humancapital.portal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase utilitaria para exponer el Idioma principal de una persona, que se pueden utilizar en el sistema.
 */
public class Idioma {
    public static final String ALEMAN ="Alemán";
    public static final String ARABE ="Árabe";
    public static final String CHINO ="Chino";
    public static final String DANES ="Danés";
    public static final String ESPANOL ="Español";
    public static final String FRANCES ="Francés";
    public static final String HINDU ="Hindú";
    public static final String HOLANDES ="Holandés";
    public static final String INDONESIO ="Indonesio";
    public static final String INGLES ="Ingles";
    public static final String ITALIANO ="Italiano";
    public static final String JAPONES ="Japonés";
    public static final String MANDARIN ="Mandarin";
    public static final String NORUEGO ="Noruego";
    public static final String POLACO ="Polaco";
    public static final String PORTUGUES ="Portugués";
    public static final String RUSO ="Ruso";
    public static final String SUECO ="Sueco";
    public static final String TURCO ="Turco";


    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1,ALEMAN);
        map.put(2,ARABE);
        map.put(3,CHINO);
        map.put(4,DANES);
        map.put(5,ESPANOL);
        map.put(6,FRANCES);
        map.put(7,HINDU);
        map.put(8,HOLANDES);
        map.put(9,INDONESIO);
        map.put(10,INGLES);
        map.put(11,ITALIANO);
        map.put(12,JAPONES);
        map.put(13,MANDARIN);
        map.put(14,NORUEGO);
        map.put(15,POLACO);
        map.put(16,PORTUGUES);
        map.put(17,RUSO);
        map.put(18,SUECO);
        map.put(19,TURCO);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
