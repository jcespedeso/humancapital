package co.com.humancapital.portal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase utilitaria para exponer el sector al que puede pertenecer una empresa, que se pueden utilizar en el sistema.
 */
public class SectorEmpresa {
    public static final String MANUFACTURA ="Manufactura";
    public static final String SERVICIOS ="Servicios";
    public static final String COMERCIO ="Comercio";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1,MANUFACTURA);
        map.put(2,SERVICIOS);
        map.put(3,COMERCIO);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
