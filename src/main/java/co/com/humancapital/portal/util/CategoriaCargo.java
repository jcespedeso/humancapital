package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase utilitaria para exponer los estados civiles de una persona, que se pueden utilizar en el sistema.
 */
@Deprecated
public class CategoriaCargo {

    public static final String ESTRATEGICO = "Estrategico";
    public static final String OPERACIONAL = "Operacional";
    public static final String TACTICO = "Táctico";

    public static Map<Integer, String> listar() {
        Map<Integer, String> map = new TreeMap<>();
        map.put(1, ESTRATEGICO);
        map.put(2, OPERACIONAL);
        map.put(3, TACTICO);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
