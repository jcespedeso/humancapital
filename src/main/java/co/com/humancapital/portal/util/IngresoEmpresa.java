package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 06/02/2020
 * Clase utilitaria para exponer el rango de ingresos aplicables a una la empresa y que se pueden utilizar en el sistema.
 */
public class IngresoEmpresa {
    public static final String ING_0_5000 = "0 a 5000";
    public static final String ING_5001_50000 = "5001 a 50.000";
    public static final String ING_50001_200000 = "50.001 a 200.000";
    public static final String ING_200001_500000 = "200.001 a 500.000";
    public static final String ING_MAS_500000 = "Más de 500.000";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1,ING_0_5000);
        map.put(2,ING_5001_50000);
        map.put(3,ING_50001_200000);
        map.put(4,ING_200001_500000);
        map.put(5,ING_MAS_500000);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
