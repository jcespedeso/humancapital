package co.com.humancapital.portal.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 06/02/2020
 * Clase utilitaria para exponer las opciones de Presencia aplicables a una la empresa y que se pueden utilizar en el sistema.
 */
public class PresenciaEmpresa {
    public static final String LOCAL = "Local"; //Presencia solo en una ciudad.
    public static final String NACIONAL = "Nacional"; //Presencia en más de una ciudad.
    public static final String MULTINACIONAL = "Multinacional"; //Empresa con presencia en varios países.

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1,LOCAL);
        map.put(2,NACIONAL);
        map.put(3,MULTINACIONAL);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 18/02/2020
     * Permite obtener un key y value especifico contenido en el mapa de constantes.
     * @param id key del valor del map.
     * @return Map con el key y value especifico.
     */
    public static Map<String, Object> get(Integer id){
        Map<String, Object> data = new LinkedHashMap<>();
        for (Map.Entry<Integer, String> entry : listar().entrySet()) {
            if (entry.getKey().equals(id)){
                data.put("id", entry.getKey());
                data.put("value", entry.getValue());
            }
        }
        return data;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
