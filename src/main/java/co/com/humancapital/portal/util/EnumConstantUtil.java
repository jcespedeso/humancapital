package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.Objects;

public final class EnumConstantUtil {
    
    public static Integer getIntIdFromValue(String value, Map<Integer, String> values) {
        return values.entrySet().stream()
                .filter(entry -> Objects.equals(values.get(entry.getKey()), value))
                .map(entry -> entry.getKey())
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("::: Id not found for value in given map"));
    }
    
    public static String getIntStringIdFromValue(String value, Map<Integer, String> values) {
        return Integer.toString(getIntIdFromValue(value, values));
    }
    
    public static String getStringIdFromValue(String value, Map<String, String> values) {
        return values.entrySet().stream()
                .filter(entry -> Objects.equals(values.get(entry.getKey()), value))
                .map(entry -> entry.getKey())
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("::: Id not found for value in given map"));
    }
}
