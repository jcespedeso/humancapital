package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 13/11/2019
 * Clase utilitaria para exponer los tipos de documentos que se pueden utilizar en el sistema.
 */
public class TipoDocumento {
    public static final String CEDULA ="Cédula de Ciudadania";
    public static final String TARJETA_EXTRANGERIA ="Tarjeta de Extranjería";
    public static final String CEDULA_EXTRANGERIA ="Cédula de Extranjería";
    public static final String PASAPORTE ="Pasaporte";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(13,CEDULA);
        map.put(21,TARJETA_EXTRANGERIA);
        map.put(22,CEDULA_EXTRANGERIA);
        map.put(41,PASAPORTE);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa.
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el Map contiene el valor de la variable, de lo contrario retorna @false.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
