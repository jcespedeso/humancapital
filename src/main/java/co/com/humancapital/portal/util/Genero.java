package co.com.humancapital.portal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase utilitaria para exponer el Genero de una persona, que se pueden utilizar en el sistema.
 */
public class Genero {
    public static final String MASCULINO ="Masculino";
    public static final String FEMENINO ="Femenino";

    public static Map<String, String> listar(){
        Map<String,String> map = new TreeMap<>();
        map.put("M",MASCULINO);
        map.put("F",FEMENINO);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(key.trim().toUpperCase())){
            return true;
        }
        return false;
    }
}
