package co.com.humancapital.portal.util;

import co.com.humancapital.portal.survey.dto.ChoiceQuestionDTO;
import co.com.humancapital.portal.survey.dto.QuestionDTO;
import co.com.humancapital.portal.survey.dto.RatingQuestionDTO;
import co.com.humancapital.portal.survey.dto.TextQuestionDTO;
import co.com.humancapital.portal.survey.entity.ChoiceQuestion;
import co.com.humancapital.portal.survey.entity.Question;
import co.com.humancapital.portal.survey.entity.RatingQuestion;
import co.com.humancapital.portal.survey.entity.TextQuestion;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;

@UtilityClass
public final class ObjectMapperUtils {
    
    private static final ModelMapper modelMapper;
    
    static {
        modelMapper = new ModelMapper();
        configure(modelMapper);
    }
    
    public static <S, D> D map(final S entityToMap, Class<D> expectedClass) {
        return modelMapper.map(entityToMap, expectedClass);
    }
    
    public static <S, D> D map(final S source, D destination) {
        modelMapper.map(source, destination);
        return destination;
    }
    
    public static <S, D> List<D> mapAll(final Collection<S> entityList, Class<D> expectedClass) {
        return entityList.stream()
                .map(entity -> map(entity, expectedClass))
                .collect(Collectors.toList());
    }
    
    public static void configure(ModelMapper modelMapper) {
        modelMapper.createTypeMap(TextQuestionDTO.class, Question.class)
                .setConverter(getQuestionDtoToQuestionConverter());
        modelMapper.createTypeMap(ChoiceQuestionDTO.class, Question.class)
                .setConverter(getQuestionDtoToQuestionConverter());
        modelMapper.createTypeMap(RatingQuestionDTO.class, Question.class)
                .setConverter(getQuestionDtoToQuestionConverter());
    
        modelMapper.createTypeMap(TextQuestion.class, QuestionDTO.class)
                .setConverter(getQuestionToQuestionDtoConverter());
        modelMapper.createTypeMap(ChoiceQuestion.class, QuestionDTO.class)
                .setConverter(getQuestionToQuestionDtoConverter());
        modelMapper.createTypeMap(RatingQuestion.class, QuestionDTO.class)
                .setConverter(getQuestionToQuestionDtoConverter());
    }
    
    private static <T extends QuestionDTO> Converter<T, Question> getQuestionDtoToQuestionConverter() {
        return context -> {
            QuestionDTO source = context.getSource();
            if (source == null) {
                return null;
            } else if (source instanceof TextQuestionDTO){
                return map(source, TextQuestion.class);
            } else if (source instanceof ChoiceQuestionDTO){
                return map(source, ChoiceQuestion.class);
            } else if (source instanceof RatingQuestionDTO){
                return map(source, RatingQuestion.class);
            } else {
                throw new IllegalArgumentException("::: QuestionDTO type not mapped");
            }
        };
    }
    
    private static <T extends Question> Converter<T, QuestionDTO> getQuestionToQuestionDtoConverter() {
        return context -> {
            Question source = context.getSource();
            if (source == null) {
                return null;
            } else if (source instanceof TextQuestion){
                return map(source, TextQuestionDTO.class);
            } else if (source instanceof ChoiceQuestion){
                return map(source, ChoiceQuestionDTO.class);
            } else if (source instanceof RatingQuestion){
                return map(source, RatingQuestionDTO.class);
            } else {
                throw new IllegalArgumentException("::: Question type not mapped");
            }
        };
    }
}