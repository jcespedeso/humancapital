package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/12/2019
 * Clase utilitaria para exponer los estados posibles del proceso de simulación, que se pueden utilizar en el sistema.
 */
public class EstadoSimulacion {
    //Estado para cuando el Empleado ha decidido flexibilizar su salario, pero no ha confirmado la simulación.
    //Se puede utilizar para guardar la simulación para que luego el Empleado la pueda retomar.
    public static final String SIMULADA ="Simulada";
    //Estado para cuando el Empleado confirma la simulación.
    public static final String CONFIRMADA ="Confirmada";
    //Estado para cuando la persona de recursos humanos validar la simulación realizada por el Empleado.
    //Solo hasta que sea validada la simulación, se podría enviar la notificación por correo al empleado.
    //Se genera el OtroSi al Contrato del empleado con las nuevas condiciones de pago.
    public static final String VALIDADA ="Validada";
    //Estado para cuando el empleado desiste de la simulación o
    //Cuando no supera la validación por parte del personal de recursos humanos.
    public static final String DESCARTADA ="Descartada";
    //Estado para cuando la auditoria externa invalida la flexibilización.
    //Esto debería generar un Otrosí al contrato que restablezca el salario del empleado.
    public static final String RECHAZADA_AUDITORIA ="Rechazada por Auditoria";
    //Estado para cuando el empleado decide no mantener la flexibilización de su salario o
    //Cuando se finaliza el contrato que dio origen a la flexibilización.
    public static final String FINALIZADA ="Finalizada";


    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1, SIMULADA);
        map.put(2, CONFIRMADA);
        map.put(3, VALIDADA);
        map.put(4, DESCARTADA);
        map.put(5, RECHAZADA_AUDITORIA);
        map.put(6, FINALIZADA);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
