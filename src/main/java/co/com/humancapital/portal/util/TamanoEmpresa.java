package co.com.humancapital.portal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase utilitaria para exponer el tamaño de una empresa, que se pueden utilizar en el sistema.
 */
public class TamanoEmpresa {
    public static final String MICRO_EMPRESA ="Microempresa"; //Hasta 10 empleados
    public static final String PEQUENA_EMPRESA ="Pequeña Empresa";//11 a 50 empleados
    public static final String MEDIANA_EMPRESA ="Mediana Empresa";//51 a 200 empleados
    public static final String GRAN_EMPRESA ="Grande Empresa";//201 empleados o más.

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1,MICRO_EMPRESA);
        map.put(2,PEQUENA_EMPRESA);
        map.put(3,MEDIANA_EMPRESA);
        map.put(4,GRAN_EMPRESA);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
