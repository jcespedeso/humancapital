package co.com.humancapital.portal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 18/11/2019
 * Clase utilitaria para exponer la tipologia del regimen salarial de un empleado, que se pueden utilizar en el sistema.
 */
public class TipoRegimen {
    public static final String ORDINARIO ="Ordinario";
    public static final String INTEGRAL ="Integral";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<Integer, String>();
        map.put(1, ORDINARIO);
        map.put(3, INTEGRAL);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro de la lista
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el Map contiene el valor de la variable, de lo contrario retorna @false.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
