package co.com.humancapital.portal.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 06/02/2020
 * Clase utilitaria para exponer las opciones de solucion aplicables por la empresa y que se pueden utilizar en el sistema.
 */
public class SolucionEmpresa {
    public static final String REMUNERACION_ESTR = "Remuneración Estratégica";
    public static final String MARCA_EMP = "Marca Empleador";
    public static final String GESTION_ESTR = "Gestión Estratégica del Talento";
    public static final String NCRL = "NCRL";
    public static final String BPO = "BPO";

    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1,REMUNERACION_ESTR);
        map.put(2,MARCA_EMP);
        map.put(3,GESTION_ESTR);
        map.put(4,NCRL);
        map.put(5,BPO);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
