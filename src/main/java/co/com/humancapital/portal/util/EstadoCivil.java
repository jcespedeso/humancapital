package co.com.humancapital.portal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase utilitaria para exponer los estados civiles de una persona, que se pueden utilizar en el sistema.
 */
public class EstadoCivil {
    public static final String SOLTERO ="Soltero(a)";
    public static final String CASADO ="Casado(a)";
    public static final String UNION_LIBRE ="Unión libre";
    public static final String UNION_HECHO ="Unión de hecho";
    public static final String SEPARADO ="Separado(a)";
    public static final String DIVORCIADO ="Divorciado(a)";
    public static final String VIUDO ="Viudo(a)";


    public static Map<Integer, String> listar(){
        Map<Integer,String> map = new TreeMap<>();
        map.put(1,SOLTERO);
        map.put(2,CASADO);
        map.put(3,UNION_LIBRE);
        map.put(4,UNION_HECHO);
        map.put(5,SEPARADO);
        map.put(6,DIVORCIADO);
        map.put(7,VIUDO);
        return map;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite validar si el parametro esta contenido dentro del mapa
     * de elementos establecidos como atributos de la clase.
     * @param key Codigo del atributo a validar.
     * @return @true si el mapa contiene el valor de la variable y @false si no esta contenida en el mapa.
     */
    public static Boolean existeKey(String key){
        if(listar().containsKey(new Integer(key))){
            return true;
        }
        return false;
    }
}
