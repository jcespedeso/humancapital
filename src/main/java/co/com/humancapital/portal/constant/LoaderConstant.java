package co.com.humancapital.portal.constant;

public final class LoaderConstant {
    public static final String EXCEL_MEDIA_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String CONTENT_DISPOSITION_HEADER = "Content-disposition";
    public static final String EXCEL_ATTACHMENT_HEADER_VALUE = "attachment; filename=%s";
    public static final int DEFAULT_ROW_HEADER_OFFSET = 2;
    public static final int DEFAULT_COLUMN_OFFSET = 1;
    
    private LoaderConstant() {}
}
