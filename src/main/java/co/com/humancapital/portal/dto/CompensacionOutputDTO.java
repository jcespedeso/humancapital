package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.util.CondicionEmpleado;
import co.com.humancapital.portal.util.TipoCompensacion;
import co.com.humancapital.portal.util.TipoContrato;
import co.com.humancapital.portal.util.TipoRegimen;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 27/12/2019
 */
public class CompensacionOutputDTO {

    private Long idCompensacion;
    private Map<String, Object> tipoCompensacion;
    private Map<String, Object> regimenLaboral;
    private Map<String, Object> condicionLaboral;
    private Map<String, Object> tipoContrato;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaIngreso;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaTermino;
    private Double salario;

    //Relaciones
    private Map<String, Object> empleado;
    private Map<String, Object> nivelArl;

    //Datos de auditoria
    private boolean activo;

    public CompensacionOutputDTO() {
    }

    public Long getIdCompensacion() {
        return idCompensacion;
    }

    public void setIdCompensacion(Long idCompensacion) {
        this.idCompensacion = idCompensacion;
    }

    public Map<String, Object> getTipoCompensacion() {
        return tipoCompensacion;
    }

    public void setTipoCompensacion(Map<String, Object> tipoCompensacion) {
        this.tipoCompensacion = tipoCompensacion;
    }

    public Map<String, Object> getRegimenLaboral() {
        return regimenLaboral;
    }

    public void setRegimenLaboral(Map<String, Object> regimenLaboral) {
        this.regimenLaboral = regimenLaboral;
    }

    public Map<String, Object> getCondicionLaboral() {
        return condicionLaboral;
    }

    public void setCondicionLaboral(Map<String, Object> condicionLaboral) {
        this.condicionLaboral = condicionLaboral;
    }

    public Map<String, Object> getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(Map<String, Object> tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Map<String, Object> getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Map<String, Object> empleado) {
        this.empleado = empleado;
    }

    public Map<String, Object> getNivelArl() {
        return nivelArl;
    }

    public void setNivelArl(Map<String, Object> nivelArl) {
        this.nivelArl = nivelArl;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Compensacion.
     * @param compensacion Objeto Compensacion.
     * @return Objeto CompensacionInputDTO
     */
    public static CompensacionOutputDTO getDTO(Compensacion compensacion){
        CompensacionOutputDTO compensacionOutputDTO = new CompensacionOutputDTO();
        compensacionOutputDTO.setIdCompensacion(compensacion.getIdCompensacion());
        if(compensacion.getTipoCompensacion() !=null){
            Map<String, Object> tipoCompData = new LinkedHashMap<>();
            tipoCompData.put("id",compensacion.getTipoCompensacion());
            tipoCompData.put("tipo", TipoCompensacion.listar().get(new Integer(compensacion.getTipoCompensacion())));
            compensacionOutputDTO.setTipoCompensacion(tipoCompData);
        }
        if(compensacion.getRegimenLaboral() !=null){
            Map<String, Object> regimenLabData = new LinkedHashMap<>();
            regimenLabData.put("id",compensacion.getRegimenLaboral());
            regimenLabData.put("tipoRegimen", TipoRegimen.listar().get(new Integer(compensacion.getRegimenLaboral())));
            compensacionOutputDTO.setRegimenLaboral(regimenLabData);
        }
        if(compensacion.getCondicionLaboral() !=null){
            Map<String, Object> condicionLabData = new LinkedHashMap<>();
            condicionLabData.put("id",compensacion.getCondicionLaboral());
            condicionLabData.put("condicionLaboral", CondicionEmpleado.listar().get(new Integer(compensacion.getCondicionLaboral())));
            compensacionOutputDTO.setCondicionLaboral(condicionLabData);
        }
        if(compensacion.getTipoContrato() !=null){
            Map<String, Object> tipoContratoData = new LinkedHashMap<>();
            tipoContratoData.put("id",compensacion.getTipoContrato());
            tipoContratoData.put("tipoContrato", TipoContrato.listar().get(new Integer(compensacion.getTipoContrato())));
            compensacionOutputDTO.setTipoContrato(tipoContratoData);
        }
        compensacionOutputDTO.setFechaIngreso(compensacion.getFechaIngreso());
        compensacionOutputDTO.setFechaTermino(compensacion.getFechaTermino());
        compensacionOutputDTO.setSalario(compensacion.getSalario());
        if(compensacion.getEmpleado() !=null){
            Map<String, Object> empleadoData = new LinkedHashMap<>();
            empleadoData.put("idEmpleado", compensacion.getEmpleado().getIdEmpleado());
            empleadoData.put("numeroDocumento", compensacion.getEmpleado().getNumeroDocumento());
            empleadoData.put("nombreCompleto", compensacion.getEmpleado().getNombre()+" "+compensacion.getEmpleado().getApellido());
            empleadoData.put("activo", compensacion.getEmpleado().isActivo());
            compensacionOutputDTO.setEmpleado(empleadoData);
        }
        if(compensacion.getNivelArl() !=null){
            Map<String, Object> nivelArlData = new LinkedHashMap<>();
            nivelArlData.put("codigo", compensacion.getNivelArl().getCodigo());
            nivelArlData.put("nivelRiesgo", compensacion.getNivelArl().getCodigo());
            compensacionOutputDTO.setNivelArl(nivelArlData);
        }

        //Datos de Auditoria
        compensacionOutputDTO.setActivo(compensacion.isActivo());
        return compensacionOutputDTO;
    }
}
