package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.CompensacionXDescuento;
import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 05/02/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class CompensacionXDescuentoOutputDTO {

    private Long idCompDescuento;
    private Double valor;
    private Map<String, Object> compensacion;
    private Map<String, Object> descuento;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad CompensacionXDescuento.
     * @param compDescuento Objeto CompensacionXDescuento.
     * @return Objeto CompensacionXDescuentoInputDTO
     */
    public static CompensacionXDescuentoOutputDTO getDTO(CompensacionXDescuento compDescuento){
        CompensacionXDescuentoOutputDTO compensacionXDescuentoOutputDTO = new CompensacionXDescuentoOutputDTO();
        compensacionXDescuentoOutputDTO.setIdCompDescuento(compDescuento.getIdCompDescuento());
        compensacionXDescuentoOutputDTO.setValor(compDescuento.getValor() == null ? 0 : compDescuento.getValor());
        if(compDescuento.getCompensacion() != null){
            Map<String,Object> empleadoData = new LinkedHashMap<>();
            empleadoData.put("idEmpleado",compDescuento.getCompensacion().getEmpleado().getIdEmpleado());
            empleadoData.put("nombre",compDescuento.getCompensacion().getEmpleado().getNombre());
            empleadoData.put("apellido",compDescuento.getCompensacion().getEmpleado().getApellido());
            empleadoData.put("empleadoActivo",compDescuento.getCompensacion().getEmpleado().isActivo());
            empleadoData.put("idCompensacion",compDescuento.getCompensacion().getIdCompensacion());
            empleadoData.put("compensacionActiva",compDescuento.getCompensacion().isActivo());
            compensacionXDescuentoOutputDTO.setCompensacion(empleadoData);
        }
        if(compDescuento.getDescuento() != null){
            Map<String,Object> descuentoData = new LinkedHashMap<>();
            descuentoData.put("idDescuento",compDescuento.getDescuento().getIdDescuento());
            descuentoData.put("nombre",compDescuento.getDescuento().getNombre());
            descuentoData.put("activo",compDescuento.getDescuento().getActivo());
            compensacionXDescuentoOutputDTO.setDescuento(descuentoData);
        }
        return compensacionXDescuentoOutputDTO;
    }
}
