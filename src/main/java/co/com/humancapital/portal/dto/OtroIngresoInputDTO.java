package co.com.humancapital.portal.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 15/12/2019
 */
public class OtroIngresoInputDTO {

    private Long idOtroIngreso;
    @NotNull
    @Size(min=1, max=100)
    private String nombre;
    private Double valorPorDefecto;

    // Relaciones
    private Long cliente;
    private Long tipoOtroIngreso;

    //Datos de Auditoria
    private Boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    public OtroIngresoInputDTO() {
    }

    public Long getIdOtroIngreso() {
        return idOtroIngreso;
    }

    public void setIdOtroIngreso(Long idOtroIngreso) {
        this.idOtroIngreso = idOtroIngreso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getValorPorDefecto() {
        return valorPorDefecto;
    }

    public void setValorPorDefecto(Double valorPorDefecto) {
        this.valorPorDefecto = valorPorDefecto;
    }

    public Long getCliente() {
        return cliente;
    }

    public void setCliente(Long cliente) {
        this.cliente = cliente;
    }

    public Long getTipoOtroIngreso() {
        return tipoOtroIngreso;
    }

    public void setTipoOtroIngreso(Long tipoOtroIngreso) {
        this.tipoOtroIngreso = tipoOtroIngreso;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

}
