package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.CompensacionXOtroIngreso;
import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 05/02/2020
 */
@NoArgsConstructor
@Getter
@Setter
public class CompensacionXOtroIngresoOutputDTO {

    private Long idCompOtroIngreso;
    private Double valor;
    private Map<String, Object> compensacion;
    private Map<String, Object> otroIngreso;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad CompensacionXOtroIngreso.
     * @param compOtroIngreso Objeto CompensacionOtroIngreso.
     * @return Objeto OtroIngresoInputDTO
     */
    public static CompensacionXOtroIngresoOutputDTO getDTO(CompensacionXOtroIngreso compOtroIngreso){
        CompensacionXOtroIngresoOutputDTO compOtroIngresoDTO = new CompensacionXOtroIngresoOutputDTO();
        compOtroIngresoDTO.setIdCompOtroIngreso(compOtroIngreso.getIdCompOtroIngreso());
        compOtroIngresoDTO.setValor(compOtroIngreso.getValor() == null ? 0 : compOtroIngreso.getValor());
        if(compOtroIngreso.getCompensacion() != null){
            Map<String,Object> empleadoData = new LinkedHashMap<>();
            empleadoData.put("idEmpleado",compOtroIngreso.getCompensacion().getEmpleado().getIdEmpleado());
            empleadoData.put("nombre",compOtroIngreso.getCompensacion().getEmpleado().getNombre());
            empleadoData.put("apellido",compOtroIngreso.getCompensacion().getEmpleado().getApellido());
            empleadoData.put("empleadoActivo",compOtroIngreso.getCompensacion().getEmpleado().isActivo());
            empleadoData.put("idCompensacion",compOtroIngreso.getCompensacion().getIdCompensacion());
            empleadoData.put("compensacionActiva",compOtroIngreso.getCompensacion().isActivo());
            compOtroIngresoDTO.setCompensacion(empleadoData);
        }
        if(compOtroIngreso.getOtroIngreso() != null){
            Map<String,Object> otroIngresoData = new LinkedHashMap<>();
            otroIngresoData.put("idOtroIngreso",compOtroIngreso.getOtroIngreso().getIdOtroIngreso());
            otroIngresoData.put("nombre",compOtroIngreso.getOtroIngreso().getNombre());
            otroIngresoData.put("activo",compOtroIngreso.getOtroIngreso().getActivo());
            compOtroIngresoDTO.setOtroIngreso(otroIngresoData);
        }
        return compOtroIngresoDTO;
    }
}
