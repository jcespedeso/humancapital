package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.DatoTributario;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatoTributarioInputDTO {
    private Long idDatoTributario;
    @NotNull
    private Integer metodoRf;
    @NotNull
    private Double porcentajeRf;
    private Double aporteVoluntarioPension;
    private Double aporteCuentaAfc;
    private Double deducibleSalud;
    private Double deducibleVivienda;
    //    Relaciones
    @NotNull
    private Long empleado;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad DatoTributario.
     * @param datoTributario Objeto DatoTributario.
     * @return Objeto DatoTributarioInputDTO
     */
    public static DatoTributarioInputDTO getDTO(DatoTributario datoTributario){
        DatoTributarioInputDTO datoTributarioInputDTO = new DatoTributarioInputDTO();
        datoTributarioInputDTO.setIdDatoTributario(datoTributario.getIdDatoTributario());
        datoTributarioInputDTO.setMetodoRf(datoTributario.getMetodoRf());
        datoTributarioInputDTO.setPorcentajeRf(datoTributario.getPorcentajeRf());
        datoTributarioInputDTO.setAporteVoluntarioPension(datoTributario.getAporteVoluntarioPension());
        datoTributarioInputDTO.setAporteCuentaAfc(datoTributario.getAporteCuentaAfc());
        datoTributarioInputDTO.setDeducibleSalud(datoTributario.getDeducibleSalud());
        datoTributarioInputDTO.setDeducibleVivienda(datoTributario.getDeducibleVivienda());
        datoTributarioInputDTO.setEmpleado(datoTributario.getEmpleado().getIdEmpleado());
        return datoTributarioInputDTO;
    }
}
