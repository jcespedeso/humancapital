package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.AsignacionParametro;
import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Parametro;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 08/02/2020
 */
public class AsignacionParametroInputDTO {

    private Long idAsignacion;
    @NotNull
    @Size(min = 1, max = 50)
    private String valor;
    @NotNull
    @Size(min = 1, max = 10)
    private String unidadMedida;

    //Relaciones
    @NotNull
    private Long cliente;
    @NotNull
    private Long parametro;

    //Auditoria
    private Boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    public AsignacionParametroInputDTO() {
    }

    public Long getIdAsignacion() {
        return idAsignacion;
    }

    public void setIdAsignacion(Long idAsignacion) {
        this.idAsignacion = idAsignacion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    public Long getCliente() {
        return cliente;
    }

    public void setCliente(Long cliente) {
        this.cliente = cliente;
    }

    public Long getParametro() {
        return parametro;
    }

    public void setParametro(Long parametro) {
        this.parametro = parametro;
    }

}
