package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.OtroIngreso;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 15/12/2019
 */
public class OtroIngresoOutputDTO {

    private Long idOtroIngreso;
    private String nombre;
    private Double valorPorDefecto;

    // Relaciones
    private Map<String,Object> cliente;
    private Map<String,Object> tipoOtroIngreso;

    //Auditoria
    private Boolean activo;

    public OtroIngresoOutputDTO() {
    }

    public Long getIdOtroIngreso() {
        return idOtroIngreso;
    }

    public void setIdOtroIngreso(Long idOtroIngreso) {
        this.idOtroIngreso = idOtroIngreso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getValorPorDefecto() {
        return valorPorDefecto;
    }

    public void setValorPorDefecto(Double valorPorDefecto) {
        this.valorPorDefecto = valorPorDefecto;
    }

    public Map<String, Object> getCliente() {
        return cliente;
    }

    public void setCliente(Map<String, Object> cliente) {
        this.cliente = cliente;
    }

    public Map<String, Object> getTipoOtroIngreso() {
        return tipoOtroIngreso;
    }

    public void setTipoOtroIngreso(Map<String, Object> tipoOtroIngreso) {
        this.tipoOtroIngreso = tipoOtroIngreso;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad OtroIngreso.
     * @param otroIngreso Objeto Cargo.
     * @return Objeto OtroIngresoInputDTO
     */
    public static OtroIngresoOutputDTO getDTO(OtroIngreso otroIngreso){
        OtroIngresoOutputDTO otroIngresoInputDTO = new OtroIngresoOutputDTO();
        otroIngresoInputDTO.setIdOtroIngreso(otroIngreso.getIdOtroIngreso());
        otroIngresoInputDTO.setNombre(otroIngreso.getNombre());
        otroIngresoInputDTO.setValorPorDefecto(otroIngreso.getValorPorDefecto());
        if(otroIngreso.getCliente()!=null){
            Map<String,Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",otroIngreso.getCliente().getIdCliente());
            clienteData.put("nit",otroIngreso.getCliente().getNit());
            clienteData.put("razonSocial",otroIngreso.getCliente().getRazonSocial());
            clienteData.put("activo",otroIngreso.getCliente().isActivo());
            otroIngresoInputDTO.setCliente(clienteData);
        }
        if(otroIngreso.getTipoOtroIngreso()!=null){
            Map<String,Object> tipoIngresoData = new LinkedHashMap<>();
            tipoIngresoData.put("idTipoIngreso",otroIngreso.getTipoOtroIngreso().getIdTipoOtroIngreso());
            tipoIngresoData.put("tipoIngreso",otroIngreso.getTipoOtroIngreso().getTipoIngreso());
            tipoIngresoData.put("activo",otroIngreso.getTipoOtroIngreso().getActivo());
            otroIngresoInputDTO.setTipoOtroIngreso(tipoIngresoData);
        }
        otroIngresoInputDTO.setActivo(otroIngreso.getActivo());
        return otroIngresoInputDTO;
    }
}
