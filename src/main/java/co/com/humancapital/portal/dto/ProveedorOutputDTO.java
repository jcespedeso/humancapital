package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Proveedor;
import co.com.humancapital.portal.util.SectorEmpresa;
import co.com.humancapital.portal.util.TipoPersona;

import java.util.Date;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 30/12/2019
 */
public class ProveedorOutputDTO {

    private Long idProveedor;
    private String nit;
    private String razonSocial;
    private String tipoPersona;
    private String sector;
    private String contactoComercial;
    private String direccion;
    private String telefono;
    private String email;

    //Auditoria
    private Boolean activo;

    public ProveedorOutputDTO() {
    }

    public Long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getContactoComercial() {
        return contactoComercial;
    }

    public void setContactoComercial(String contactoComercial) {
        this.contactoComercial = contactoComercial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Proveedor.
     * @param proveedor Objeto Proveedor.
     * @return Objeto ProveedorInputDTO
     */
    public static ProveedorOutputDTO getDTO(Proveedor proveedor){
        ProveedorOutputDTO proveedorInputDTO = new ProveedorOutputDTO();
        proveedorInputDTO.setIdProveedor(proveedor.getIdProveedor());
        proveedorInputDTO.setNit(proveedor.getNit());
        proveedorInputDTO.setRazonSocial(proveedor.getRazonSocial());
        proveedorInputDTO.setTipoPersona(TipoPersona.listar().get(new Integer(proveedor.getTipoPersona())));
        proveedorInputDTO.setSector(SectorEmpresa.listar().get(new Integer(proveedor.getSector())));
        proveedorInputDTO.setContactoComercial(proveedor.getContactoComercial());
        proveedorInputDTO.setDireccion(proveedor.getDireccion());
        proveedorInputDTO.setTelefono(proveedor.getTelefono());
        proveedorInputDTO.setEmail(proveedor.getEmail());

        proveedorInputDTO.setActivo(proveedor.getActivo());
        return proveedorInputDTO;
    }
}
