package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.SalarioFlexible;
import co.com.humancapital.portal.util.EstadoSimulacion;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 21/12/2019
 */
public class SalarioFlexibleOutputDTO {

    private Long idSalarioFlexible;
    private Integer porcentajeFlexible;
    private Integer valorPlus;
    private Double valorSalarioFlex;
    private Double ahorroEmpleado;
    private Double ahorroEmpresa;
    private Map<String,Object> estadoSimulacion;
    private String observacion;

    //    Relaciones
    private Map<String,Object> compensacion;

    public SalarioFlexibleOutputDTO() {
    }

    public Long getIdSalarioFlexible() {
        return idSalarioFlexible;
    }

    public void setIdSalarioFlexible(Long idSalarioFlexible) {
        this.idSalarioFlexible = idSalarioFlexible;
    }

    public Integer getPorcentajeFlexible() {
        return porcentajeFlexible;
    }

    public void setPorcentajeFlexible(Integer porcentajeFlexible) {
        this.porcentajeFlexible = porcentajeFlexible;
    }

    public Integer getValorPlus() {
        return valorPlus;
    }

    public void setValorPlus(Integer valorPlus) {
        this.valorPlus = valorPlus;
    }

    public Double getValorSalarioFlex() {
        return valorSalarioFlex;
    }

    public void setValorSalarioFlex(Double valorSalarioFlex) {
        this.valorSalarioFlex = valorSalarioFlex;
    }

    public Double getAhorroEmpleado() {
        return ahorroEmpleado;
    }

    public void setAhorroEmpleado(Double ahorroEmpleado) {
        this.ahorroEmpleado = ahorroEmpleado;
    }

    public Double getAhorroEmpresa() {
        return ahorroEmpresa;
    }

    public void setAhorroEmpresa(Double ahorroEmpresa) {
        this.ahorroEmpresa = ahorroEmpresa;
    }

    public Map<String, Object> getEstadoSimulacion() {
        return estadoSimulacion;
    }

    public void setEstadoSimulacion(Map<String, Object> estadoSimulacion) {
        this.estadoSimulacion = estadoSimulacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Map<String, Object> getCompensacion() {
        return compensacion;
    }

    public void setCompensacion(Map<String, Object> compensacion) {
        this.compensacion = compensacion;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad SalarioFlexible.
     * @param salarioFlexible Objeto SalarioFlexible.
     * @return Objeto SalarioFlexibleInputDTO
     */
    public static SalarioFlexibleOutputDTO getDTO(SalarioFlexible salarioFlexible){
        SalarioFlexibleOutputDTO salarioFlexibleOutputDTO = new SalarioFlexibleOutputDTO();
        salarioFlexibleOutputDTO.setIdSalarioFlexible(salarioFlexible.getIdSalarioFlexible());
        salarioFlexibleOutputDTO.setPorcentajeFlexible(salarioFlexible.getPorcentajeFlexible());
        salarioFlexibleOutputDTO.setValorPlus(salarioFlexible.getValorPlus());
        salarioFlexibleOutputDTO.setValorSalarioFlex(salarioFlexible.getValorSalarioFlex());
        salarioFlexibleOutputDTO.setAhorroEmpleado(salarioFlexible.getAhorroEmpleado());
        salarioFlexibleOutputDTO.setAhorroEmpresa(salarioFlexible.getAhorroEmpresa());
        if(salarioFlexible.getEstadoSimulacion() !=null){
            Map<String, Object> salarioflexData = new LinkedHashMap<>();
            salarioflexData.put("id", salarioFlexible.getEstadoSimulacion());
            salarioflexData.put("estado",EstadoSimulacion.listar().get(new Integer(salarioFlexible.getEstadoSimulacion())));
            salarioFlexibleOutputDTO.setEstadoSimulacion(salarioflexData);
        }
        if(salarioFlexible.getCompensacion() !=null){
            Map<String, Object> salarioflexData = new LinkedHashMap<>();
            salarioflexData.put("idCompensacio", salarioFlexible.getCompensacion().getIdCompensacion());
            salarioflexData.put("idEmpleado", salarioFlexible.getCompensacion().getEmpleado().getIdEmpleado());
            salarioflexData.put("Empleado", salarioFlexible.getCompensacion().getEmpleado().getNombre() +" "+salarioFlexible.getCompensacion().getEmpleado().getApellido());
            salarioflexData.put("activo",salarioFlexible.getCompensacion().isActivo());
            salarioFlexibleOutputDTO.setCompensacion(salarioflexData);
        }
        salarioFlexibleOutputDTO.setObservacion(salarioFlexible.getObservacion());
        return salarioFlexibleOutputDTO;
    }
}
