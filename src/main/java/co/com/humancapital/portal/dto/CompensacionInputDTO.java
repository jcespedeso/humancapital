package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Compensacion;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Basic;
import javax.validation.constraints.*;
import java.util.Date;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 27/12/2019
 */
public class CompensacionInputDTO {

    private Long idCompensacion;
    @NotNull
    @Size(min = 1, max = 2)
    private String tipoCompensacion;
    @NotNull
    @Size(min = 1, max = 2)
    private String regimenLaboral;
    @NotNull
    @Size(min = 1, max = 2)
    private String condicionLaboral;
    @NotNull
    @Size(min = 1, max = 2)
    private String tipoContrato;
    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd")
    @PastOrPresent(message = "La fecha del contrato puede ser anterior a la fecha actual o la actual.")
    private Date fechaIngreso;
    @Basic(optional = true)
    @JsonFormat(pattern="yyyy-MM-dd")
    @Future(message = "La fecha de terminación del contrato debe ser posterior a la fecha actual.")
    private Date fechaTermino;
    @NotNull
    @PositiveOrZero(message = "No se permiten numeros negativos.")
    private Double salario;

    //Relaciones
    @NotNull
    private Long empleado;
    @NotNull
    private Integer nivelArl;

    //Datos de auditoria
    private boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    public CompensacionInputDTO() {
    }

    public Long getIdCompensacion() {
        return idCompensacion;
    }

    public void setIdCompensacion(Long idCompensacion) {
        this.idCompensacion = idCompensacion;
    }

    public String getTipoCompensacion() {
        return tipoCompensacion;
    }

    public void setTipoCompensacion(String tipoCompensacion) {
        this.tipoCompensacion = tipoCompensacion;
    }

    public String getRegimenLaboral() {
        return regimenLaboral;
    }

    public void setRegimenLaboral(String regimenLaboral) {
        this.regimenLaboral = regimenLaboral;
    }

    public String getCondicionLaboral() {
        return condicionLaboral;
    }

    public void setCondicionLaboral(String condicionLaboral) {
        this.condicionLaboral = condicionLaboral;
    }

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Long getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Long empleado) {
        this.empleado = empleado;
    }

    public Integer getNivelArl() {
        return nivelArl;
    }

    public void setNivelArl(Integer nivelArl) {
        this.nivelArl = nivelArl;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Compensacion.
     * @param compensacion Objeto Compensacion.
     * @return Objeto CompensacionInputDTO
     */
    public static CompensacionInputDTO getDTO(Compensacion compensacion){
        CompensacionInputDTO compensacionInputDTO = new CompensacionInputDTO();
        compensacionInputDTO.setIdCompensacion(compensacion.getIdCompensacion());
        compensacionInputDTO.setTipoCompensacion(compensacion.getTipoCompensacion());
        compensacionInputDTO.setRegimenLaboral(compensacion.getRegimenLaboral());
        compensacionInputDTO.setCondicionLaboral(compensacion.getCondicionLaboral());
        compensacionInputDTO.setTipoContrato(compensacion.getTipoContrato());
        compensacionInputDTO.setFechaIngreso(compensacion.getFechaIngreso());
        compensacionInputDTO.setFechaTermino(compensacion.getFechaTermino());
        compensacionInputDTO.setSalario(compensacion.getSalario());
        compensacionInputDTO.setEmpleado(compensacion.getEmpleado().getIdEmpleado());
        
        if (compensacion.getNivelArl() != null) {
            compensacionInputDTO.setNivelArl(compensacion.getNivelArl().getCodigo());
        }
        
        //Datos de Auditoria
        compensacionInputDTO.setActivo(compensacion.isActivo());
        compensacionInputDTO.setFechaCreacion(compensacion.getFechaCreacion());
        compensacionInputDTO.setUsuarioCrea(compensacion.getUsuarioCrea());
        compensacionInputDTO.setFechaModifica(compensacion.getFechaModifica());
        compensacionInputDTO.setUsuarioModifica(compensacion.getUsuarioModifica());
        compensacionInputDTO.setFechaInactivacion(compensacion.getFechaInactivacion());
        compensacionInputDTO.setUsuarioInactiva(compensacion.getUsuarioInactiva());
        return compensacionInputDTO;
    }
}
