package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.AsignacionParametro;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Parametro;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 08/02/2020
 */
public class AsignacionParametroOutputDTO {

    private Long idAsignacion;
    private String valor;
    private String unidadMedida;

    //Relaciones
    private Map<String,Object> cliente;
    private Map<String,Object> parametro;

    //Auditoria
    private Boolean activo;

    public AsignacionParametroOutputDTO() {
    }

    public Long getIdAsignacion() {
        return idAsignacion;
    }

    public void setIdAsignacion(Long idAsignacion) {
        this.idAsignacion = idAsignacion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Map<String, Object> getCliente() {
        return cliente;
    }

    public void setCliente(Map<String, Object> cliente) {
        this.cliente = cliente;
    }

    public Map<String, Object> getParametro() {
        return parametro;
    }

    public void setParametro(Map<String, Object> parametro) {
        this.parametro = parametro;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad AsignacionParametro.
     * @param asignacionParametro Objeto AsignacionParametro.
     * @return Objeto AsignacionParametroOutputDTO
     */
    public static AsignacionParametroOutputDTO getDTO(AsignacionParametro asignacionParametro){
        AsignacionParametroOutputDTO asignacionParametroInputDTO = new AsignacionParametroOutputDTO();
        asignacionParametroInputDTO.setIdAsignacion(asignacionParametro.getIdAsignacion());
        asignacionParametroInputDTO.setValor(asignacionParametro.getValor());
        asignacionParametroInputDTO.setUnidadMedida(asignacionParametro.getUnidadMedida());
        if(asignacionParametro.getCliente() != null){
            Map<String, Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",asignacionParametro.getCliente().getIdCliente());
            clienteData.put("nit", asignacionParametro.getCliente().getNit());
            clienteData.put("razonSocial",asignacionParametro.getCliente().getRazonSocial());
            clienteData.put("Activo",asignacionParametro.getCliente().isActivo());
            asignacionParametroInputDTO.setCliente(clienteData);
        }
        if(asignacionParametro.getParametro() != null){
            Map<String, Object> parametroData = new LinkedHashMap<>();
            parametroData.put("idParametro",asignacionParametro.getParametro().getIdParametro());
            parametroData.put("nombre", asignacionParametro.getParametro().getNombre());
            parametroData.put("valor",asignacionParametro.getParametro().getValor());
            parametroData.put("unidadMedida",asignacionParametro.getParametro().getUnidadMedida());
            parametroData.put("normativo",asignacionParametro.getParametro().getNormativo());
            parametroData.put("Activo",asignacionParametro.getParametro().getActivo());
            asignacionParametroInputDTO.setParametro(parametroData);
        }

        //Datos Auditoria
        asignacionParametroInputDTO.setActivo(asignacionParametro.getActivo());
        return asignacionParametroInputDTO;
    }
}
