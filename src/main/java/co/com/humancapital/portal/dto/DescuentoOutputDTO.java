package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Descuento;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 15/12/2019
 */
public class DescuentoOutputDTO {

    private Long idDescuento;
    private String nombre;
    private Double valorPorDefecto;

    // Relaciones
    private Map<String, Object> cliente;

    //Auditoria
    private boolean activo;

    public DescuentoOutputDTO() {
    }

    public Long getIdDescuento() {
        return idDescuento;
    }

    public void setIdDescuento(Long idDescuento) {
        this.idDescuento = idDescuento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getValorPorDefecto() {
        return valorPorDefecto;
    }

    public void setValorPorDefecto(Double valorPorDefecto) {
        this.valorPorDefecto = valorPorDefecto;
    }

    public Map<String, Object> getCliente() {
        return cliente;
    }

    public void setCliente(Map<String, Object> cliente) {
        this.cliente = cliente;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Descuento.
     * @param descuento Objeto Descuento.
     * @return Objeto DescuentoInputDTO
     */
    public static DescuentoOutputDTO getDTO(Descuento descuento){
        DescuentoOutputDTO descuentoOutputDTO = new DescuentoOutputDTO();
        descuentoOutputDTO.setIdDescuento(descuento.getIdDescuento());
        descuentoOutputDTO.setNombre(descuento.getNombre());
        descuentoOutputDTO.setValorPorDefecto(descuento.getValorPorDefecto());
        if(descuento.getCliente() !=null){
            Map<String, Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",descuento.getCliente().getIdCliente());
            clienteData.put("nit",descuento.getCliente().getNit());
            clienteData.put("razonSocial",descuento.getCliente().getRazonSocial());
            clienteData.put("Activo",descuento.getCliente().isActivo());
            descuentoOutputDTO.setCliente(clienteData);
        }

        //Datos de Auditoria
        descuentoOutputDTO.setActivo(descuento.getActivo());
        return descuentoOutputDTO;
    }
}
