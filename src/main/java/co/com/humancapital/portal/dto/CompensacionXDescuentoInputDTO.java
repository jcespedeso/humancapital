package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.CompensacionXDescuento;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 20/12/2019
 */
public class CompensacionXDescuentoInputDTO {

    private Long idCompDescuento;
    @NotNull
    private Double valor;
    @NotNull
    private Long compensacion;
    @NotNull
    private Long descuento;

    public CompensacionXDescuentoInputDTO() {
    }

    public Long getIdCompDescuento() {
        return idCompDescuento;
    }

    public void setIdCompDescuento(Long idCompDescuento) {
        this.idCompDescuento = idCompDescuento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Long getCompensacion() {
        return compensacion;
    }

    public void setCompensacion(Long compensacion) {
        this.compensacion = compensacion;
    }

    public Long getDescuento() {
        return descuento;
    }

    public void setDescuento(Long descuento) {
        this.descuento = descuento;
    }

}
