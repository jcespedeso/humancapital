package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Modulo;
import co.com.humancapital.portal.entity.Producto;

import javax.validation.constraints.*;
import java.util.Collection;
import java.util.Date;

/**
 * @author Johan Céspedes Ortega
 * @date 11/01/2020
 * Clase DTO para las entradas de Cliente.
 */
public class ClienteInputDTO {

    private Long idCliente;
    @NotNull(message = "El El Número de Identificación Tributaria - NIT no puede estar vacio.")
    @Size(min = 8, max = 20, message = "El Número de Identificación Tributaria - NIT debe tener entre 8 a 15 digitos.")
    @NotBlank(message = "En el NIT no se permiten espacios en blanco.")
    @Pattern(regexp = "(^[0-9]+-{1}[0-9]{1})", message = "El numero de Registro Tributario debe mantener el siguiente formato de Ej: 123123123-1 ")
    private String nit;
    @NotNull(message = "La Razon Social del Cliente no puede estar vacia.")
    @Size(min = 2, max = 150, message = "La Razón Social del Cliente debe tener entre 2 y 150 caracteres")
    private String razonSocial;
    @Size(max = 2)
    private String tipoPersona;
    @Size(max = 2)
    private String sector;
    @Size(max = 75)
    private String contactoComercial;
    @Size(max = 75)
    private String direccion;
    @Size(max = 15)
    private String telefono;
    @Size(max = 100)
    @Email(message = "El valor ingresado, no corresponde a una direccion de correo electronico valido.")
    private String email;
    @NotNull
    private String tipo;
    @Size(max = 2)
    private String solucionImplementada;
    @Size(max = 2)
    private String tamanoEmpresa;
    @Size(max = 2)
    private String presencia;
    @Size(max = 2)
    private String ingreso;

    //Datos Auditoria
    private boolean activo;
    private Date fechaCreacion;
    private long usuarioCrea;
    private Date fechaModifica;
    private long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;
    private Long clasificacionCliente;

    //Datos de Usuario
    private String username;
    @Pattern(regexp = "^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{6,16}$", message = "El Password debe tener como minimo 6 digitos asi minimo 1 Minuscula, 1 Mayuscula y Numeros Ej: Ejemp123 ")
    private String password;

    //    Relaciones
    private Collection<Modulo> moduloCollection;
    private Collection<Producto> productoCollection;

    public ClienteInputDTO() {
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getSolucionImplementada() {
        return solucionImplementada;
    }

    public void setSolucionImplementada(String solucionImplementada) {
        this.solucionImplementada = solucionImplementada;
    }

    public String getTamanoEmpresa() {
        return tamanoEmpresa;
    }

    public void setTamanoEmpresa(String tamanoEmpresa) {
        this.tamanoEmpresa = tamanoEmpresa;
    }

    public String getPresencia() {
        return presencia;
    }

    public void setPresencia(String presencia) {
        this.presencia = presencia;
    }

    public String getIngreso() {
        return ingreso;
    }

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getContactoComercial() {
        return contactoComercial;
    }

    public void setContactoComercial(String contactoComercial) {
        this.contactoComercial = contactoComercial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    public Long getClasificacionCliente() {
        return clasificacionCliente;
    }

    public void setClasificacionCliente(Long clasificacionCliente) {
        this.clasificacionCliente = clasificacionCliente;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Modulo> getModuloCollection() {
        return moduloCollection;
    }

    public void setModuloCollection(Collection<Modulo> moduloCollection) {
        this.moduloCollection = moduloCollection;
    }

    public Collection<Producto> getProductoCollection() {
        return productoCollection;
    }

    public void setProductoCollection(Collection<Producto> productoCollection) {
        this.productoCollection = productoCollection;
    }

}
