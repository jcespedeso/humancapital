package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.util.IngresoEmpresa;
import co.com.humancapital.portal.util.PresenciaEmpresa;
import co.com.humancapital.portal.util.SectorEmpresa;
import co.com.humancapital.portal.util.SolucionEmpresa;
import co.com.humancapital.portal.util.TamanoEmpresa;
import co.com.humancapital.portal.util.TipoEmpresa;
import co.com.humancapital.portal.util.TipoPersona;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Johan Céspedes Ortega
 * @date 11/01/2020 Clase DTO para las Salidas de Cliente.
 */

public class ClienteOutputDTO {
    
    private Long idCliente;
    private String nit;
    private String razonSocial;
    private String tipoPersona;
    private String sector;
    private String contactoComercial;
    private String direccion;
    private String telefono;
    private String email;
    private boolean principal;
    private String tipo;
    private String solucionImplementada;
    private String tamanoEmpresa;
    private Map<String, Object> presencia;
    private String ingreso;
    
    //Datos Auditoria
    private boolean activo;
    
    //    Relaciones
    private Map<String, Object> clasificacionCliente;

    public ClienteOutputDTO() {
    }
    
    public Long getIdCliente() {
        return idCliente;
    }
    
    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }
    
    public String getNit() {
        return nit;
    }
    
    public void setNit(String nit) {
        this.nit = nit;
    }
    
    public String getRazonSocial() {
        return razonSocial;
    }
    
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    public String getTipoPersona() {
        return tipoPersona;
    }
    
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }
    
    public String getSector() {
        return sector;
    }
    
    public void setSector(String sector) {
        this.sector = sector;
    }
    
    public String getContactoComercial() {
        return contactoComercial;
    }
    
    public void setContactoComercial(String contactoComercial) {
        this.contactoComercial = contactoComercial;
    }
    
    public String getDireccion() {
        return direccion;
    }
    
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public String getTelefono() {
        return telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public boolean getPrincipal() {
        return principal;
    }
    
    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }
    
    public String getTipo() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public String getSolucionImplementada() {
        return solucionImplementada;
    }
    
    public void setSolucionImplementada(String solucionImplementada) {
        this.solucionImplementada = solucionImplementada;
    }
    
    public String getTamanoEmpresa() {
        return tamanoEmpresa;
    }
    
    public void setTamanoEmpresa(String tamanoEmpresa) {
        this.tamanoEmpresa = tamanoEmpresa;
    }
    
    public Map<String, Object> getPresencia() {
        return presencia;
    }
    
    public void setPresencia(Map<String, Object> presencia) {
        this.presencia = presencia;
    }
    
    public String getIngreso() {
        return ingreso;
    }
    
    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }
    
    public boolean getActivo() {
        return activo;
    }
    
    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    public Map<String, Object> getClasificacionCliente() {
        return clasificacionCliente;
    }
    
    public void setClasificacionCliente(Map<String, Object> clasificacionCliente) {
        this.clasificacionCliente = clasificacionCliente;
    }
    
    /**
     * Metodo que permite optener información puntual de un registro de Cliente.
     *
     * @param cliente Objeto Usuario
     * @return ClienteOutputDTO
     */
    public static ClienteOutputDTO getDTO(Cliente cliente) {
        ClienteOutputDTO clienteDTO = new ClienteOutputDTO();
        clienteDTO.setIdCliente(cliente.getIdCliente());
        clienteDTO.setNit(cliente.getNit());
        clienteDTO.setRazonSocial(cliente.getRazonSocial());
        if (cliente.getTipoPersona() != null) {
            clienteDTO.setTipoPersona(TipoPersona.listar().get(new Integer(cliente.getTipoPersona())));
        }
        if (cliente.getClasificacionCliente() != null) {
            Map<String, Object> clasificacionData = new LinkedHashMap<>();
            clasificacionData.put("idClasificacion", cliente.getClasificacionCliente().getIdClasificacion());
            clasificacionData.put("tipoSociedad", cliente.getClasificacionCliente().getTipoEspecifico());
            clienteDTO.setClasificacionCliente(clasificacionData);
        }
        if (cliente.getSector() != null) {
            clienteDTO.setSector(SectorEmpresa.listar().get(new Integer(cliente.getSector())));
        }
        clienteDTO.setContactoComercial(cliente.getContactoComercial());
        clienteDTO.setDireccion(cliente.getDireccion());
        clienteDTO.setTelefono(cliente.getTelefono());
        clienteDTO.setEmail(cliente.getEmail());
        clienteDTO.setPrincipal(cliente.isPrincipal());
        if (cliente.getTipo() != null) {
            clienteDTO.setTipo(TipoEmpresa.listar().get(new Integer(cliente.getTipo())));
        }
        if (cliente.getSolucionImplementada() != null) {
            clienteDTO.setSolucionImplementada(
                    SolucionEmpresa.listar().get(new Integer(cliente.getSolucionImplementada())));
        }
        if (cliente.getTamanoEmpresa() != null) {
            clienteDTO.setTamanoEmpresa(TamanoEmpresa.listar().get(new Integer(cliente.getTamanoEmpresa())));
        }
        if (cliente.getPresencia() != null) {
            clienteDTO.setPresencia(PresenciaEmpresa.get(new Integer(cliente.getPresencia())));
        }
        if (cliente.getIngreso() != null) {
            clienteDTO.setIngreso(IngresoEmpresa.listar().get(new Integer(cliente.getIngreso())));
        }
        //Datos Auditoria
        clienteDTO.setActivo(cliente.isActivo());
        return clienteDTO;
    }
    
}
