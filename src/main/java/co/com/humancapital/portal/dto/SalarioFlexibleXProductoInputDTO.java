package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.SalarioFlexibleXProducto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 22/12/2019
 */
public class SalarioFlexibleXProductoInputDTO {

    private Long idSalarioProducto;
    @NotNull
    private Integer valorProducto;
    @NotNull
    private Long producto;
    @NotNull
    private Long salarioFlexible;

    public SalarioFlexibleXProductoInputDTO() {
    }

    public Long getIdSalarioProducto() {
        return idSalarioProducto;
    }

    public void setIdSalarioProducto(Long idSalarioProducto) {
        this.idSalarioProducto = idSalarioProducto;
    }

    public Integer getValorProducto() {
        return valorProducto;
    }

    public void setValorProducto(Integer valorProducto) {
        this.valorProducto = valorProducto;
    }

    public Long getProducto() {
        return producto;
    }

    public void setProducto(Long producto) {
        this.producto = producto;
    }

    public Long getSalarioFlexible() {
        return salarioFlexible;
    }

    public void setSalarioFlexible(Long salarioFlexible) {
        this.salarioFlexible = salarioFlexible;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad SalarioFlexibleXProducto.
     * @param salarioFlexibleXProducto Objeto SalarioFlexibleXProducto.
     * @return Objeto SalarioFlexibleXProductoInputDTO.
     */
    public static SalarioFlexibleXProductoInputDTO getDTO(SalarioFlexibleXProducto salarioFlexibleXProducto){
        SalarioFlexibleXProductoInputDTO salarioFlexibleXProductoInputDTO = new SalarioFlexibleXProductoInputDTO();
        salarioFlexibleXProductoInputDTO.setIdSalarioProducto(salarioFlexibleXProducto.getIdSalarioProducto());
        salarioFlexibleXProductoInputDTO.setValorProducto(salarioFlexibleXProducto.getValorProducto());
        salarioFlexibleXProductoInputDTO.setSalarioFlexible(salarioFlexibleXProducto.getSalarioFlexible().getIdSalarioFlexible());
        salarioFlexibleXProductoInputDTO.setProducto(salarioFlexibleXProducto.getProducto().getIdProducto());
        return salarioFlexibleXProductoInputDTO;
    }
}
