package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.AsignacionParametro;
import co.com.humancapital.portal.entity.Parametro;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 08/02/2020
 */
public class ParametroOutputDTO {

    private Long idParametro;
    private String nombre;
    private String valor;
    private String unidadMedida;
    private boolean normativo;

    //Auditoria
    private Boolean activo;

    public ParametroOutputDTO() {
    }

    public Long getIdParametro() {
        return idParametro;
    }

    public void setIdParametro(Long idParametro) {
        this.idParametro = idParametro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public boolean getNormativo() {
        return normativo;
    }

    public void setNormativo(boolean normativo) {
        this.normativo = normativo;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Parametro.
     * @param parametro Objeto Parametro.
     * @return Objeto ParametroOutputDTO
     */
    public static ParametroOutputDTO getDTO(Parametro parametro){
        ParametroOutputDTO parametroOutputDTO = new ParametroOutputDTO();
        parametroOutputDTO.setIdParametro(parametro.getIdParametro());
        parametroOutputDTO.setNombre(parametro.getNombre());
        parametroOutputDTO.setValor(parametro.getValor());
        parametroOutputDTO.setUnidadMedida(parametro.getUnidadMedida());
        parametroOutputDTO.setNormativo(parametro.getNormativo());

        //Datos Auditoria
        parametroOutputDTO.setActivo(parametro.getActivo());
        return parametroOutputDTO;
    }
}
