package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Perfil;
import co.com.humancapital.portal.entity.Privilegio;
import co.com.humancapital.portal.entity.Usuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@NoArgsConstructor
@Getter
@Setter
public class UsuarioOutputDTO {

    private Long idUsuario;
    private String username;

    //Auditoria
    private boolean activo;

    //Relaciones
    private Map<String,Object> empleado;
    private Map<String,Object> cliente;
    private Collection<Map<String,Object>> perfilCollection;
    

    /**
     * Metodo que permite optener información puntual de un registro de Usuario.
     * @param usuario Objeto Usuario
     * @return UsuarioOutputDTO
     */
    public static UsuarioOutputDTO getDTO(Usuario usuario){
        UsuarioOutputDTO usuDTO = new UsuarioOutputDTO();
        usuDTO.setIdUsuario(usuario.getIdUsuario());
        usuDTO.setUsername(usuario.getUsername());
        if(usuario.getEmpleado() !=null){
            Map<String, Object> empleadoData = new LinkedHashMap<>();
            empleadoData.put("idEmpleado",usuario.getEmpleado().getIdEmpleado());
            empleadoData.put("documento",usuario.getEmpleado().getNumeroDocumento());
            empleadoData.put("nombre",usuario.getEmpleado().getNombre()+" "+usuario.getEmpleado().getApellido());
            empleadoData.put("Activo",usuario.getEmpleado().isActivo());
            usuDTO.setEmpleado(empleadoData);
        }
        if(usuario.getCliente() !=null){
            Map<String, Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",usuario.getCliente().getIdCliente());
            clienteData.put("nit",usuario.getCliente().getNit());
            clienteData.put("razonSocial",usuario.getCliente().getRazonSocial());
            clienteData.put("Activo",usuario.getCliente().isActivo());
            usuDTO.setCliente(clienteData);
        }
        usuDTO.setActivo(usuario.isActivo());

        if(usuario.getPerfilCollection().size()>0){
            Collection perfilDataCollection = new ArrayList();
            for (Perfil perfil: usuario.getPerfilCollection()) {
                Map<String, Object> perfilData = new LinkedHashMap<>();
                perfilData.put("idPerfil",perfil.getIdPerfil());
                perfilData.put("nombre",perfil.getNombre());
                perfilData.put("descripcion",perfil.getDescripcion());
                if(perfil.getPrivilegioCollection().size()>0){
                    List<PrivilegioOutputDTO> list = new LinkedList<>();
                    for (Privilegio privilegio: perfil.getPrivilegioCollection()) {
                        list.add(PrivilegioOutputDTO.getDTO(privilegio));
                    }
                    perfilData.put("privilegioCollection",list);
                }
                perfilDataCollection.add(perfilData);
            }
            usuDTO.setPerfilCollection(perfilDataCollection);
        }
        return usuDTO;
    }

}
