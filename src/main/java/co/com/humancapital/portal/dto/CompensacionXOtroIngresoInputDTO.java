package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.CompensacionXOtroIngreso;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 19/12/2019
 */
public class CompensacionXOtroIngresoInputDTO {

    private Long idCompOtroIngreso;
    @NotNull
    private Double valor;
    @NotNull
    private Long compensacion;
    @NotNull
    private Long otroIngreso;

    public CompensacionXOtroIngresoInputDTO() {
    }

    public Long getIdCompOtroIngreso() {
        return idCompOtroIngreso;
    }

    public void setIdCompOtroIngreso(Long idCompOtroIngreso) {
        this.idCompOtroIngreso = idCompOtroIngreso;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Long getCompensacion() {
        return compensacion;
    }

    public void setCompensacion(Long compensacion) {
        this.compensacion = compensacion;
    }

    public Long getOtroIngreso() {
        return otroIngreso;
    }

    public void setOtroIngreso(Long otroIngreso) {
        this.otroIngreso = otroIngreso;
    }

}
