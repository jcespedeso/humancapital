package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Departamento;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 11/01/2020
 */
public class DepartamentoOutputDTO {

    private Long idDepartamento;
    private String nombre;
    private Map<String,Object> jefe;
    private Map<String,Object> cliente;

    //Auditoria
    private Boolean activo;

    public DepartamentoOutputDTO() {
    }

    public Long getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Long idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Map<String, Object> getJefe() {
        return jefe;
    }

    public void setJefe(Map<String, Object> jefe) {
        this.jefe = jefe;
    }

    public Map<String, Object> getCliente() {
        return cliente;
    }

    public void setCliente(Map<String, Object> cliente) {
        this.cliente = cliente;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Departamento.
     * @param depto Objeto Departamento.
     * @return Objeto DepartamentoOutputDTO
     */
    public static DepartamentoOutputDTO getDTO(Departamento depto){
        DepartamentoOutputDTO deptoDTO = new DepartamentoOutputDTO();
        deptoDTO.setIdDepartamento(depto.getIdDepartamento());
        deptoDTO.setNombre(depto.getNombre());
        if(depto.getJefe() !=null){
            Map<String, Object> empleadoData = new LinkedHashMap<>();
            empleadoData.put("idEmpleado",depto.getJefe());
            deptoDTO.setJefe(empleadoData);
        }
        if(depto.getCliente() !=null){
            Map<String, Object> clienteData = new LinkedHashMap<>();
            clienteData.put("idCliente",depto.getCliente().getIdCliente());
            clienteData.put("nit", depto.getCliente().getNit());
            clienteData.put("razonSocial",depto.getCliente().getRazonSocial());
            clienteData.put("Activo",depto.getCliente().isActivo());
            deptoDTO.setCliente(clienteData);
        }
        deptoDTO.setActivo(depto.isActivo());
        return deptoDTO;
    }
}
