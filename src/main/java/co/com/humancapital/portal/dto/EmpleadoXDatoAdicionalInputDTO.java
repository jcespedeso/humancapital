package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.CompensacionXDescuento;
import co.com.humancapital.portal.entity.DatoAdicionalEmpleado;
import co.com.humancapital.portal.entity.EmpleadoXDatoAdicional;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 04/02/2020
 */
public class EmpleadoXDatoAdicionalInputDTO {

    private Long idEmpleadoDato;
    @NotNull
    private String value;
    @NotNull
    private Long empleado;
    @NotNull
    private Long datoAdicionalEmpleado;

    public EmpleadoXDatoAdicionalInputDTO() {
    }

    public Long getIdEmpleadoDato() {
        return idEmpleadoDato;
    }

    public void setIdEmpleadoDato(Long idEmpleadoDato) {
        this.idEmpleadoDato = idEmpleadoDato;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Long empleado) {
        this.empleado = empleado;
    }

    public Long getDatoAdicionalEmpleado() {
        return datoAdicionalEmpleado;
    }

    public void setDatoAdicionalEmpleado(Long datoAdicionalEmpleado) {
        this.datoAdicionalEmpleado = datoAdicionalEmpleado;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad CompensacionXDescuento.
     * @param empleadoXDatoAdicional Objeto CompensacionXDescuento.
     * @return Objeto CompensacionXDescuentoInputDTO
     */
    public static EmpleadoXDatoAdicionalInputDTO getDTO(EmpleadoXDatoAdicional empleadoXDatoAdicional){
        EmpleadoXDatoAdicionalInputDTO empleadoXDatoAdicionalInputDTO = new EmpleadoXDatoAdicionalInputDTO();
        empleadoXDatoAdicionalInputDTO.setIdEmpleadoDato(empleadoXDatoAdicional.getIdEmpleadoDato());
        empleadoXDatoAdicionalInputDTO.setValue(empleadoXDatoAdicional.getValue());
        empleadoXDatoAdicionalInputDTO.setEmpleado(empleadoXDatoAdicional.getEmpleado().getIdEmpleado());
        empleadoXDatoAdicionalInputDTO.setDatoAdicionalEmpleado(empleadoXDatoAdicional.getDatoAdicionalEmpleado().getIdDatoEmpleado());
        return empleadoXDatoAdicionalInputDTO;
    }
}
