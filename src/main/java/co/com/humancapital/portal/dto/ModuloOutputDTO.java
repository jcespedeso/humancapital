package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Modulo;

/**
 * @author Johan Céspedes Ortega
 * @date 14/01/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Modulo.
 */

public class ModuloOutputDTO {

    private Long idModulo;
    private String nombre;
    private String descripcion;

    //Auditoria
    private boolean activo;

    public ModuloOutputDTO() {
    }

    public Long getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(Long idModulo) {
        this.idModulo = idModulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * Metodo que permite optener información puntual de un registro de Modulo.
     * @param modulo Objeto Usuario
     * @return ModuloOutputDTO
     */
    public static ModuloOutputDTO getDTO(Modulo modulo){
        ModuloOutputDTO moduloDTO = new ModuloOutputDTO();
        moduloDTO.setIdModulo(modulo.getIdModulo());
        moduloDTO.setNombre(modulo.getNombre());
        moduloDTO.setDescripcion(modulo.getDescripcion());
        moduloDTO.setActivo(modulo.getActivo());
        return moduloDTO;
    }

}
