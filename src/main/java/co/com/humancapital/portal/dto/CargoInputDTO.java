package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Cargo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 10/12/2019
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CargoInputDTO {

    private Long idCargo;
    @NotEmpty(message = "El nombre del Cargo no puede estar vacio.")
    @Size(max = 75, message = "El nombre del Cargo no puede tener mas de 75 caracteres.")
    private String nombre;
    @NotNull(message = "La categoria del Cargo no puede estar vacia.")
    private Long categoria;
    @NotNull(message = "El departamento al cual se asignará el Cargo no puede estar vacio.")
    private Long departamento;
    @PositiveOrZero(message = "El valor de Peso y Orden debe ser un numero entero positivo.")
    private Integer pesoOrden;
    private Long cargoPadre;
    private boolean cargoJefe;

    //Datos Auditoria
    private Boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

}
