package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.ContactoEmergencia;
import co.com.humancapital.portal.service.EmpleadoService;
import co.com.humancapital.portal.util.Parentesco;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 27/12/2019
 */
@Service
public class ContactoEmergenciaOutputDTO {

    @Autowired
    EmpleadoService empleadoService;

    private Long idContacto;
    private String nombre;
    private String apellido;
    private Map<String,Object> parentesco;
    private String telefono;
    private String email;
    //Relaciones
    private Map<String,Object> empleado;

    public ContactoEmergenciaOutputDTO() {
    }

    public EmpleadoService getEmpleadoService() {
        return empleadoService;
    }

    public void setEmpleadoService(EmpleadoService empleadoService) {
        this.empleadoService = empleadoService;
    }

    public Long getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Long idContacto) {
        this.idContacto = idContacto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Map<String, Object> getParentesco() {
        return parentesco;
    }

    public void setParentesco(Map<String, Object> parentesco) {
        this.parentesco = parentesco;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, Object> getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Map<String, Object> empleado) {
        this.empleado = empleado;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad ContactoEmergencia.
     * @param contactoEmergencia Objeto ContactoEmergencia.
     * @return Objeto ContactoEmergenciaDTO
     */
    public static ContactoEmergenciaOutputDTO getDTO(ContactoEmergencia contactoEmergencia){
        ContactoEmergenciaOutputDTO contactoEmergenciaInputDTO = new ContactoEmergenciaOutputDTO();
        contactoEmergenciaInputDTO.setIdContacto(contactoEmergencia.getIdContacto());
        contactoEmergenciaInputDTO.setNombre(contactoEmergencia.getNombre());
        contactoEmergenciaInputDTO.setApellido(contactoEmergencia.getApellido());
        Map<String, Object> parentescoData = new LinkedHashMap<>();
        parentescoData.put("id", contactoEmergencia.getParentesco());
        parentescoData.put("parentesco", Parentesco.listar().get(new Integer(contactoEmergencia.getParentesco())));
        contactoEmergenciaInputDTO.setParentesco(parentescoData);
        contactoEmergenciaInputDTO.setTelefono(contactoEmergencia.getTelefono());
        contactoEmergenciaInputDTO.setEmail(contactoEmergencia.getEmail());
        Map<String, Object> empleadoData = new LinkedHashMap<>();
        empleadoData.put("idEmpleado", contactoEmergencia.getEmpleado().getIdEmpleado());
        empleadoData.put("numeroDocumento", contactoEmergencia.getEmpleado().getNumeroDocumento());
        empleadoData.put("nombreCompleto", contactoEmergencia.getEmpleado().getNombre()+" "+contactoEmergencia.getEmpleado().getApellido());
        empleadoData.put("activo", contactoEmergencia.getEmpleado().isActivo());
        contactoEmergenciaInputDTO.setEmpleado(empleadoData);
        return contactoEmergenciaInputDTO;
    }
}
