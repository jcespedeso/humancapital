package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.EmpleadoXDatoAdicional;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.validation.constraints.*;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmpleadoInputDTO {

    private Long idEmpleado;
    private String codigo;
    @Size(max = 25)
    private String codigoNomina;
    @Size(max = 25)
    private String centroCosto;
    @NotNull
    @Size(min = 1, max = 2)
    private String tipoDocumento;
    @NotNull
    @Size(min = 8, max = 20)
    private String numeroDocumento;
    @NotNull
    @Size(min = 1, max = 100)
    private String nombre;
    @NotNull
    @Size(min = 1, max = 100)
    private String apellido;
    @Past(message = "La Fecha de Nacimiento debe ser anterior a la fecha actual.")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaNacimiento;
    @NotNull
    @Size(min = 1, max = 2)
    private String genero;
    @NotNull
    @Size(min = 1, max = 20)
    private String estadoCivil;
    @NotNull
    @Size(min = 1, max = 100)
    @Email(message = "El valor ingresado, no corresponde a una direccion de correo electronico valido.")
    private String email;
    @Size(max = 15)
    private String telefono;
    @NotNull
    @Size(min = 1, max = 50)
    private String ciudad;
    @NotNull
    @Size(min = 1, max = 50)
    private String nacionalidad;
    @Size(max = 25)
    private String idioma;
    private String foto;
    private Long categoria;
    @NotNull
    private String regimenPension;
    @NotNull
    private boolean dependientes;
    @NotNull
    private boolean cambiarContrasena;
    @NotNull
    private boolean puedeSimular;

    //Datos de Usuario
    private String username;
    @Pattern(regexp = "^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{6,16}$", message = "El Password debe tener como minimo 6 digitos asi minimo 1 Minuscula, 1 Mayuscula y Numeros Ej: Ejemp123 ")
    private String password;

    //Datos de Auditoria
    private boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    //    Relaciones
    @NotNull
    private Long cargo;
    private Collection<EmpleadoXDatoAdicional> datoAdicionalCollection;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Empleado.
     * @param empleado Objeto Empleado.
     * @return Objeto EmpleadoInputDTO
     */
    public static EmpleadoInputDTO getDTO(Empleado empleado){
        EmpleadoInputDTO empleadoInputDTO = new EmpleadoInputDTO();
        empleadoInputDTO.setIdEmpleado(empleado.getIdEmpleado());
        empleadoInputDTO.setCodigo(empleado.getCodigo());
        empleadoInputDTO.setCodigoNomina(empleado.getCodigoNomina());
        empleadoInputDTO.setCentroCosto(empleado.getCentroCosto());
        empleadoInputDTO.setTipoDocumento(empleado.getTipoDocumento());
        empleadoInputDTO.setNumeroDocumento(empleado.getNumeroDocumento());
        empleadoInputDTO.setNombre(empleado.getNombre());
        empleadoInputDTO.setApellido(empleado.getApellido());
        empleadoInputDTO.setFechaNacimiento(empleado.getFechaNacimiento());
        empleadoInputDTO.setGenero(empleado.getGenero());
        empleadoInputDTO.setEstadoCivil(empleado.getEstadoCivil());
        empleadoInputDTO.setEmail(empleado.getEmail());
        empleadoInputDTO.setTelefono(empleado.getTelefono());
        empleadoInputDTO.setCiudad(empleado.getCiudad());
        empleadoInputDTO.setNacionalidad(empleado.getNacionalidad());
        empleadoInputDTO.setIdioma(empleado.getIdioma());
        empleadoInputDTO.setCategoria(empleado.getCategoria());
        empleadoInputDTO.setRegimenPension(empleado.getRegimenPension());
        empleadoInputDTO.setDependientes(empleado.isDependientes());
        empleadoInputDTO.setCambiarContrasena(empleado.isCambiarContrasena());
        empleadoInputDTO.setPuedeSimular(empleado.isPuedeSimular());
        empleadoInputDTO.setCargo(empleado.getCargo().getIdCargo());
        //Datos Auditoria
        empleadoInputDTO.setActivo(empleado.isActivo());
        empleadoInputDTO.setFechaCreacion(empleado.getFechaCreacion());
        empleadoInputDTO.setUsuarioCrea(empleado.getUsuarioCrea());
        empleadoInputDTO.setFechaModifica(empleado.getFechaModifica());
        empleadoInputDTO.setUsuarioModifica(empleado.getUsuarioModifica());
        empleadoInputDTO.setFechaInactivacion(empleado.getFechaInactivacion());
        empleadoInputDTO.setUsuarioInactiva(empleado.getUsuarioInactiva());
        return empleadoInputDTO;
    }
}
