package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.DatoTributario;
import co.com.humancapital.portal.util.MetodoRf;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class DatoTributarioOutputDTO {

    private Long idDatoTributario;
    private Map<String, Object> metodoRf;
    private Double porcentajeRf;
    private Double aporteVoluntarioPension;
    private Double aporteCuentaAfc;
    private Double deducibleSalud;
    private Double deducibleVivienda;

    //    Relaciones
    private Map<String, Object> empleado;

    public DatoTributarioOutputDTO() {
    }

    public Long getIdDatoTributario() {
        return idDatoTributario;
    }

    public void setIdDatoTributario(Long idDatoTributario) {
        this.idDatoTributario = idDatoTributario;
    }

    public Map<String, Object> getMetodoRf() {
        return metodoRf;
    }

    public void setMetodoRf(Map<String, Object> metodoRf) {
        this.metodoRf = metodoRf;
    }

    public Double getPorcentajeRf() {
        return porcentajeRf;
    }

    public void setPorcentajeRf(Double porcentajeRf) {
        this.porcentajeRf = porcentajeRf;
    }

    public Double getAporteVoluntarioPension() {
        return aporteVoluntarioPension;
    }

    public void setAporteVoluntarioPension(Double aporteVoluntarioPension) {
        this.aporteVoluntarioPension = aporteVoluntarioPension;
    }

    public Double getAporteCuentaAfc() {
        return aporteCuentaAfc;
    }

    public void setAporteCuentaAfc(Double aporteCuentaAfc) {
        this.aporteCuentaAfc = aporteCuentaAfc;
    }

    public Double getDeducibleSalud() {
        return deducibleSalud;
    }

    public void setDeducibleSalud(Double deducibleSalud) {
        this.deducibleSalud = deducibleSalud;
    }

    public Double getDeducibleVivienda() {
        return deducibleVivienda;
    }

    public void setDeducibleVivienda(Double deducibleVivienda) {
        this.deducibleVivienda = deducibleVivienda;
    }

    public Map<String, Object> getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Map<String, Object> empleado) {
        this.empleado = empleado;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad DatoTributario.
     * @param datoTributario Objeto DatoTributario.
     * @return Objeto DatoTributarioInputDTO
     */
    public static DatoTributarioOutputDTO getDTO(DatoTributario datoTributario){
        DatoTributarioOutputDTO datoTributarioOutputDTO = new DatoTributarioOutputDTO();
        datoTributarioOutputDTO.setIdDatoTributario(datoTributario.getIdDatoTributario());
        if(datoTributario.getMetodoRf() !=null){
            Map<String, Object> metodoRfData = new LinkedHashMap<>();
            metodoRfData.put("id",datoTributario.getMetodoRf());
            metodoRfData.put("metodo", MetodoRf.listar().get(datoTributario.getMetodoRf()));
            datoTributarioOutputDTO.setMetodoRf(metodoRfData);
        }
        datoTributarioOutputDTO.setPorcentajeRf(datoTributario.getPorcentajeRf());
        datoTributarioOutputDTO.setAporteVoluntarioPension(datoTributario.getAporteVoluntarioPension());
        datoTributarioOutputDTO.setAporteCuentaAfc(datoTributario.getAporteCuentaAfc());
        datoTributarioOutputDTO.setDeducibleSalud(datoTributario.getDeducibleSalud());
        datoTributarioOutputDTO.setDeducibleVivienda(datoTributario.getDeducibleVivienda());
        if(datoTributario.getEmpleado() !=null){
            Map<String, Object> empleadoData = new LinkedHashMap<>();
            empleadoData.put("idEmpleado", datoTributario.getEmpleado().getIdEmpleado());
            empleadoData.put("numeroDocumento", datoTributario.getEmpleado().getNumeroDocumento());
            empleadoData.put("nombreCompleto", datoTributario.getEmpleado().getNombre()+" "+datoTributario.getEmpleado().getApellido());
            empleadoData.put("activo", datoTributario.getEmpleado().isActivo());
            datoTributarioOutputDTO.setEmpleado(empleadoData);
        }
        return datoTributarioOutputDTO;
    }
}
