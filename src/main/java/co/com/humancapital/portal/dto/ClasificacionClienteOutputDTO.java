package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.ClasificacionCliente;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.util.*;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Johan Céspedes Ortega
 * @date 07/02/2020
 * Clase DTO para las Salidas de ClasificacionCliente.
 */

public class ClasificacionClienteOutputDTO {

    private Long idClasificacion;
    private String tipoEspecifico;

    public ClasificacionClienteOutputDTO() {
    }

    public Long getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(Long idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public String getTipoEspecifico() {
        return tipoEspecifico;
    }

    public void setTipoEspecifico(String tipoEspecifico) {
        this.tipoEspecifico = tipoEspecifico;
    }

    /**
     * Metodo que permite obtener información puntual de un registro de ClasificacionCliente.
     * @param clasifCliente Objeto Usuario
     * @return Objeto ClasificacionClienteOutputDTO
     */
    public static ClasificacionClienteOutputDTO getDTO(ClasificacionCliente clasifCliente){
        ClasificacionClienteOutputDTO clasificacionClienteOutputDTO = new ClasificacionClienteOutputDTO();
        clasificacionClienteOutputDTO.setIdClasificacion(clasifCliente.getIdClasificacion());
        clasificacionClienteOutputDTO.setTipoEspecifico(clasifCliente.getTipoEspecifico());
        return clasificacionClienteOutputDTO;
    }

}
