package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Cargo;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 11/01/2020
 */
public class CargoOutputDTO {

    private Long idCargo;
    private String nombre;
    private Map<String,Object> categoria;
    private Map<String,Object> departamento;
    private Boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    public CargoOutputDTO() {
    }

    public Long getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Long idCargo) {
        this.idCargo = idCargo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Map<String, Object> getCategoria() {
        return categoria;
    }

    public void setCategoria(Map<String, Object> categoria) {
        this.categoria = categoria;
    }

    public Map<String, Object> getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Map<String, Object> departamento) {
        this.departamento = departamento;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Cargo.
     * @param cargo Objeto Cargo.
     * @return Objeto CargoInputDTO
     */
    public static CargoOutputDTO getDTO(Cargo cargo){
        CargoOutputDTO cargoOutputDTO = new CargoOutputDTO();
        cargoOutputDTO.setIdCargo(cargo.getIdCargo());
        cargoOutputDTO.setNombre(cargo.getNombre());
        if(cargo.getCategoria() !=null){
            Map<String, Object> categoriaData = new LinkedHashMap<>();
            categoriaData.put("idCategoria",cargo.getCategoria());
            categoriaData.put("nombre",cargo.getCategoria().getNombre());
            categoriaData.put("activo",cargo.getCategoria().isActivo());
            cargoOutputDTO.setCategoria(categoriaData);
        }
        if(cargo.getDepartamento() !=null){
            Map<String, Object> departamentoData = new LinkedHashMap<>();
            departamentoData.put("idDepartamento",cargo.getDepartamento().getIdDepartamento());
            departamentoData.put("nombre",cargo.getDepartamento().getNombre());
            departamentoData.put("activo",cargo.getDepartamento().isActivo());
            cargoOutputDTO.setDepartamento(departamentoData);
        }
        //Datos Auditoria
        cargoOutputDTO.setActivo(cargo.isActivo());
        cargoOutputDTO.setFechaCreacion(cargo.getFechaCreacion());
        cargoOutputDTO.setUsuarioCrea(cargo.getUsuarioCrea());
        cargoOutputDTO.setFechaModifica(cargo.getFechaModifica());
        cargoOutputDTO.setUsuarioModifica(cargo.getUsuarioModifica());
        cargoOutputDTO.setFechaInactivacion(cargo.getFechaInactivacion());
        cargoOutputDTO.setUsuarioInactiva(cargo.getUsuarioInactiva());
        return cargoOutputDTO;
    }
}
