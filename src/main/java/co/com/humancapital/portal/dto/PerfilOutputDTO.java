package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Perfil;
import co.com.humancapital.portal.entity.Privilegio;
import co.com.humancapital.portal.entity.Usuario;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.*;

public class PerfilOutputDTO {

    private Long idPerfil;
    private String nombre;
    private String descripcion;

    //Auditoria
    private boolean activo;

    //Relaciones
    private Collection<Map<String,Object>> privilegioCollection;

    public PerfilOutputDTO() {
    }

    public Long getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Long idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Collection<Map<String, Object>> getPrivilegioCollection() {
        return privilegioCollection;
    }

    public void setPrivilegioCollection(Collection<Map<String, Object>> privilegioCollection) {
        this.privilegioCollection = privilegioCollection;
    }

    /**
     * Metodo que permite optener información puntual de un registro de Usuario.
     * @param perfil Objeto Usuario
     * @return UsuarioOutputDTO
     */
    public static PerfilOutputDTO getDTO(Perfil perfil){
        PerfilOutputDTO perfilDTO = new PerfilOutputDTO();
        perfilDTO.setIdPerfil(perfil.getIdPerfil());
        perfilDTO.setNombre(perfil.getNombre());
        perfilDTO.setDescripcion(perfil.getDescripcion());
        perfilDTO.setActivo(perfil.getActivo());
        if(perfil.getPrivilegioCollection().size()>0){
            Collection privilegioDataCollection = new ArrayList();
            for (Privilegio privilegio: perfil.getPrivilegioCollection()) {
                Map<String, Object> privielegioData = new LinkedHashMap<>();
                privielegioData.put("codigoPrivilegio",privilegio.getCodigoPrivilegio());
                privielegioData.put("nombrePrivilegio",privilegio.getNombrePrivilegio());
                privielegioData.put("categoria",privilegio.getCategoria());
                privilegioDataCollection.add(privielegioData);
            }
            perfilDTO.setPrivilegioCollection(privilegioDataCollection);
        }
        return perfilDTO;
    }

}
