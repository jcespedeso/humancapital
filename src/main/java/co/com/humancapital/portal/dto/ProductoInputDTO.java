package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.*;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 27/12/2019
 */
public class ProductoInputDTO {

    private Long idProducto;
    @NotNull
    @Size(min = 1, max = 150)
    private String nombre;
    @NotNull
    @Size(min = 1, max = 2)
    private String naturaleza;
    @NotNull
    private boolean seguridadSocial;
    @NotNull
    private boolean ingresoFiscal;
    @NotNull
    @Size(min = 1, max = 255)
    private String parametro;
    @NotNull
    @Size(min = 1, max = 25)
    private String beneficioTributario;
    @NotNull
    @Size(min = 1, max = 25)
    private String parametroBt;
    @NotNull
    private BigInteger monto;
    @NotNull
    @Size(min = 1, max = 2147483647)
    private String descripcion;

    //    Relaciones
    private Long proveedor;

    //Datos de auditoria
    private Boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    public ProductoInputDTO() {
    }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNaturaleza() {
        return naturaleza;
    }

    public void setNaturaleza(String naturaleza) {
        this.naturaleza = naturaleza;
    }

    public boolean getSeguridadSocial() {
        return seguridadSocial;
    }

    public void setSeguridadSocial(boolean seguridadSocial) {
        this.seguridadSocial = seguridadSocial;
    }

    public boolean getIngresoFiscal() {
        return ingresoFiscal;
    }

    public void setIngresoFiscal(boolean ingresoFiscal) {
        this.ingresoFiscal = ingresoFiscal;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getBeneficioTributario() {
        return beneficioTributario;
    }

    public void setBeneficioTributario(String beneficioTributario) {
        this.beneficioTributario = beneficioTributario;
    }

    public String getParametroBt() {
        return parametroBt;
    }

    public void setParametroBt(String parametroBt) {
        this.parametroBt = parametroBt;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getProveedor() {
        return proveedor;
    }

    public void setProveedor(Long proveedor) {
        this.proveedor = proveedor;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

}
