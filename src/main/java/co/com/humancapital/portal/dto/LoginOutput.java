package co.com.humancapital.portal.dto;

/**
 * @author Alejandro Herrera Montilla
 * @project humancapital
 * @date 10/01/2020
 */
public class LoginOutput extends AbstractOutput {

    private UsuarioOutputDTO usuario;
    private String token;

    public LoginOutput() {
    }

    public UsuarioOutputDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioOutputDTO usuario) {
        this.usuario = usuario;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
