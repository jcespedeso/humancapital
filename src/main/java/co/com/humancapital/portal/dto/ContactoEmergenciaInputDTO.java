package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.ContactoEmergencia;
import co.com.humancapital.portal.service.EmpleadoService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 27/12/2019
 */
@Service
public class ContactoEmergenciaInputDTO {

    @Autowired
    EmpleadoService empleadoService;

    private Long idContacto;
    @NotNull
    @Size(min = 1, max = 50)
    private String nombre;
    @NotNull
    @Size(min = 1, max = 50)
    private String apellido;
    @NotNull
    @Size(min = 1, max = 2)
    private String parentesco;
    @NotNull
    @Size(min = 1, max = 20)
    private String telefono;
    @Size(max = 75)
    @Email(message = "El valor ingresado, no corresponde a una direccion de correo electronico valido.")
    private String email;
    //Relaciones
    private Long empleado;

    public ContactoEmergenciaInputDTO() {
    }

    public EmpleadoService getEmpleadoService() {
        return empleadoService;
    }

    public void setEmpleadoService(EmpleadoService empleadoService) {
        this.empleadoService = empleadoService;
    }

    public Long getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Long idContacto) {
        this.idContacto = idContacto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Long empleado) {
        this.empleado = empleado;
    }

}
