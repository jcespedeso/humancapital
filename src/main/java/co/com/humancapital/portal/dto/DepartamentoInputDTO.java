package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Departamento;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 15/12/2019
 */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DepartamentoInputDTO {
    private Long idDepartamento;
    @NotEmpty(message = "El nombre del Departamento no puede estar vacio.")
    @Size(max = 75, message = "El nombre del Departamento no puede tener mas de 75 caracteres.")
    private String nombre;
    @NotNull(message = "El Cliente al cual se asigna el Departamento no puede estar vacio.")
    private Long cliente;
    @PositiveOrZero(message = "El Valor de Peso y Orden del Departamento debe ser un numero positivo.")
    private Integer pesoOrden;
    private Long deptoPadre;

    //Auditoria
    private Boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Departamento.
     * @param depto Objeto Departamento.
     * @return Objeto DepartamentoInputDTO
     */
    public static DepartamentoInputDTO getDTO(Departamento depto){
        DepartamentoInputDTO deptoDTO = new DepartamentoInputDTO();
        deptoDTO.setIdDepartamento(depto.getIdDepartamento());
        deptoDTO.setNombre(depto.getNombre());
        deptoDTO.setCliente(depto.getCliente().getIdCliente());
        deptoDTO.setActivo(depto.isActivo());
        deptoDTO.setFechaCreacion(depto.getFechaCreacion());
        deptoDTO.setUsuarioCrea(depto.getUsuarioCrea());
        deptoDTO.setFechaModifica(depto.getFechaModifica());
        deptoDTO.setUsuarioModifica(depto.getUsuarioModifica());
        deptoDTO.setFechaInactivacion(depto.getFechaInactivacion());
        deptoDTO.setUsuarioInactiva(depto.getUsuarioInactiva());
        return deptoDTO;
    }
}
