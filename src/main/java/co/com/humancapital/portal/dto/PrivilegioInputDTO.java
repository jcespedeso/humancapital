package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Perfil;
import co.com.humancapital.portal.entity.Privilegio;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

public class PrivilegioInputDTO {

    @NotNull
    @Size(min = 1, max = 100)
    private String codigoPrivilegio;
    @NotNull
    @Size(min = 1, max = 100)
    private String nombrePrivilegio;
    @NotNull
    @Size(min = 1, max = 100)
    private String categoria;

    public PrivilegioInputDTO() {
    }

    public String getCodigoPrivilegio() {
        return codigoPrivilegio;
    }

    public void setCodigoPrivilegio(String codigoPrivilegio) {
        this.codigoPrivilegio = codigoPrivilegio;
    }

    public String getNombrePrivilegio() {
        return nombrePrivilegio;
    }

    public void setNombrePrivilegio(String nombrePrivilegio) {
        this.nombrePrivilegio = nombrePrivilegio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

}
