package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.DatoAdicionalEmpleado;
import co.com.humancapital.portal.entity.Descuento;
import co.com.humancapital.portal.entity.EmpleadoXDatoAdicional;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 04/02/2020
 */
public class DatoAdicionalEmpInputDTO {

    private Long idDatoEmpleado;
    @NotNull
    @Size(min = 1, max = 75)
    private String nombre;

    //Auditoria
    private boolean activo;
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    public DatoAdicionalEmpInputDTO() {
    }

    public Long getIdDatoEmpleado() {
        return idDatoEmpleado;
    }

    public void setIdDatoEmpleado(Long idDatoEmpleado) {
        this.idDatoEmpleado = idDatoEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

}
