package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.DatoAdicionalEmpleado;

import java.util.Date;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 04/02/2020
 */
public class DatoAdicionalEmpOutputDTO {

    private Long idDatoEmpleado;
    private String nombre;

    //Datos de auditoria
    private boolean activo;

    public DatoAdicionalEmpOutputDTO() {
    }

    public Long getIdDatoEmpleado() {
        return idDatoEmpleado;
    }

    public void setIdDatoEmpleado(Long idDatoEmpleado) {
        this.idDatoEmpleado = idDatoEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad DatoAdicionalEmpleado.
     * @param datoAdicionalEmpleado Objeto Descuento.
     * @return Objeto DescuentoInputDTO
     */
    public static DatoAdicionalEmpOutputDTO getDTO(DatoAdicionalEmpleado datoAdicionalEmpleado){
        DatoAdicionalEmpOutputDTO datoAdicionalEmpInputDTO = new DatoAdicionalEmpOutputDTO();
        datoAdicionalEmpInputDTO.setIdDatoEmpleado(datoAdicionalEmpleado.getIdDatoEmpleado());
        datoAdicionalEmpInputDTO.setNombre(datoAdicionalEmpleado.getNombre());

        //Datos de Auditoria
        datoAdicionalEmpInputDTO.setActivo(datoAdicionalEmpleado.getActivo());
        return datoAdicionalEmpInputDTO;
    }
}
