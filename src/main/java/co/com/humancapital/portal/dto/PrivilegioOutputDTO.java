package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Privilegio;

import java.io.Serializable;

public class PrivilegioOutputDTO {

    private String codigoPrivilegio;
    private String nombrePrivilegio;
    private String categoria;

    public PrivilegioOutputDTO() {
    }

    public String getCodigoPrivilegio() {
        return codigoPrivilegio;
    }

    public void setCodigoPrivilegio(String codigoPrivilegio) {
        this.codigoPrivilegio = codigoPrivilegio;
    }

    public String getNombrePrivilegio() {
        return nombrePrivilegio;
    }

    public void setNombrePrivilegio(String nombrePrivilegio) {
        this.nombrePrivilegio = nombrePrivilegio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Privilegio.
     * @param privilegio de un perfil de Usuario
     * @return PrivilegioOutputDTO
     */
    public static PrivilegioOutputDTO getDTO(Privilegio privilegio){
        PrivilegioOutputDTO privilegioDTO = new PrivilegioOutputDTO();
        privilegioDTO.setCodigoPrivilegio(privilegio.getCodigoPrivilegio());
        privilegioDTO.setCategoria(privilegio.getCategoria());
        privilegioDTO.setNombrePrivilegio(privilegio.getNombrePrivilegio());
        return privilegioDTO;
    }

}
