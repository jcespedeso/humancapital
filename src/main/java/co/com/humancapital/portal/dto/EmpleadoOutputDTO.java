package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.EmpleadoXDatoAdicional;
import co.com.humancapital.portal.util.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class EmpleadoOutputDTO {
// Todo intentar inyectar Categoria Repository o Service
//    private final Categoria categoria;

    private Long idEmpleado;
    private String codigo;
    private String codigoNomina;
    private String centroCosto;
    private String tipoDocumento;
    private String numeroDocumento;
    private String nombre;
    private String apellido;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaNacimiento;
    private String genero;
    private String estadoCivil;
    private String email;
    private String telefono;
    private String ciudad;
    private String nacionalidad;
    private String idioma;
    private Map<String, Object> categoria;
    private String regimenPension;
    private boolean dependientes;
    private boolean cambiarContrasena;
    private boolean puedeSimular;
    
    //Datos de Auditoria
    private boolean activo;

    //    Relaciones
    private Map<String, Object> usuario;
    private Map<String, Object> cargo;
    private Map<String, Object> datoTributario;
    private List<Map<String, Object>> datoAdicionalCollection;

    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad
     * Empleado.
     * @param empleado Objeto Empleado.
     * @return Objeto EmpleadoInputDTO
     */
    public static EmpleadoOutputDTO getDTO(Empleado empleado) {
        EmpleadoOutputDTO empleadoOutputDTO = new EmpleadoOutputDTO();
        empleadoOutputDTO.setIdEmpleado(empleado.getIdEmpleado());
        empleadoOutputDTO.setCodigo(empleado.getCodigo());
        empleadoOutputDTO.setCodigoNomina(empleado.getCodigoNomina());
        empleadoOutputDTO.setCentroCosto(empleado.getCentroCosto());
        empleadoOutputDTO.setTipoDocumento(TipoDocumento.listar().get(new Integer(empleado.getTipoDocumento())));
        empleadoOutputDTO.setNumeroDocumento(empleado.getNumeroDocumento());
        empleadoOutputDTO.setNombre(empleado.getNombre());
        empleadoOutputDTO.setApellido(empleado.getApellido());
        empleadoOutputDTO.setFechaNacimiento(empleado.getFechaNacimiento());
        empleadoOutputDTO.setGenero(Genero.listar().get(empleado.getGenero()));
        empleadoOutputDTO.setEstadoCivil(EstadoCivil.listar().get(new Integer(empleado.getEstadoCivil())));
        empleadoOutputDTO.setEmail(empleado.getEmail());
        empleadoOutputDTO.setTelefono(empleado.getTelefono());
        empleadoOutputDTO.setCiudad(empleado.getCiudad());
        empleadoOutputDTO.setNacionalidad(empleado.getNacionalidad());
        empleadoOutputDTO.setIdioma(Idioma.listar().get(new Integer(empleado.getIdioma())));
        if (empleado.getCategoria() != null) {
            Map<String, Object> categoriaData = new LinkedHashMap<>();
            categoriaData.put("idCategoria", empleado.getCategoria());
            // Todo obtener datos de Categoria
            categoriaData.put("nombre", "por establecer");
            categoriaData.put("Activo", "por establecer");
            empleadoOutputDTO.setCategoria(categoriaData);
        }
        empleadoOutputDTO.setRegimenPension(RegimenPension.listar().get(new Integer(empleado.getRegimenPension())));
        empleadoOutputDTO.setDependientes(empleado.isDependientes());
        empleadoOutputDTO.setCambiarContrasena(empleado.isCambiarContrasena());
        empleadoOutputDTO.setPuedeSimular(empleado.isPuedeSimular());
        if (empleado.getUsuario() != null) {
            Map<String, Object> usuarioData = new LinkedHashMap<>();
            usuarioData.put("idUsuario", empleado.getUsuario().getIdUsuario());
            usuarioData.put("username", empleado.getUsuario().getUsername());
            usuarioData.put("Activo", empleado.getUsuario().isActivo());
            empleadoOutputDTO.setUsuario(usuarioData);
        }
        if (empleado.getCargo() != null) {
            Map<String, Object> cargoData = new LinkedHashMap<>();
            cargoData.put("idCargo", empleado.getCargo().getIdCargo());
            cargoData.put("nombre", empleado.getCargo().getNombre());
            cargoData.put("Activo", empleado.getCargo().isActivo());
            empleadoOutputDTO.setCargo(cargoData);
        }
        if (empleado.getDatoTributario() != null) {
            Map<String, Object> datoData = new LinkedHashMap<>();
            datoData.put("metodoRf", MetodoRf.listar().get(empleado.getDatoTributario().getMetodoRf()));
            datoData.put("porcentajeRf", empleado.getDatoTributario().getPorcentajeRf());
            datoData.put("aporteVoluntarioPension", empleado.getDatoTributario().getAporteVoluntarioPension());
            datoData.put("aporteCuentaAfc", empleado.getDatoTributario().getAporteCuentaAfc());
            datoData.put("deducibleSalud", empleado.getDatoTributario().getDeducibleSalud());
            datoData.put("deducibleVivienda", empleado.getDatoTributario().getDeducibleVivienda());
            empleadoOutputDTO.setDatoTributario(datoData);
        }
        if (empleado.getDatoAdicionalCollection() != null && empleado.getDatoAdicionalCollection().size() > 0) {
            List<Map<String, Object>> dataAdList = new ArrayList<>();
            for (EmpleadoXDatoAdicional datoAdicional : empleado.getDatoAdicionalCollection()) {
                Map<String, Object> datoAdData = new LinkedHashMap<>();
                datoAdData.put("idDato", datoAdicional.getDatoAdicionalEmpleado().getIdDatoEmpleado());
                datoAdData.put("nombre", datoAdicional.getDatoAdicionalEmpleado().getNombre());
                datoAdData.put("valor", datoAdicional.getValue());
                dataAdList.add(datoAdData);
            }
            empleadoOutputDTO.setDatoAdicionalCollection(dataAdList);
        }
        //Datos Auditoria
        empleadoOutputDTO.setActivo(empleado.isActivo());
        return empleadoOutputDTO;
    }
}
