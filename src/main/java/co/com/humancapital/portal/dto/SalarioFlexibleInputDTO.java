package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.SalarioFlexible;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 21/12/2019
 */
public class SalarioFlexibleInputDTO {

    private Long idSalarioFlexible;
    @NotNull
    private Integer porcentajeFlexible;
    @NotNull
    private Integer valorPlus;
    @NotNull
    private Double valorSalarioFlex;
    @NotNull
    @Size(min = 1, max = 2)
    private String estadoSimulacion;
    @NotNull
    private Double ahorroEmpleado;
    @NotNull
    private Double ahorroEmpresa;
    @Size(max = 2147483647)
    private String observacion;

    //Datos de Auditoria
    private Date fechaCreacion;
    private Long usuarioCrea;
    private Date fechaModifica;
    private Long usuarioModifica;
    private Date fechaInactivacion;
    private Long usuarioInactiva;

    //    Relaciones
    private Long compensacion;

    public SalarioFlexibleInputDTO() {
    }

    public Long getIdSalarioFlexible() {
        return idSalarioFlexible;
    }

    public void setIdSalarioFlexible(Long idSalarioFlexible) {
        this.idSalarioFlexible = idSalarioFlexible;
    }

    public Integer getPorcentajeFlexible() {
        return porcentajeFlexible;
    }

    public void setPorcentajeFlexible(Integer porcentajeFlexible) {
        this.porcentajeFlexible = porcentajeFlexible;
    }

    public Integer getValorPlus() {
        return valorPlus;
    }

    public void setValorPlus(Integer valorPlus) {
        this.valorPlus = valorPlus;
    }

    public Double getValorSalarioFlex() {
        return valorSalarioFlex;
    }

    public void setValorSalarioFlex(Double valorSalarioFlex) {
        this.valorSalarioFlex = valorSalarioFlex;
    }

    public Double getAhorroEmpleado() {
        return ahorroEmpleado;
    }

    public void setAhorroEmpleado(Double ahorroEmpleado) {
        this.ahorroEmpleado = ahorroEmpleado;
    }

    public Double getAhorroEmpresa() {
        return ahorroEmpresa;
    }

    public void setAhorroEmpresa(Double ahorroEmpresa) {
        this.ahorroEmpresa = ahorroEmpresa;
    }

    public String getEstadoSimulacion() {
        return estadoSimulacion;
    }

    public void setEstadoSimulacion(String estadoSimulacion) {
        this.estadoSimulacion = estadoSimulacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    public Long getCompensacion() {
        return compensacion;
    }

    public void setCompensacion(Long compensacion) {
        this.compensacion = compensacion;
    }

}
