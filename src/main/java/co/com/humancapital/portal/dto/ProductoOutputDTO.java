package co.com.humancapital.portal.dto;

import co.com.humancapital.portal.entity.Producto;

import java.math.BigInteger;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 14/01/2020
 */
public class ProductoOutputDTO {

    private Long idProducto;
    private String nombre;
    private String naturaleza;
    private boolean seguridadSocial;
    private boolean ingresoFiscal;
    private String parametro;
    private String beneficioTributario;
    private String parametroBt;
    private BigInteger monto;
    private String descripcion;

    //    Relaciones
    private Map<String,Object> proveedor;

    //Datos de auditoria
    private Boolean activo;

    public ProductoOutputDTO() {
    }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNaturaleza() {
        return naturaleza;
    }

    public void setNaturaleza(String naturaleza) {
        this.naturaleza = naturaleza;
    }

    public boolean getSeguridadSocial() {
        return seguridadSocial;
    }

    public void setSeguridadSocial(boolean seguridadSocial) {
        this.seguridadSocial = seguridadSocial;
    }

    public boolean getIngresoFiscal() {
        return ingresoFiscal;
    }

    public void setIngresoFiscal(boolean ingresoFiscal) {
        this.ingresoFiscal = ingresoFiscal;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getBeneficioTributario() {
        return beneficioTributario;
    }

    public void setBeneficioTributario(String beneficioTributario) {
        this.beneficioTributario = beneficioTributario;
    }

    public String getParametroBt() {
        return parametroBt;
    }

    public void setParametroBt(String parametroBt) {
        this.parametroBt = parametroBt;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Map<String, Object> getProveedor() {
        return proveedor;
    }

    public void setProveedor(Map<String, Object> proveedor) {
        this.proveedor = proveedor;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar los datos a mostrar dependientes de la entidad Producto.
     * @param producto Objeto Producto.
     * @return Objeto ProductoInputDTO
     */
    public static ProductoOutputDTO getDTO(Producto producto){
        ProductoOutputDTO productoInputDTO = new ProductoOutputDTO();
        productoInputDTO.setIdProducto(producto.getIdProducto());
        productoInputDTO.setNombre(producto.getNombre());
        productoInputDTO.setNaturaleza(producto.getNaturaleza());
        productoInputDTO.setSeguridadSocial(producto.getSeguridadSocial());
        productoInputDTO.setIngresoFiscal(producto.getIngresoFiscal());
        productoInputDTO.setParametro(producto.getParametro());
        productoInputDTO.setBeneficioTributario(producto.getBeneficioTributario());
        productoInputDTO.setParametroBt(producto.getParametroBt());
        productoInputDTO.setMonto(producto.getMonto());
        productoInputDTO.setDescripcion(producto.getDescripcion());
        if(producto.getProveedor() !=null){
            Map<String, Object> proveedorData = new LinkedHashMap<>();
            proveedorData.put("idProveedor",producto.getProveedor().getIdProveedor());
            proveedorData.put("nit",producto.getProveedor().getNit());
            proveedorData.put("razonSocial",producto.getProveedor().getRazonSocial());
            productoInputDTO.setProveedor(proveedorData);
        }

        //Datos Auditoria
        productoInputDTO.setActivo(producto.getActivo());
        return productoInputDTO;
    }
}
