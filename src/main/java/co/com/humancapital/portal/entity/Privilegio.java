/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"PRIVILEGIO\"")
public class Privilegio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CODIGO_PRIVILEGIO")
    private String codigoPrivilegio;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE_PRIVILEGIO")
    private String nombrePrivilegio;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CATEGORIA")
    private String categoria;

    @ManyToMany(mappedBy = "privilegioCollection")
    private Collection<Perfil> perfilCollection;

    public Privilegio() {
    }

    public Privilegio(String codigoPrivilegio) {
        this.codigoPrivilegio = codigoPrivilegio;
    }

    public Privilegio(String codigoPrivilegio, String nombrePrivilegio, String categoria) {
        this.codigoPrivilegio = codigoPrivilegio;
        this.nombrePrivilegio = nombrePrivilegio;
        this.categoria = categoria;
    }

    public String getCodigoPrivilegio() {
        return codigoPrivilegio;
    }

    public void setCodigoPrivilegio(String codigoPrivilegio) {
        this.codigoPrivilegio = codigoPrivilegio;
    }

    public String getNombrePrivilegio() {
        return nombrePrivilegio;
    }

    public void setNombrePrivilegio(String nombrePrivilegio) {
        this.nombrePrivilegio = nombrePrivilegio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Collection<Perfil> getPerfilCollection() {
        return perfilCollection;
    }

    public void setPerfilCollection(Collection<Perfil> perfilCollection) {
        this.perfilCollection = perfilCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoPrivilegio != null ? codigoPrivilegio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Privilegio)) {
            return false;
        }
        Privilegio other = (Privilegio) object;
        if ((this.codigoPrivilegio == null && other.codigoPrivilegio != null) || (this.codigoPrivilegio != null && !this.codigoPrivilegio.equals(other.codigoPrivilegio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.Privilegio[ codigoPrivilegio=" + codigoPrivilegio + " ]";
    }
    
}
