package co.com.humancapital.portal.entity;

import co.com.humancapital.portal.get.entity.Competencia;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"DEPARTAMENTO\"")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Departamento implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DEPARTAMENTO")
    private Long idDepartamento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "JEFE")
    private Long jefe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;
    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaInactivacion;
    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;
    @Column(name = "PESO_ORDEN")
    private Integer pesoOrden;

//    Relaciones
    @ManyToMany(mappedBy = "departamentoCollection")
    @JsonIgnore
    @ToString.Exclude
    private Collection<Competencia> competenciaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "departamento")
    @JsonIgnore
    @ToString.Exclude
    private Collection<Cargo> cargoCollection;
    @JoinColumn(name = "CLIENTE", referencedColumnName = "ID_CLIENTE")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Cliente cliente;
    @JoinColumn(name = "DEPTO_PADRE", referencedColumnName = "ID_DEPARTAMENTO")
    @ManyToOne
    @ToString.Exclude
    private Departamento deptoPadre;

    public Departamento(Long idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
}
