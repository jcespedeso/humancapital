/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 10/12/2019
 */
@Entity
@Table(name = "\"SALARIO_FLEXIBLE_X_PRODUCTO\"")
public class SalarioFlexibleXProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SALARIO_PRODUCTO")
    private Long idSalarioProducto;

    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR_PRODUCTO")
    private Integer valorProducto;

    //    Relaciones
    @NotNull
    @JoinColumn(name = "PRODUCTO", referencedColumnName = "ID_PRODUCTO")
    @ManyToOne(optional = false)
    private Producto producto;

    @NotNull
    @JoinColumn(name = "SALARIO_FLEXIBLE", referencedColumnName = "ID_SALARIO_FLEXIBLE")
    @ManyToOne(optional = false)
    private SalarioFlexible salarioFlexible;

    public SalarioFlexibleXProducto() {
    }

    public SalarioFlexibleXProducto(Long idSalarioProducto) {
        this.idSalarioProducto = idSalarioProducto;
    }

    public SalarioFlexibleXProducto(Long idSalarioProducto, Integer valorProducto) {
        this.idSalarioProducto = idSalarioProducto;
        this.valorProducto = valorProducto;
    }

    public Long getIdSalarioProducto() {
        return idSalarioProducto;
    }

    public void setIdSalarioProducto(Long idSalarioProducto) {
        this.idSalarioProducto = idSalarioProducto;
    }

    public Integer getValorProducto() {
        return valorProducto;
    }

    public void setValorProducto(Integer valorProducto) {
        this.valorProducto = valorProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public SalarioFlexible getSalarioFlexible() {
        return salarioFlexible;
    }

    public void setSalarioFlexible(SalarioFlexible salarioFlexible) {
        this.salarioFlexible = salarioFlexible;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSalarioProducto != null ? idSalarioProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalarioFlexibleXProducto)) {
            return false;
        }
        SalarioFlexibleXProducto other = (SalarioFlexibleXProducto) object;
        if ((this.idSalarioProducto == null && other.idSalarioProducto != null) || (this.idSalarioProducto != null && !this.idSalarioProducto.equals(other.idSalarioProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.SalarioFlexibleXProducto[ idSalarioProducto=" + idSalarioProducto + " ]";
    }
    
}
