/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 10/12/2019
 */
@Entity
@Table(name = "\"DATO_TRIBUTARIO\"")
@Data
@NoArgsConstructor
public class DatoTributario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DATO_TRIBUTARIO")
    private Long idDatoTributario;

    @Basic(optional = false)
    @NotNull
    @Column(name = "METODO_RF")
    private Integer metodoRf;

    @Basic(optional = false)
    @NotNull
    @Column(name = "PORCENTAJE_RF")
    private Double porcentajeRf;

    @Column(name = "APORTE_VOLUNTARIO_PENSION")
    private Double aporteVoluntarioPension;

    @Column(name = "APORTE_CUENTA_AFC")
    private Double aporteCuentaAfc;

    @Column(name = "DEDUCIBLE_SALUD")
    private Double deducibleSalud;

    @Column(name = "DEDUCIBLE_VIVIENDA")
    private Double deducibleVivienda;

//    Relaciones
    @NotNull
    @JoinColumn(name = "EMPLEADO", referencedColumnName = "ID_EMPLEADO")
    @OneToOne
    @ToString.Exclude
    private Empleado empleado;

    public DatoTributario(Long idDatoTributario) {
        this.idDatoTributario = idDatoTributario;
    }

    public DatoTributario(Long idDatoTributario, Integer metodoRf, Double porcentajeRf) {
        this.idDatoTributario = idDatoTributario;
        this.metodoRf = metodoRf;
        this.porcentajeRf = porcentajeRf;
    }

    public Long getIdDatoTributario() {
        return idDatoTributario;
    }

    public void setIdDatoTributario(Long idDatoTributario) {
        this.idDatoTributario = idDatoTributario;
    }

    public Integer getMetodoRf() {
        return metodoRf;
    }

    public void setMetodoRf(Integer metodoRf) {
        this.metodoRf = metodoRf;
    }

    public Double getPorcentajeRf() {
        return porcentajeRf;
    }

    public void setPorcentajeRf(Double porcentajeRf) {
        this.porcentajeRf = porcentajeRf;
    }

    public Double getAporteVoluntarioPension() {
        return aporteVoluntarioPension;
    }

    public void setAporteVoluntarioPension(Double aporteVoluntarioPension) {
        this.aporteVoluntarioPension = aporteVoluntarioPension;
    }

    public Double getAporteCuentaAfc() {
        return aporteCuentaAfc;
    }

    public void setAporteCuentaAfc(Double aporteCuentaAfc) {
        this.aporteCuentaAfc = aporteCuentaAfc;
    }

    public Double getDeducibleSalud() {
        return deducibleSalud;
    }

    public void setDeducibleSalud(Double deducibleSalud) {
        this.deducibleSalud = deducibleSalud;
    }

    public Double getDeducibleVivienda() {
        return deducibleVivienda;
    }

    public void setDeducibleVivienda(Double deducibleVivienda) {
        this.deducibleVivienda = deducibleVivienda;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDatoTributario != null ? idDatoTributario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatoTributario)) {
            return false;
        }
        DatoTributario other = (DatoTributario) object;
        if ((this.idDatoTributario == null && other.idDatoTributario != null) || (this.idDatoTributario != null && !this.idDatoTributario.equals(other.idDatoTributario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.DatoTributario[ idDatoTributario=" + idDatoTributario + " ]";
    }
    
}
