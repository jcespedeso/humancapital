package co.com.humancapital.portal.entity;

import co.com.humancapital.portal.get.entity.Encuesta;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

/**
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"EMPLEADO\"")
@Data
@NoArgsConstructor
public class Empleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_EMPLEADO")
    private Long idEmpleado;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "CODIGO")
    private String codigo;

    @Size(max = 25)
    @Column(name = "CODIGO_NOMINA")
    private String codigoNomina;

    @Size(max = 25)
    @Column(name = "CENTRO_COSTO")
    private String centroCosto;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "TIPO_DOCUMENTO")
    private String tipoDocumento;

    @NotNull
    @Size(min = 8, max = 20)
    @Column(name = "NUMERO_DOCUMENTO")
    private String numeroDocumento;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APELLIDO")
    private String apellido;

    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaNacimiento;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "GENERO")
    private String genero;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ESTADO_CIVIL")
    private String estadoCivil;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "EMAIL")
    private String email;

    @Size(max = 15)
    @Column(name = "TELEFONO")
    private String telefono;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CIUDAD")
    private String ciudad;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NACIONALIDAD")
    private String nacionalidad;

    @Size(max = 25)
    @Column(name = "IDIOMA")
    private String idioma;

    @Size(max = 255)
    @Column(name = "FOTO")
    @JsonIgnore
    private String foto;

    @Basic(optional = false)
    @NotNull
    @Column(name = "CATEGORIA")
    private Long categoria;

    @Basic(optional = false)
    @NotNull
    @Column(name = "REGIMEN_PENSION")
    private String regimenPension;

    @Basic(optional = false)
    @NotNull
    @Column(name = "DEPENDIENTES")
    private boolean dependientes;

    @Basic(optional = false)
    @NotNull
    @Column(name = "CAMBIAR_CONTRASENA")
    private boolean cambiarContrasena;

    @Basic(optional = false)
    @NotNull
    @Column(name = "PUEDE_SIMULAR")
    private boolean puedeSimular;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;

    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaInactivacion;

    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

//    Relaciones
    @JoinTable(name = "\"EMPLEADO_X_ENCUESTA\"", joinColumns = {
            @JoinColumn(name = "EMPLEADO", referencedColumnName = "ID_EMPLEADO")}, inverseJoinColumns = {
            @JoinColumn(name = "ENCUESTA", referencedColumnName = "ID_ENCUESTA")})
    @ManyToMany
    @ToString.Exclude
    private Collection<Encuesta> encuestaCollection;

    @JoinColumn(name = "CARGO", referencedColumnName = "ID_CARGO")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Cargo cargo;

    @OneToOne(mappedBy = "empleado")
    @ToString.Exclude
    private DatoTributario datoTributario;

    @OneToOne(mappedBy = "empleado")
    @ToString.Exclude
    private Usuario usuario;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empleado")
    @ToString.Exclude
    private Collection<ContactoEmergencia> contactoEmergenciaCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empleado")
    @ToString.Exclude
    private Collection<Compensacion> compensacionCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empleado")
    @ToString.Exclude
    private Collection<EmpleadoXDatoAdicional> datoAdicionalCollection;

    public Empleado(Long idEmpleado) {
        this.idEmpleado = idEmpleado;
    }
    
    @Transient
    public void addCompensacion(Compensacion compensacion) {
        if (Objects.isNull(compensacionCollection)) {
            compensacionCollection = new ArrayList<>();
        }
        compensacion.setActivo(true);
        compensacionCollection.forEach(c -> c.setActivo(false));
        compensacionCollection.add(compensacion);
    }
}
