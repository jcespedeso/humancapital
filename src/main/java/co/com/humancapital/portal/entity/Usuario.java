package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author jcespedeso at gibor.builders
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "\"USUARIO\"")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_USUARIO")
    private Long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "USERNAME")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 6, max = 200)
    @Column(name = "PASSWORD")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaModifica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;
    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaInactivacion;
    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

    //    Relaciones
    @JoinTable(name = "USUARIO_X_PERFIL", joinColumns = {
            @JoinColumn(name = "USUARIO", referencedColumnName = "ID_USUARIO")}, inverseJoinColumns = {
            @JoinColumn(name = "PERFIL", referencedColumnName = "ID_PERFIL")})
    @ManyToMany
    @ToString.Exclude
    private Collection<Perfil> perfilCollection;

    @JoinColumn(name = "EMPLEADO", referencedColumnName = "ID_EMPLEADO")
    @OneToOne
    @ToString.Exclude
    private Empleado empleado;

    @JoinColumn(name = "CLIENTE", referencedColumnName = "ID_CLIENTE")
    @ManyToOne
    @ToString.Exclude
    private Cliente cliente;

    public Usuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

}
