/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 04/02/2020
 */
@Entity
@Table(name = "\"EMPLEADO_X_DATO_ADICIONAL\"")
public class EmpleadoXDatoAdicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_EMPLEADO_DATO")
    private Long idEmpleadoDato;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "VALUE")
    private String value;
    @NotNull
    @JoinColumn(name = "EMPLEADO", referencedColumnName = "ID_EMPLEADO")
    @ManyToOne(optional = false)
    private Empleado empleado;
    @NotNull
    @JoinColumn(name = "DATO_ADICIONAL_EMPLEADO", referencedColumnName = "ID_DATO_EMPLEADO")
    @ManyToOne(optional = false)
    private DatoAdicionalEmpleado datoAdicionalEmpleado;

    public EmpleadoXDatoAdicional() {
    }

    public EmpleadoXDatoAdicional(Long idEmpleadoDato) {
        this.idEmpleadoDato = idEmpleadoDato;
    }

    public EmpleadoXDatoAdicional(Long idEmpleadoDato, String value) {
        this.idEmpleadoDato = idEmpleadoDato;
        this.value = value;
    }

    public Long getIdEmpleadoDato() {
        return idEmpleadoDato;
    }

    public void setIdEmpleadoDato(Long idEmpleadoDato) {
        this.idEmpleadoDato = idEmpleadoDato;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public DatoAdicionalEmpleado getDatoAdicionalEmpleado() {
        return datoAdicionalEmpleado;
    }

    public void setDatoAdicionalEmpleado(DatoAdicionalEmpleado datoAdicionalEmpleado) {
        this.datoAdicionalEmpleado = datoAdicionalEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpleadoDato != null ? idEmpleadoDato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmpleadoXDatoAdicional)) {
            return false;
        }
        EmpleadoXDatoAdicional other = (EmpleadoXDatoAdicional) object;
        if ((this.idEmpleadoDato == null && other.idEmpleadoDato != null) || (this.idEmpleadoDato != null && !this.idEmpleadoDato.equals(other.idEmpleadoDato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.EmpleadoXDatoAdicional[ idEmpleadoDato=" + idEmpleadoDato + " ]";
    }
    
}
