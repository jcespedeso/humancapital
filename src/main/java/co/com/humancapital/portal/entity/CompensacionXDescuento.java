/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 10/12/2019
 */
@Entity
@Table(name = "\"COMPENSACION_X_DESCUENTO\"")
public class CompensacionXDescuento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COMP_DESCUENTO")
    private Long idCompDescuento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR")
    private Double valor;
    @NotNull
    @JoinColumn(name = "COMPENSACION", referencedColumnName = "ID_COMPENSACION")
    @ManyToOne(optional = false)
    private Compensacion compensacion;
    @NotNull
    @JoinColumn(name = "DESCUENTO", referencedColumnName = "ID_DESCUENTO")
    @ManyToOne(optional = false)
    private Descuento descuento;

    public CompensacionXDescuento() {
    }

    public CompensacionXDescuento(Long idCompDescuento) {
        this.idCompDescuento = idCompDescuento;
    }

    public CompensacionXDescuento(Long idCompDescuento, Double valor) {
        this.idCompDescuento = idCompDescuento;
        this.valor = valor;
    }

    public Long getIdCompDescuento() {
        return idCompDescuento;
    }

    public void setIdCompDescuento(Long idCompDescuento) {
        this.idCompDescuento = idCompDescuento;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Compensacion getCompensacion() {
        return compensacion;
    }

    public void setCompensacion(Compensacion compensacion) {
        this.compensacion = compensacion;
    }

    public Descuento getDescuento() {
        return descuento;
    }

    public void setDescuento(Descuento descuento) {
        this.descuento = descuento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompDescuento != null ? idCompDescuento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompensacionXDescuento)) {
            return false;
        }
        CompensacionXDescuento other = (CompensacionXDescuento) object;
        if ((this.idCompDescuento == null && other.idCompDescuento != null) || (this.idCompDescuento != null && !this.idCompDescuento.equals(other.idCompDescuento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.CompensacionXDescuento[ idCompDescuento=" + idCompDescuento + " ]";
    }
    
}
