/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 10/12/2019
 */
@Entity
@Table(name = "\"SALARIO_FLEXIBLE\"")
public class SalarioFlexible implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SALARIO_FLEXIBLE")
    private Long idSalarioFlexible;

    @Basic(optional = false)
    @NotNull
    @Column(name = "PORCENTAJE_FLEXIBLE")
    private Integer porcentajeFlexible;

    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR_PLUS")
    private Integer valorPlus;

    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR_SALARIO_FLEX")
    private Double valorSalarioFlex;

    @Basic(optional = false)
    @NotNull
    @Column(name = "AHORRO_EMPLEADO")
    private Double ahorroEmpleado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "AHORRO_EMPRESA")
    private Double ahorroEmpresa;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ESTADO_SIMULACION")
    private String estadoSimulacion;

    @Size(max = 2147483647)
    @Column(name = "OBSERVACION")
    private String observacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;
    @Column(name = "FECHA_INACTIVACION")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInactivacion;
    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

    //    Relaciones
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "salarioFlexible")
    private Collection<SalarioFlexibleXProducto> salarioFlexibleXProductoCollection;

    @JoinColumn(name = "COMPENSACION", referencedColumnName = "ID_COMPENSACION")
    @ManyToOne
    private Compensacion compensacion;

    public SalarioFlexible() {
    }

    public SalarioFlexible(Long idSalarioFlexible) {
        this.idSalarioFlexible = idSalarioFlexible;
    }

    public SalarioFlexible(Long idSalarioFlexible, Integer porcentajeFlexible, Double valorSalarioFlex, String estadoSimulacion) {
        this.idSalarioFlexible = idSalarioFlexible;
        this.porcentajeFlexible = porcentajeFlexible;
        this.valorSalarioFlex = valorSalarioFlex;
        this.estadoSimulacion = estadoSimulacion;
    }

    public Long getIdSalarioFlexible() {
        return idSalarioFlexible;
    }

    public void setIdSalarioFlexible(Long idSalarioFlexible) {
        this.idSalarioFlexible = idSalarioFlexible;
    }

    public Integer getPorcentajeFlexible() {
        return porcentajeFlexible;
    }

    public void setPorcentajeFlexible(Integer porcentajeFlexible) {
        this.porcentajeFlexible = porcentajeFlexible;
    }

    public Integer getValorPlus() {
        return valorPlus;
    }

    public void setValorPlus(Integer valorPlus) {
        this.valorPlus = valorPlus;
    }

    public Double getValorSalarioFlex() {
        return valorSalarioFlex;
    }

    public void setValorSalarioFlex(Double valorSalarioFlex) {
        this.valorSalarioFlex = valorSalarioFlex;
    }

    public Double getAhorroEmpleado() {
        return ahorroEmpleado;
    }

    public void setAhorroEmpleado(Double ahorroEmpleado) {
        this.ahorroEmpleado = ahorroEmpleado;
    }

    public Double getAhorroEmpresa() {
        return ahorroEmpresa;
    }

    public void setAhorroEmpresa(Double ahorroEmpresa) {
        this.ahorroEmpresa = ahorroEmpresa;
    }

    public String getEstadoSimulacion() {
        return estadoSimulacion;
    }

    public void setEstadoSimulacion(String estadoSimulacion) {
        this.estadoSimulacion = estadoSimulacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    public Collection<SalarioFlexibleXProducto> getSalarioFlexibleXProductoCollection() {
        return salarioFlexibleXProductoCollection;
    }

    public void setSalarioFlexibleXProductoCollection(Collection<SalarioFlexibleXProducto> salarioFlexibleXProductoCollection) {
        this.salarioFlexibleXProductoCollection = salarioFlexibleXProductoCollection;
    }

    public Compensacion getCompensacion() {
        return compensacion;
    }

    public void setCompensacion(Compensacion compensacion) {
        this.compensacion = compensacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSalarioFlexible != null ? idSalarioFlexible.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalarioFlexible)) {
            return false;
        }
        SalarioFlexible other = (SalarioFlexible) object;
        if ((this.idSalarioFlexible == null && other.idSalarioFlexible != null) || (this.idSalarioFlexible != null && !this.idSalarioFlexible.equals(other.idSalarioFlexible))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.SalarioFlexible[ idSalarioFlexible=" + idSalarioFlexible + " ]";
    }
    
}
