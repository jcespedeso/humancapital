/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"LOG_GENERAL\"")
public class LogGeneral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_LOG")
    private Long idLog;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO")
    private long usuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "ENTIDAD")
    private String entidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ID_REGISTRO")
    private String idRegistro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "ATRIBUTO")
    private String atributo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "VALOR_MODIFICADO")
    private String valorModificado;
    @Lob
    @Column(name = "REMOTO_IP")
    private Object remotoIp;
    @Size(max = 75)
    @Column(name = "ROMOTO_HOST")
    private String romotoHost;
    @Size(max = 75)
    @Column(name = "NAVEGADOR")
    private String navegador;

    public LogGeneral() {
    }

    public LogGeneral(Long idLog) {
        this.idLog = idLog;
    }

    public LogGeneral(Long idLog, Date fecha, long usuario, String entidad, String idRegistro, String atributo, String valorModificado) {
        this.idLog = idLog;
        this.fecha = fecha;
        this.usuario = usuario;
        this.entidad = entidad;
        this.idRegistro = idRegistro;
        this.atributo = atributo;
        this.valorModificado = valorModificado;
    }

    public Long getIdLog() {
        return idLog;
    }

    public void setIdLog(Long idLog) {
        this.idLog = idLog;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public long getUsuario() {
        return usuario;
    }

    public void setUsuario(long usuario) {
        this.usuario = usuario;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(String idRegistro) {
        this.idRegistro = idRegistro;
    }

    public String getAtributo() {
        return atributo;
    }

    public void setAtributo(String atributo) {
        this.atributo = atributo;
    }

    public String getValorModificado() {
        return valorModificado;
    }

    public void setValorModificado(String valorModificado) {
        this.valorModificado = valorModificado;
    }

    public Object getRemotoIp() {
        return remotoIp;
    }

    public void setRemotoIp(Object remotoIp) {
        this.remotoIp = remotoIp;
    }

    public String getRomotoHost() {
        return romotoHost;
    }

    public void setRomotoHost(String romotoHost) {
        this.romotoHost = romotoHost;
    }

    public String getNavegador() {
        return navegador;
    }

    public void setNavegador(String navegador) {
        this.navegador = navegador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLog != null ? idLog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogGeneral)) {
            return false;
        }
        LogGeneral other = (LogGeneral) object;
        if ((this.idLog == null && other.idLog != null) || (this.idLog != null && !this.idLog.equals(other.idLog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.LogGeneral[ idLog=" + idLog + " ]";
    }
    
}
