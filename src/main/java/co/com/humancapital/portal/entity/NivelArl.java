package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 8/12/2019
 */
@Entity
@Table(name = "\"NIVEL_ARL\"")
public class NivelArl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private Integer codigo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "NIVEL_RIESGO")
    private String nivelRiesgo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "PORCENTAJE_APORTE")
    private Double porcentajeAporte;

    @Column(name = "VALOR_APORTE", scale = 2)
    private Double valorAporte;

    //Relaciones
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nivelArl")
    @JsonIgnore
    private Collection<Compensacion> compensacionCollection;

    public NivelArl() {
    }

    public NivelArl(Integer codigo) {
        this.codigo = codigo;
    }

    public NivelArl(Integer codigo, String nivelRiesgo, Double porcentajeAporte) {
        this.codigo = codigo;
        this.nivelRiesgo = nivelRiesgo;
        this.porcentajeAporte = porcentajeAporte;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNivelRiesgo() {
        return nivelRiesgo;
    }

    public void setNivelRiesgo(String nivelRiesgo) {
        this.nivelRiesgo = nivelRiesgo;
    }

    public double getPorcentajeAporte() {
        return porcentajeAporte;
    }

    public void setPorcentajeAporte(Double porcentajeAporte) {
        this.porcentajeAporte = porcentajeAporte;
    }

    public Double getValorAporte() {
        return valorAporte;
    }

    public void setValorAporte(Double valorAporte) {
        this.valorAporte = valorAporte;
    }

    public Collection<Compensacion> getCompensacionCollection() {
        return compensacionCollection;
    }

    public void setCompensacionCollection(Collection<Compensacion> compensacionCollection) {
        this.compensacionCollection = compensacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivelArl)) {
            return false;
        }
        NivelArl other = (NivelArl) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.NivelArl[ codigo=" + codigo + " ]";
    }

}
