/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 10/12/2019
 */
@Entity
@Table(name = "\"OTRO_INGRESO\"")
public class OtroIngreso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_OTRO_INGRESO")
    private Long idOtroIngreso;

    @Basic(optional = false)
    @NotNull
    @Size(min=1, max=100)
    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "VALOR_POR_DEFECTO")
    private Double valorPorDefecto;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private long usuarioCrea;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private long usuarioModifica;

    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaInactivacion;

    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

//    Relaciones
    @JoinColumn(name = "CLIENTE", referencedColumnName = "ID_CLIENTE")
    @ManyToOne(optional = false)
    private Cliente cliente;

    @JoinColumn(name = "TIPO_OTRO_INGRESO", referencedColumnName = "ID_TIPO_OTRO_INGRESO")
    @ManyToOne(optional = false)
    private TipoOtroIngreso tipoOtroIngreso;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "otroIngreso")
    private Collection<CompensacionXOtroIngreso> compensacionXOtroIngresoCollection;

    public OtroIngreso() {
    }

    public OtroIngreso(Long idOtroIngreso) {
        this.idOtroIngreso = idOtroIngreso;
    }

    public OtroIngreso(Long idOtroIngreso, String nombre, boolean activo, Date fechaCreacion, Long usuarioCrea, Date fechaModifica, Long usuarioModifica) {
        this.idOtroIngreso = idOtroIngreso;
        this.nombre = nombre;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCrea = usuarioCrea;
        this.fechaModifica = fechaModifica;
        this.usuarioModifica = usuarioModifica;
    }

    public Long getIdOtroIngreso() {
        return idOtroIngreso;
    }

    public void setIdOtroIngreso(Long idOtroIngreso) {
        this.idOtroIngreso = idOtroIngreso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getValorPorDefecto() {
        return valorPorDefecto;
    }

    public void setValorPorDefecto(Double valorPorDefecto) {
        this.valorPorDefecto = valorPorDefecto;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoOtroIngreso getTipoOtroIngreso() {
        return tipoOtroIngreso;
    }

    public void setTipoOtroIngreso(TipoOtroIngreso tipoOtroIngreso) {
        this.tipoOtroIngreso = tipoOtroIngreso;
    }

    public Collection<CompensacionXOtroIngreso> getCompensacionXOtroIngresoCollection() {
        return compensacionXOtroIngresoCollection;
    }

    public void setCompensacionXOtroIngresoCollection(Collection<CompensacionXOtroIngreso> compensacionXOtroIngresoCollection) {
        this.compensacionXOtroIngresoCollection = compensacionXOtroIngresoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOtroIngreso != null ? idOtroIngreso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OtroIngreso)) {
            return false;
        }
        OtroIngreso other = (OtroIngreso) object;
        if ((this.idOtroIngreso == null && other.idOtroIngreso != null) || (this.idOtroIngreso != null && !this.idOtroIngreso.equals(other.idOtroIngreso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.OtroIngreso[ idOtroIngreso=" + idOtroIngreso + " ]";
    }
    
}
