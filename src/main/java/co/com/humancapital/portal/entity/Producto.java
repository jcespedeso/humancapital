/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"PRODUCTO\"")
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PRODUCTO")
    private Long idProducto;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "NATURALEZA")
    private String naturaleza;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEGURIDAD_SOCIAL")
    private boolean seguridadSocial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INGRESO_FISCAL")
    private boolean ingresoFiscal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "PARAMETRO")
    private String parametro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "BENEFICIO_TRIBUTARIO")
    private String beneficioTributario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "PARAMETRO_BT")
    private String parametroBt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MONTO")
    private BigInteger monto;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;

    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaInactivacion;

    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

    //    Relaciones
    @ManyToMany(mappedBy = "productoCollection")
    @JsonIgnore
    private Collection<Modulo> moduloCollection;

    @ManyToMany(mappedBy = "productoCollection")
    @JsonIgnore
    private Collection<Cliente> clienteCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    @JsonIgnore
    private Collection<SalarioFlexibleXProducto> salarioFlexibleXProductoCollection;

    @JoinColumn(name = "PROVEEDOR", referencedColumnName = "ID_PROVEEDOR")
    @ManyToOne
    private Proveedor proveedor;

    public Producto() {
    }

    public Producto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public Producto(Long idProducto, String nombre, boolean activo, Date fechaCreacion, Long usuarioCrea, Date fechaModifica, Long usuarioModifica, String naturaleza, boolean seguridadSocial, boolean ingresoFiscal, String parametro, String beneficioTributario, String parametroBt, BigInteger monto) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.naturaleza = naturaleza;
        this.seguridadSocial = seguridadSocial;
        this.ingresoFiscal = ingresoFiscal;
        this.parametro = parametro;
        this.beneficioTributario = beneficioTributario;
        this.parametroBt = parametroBt;
        this.monto = monto;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCrea = usuarioCrea;
        this.fechaModifica = fechaModifica;
        this.usuarioModifica = usuarioModifica;
    }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNaturaleza() {
        return naturaleza;
    }

    public void setNaturaleza(String naturaleza) {
        this.naturaleza = naturaleza;
    }

    public boolean getSeguridadSocial() {
        return seguridadSocial;
    }

    public void setSeguridadSocial(boolean seguridadSocial) {
        this.seguridadSocial = seguridadSocial;
    }

    public boolean getIngresoFiscal() {
        return ingresoFiscal;
    }

    public void setIngresoFiscal(boolean ingresoFiscal) {
        this.ingresoFiscal = ingresoFiscal;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getBeneficioTributario() {
        return beneficioTributario;
    }

    public void setBeneficioTributario(String beneficioTributario) {
        this.beneficioTributario = beneficioTributario;
    }

    public String getParametroBt() {
        return parametroBt;
    }

    public void setParametroBt(String parametroBt) {
        this.parametroBt = parametroBt;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    public Collection<Modulo> getModuloCollection() {
        return moduloCollection;
    }

    public void setModuloCollection(Collection<Modulo> moduloCollection) {
        this.moduloCollection = moduloCollection;
    }

    public Collection<Cliente> getClienteCollection() {
        return clienteCollection;
    }

    public void setClienteCollection(Collection<Cliente> clienteCollection) {
        this.clienteCollection = clienteCollection;
    }

    public Collection<SalarioFlexibleXProducto> getSalarioFlexibleXProductoCollection() {
        return salarioFlexibleXProductoCollection;
    }

    public void setSalarioFlexibleXProductoCollection(Collection<SalarioFlexibleXProducto> salarioFlexibleXProductoCollection) {
        this.salarioFlexibleXProductoCollection = salarioFlexibleXProductoCollection;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.Producto[ idProducto=" + idProducto + " ]";
    }
    
}
