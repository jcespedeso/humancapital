/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 19/12/2019
 */
@Entity
@Table(name = "\"COMPENSACION_X_OTRO_INGRESO\"")
public class CompensacionXOtroIngreso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COMP_OTRO_INGRESO")
    private Long idCompOtroIngreso;

    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR")
    private Double valor;

//    Relaciones
    @NotNull
    @JoinColumn(name = "COMPENSACION", referencedColumnName = "ID_COMPENSACION")
    @ManyToOne(optional = false)
    private Compensacion compensacion;

    @NotNull
    @JoinColumn(name = "OTRO_INGRESO", referencedColumnName = "ID_OTRO_INGRESO")
    @ManyToOne(optional = false)
    private OtroIngreso otroIngreso;

    public CompensacionXOtroIngreso() {
    }

    public CompensacionXOtroIngreso(Long idCompOtroIngreso) {
        this.idCompOtroIngreso = idCompOtroIngreso;
    }

    public CompensacionXOtroIngreso(Long idCompOtroIngreso, double valor) {
        this.idCompOtroIngreso = idCompOtroIngreso;
        this.valor = valor;
    }

    public Long getIdCompOtroIngreso() {
        return idCompOtroIngreso;
    }

    public void setIdCompOtroIngreso(Long idCompOtroIngreso) {
        this.idCompOtroIngreso = idCompOtroIngreso;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Compensacion getCompensacion() {
        return compensacion;
    }

    public void setCompensacion(Compensacion compensacion) {
        this.compensacion = compensacion;
    }

    public OtroIngreso getOtroIngreso() {
        return otroIngreso;
    }

    public void setOtroIngreso(OtroIngreso otroIngreso) {
        this.otroIngreso = otroIngreso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompOtroIngreso != null ? idCompOtroIngreso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompensacionXOtroIngreso)) {
            return false;
        }
        CompensacionXOtroIngreso other = (CompensacionXOtroIngreso) object;
        if ((this.idCompOtroIngreso == null && other.idCompOtroIngreso != null) || (this.idCompOtroIngreso != null && !this.idCompOtroIngreso.equals(other.idCompOtroIngreso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.CompensacionXOtroIngreso[ idCompOtroIngreso=" + idCompOtroIngreso + " ]";
    }
    
}
