/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"COMPENSACION\"")
@Data
@NoArgsConstructor
public class Compensacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_COMPENSACION")
    private Long idCompensacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "TIPO_COMPENSACION")
    private String tipoCompensacion;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "REGIMEN_LABORAL")
    private String regimenLaboral;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CONDICION_LABORAL")
    private String condicionLaboral;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "TIPO_CONTRATO")
    private String tipoContrato;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaIngreso;

    @Basic//(optional = false)
    @Column(name = "FECHA_TERMINO")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date fechaTermino;

    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARIO")
    private double salario;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;

    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaInactivacion;

    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

    //Relaciones
    @OneToMany(mappedBy = "compensacion")
    @JsonIgnore
    @ToString.Exclude
    private Collection<SalarioFlexible> salarioFlexibleCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "compensacion")
    @JsonIgnore
    @ToString.Exclude
    private Collection<CompensacionXDescuento> compensacionXDescuentoCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "compensacion")
    @JsonIgnore
    @ToString.Exclude
    private Collection<CompensacionXOtroIngreso> compensacionXOtroIngresoCollection;

    @JoinColumn(name = "EMPLEADO", referencedColumnName = "ID_EMPLEADO")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Empleado empleado;

    @JoinColumn(name = "NIVEL_ARL", referencedColumnName = "CODIGO")
    @ManyToOne//(optional = false)
    @ToString.Exclude
    private NivelArl nivelArl;

    public Compensacion(Long idCompensacion) {
        this.idCompensacion = idCompensacion;
    }
}
