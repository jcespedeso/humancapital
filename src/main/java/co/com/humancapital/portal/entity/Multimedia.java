package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/11/2019
 */
@Entity
@Table(name = "\"MULTIMEDIA\"")
public class Multimedia implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_MULTIMEDIA")
    private Long idMultimedia;
    @Column(name = "FECHA_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaRegistro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "PROCEDENCIA")
    private String procedencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ID_PROCEDENCIA")
    private String idProcedencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONTENT_TYPE")
    private String contentType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "NOMBRE_ORIGINAL")
    private String nombreOriginal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NOMBRE_FINAL")
    private String nombreFinal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "UBICACION_DISCO")
    private String ubicacionDisco;

    public Multimedia() {
    }

    public Multimedia(Date fechaRegistro, @NotNull @Size(min = 1, max = 50) String procedencia, @NotNull @Size(min = 1, max = 100) String idProcedencia, @NotNull @Size(min = 1, max = 100) String contentType, @NotNull @Size(min = 1, max = 150) String nombreOriginal, @NotNull @Size(min = 1, max = 50) String nombreFinal, @NotNull @Size(min = 1, max = 100) String ubicacionDisco) {
        this.fechaRegistro = fechaRegistro;
        this.procedencia = procedencia;
        this.idProcedencia = idProcedencia;
        this.contentType = contentType;
        this.nombreOriginal = nombreOriginal;
        this.nombreFinal = nombreFinal;
        this.ubicacionDisco = ubicacionDisco;
    }

    public Long getIdMultimedia() {
        return idMultimedia;
    }

    public void setIdMultimedia(Long idMultimedia) {
        this.idMultimedia = idMultimedia;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getIdProcedencia() {
        return idProcedencia;
    }

    public void setIdProcedencia(String idProcedencia) {
        this.idProcedencia = idProcedencia;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getNombreOriginal() {
        return nombreOriginal;
    }

    public void setNombreOriginal(String nombreOriginal) {
        this.nombreOriginal = nombreOriginal;
    }

    public String getNombreFinal() {
        return nombreFinal;
    }

    public void setNombreFinal(String nombreFinal) {
        this.nombreFinal = nombreFinal;
    }

    public String getUbicacionDisco() {
        return ubicacionDisco;
    }

    public void setUbicacionDisco(String ubicacionDisco) {
        this.ubicacionDisco = ubicacionDisco;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Multimedia that = (Multimedia) o;

        if (!idMultimedia.equals(that.idMultimedia)) return false;
        if (!fechaRegistro.equals(that.fechaRegistro)) return false;
        if (!procedencia.equals(that.procedencia)) return false;
        if (!idProcedencia.equals(that.idProcedencia)) return false;
        if (!contentType.equals(that.contentType)) return false;
        if (!nombreOriginal.equals(that.nombreOriginal)) return false;
        if (!nombreFinal.equals(that.nombreFinal)) return false;
        return ubicacionDisco.equals(that.ubicacionDisco);
    }

    @Override
    public int hashCode() {
        int result = idMultimedia.hashCode();
        result = 31 * result + fechaRegistro.hashCode();
        result = 31 * result + procedencia.hashCode();
        result = 31 * result + idProcedencia.hashCode();
        result = 31 * result + contentType.hashCode();
        result = 31 * result + nombreOriginal.hashCode();
        result = 31 * result + nombreFinal.hashCode();
        result = 31 * result + ubicacionDisco.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Multimedia{" +
                "idMultimedia=" + idMultimedia +
                ", fechaRegistro=" + fechaRegistro +
                ", procedencia='" + procedencia + '\'' +
                ", idProcedencia='" + idProcedencia + '\'' +
                ", contentType='" + contentType + '\'' +
                ", nombreOriginal='" + nombreOriginal + '\'' +
                ", nombreFinal='" + nombreFinal + '\'' +
                ", ubicacionDisco='" + ubicacionDisco + '\'' +
                '}';
    }
}
