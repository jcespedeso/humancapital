/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 04/02/2020
 */
@Entity
@Table(name = "\"DATO_ADICIONAL_EMPLEADO\"")
public class DatoAdicionalEmpleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_DATO_EMPLEADO")
    private Long idDatoEmpleado;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "NOMBRE")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;

    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaInactivacion;

    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

//    Relaciones
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "datoAdicionalEmpleado")
    private Collection<EmpleadoXDatoAdicional> empleadoXDatoAdicionalCollection;

    public DatoAdicionalEmpleado() {
    }

    public DatoAdicionalEmpleado(Long idDescuento) {
        this.idDatoEmpleado = idDescuento;
    }

    public DatoAdicionalEmpleado(Long idDatoEmpleado, String nombre, boolean activo, Date fechaCreacion, Long usuarioCrea, Date fechaModifica, Long usuarioModifica) {
        this.idDatoEmpleado = idDatoEmpleado;
        this.nombre = nombre;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCrea = usuarioCrea;
        this.fechaModifica = fechaModifica;
        this.usuarioModifica = usuarioModifica;
    }

    public Long getIdDatoEmpleado() {
        return idDatoEmpleado;
    }

    public void setIdDatoEmpleado(Long idDatoEmpleado) {
        this.idDatoEmpleado = idDatoEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    public Collection<EmpleadoXDatoAdicional> getEmpleadoXDatoAdicionalCollection() {
        return empleadoXDatoAdicionalCollection;
    }

    public void setEmpleadoXDatoAdicionalCollection(Collection<EmpleadoXDatoAdicional> empleadoXDatoAdicionalCollection) {
        this.empleadoXDatoAdicionalCollection = empleadoXDatoAdicionalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDatoEmpleado != null ? idDatoEmpleado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatoAdicionalEmpleado)) {
            return false;
        }
        DatoAdicionalEmpleado other = (DatoAdicionalEmpleado) object;
        if ((this.idDatoEmpleado == null && other.idDatoEmpleado != null) || (this.idDatoEmpleado != null && !this.idDatoEmpleado.equals(other.idDatoEmpleado))) {
            return false;
        }
        return true;
    }

//    @Override
//    public String toString() {
//        return "co.com.humancapital.portal.entity.Descuento[ idDescuento=" + idDescuento + " ]";
//    }


    @Override
    public String toString() {
        return "Descuento{" +
                "idDescuento=" + idDatoEmpleado +
                ", nombre='" + nombre + '\'' +
                ", activo=" + activo +
                ", fechaCreacion=" + fechaCreacion +
                ", usuarioCrea=" + usuarioCrea +
                ", fechaModifica=" + fechaModifica +
                ", usuarioModifica=" + usuarioModifica +
                ", fechaInactivacion=" + fechaInactivacion +
                ", usuarioInactiva=" + usuarioInactiva +
                '}';
    }
}
