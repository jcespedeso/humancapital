/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import co.com.humancapital.portal.get.entity.Competencia;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author jcespedeso at gibor.builders
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "\"CLIENTE\"")
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CLIENTE")
    private Long idCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NIT")
    private String nit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "RAZON_SOCIAL")
    private String razonSocial;
    @Size(min = 1, max = 2)
    @Column(name = "TIPO_PERSONA")
    private String tipoPersona;
    @Size(min = 1, max = 2)
    @Column(name = "SECTOR")
    private String sector;
    @Size(min = 1, max = 75)
    @Column(name = "CONTACTO_COMERCIAL")
    private String contactoComercial;
    @Size(max = 75)
    @Column(name = "DIRECCION")
    private String direccion;
    @Size(max = 15)
    @Column(name = "TELEFONO")
    private String telefono;
    @Size(min = 1, max = 100)
    @Column(name = "EMAIL")
    private String email;
    @Basic(optional = false)
    @Column(name = "PRINCIPAL")
    private boolean principal;
    @Size(min = 1, max = 2)
    @Column(name = "TIPO")
    private String tipo;
    @Size(min = 1, max = 2)
    @Column(name = "SOLUCION_IMPLEMENTADA")
    private String solucionImplementada;
    @Size(min = 1, max = 2)
    @Column(name = "TAMANO_EMPRESA")
    private String tamanoEmpresa;
    @Size(min = 1, max = 2)
    @Column(name = "PRESENCIA")
    private String presencia;
    @Size(min = 1, max = 2)
    @Column(name = "INGRESO")
    private String ingreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private long usuarioCrea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private long usuarioModifica;
    @Column(name = "FECHA_INACTIVACION")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInactivacion;
    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

    //    Relaciones
    @JoinTable(name = "\"CLIENTE_X_MODULO\"", joinColumns = {
        @JoinColumn(name = "CLIENTE", referencedColumnName = "ID_CLIENTE")}, inverseJoinColumns = {
        @JoinColumn(name = "MODULO", referencedColumnName = "ID_MODULO")})
    @ManyToMany
    @ToString.Exclude
    private Collection<Modulo> moduloCollection;

    @JoinTable(name = "\"CLIENTE_X_PRODUCTO\"", joinColumns = {
            @JoinColumn(name = "CLIENTE", referencedColumnName = "ID_CLIENTE")}, inverseJoinColumns = {
            @JoinColumn(name = "PRODUCTO", referencedColumnName = "ID_PRODUCTO")})
    @ManyToMany
    @ToString.Exclude
    private Collection<Producto> productoCollection;

    @JoinColumn(name = "CLASIFICACION_CLIENTE", referencedColumnName = "ID_CLASIFICACION")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private ClasificacionCliente clasificacionCliente;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    @ToString.Exclude
    private Collection<AsignacionParametro> asignacionParametroCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    @ToString.Exclude
    private Collection<OtroIngreso> otroIngresoCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    @ToString.Exclude
    private Collection<Departamento> departamentoCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    @ToString.Exclude
    private Collection<Competencia> competenciaCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    @ToString.Exclude
    private Collection<Descuento> descuentoCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    @ToString.Exclude
    private Collection<Usuario> usuarioCollection;

    public Cliente(Long idCliente) {
        this.idCliente = idCliente;
    }
}
