/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import co.com.humancapital.portal.get.entity.Categoria;
import co.com.humancapital.portal.get.entity.Competencia;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;


/**
 * @author jcespedeso at gibor.builders
 */
@Entity
@Table(name = "\"CARGO\"")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Cargo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CARGO")
    private Long idCargo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;
    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaInactivacion;
    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;
    @Column(name = "PESO_ORDEN")
    private Integer pesoOrden;
    @Column(name = "CARGO_JEFE")
    private boolean cargoJefe;

//    Relaciones
    @ManyToMany(mappedBy = "cargoCollection")
    @ToString.Exclude
    private Collection<Competencia> competenciaCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cargo")
    @ToString.Exclude
    private Collection<Empleado> empleadoCollection;

    @JoinColumn(name = "DEPARTAMENTO", referencedColumnName = "ID_DEPARTAMENTO")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Departamento departamento;

    @JoinColumn(name = "CATEGORIA", referencedColumnName = "ID_CATEGORIA")
    @ManyToOne(optional = false)
    @ToString.Exclude
    private Categoria categoria;

    @JoinColumn(name = "CARGO_PADRE", referencedColumnName = "ID_CARGO")
    @ManyToOne
    private Cargo cargoPadre;

    public Cargo(Long idCargo) {
        this.idCargo = idCargo;
    }

}
