/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.humancapital.portal.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * @author Alejandro Herrera Montilla  - alhemoasde@gmail.com
 * @project humancapital
 * @date 10/12/2019
 */
@Entity
@Table(name = "\"TIPO_OTRO_INGRESO\"")
public class TipoOtroIngreso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_TIPO_OTRO_INGRESO")
    private Long idTipoOtroIngreso;

    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_INGRESO")
    private String tipoIngreso;

    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARIAL")
    private boolean salarial;

    @Column(name = "GRAVADO")
    private Boolean gravado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "RECOMPONE")
    private boolean recompone;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FLEXIBILIZA")
    private boolean flexibiliza;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVO")
    private boolean activo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaCreacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_CREA")
    private Long usuarioCrea;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_MODIFICA")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaModifica;

    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_MODIFICA")
    private Long usuarioModifica;

    @Column(name = "FECHA_INACTIVACION")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date fechaInactivacion;

    @Column(name = "USUARIO_INACTIVA")
    private Long usuarioInactiva;

    //Relaciones
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoOtroIngreso")
    @JsonIgnore
    private Collection<OtroIngreso> otroIngresoCollection;

    public TipoOtroIngreso() {
    }

    public TipoOtroIngreso(Long idTipoOtroIngreso) {
        this.idTipoOtroIngreso = idTipoOtroIngreso;
    }

    public TipoOtroIngreso(Long idTipoOtroIngreso, String tipoIngreso, boolean salarial, boolean recompone, boolean flexibiliza, boolean activo, Date fechaCreacion, Long usuarioCrea, Date fechaModifica, Long usuarioModifica) {
        this.idTipoOtroIngreso = idTipoOtroIngreso;
        this.tipoIngreso = tipoIngreso;
        this.salarial = salarial;
        this.recompone = recompone;
        this.flexibiliza = flexibiliza;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCrea = usuarioCrea;
        this.fechaModifica = fechaModifica;
        this.usuarioModifica = usuarioModifica;
    }

    public Long getIdTipoOtroIngreso() {
        return idTipoOtroIngreso;
    }

    public void setIdTipoOtroIngreso(Long idTipoOtroIngreso) {
        this.idTipoOtroIngreso = idTipoOtroIngreso;
    }

    public String getTipoIngreso() {
        return tipoIngreso;
    }

    public void setTipoIngreso(String tipoIngreso) {
        this.tipoIngreso = tipoIngreso;
    }

    public boolean getSalarial() {
        return salarial;
    }

    public void setSalarial(boolean salarial) {
        this.salarial = salarial;
    }

    public Boolean getGravado() {
        return gravado;
    }

    public void setGravado(Boolean gravado) {
        this.gravado = gravado;
    }

    public boolean getRecompone() {
        return recompone;
    }

    public void setRecompone(boolean recompone) {
        this.recompone = recompone;
    }

    public boolean getFlexibiliza() {
        return flexibiliza;
    }

    public void setFlexibiliza(boolean flexibiliza) {
        this.flexibiliza = flexibiliza;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(Long usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public Long getUsuarioModifica() {
        return usuarioModifica;
    }

    public void setUsuarioModifica(Long usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }

    public Date getFechaInactivacion() {
        return fechaInactivacion;
    }

    public void setFechaInactivacion(Date fechaInactivacion) {
        this.fechaInactivacion = fechaInactivacion;
    }

    public Long getUsuarioInactiva() {
        return usuarioInactiva;
    }

    public void setUsuarioInactiva(Long usuarioInactiva) {
        this.usuarioInactiva = usuarioInactiva;
    }

    public Collection<OtroIngreso> getOtroIngresoCollection() {
        return otroIngresoCollection;
    }

    public void setOtroIngresoCollection(Collection<OtroIngreso> otroIngresoCollection) {
        this.otroIngresoCollection = otroIngresoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoOtroIngreso != null ? idTipoOtroIngreso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoOtroIngreso)) {
            return false;
        }
        TipoOtroIngreso other = (TipoOtroIngreso) object;
        if ((this.idTipoOtroIngreso == null && other.idTipoOtroIngreso != null) || (this.idTipoOtroIngreso != null && !this.idTipoOtroIngreso.equals(other.idTipoOtroIngreso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.humancapital.portal.entity.TipoOtroIngreso[ idTipoOtroIngreso=" + idTipoOtroIngreso + " ]";
    }
    
}
