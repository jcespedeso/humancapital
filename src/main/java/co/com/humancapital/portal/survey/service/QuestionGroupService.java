package co.com.humancapital.portal.survey.service;

import co.com.humancapital.portal.survey.dto.QuestionGroupDTO;
import co.com.humancapital.portal.survey.entity.QuestionGroup;
import co.com.humancapital.portal.survey.repository.QuestionGroupRepository;
import co.com.humancapital.portal.util.ObjectMapperUtils;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class QuestionGroupService {
    
    private final QuestionGroupRepository groupRepository;
    
    public QuestionGroupDTO save(QuestionGroupDTO groupDTO) {
        QuestionGroup questionGroup = ObjectMapperUtils.map(groupDTO, QuestionGroup.class);
        questionGroup = groupRepository.save(questionGroup);
        return ObjectMapperUtils.map(questionGroup, QuestionGroupDTO.class);
    }
    
    public List<QuestionGroupDTO> listAll(Long clientId) {
        return ObjectMapperUtils.mapAll(groupRepository.findAllByClientId(clientId), QuestionGroupDTO.class);
    }
    
    public void delete(Long clientId, Long groupId) {
        groupRepository.findByIdAndClientId(groupId, clientId)
                .ifPresent(group -> groupRepository.delete(group));
    }
}
