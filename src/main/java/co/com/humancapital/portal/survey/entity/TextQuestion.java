package co.com.humancapital.portal.survey.entity;

import co.com.humancapital.portal.survey.type.QuestionTextType;
import javax.persistence.Entity;
import lombok.Data;

@Entity
@Data
public class TextQuestion extends Question {
    
    private QuestionTextType questionTextType;
    private String placeHolder;
}
