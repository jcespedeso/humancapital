package co.com.humancapital.portal.survey.dto;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionCategoryDTO {
    private Long id;
    @NotNull
    private Long clientId;
    @NotNull
    private String name;
    private QuestionGroupDTO questionGroup;
}
