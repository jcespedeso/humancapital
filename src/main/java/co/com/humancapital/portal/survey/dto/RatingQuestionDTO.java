package co.com.humancapital.portal.survey.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RatingQuestionDTO extends QuestionDTO {
    private int amountOptions;
    private String minRateDescription;
    private String maxRateDescription;
}