package co.com.humancapital.portal.survey.repository;

import co.com.humancapital.portal.survey.entity.QuestionGroup;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionGroupRepository extends CrudRepository<QuestionGroup, Long> {
    List<QuestionGroup> findAllByClientId(Long clientId);
    Optional<QuestionGroup> findByIdAndClientId(Long id, Long clientId);
}
