package co.com.humancapital.portal.survey.repository;

import co.com.humancapital.portal.survey.entity.Survey;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyRepository extends PagingAndSortingRepository<Survey, Long> {
    List<Survey> findAllByClientId(Long clientId);
    Optional<Survey> findByIdAndClientId(Long id, Long clientId);
    boolean existsByIdAndClientId(Long id, Long clientId);
}