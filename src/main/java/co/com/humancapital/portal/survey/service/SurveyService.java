package co.com.humancapital.portal.survey.service;

import co.com.humancapital.portal.survey.dto.QuestionDTO;
import co.com.humancapital.portal.survey.dto.QuestionGroupDTO;
import co.com.humancapital.portal.survey.dto.SurveyDTO;
import co.com.humancapital.portal.survey.dto.TextQuestionDTO;
import co.com.humancapital.portal.survey.entity.Question;
import co.com.humancapital.portal.survey.entity.Survey;
import co.com.humancapital.portal.survey.entity.TextQuestion;
import co.com.humancapital.portal.survey.repository.QuestionRepository;
import co.com.humancapital.portal.survey.repository.SurveyRepository;
import co.com.humancapital.portal.survey.type.QuestionTextType;
import co.com.humancapital.portal.survey.type.QuestionType;
import co.com.humancapital.portal.util.ObjectMapperUtils;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static co.com.humancapital.portal.util.ObjectMapperUtils.map;
import static co.com.humancapital.portal.util.ObjectMapperUtils.mapAll;

@Service
@RequiredArgsConstructor
public class SurveyService {
    private final SurveyRepository surveyRepository;
    private final QuestionService questionService;
    
    @Transactional
    public SurveyDTO saveUpdate(SurveyDTO surveyDTO) {
        Survey survey = map(surveyDTO, Survey.class);
        Survey persistedSurvey = surveyRepository.save(survey);
        SurveyDTO persistedSurveyDTO = map(persistedSurvey, SurveyDTO.class);
        
        List<QuestionDTO> questionDTOList = questionService.saveUpdateAll(persistedSurveyDTO, surveyDTO.getQuestions());
        persistedSurveyDTO.setQuestions(questionDTOList);
        return persistedSurveyDTO;
    }
    
    public List<SurveyDTO> list(Long clientId) {
        return mapAll(surveyRepository.findAllByClientId(clientId), SurveyDTO.class);
    }
    
    public Optional<Survey> findModel(Long surveyId) {
        return surveyRepository.findById(surveyId);
    }
    
    public Optional<SurveyDTO> find(Long surveyId) {
        return surveyRepository.findById(surveyId)
                .map(survey -> map(survey, SurveyDTO.class));
    }
    
    public SurveyDTO findWithQuestions(Long surveyId) {
        SurveyDTO surveyDTO = surveyRepository.findById(surveyId)
                .map(survey -> map(survey, SurveyDTO.class))
                .orElseThrow(() -> new IllegalArgumentException("::: Survey no encontrada para el id " + surveyId));
        List<QuestionDTO> questionDTOList = questionService.findBySurvey(surveyDTO.getId());
        surveyDTO.setQuestions(questionDTOList);
        return surveyDTO;
    }
    
    public boolean exist(Long clientId, Long surveyId) {
        return surveyRepository.existsByIdAndClientId(surveyId, clientId);
    }
    
    public void delete(Long clientId, Long surveyId) {
        surveyRepository.findByIdAndClientId(surveyId, clientId)
                .ifPresent(survey -> surveyRepository.delete(survey));
    }
}
