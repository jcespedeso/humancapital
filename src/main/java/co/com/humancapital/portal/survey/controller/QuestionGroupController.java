package co.com.humancapital.portal.survey.controller;

import co.com.humancapital.portal.survey.dto.QuestionGroupDTO;
import co.com.humancapital.portal.survey.service.QuestionGroupService;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("{clientId}/survey/group")
@RequiredArgsConstructor
public class QuestionGroupController {
    private final QuestionGroupService groupService;
    
    @PostMapping
    public QuestionGroupDTO save(@RequestBody @Valid @NotNull QuestionGroupDTO groupDTO) {
        return groupService.save(groupDTO);
    }
    
    @GetMapping
    public List<QuestionGroupDTO> listAll(@PathVariable Long clientId) {
        return groupService.listAll(clientId);
    }
    
    @DeleteMapping("/{groupId}")
    public void delete(@PathVariable Long clientId, @PathVariable Long groupId) {
        groupService.delete(clientId, groupId);
    }
}