package co.com.humancapital.portal.survey.dto;

import co.com.humancapital.portal.survey.type.QuestionType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@JsonInclude(Include.NON_NULL)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @Type(value = TextQuestionDTO.class, name = "text"),
        @Type(value = RatingQuestionDTO.class, name = "rating"),
        @Type(value = ChoiceQuestionDTO.class, name = "choice")
})
public abstract class QuestionDTO {
    
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String title;
    @NotNull
    private QuestionType questionType;
    @NotNull
    private QuestionCategoryDTO questionCategory;
    private boolean required;
}