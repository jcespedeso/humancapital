package co.com.humancapital.portal.survey.dto;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionnaireResponseDTO {
    //Entry = <QuestionId, ChoiceValue>
    private Map<Long, String> responses;
}
