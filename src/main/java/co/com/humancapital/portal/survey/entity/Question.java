package co.com.humancapital.portal.survey.entity;

import co.com.humancapital.portal.survey.type.QuestionType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Entity
@Accessors(chain = true)
@NoArgsConstructor
public abstract class Question extends AuditableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String title;
    private QuestionType questionType;
    @ManyToOne(optional = false)
    private QuestionCategory questionCategory;
    @ManyToOne(optional = false)
    private Survey survey;
    private boolean required;
}