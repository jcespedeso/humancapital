package co.com.humancapital.portal.survey.type;

public enum QuestionTextType {
    TEXT,
    DATE,
    EMAIL,
    COLOR
}