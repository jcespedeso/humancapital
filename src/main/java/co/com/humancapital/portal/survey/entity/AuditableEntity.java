package co.com.humancapital.portal.survey.entity;

import java.time.LocalDate;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public abstract class AuditableEntity {
    private boolean activo;
    private long usuarioCrea;
    private long usuarioModifica;
    private Long usuarioInactiva;
    @NotNull
    private LocalDate fechaCreacion = LocalDate.now();
    private LocalDate fechaModifica;
    private LocalDate fechaInactivacion;
}
