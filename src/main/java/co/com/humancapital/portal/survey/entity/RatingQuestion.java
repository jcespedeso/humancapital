package co.com.humancapital.portal.survey.entity;

import javax.persistence.Entity;
import lombok.Data;

@Entity
@Data
public class RatingQuestion extends Question {
    private int amountOptions;
    private String minRateDescription;
    private String maxRateDescription;
}
