package co.com.humancapital.portal.survey.dto;

import co.com.humancapital.portal.survey.type.QuestionTextType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextQuestionDTO extends QuestionDTO {
    
    private QuestionTextType questionTextType;
    private String placeHolder;
}
