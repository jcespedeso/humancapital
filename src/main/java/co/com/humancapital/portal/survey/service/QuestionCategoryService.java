package co.com.humancapital.portal.survey.service;

import co.com.humancapital.portal.survey.dto.QuestionCategoryDTO;
import co.com.humancapital.portal.survey.dto.QuestionGroupDTO;
import co.com.humancapital.portal.survey.entity.QuestionCategory;
import co.com.humancapital.portal.survey.repository.QuestionCategoryRepository;
import co.com.humancapital.portal.util.ObjectMapperUtils;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class QuestionCategoryService {
    private final QuestionCategoryRepository categoryRepository;
    
    public QuestionCategoryDTO save(QuestionCategoryDTO categoryDTO) {
        QuestionCategory category = ObjectMapperUtils.map(categoryDTO, QuestionCategory.class);
        category = categoryRepository.save(category);
        return ObjectMapperUtils.map(category, QuestionCategoryDTO.class);
    }
    
    public List<QuestionCategoryDTO> listAll(Long clientId) {
        return ObjectMapperUtils.mapAll(categoryRepository.findAllByClientId(clientId), QuestionCategoryDTO.class);
    }
    
    public void delete(Long clientId, Long categoryId) {
        categoryRepository.findByIdAndClientId(categoryId, clientId)
                .ifPresent(category -> categoryRepository.deleteById(categoryId));
    }
}