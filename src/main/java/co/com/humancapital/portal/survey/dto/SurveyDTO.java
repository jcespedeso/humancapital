package co.com.humancapital.portal.survey.dto;

import java.time.LocalDate;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyDTO {
    private Long id;
    private boolean enabled;
    @NotNull
    private Long clientId;
    @NotNull
    private String name;
    @NotNull
    private LocalDate startDate;
    @NotNull
    private LocalDate endDate;
    private boolean timeoutEnabled;
    private int timeoutSeconds;
    private String disabledMessage;
    private String emailInvitationSubject;
    private String emailInvitationMessage;
    private String emailReminderSubject;
    private String emailReminderMessage;
    private List<@Valid  QuestionDTO> questions;
}
