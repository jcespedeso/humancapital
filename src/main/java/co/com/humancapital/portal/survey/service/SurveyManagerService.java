package co.com.humancapital.portal.survey.service;

import co.com.humancapital.portal.survey.entity.Survey;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class SurveyManagerService {
    private final QuestionService questionService;
    private final SurveyService surveyService;
    private final QuestionnaireService questionnaireService;
    
    public void saveUpdateQuestionnaire(Long clientId, Long surveyId, List<Long> evaluatorIds) {
        Survey survey = surveyService.findModel(surveyId)
                .filter(s -> s.getClientId() == clientId)
                .orElseThrow(() -> new IllegalArgumentException("::: Survey no encontrado con el id ["
                        + surveyId + "] para el cliente [" + clientId + "]"));
        
        questionnaireService.saveUpdateForEvaluators(survey, evaluatorIds);
    }
    
    @Transactional
    public void deleteSurvey(Long clientId, Long surveyId) {
        if (surveyService.exist(clientId, surveyId)) {
            questionnaireService.deleteBySurvey(surveyId);
            questionService.deleteBySurvey(surveyId);
            surveyService.delete(clientId, surveyId);
        } else {
            throw new IllegalArgumentException("::: Survey not found with id [" + surveyId + "]");
        }
    }
}
