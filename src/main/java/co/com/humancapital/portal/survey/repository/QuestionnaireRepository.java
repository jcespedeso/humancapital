package co.com.humancapital.portal.survey.repository;

import co.com.humancapital.portal.survey.entity.Questionnaire;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionnaireRepository extends PagingAndSortingRepository<Questionnaire, Long> {
    Optional<Questionnaire> findByIdAndSurveyId(Long id, Long surveyId);
    Optional<Questionnaire> findByEvaluatorIdAndSurveyId(Long evaluatorId, Long surveyId);
    void deleteBySurveyId(Long surveyId);
}