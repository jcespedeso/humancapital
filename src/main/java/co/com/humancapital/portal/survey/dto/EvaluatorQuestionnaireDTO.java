package co.com.humancapital.portal.survey.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EvaluatorQuestionnaireDTO {
    private Long questionId;
    private List<Long> employeeId;
}
