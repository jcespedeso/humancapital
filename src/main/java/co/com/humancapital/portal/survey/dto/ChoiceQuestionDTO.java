package co.com.humancapital.portal.survey.dto;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChoiceQuestionDTO extends QuestionDTO {
    private int columnsCount;
    private Map<Integer, String> choices;
}
