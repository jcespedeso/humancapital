package co.com.humancapital.portal.survey.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Data
public class Survey extends AuditableEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private LocalDate startDate;
    private boolean enabled;
    private Long clientId;
    @NotNull
    private LocalDate endDate;
    private boolean timeoutEnabled;
    private int timeoutSeconds;
    private String disabledMessage;
    private String emailInvitationSubject;
    @Column(columnDefinition = "text")
    private String emailInvitationMessage;
    private String emailReminderSubject;
    @Column(columnDefinition = "text")
    private String emailReminderMessage;
}