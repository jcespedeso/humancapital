package co.com.humancapital.portal.survey.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionGroupDTO {
    private Long id;
    @NotNull
    private Long clientId;
    @NotEmpty
    private String name;
}
