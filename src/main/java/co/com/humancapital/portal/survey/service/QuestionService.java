package co.com.humancapital.portal.survey.service;

import co.com.humancapital.portal.survey.dto.QuestionDTO;
import co.com.humancapital.portal.survey.dto.SurveyDTO;
import co.com.humancapital.portal.survey.entity.Question;
import co.com.humancapital.portal.survey.entity.Survey;
import co.com.humancapital.portal.survey.repository.QuestionRepository;
import co.com.humancapital.portal.util.ObjectMapperUtils;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static co.com.humancapital.portal.util.ObjectMapperUtils.map;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuestionService {
    private final QuestionRepository questionRepository;
    
    public QuestionDTO saveUpdate(QuestionDTO questionDTO) {
        Question question = map(questionDTO, Question.class);
        question = questionRepository.save(question);
        return map(question, QuestionDTO.class);
    }
    
    public List<QuestionDTO> findBySurvey(Long surveyId) {
        return ObjectMapperUtils.mapAll(questionRepository.findAllBySurveyId(surveyId), QuestionDTO.class);
    }
    
    public List<Question> findBySurveyModel(Long surveyId) {
        return questionRepository.findAllBySurveyId(surveyId);
    }
    
    public List<QuestionDTO> saveUpdateAll(SurveyDTO surveyDTO, List<QuestionDTO> questions) {
        Survey survey = ObjectMapperUtils.map(surveyDTO, Survey.class);
        
        return questions.stream()
                .map(questionDTO -> map(questionDTO, Question.class))
                .map(question -> question.setSurvey(survey))
                .map(question -> questionRepository.save(question))
                .map(question -> map(question, QuestionDTO.class))
                .collect(Collectors.toList());
    }
    
    public void deleteBySurvey(Long surveyId) {
        questionRepository.deleteBySurveyId(surveyId);
    }
}