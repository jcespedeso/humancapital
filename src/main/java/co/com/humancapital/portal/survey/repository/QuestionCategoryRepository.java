package co.com.humancapital.portal.survey.repository;

import co.com.humancapital.portal.survey.entity.QuestionCategory;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionCategoryRepository extends CrudRepository<QuestionCategory, Long> {
    List<QuestionCategory> findAllByClientId(Long clientId);
    Optional<QuestionCategory> findByIdAndClientId(Long id, Long clientId);
}
