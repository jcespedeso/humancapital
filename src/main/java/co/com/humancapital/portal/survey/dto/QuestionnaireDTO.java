package co.com.humancapital.portal.survey.dto;

import java.util.Map;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionnaireDTO {
    private Long id;
    @NotNull
    private Long evaluatorId;
    @NotEmpty
    private Map<Long, String> answers;
}