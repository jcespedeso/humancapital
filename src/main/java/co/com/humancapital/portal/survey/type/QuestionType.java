package co.com.humancapital.portal.survey.type;

public enum QuestionType {
    TEXT,
    CHECKBOX,
    RADIO,
    OPEN,
    IMAGE_SELECTOR,
    RATING,
    DROPDOWN
}