package co.com.humancapital.portal.survey.repository;

import co.com.humancapital.portal.survey.entity.Question;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends PagingAndSortingRepository<Question, Long> {
    List<Question> findAllBySurveyId(Long surveyId);
    void deleteBySurveyId(Long surveyId);
}