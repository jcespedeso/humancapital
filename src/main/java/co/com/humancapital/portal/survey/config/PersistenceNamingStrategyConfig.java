package co.com.humancapital.portal.survey.config;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class PersistenceNamingStrategyConfig extends PhysicalNamingStrategyStandardImpl {
    
    @Override
    public Identifier toPhysicalColumnName(final Identifier identifier, final JdbcEnvironment jdbcEnv) {
        return convertToSnakeUpperCase(identifier);
    }
    
    @Override
    public Identifier toPhysicalSequenceName(final Identifier identifier, final JdbcEnvironment jdbcEnv) {
        return convertToSnakeUpperCase(identifier);
    }
    
    @Override
    public Identifier toPhysicalTableName(final Identifier identifier, final JdbcEnvironment jdbcEnv) {
        return convertToSnakeUpperCase(identifier);
    }
    
    private Identifier convertToSnakeUpperCase(final Identifier identifier) {
        if (identifier == null) {
            return null;
        }
        
        SnakeCaseStrategy strategy = new PropertyNamingStrategy.SnakeCaseStrategy();
        String newIdentifier = strategy.translate(identifier.getText());
        return Identifier.toIdentifier(newIdentifier.toUpperCase(), identifier.isQuoted());
    }
}