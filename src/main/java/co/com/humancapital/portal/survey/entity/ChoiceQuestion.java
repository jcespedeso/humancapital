package co.com.humancapital.portal.survey.entity;

import java.util.Map;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.MapKeyColumn;
import lombok.Data;

@Entity
@Data
public class ChoiceQuestion extends Question {
    private int columnsCount;
    //Entry = <Value, Label>
    @ElementCollection
    @MapKeyColumn(name = "CHOICE_ID")
    @Column(name = "CHOICE_VALUE")
    private Map<Integer, String> choices;
}