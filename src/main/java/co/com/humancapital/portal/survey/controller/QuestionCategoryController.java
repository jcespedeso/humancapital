package co.com.humancapital.portal.survey.controller;

import co.com.humancapital.portal.survey.dto.QuestionCategoryDTO;
import co.com.humancapital.portal.survey.dto.QuestionGroupDTO;
import co.com.humancapital.portal.survey.service.QuestionCategoryService;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("{clientId}/survey/category")
@RequiredArgsConstructor
public class QuestionCategoryController {
    private final QuestionCategoryService categoryService;
    
    @PostMapping
    public QuestionCategoryDTO save(@RequestBody @Valid @NotNull QuestionCategoryDTO categoryDTO) {
        return categoryService.save(categoryDTO);
    }
    
    @GetMapping
    public List<QuestionCategoryDTO> listAll(@PathVariable Long clientId) {
        return categoryService.listAll(clientId);
    }
    
    @DeleteMapping("/{categoryId}")
    public void delete(@PathVariable Long clientId, @PathVariable Long categoryId) {
        categoryService.delete(clientId, categoryId);
    }
}