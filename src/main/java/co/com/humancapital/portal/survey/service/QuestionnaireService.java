package co.com.humancapital.portal.survey.service;

import co.com.humancapital.portal.survey.dto.QuestionnaireDTO;
import co.com.humancapital.portal.survey.entity.Question;
import co.com.humancapital.portal.survey.entity.Questionnaire;
import co.com.humancapital.portal.survey.entity.Survey;
import co.com.humancapital.portal.survey.repository.QuestionnaireRepository;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static co.com.humancapital.portal.util.ObjectMapperUtils.map;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuestionnaireService {
    private final QuestionService questionService;
    private final QuestionnaireRepository questionnaireRepository;
    
    protected void saveUpdateForEvaluators(Survey survey, List<Long> evaluatorIds) {
        Map<Long, String> answers = questionService.findBySurveyModel(survey.getId()).stream()
                .collect(Collectors.toMap(Question::getId, question -> StringUtils.EMPTY));
        
        evaluatorIds.forEach(evaluatorId -> {
            Questionnaire questionnaire = Questionnaire.builder()
                    .survey(survey)
                    .evaluatorId(evaluatorId)
                    .answers(answers)
                    .build();
            questionnaireRepository.save(questionnaire);
        });
    }
    
    public QuestionnaireDTO findByEvaluator(Long surveyId, Long evaluatorId) {
        return questionnaireRepository.findByEvaluatorIdAndSurveyId(evaluatorId, surveyId)
                .map(questionnaire -> map(questionnaire, QuestionnaireDTO.class))
                .orElseThrow(() -> new IllegalArgumentException("::: Questionnaire no encontrado con el survey ["
                        + surveyId + "] y evaluatorId [" + evaluatorId + "]"));
    }
    
    
    
    public QuestionnaireDTO update(Long clientId, Long surveyId, QuestionnaireDTO questionnaireDTO) {
        Questionnaire questionnaire = questionnaireRepository
                .findByEvaluatorIdAndSurveyId(questionnaireDTO.getEvaluatorId(), surveyId)
                .filter(persistedQuestionnaire -> persistedQuestionnaire.getSurvey().getClientId() == clientId)
                .orElseThrow(() -> new IllegalArgumentException("::: Cuestionario con evaluador ["
                        + questionnaireDTO.getEvaluatorId() + "] no encontrado en el cliente [" + clientId + "]"));
        
        Map<Long, String> answers = questionnaireDTO.getAnswers();
        questionnaire.getAnswers().entrySet().forEach(entry -> {
            if (answers.containsKey(entry.getKey())) {
                entry.setValue(answers.get(entry.getKey()));
            }
        });
        
        questionnaire = questionnaireRepository.save(questionnaire);
        return map(questionnaire, QuestionnaireDTO.class);
    }
    
    public void deleteBySurvey(Long surveyId) {
        questionnaireRepository.deleteBySurveyId(surveyId);
    }
}
