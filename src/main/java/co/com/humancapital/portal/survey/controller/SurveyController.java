package co.com.humancapital.portal.survey.controller;

import co.com.humancapital.portal.survey.dto.QuestionnaireDTO;
import co.com.humancapital.portal.survey.dto.SurveyDTO;
import co.com.humancapital.portal.survey.service.QuestionnaireService;
import co.com.humancapital.portal.survey.service.SurveyManagerService;
import co.com.humancapital.portal.survey.service.SurveyService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("{clientId}/survey")
@RequiredArgsConstructor
public class SurveyController {
    private final SurveyService surveyService;
    private final QuestionnaireService questionnaireService;
    private final SurveyManagerService surveyManagerService;
    
    @GetMapping
    public List<SurveyDTO> list(@PathVariable Long clientId) {
        return surveyService.list(clientId);
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SurveyDTO save(@RequestBody @Valid SurveyDTO surveyDTO) {
        return surveyService.saveUpdate(surveyDTO);
    }
    
    @DeleteMapping("/{surveyId}")
    public void delete(@PathVariable Long clientId, @PathVariable Long surveyId) {
        surveyManagerService.deleteSurvey(clientId, surveyId);
    }
    
    @PostMapping("/{surveyId}/questionnaire")
    public void createQuestionnaire(@PathVariable Long clientId,
            @PathVariable Long surveyId, @RequestBody List<Long> evaluatorsId) {
        surveyManagerService.saveUpdateQuestionnaire(clientId, surveyId, evaluatorsId);
    }
    
    @GetMapping("/{surveyId}/questionnaire/{evaluatorId}")
    public QuestionnaireDTO findQuestionnaireByEvaluator(@PathVariable Long surveyId, @PathVariable Long evaluatorId) {
        return questionnaireService.findByEvaluator(surveyId, evaluatorId);
    }
    
    @PostMapping("/{surveyId}/questionnaire/{evalua torId}")
    public QuestionnaireDTO updateAnswersInQuestionnaire(@PathVariable Long clientId,
            @PathVariable Long surveyId, @RequestBody QuestionnaireDTO questionnaire) {
        return questionnaireService.update(clientId, surveyId, questionnaire);
    }
}