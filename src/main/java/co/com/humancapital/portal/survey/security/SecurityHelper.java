package co.com.humancapital.portal.survey.security;

import co.com.humancapital.portal.entity.Usuario;
import co.com.humancapital.portal.exception.JWTException;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.JWTService;
import co.com.humancapital.portal.service.UsuarioService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class SecurityHelper {
    public static final String AUTH_JWT_HEADER = "Authorization";
    private final JWTService jwtService;
    private final UsuarioService usuarioService;
    
    public Long getClientId(String headerToken) {
        try {
            String username = jwtService.validate(jwtService.getToken(headerToken));
            Usuario usuario = usuarioService.findByUsernameAndActivoTrue(username)
                    .orElseThrow(() -> new ResourceNotFoundException("Usuario no encontrado."));
            return usuario.getCliente().getIdCliente();
        } catch (JWTException e) {
            log.warn("::: Error tratando de obtener info del JWT", e);
            throw new UnauthorizedException("Sesión finalizada");
        }
    }
}
