package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.ClienteInputDTO;
import co.com.humancapital.portal.dto.ClienteOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.ClienteService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Céspedes Ortega
 * @date 13/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Cliente.
 */
@RestController
@RequestMapping("/cliente")
public class ClienteController extends AbstractController<ClienteInputDTO, GeneralOutput, Long> {

    private final String VIEW   = "EMPRESA_VER";
    private final String CREATE = "EMPRESA_CREAR";
    private final String UPDATE = "EMPRESA_EDITAR";
    private final String DELETE = "EMPRESA_ELIMINAR";
    private final String ASIGNAR_MODULO = "ASIGNAR_MODULO";
    private final String ASIGNAR_PRODUCTO = "ASIGNAR_PRODUCTO";

    private final ClienteService clienteService;

    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),ClienteOutputDTO.getDTO(clienteService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"razonSocial","nit"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ClienteOutputDTO> clienteDTOList=new ArrayList<>();
        for (Cliente cliente:clienteService.findAll(pageable)) {
            ClienteOutputDTO clienteDTO = ClienteOutputDTO.getDTO(cliente);
            clienteDTOList.add(clienteDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),clienteDTOList, clienteService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody ClienteInputDTO entity, @RequestHeader("Authorization") String token ) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        clienteService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update (@Valid @RequestBody ClienteInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        clienteService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete (@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        clienteService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/nit/{nit}")
    public GeneralOutput getByNit(@PathVariable String nit, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),ClienteOutputDTO.getDTO(clienteService.findNit(nit)));
    }

    @PutMapping("/asignarModulo")
    public GeneralOutput asignarModulo(@RequestBody ClienteInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token.trim(),ASIGNAR_MODULO)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        clienteService.asignarMudulo(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Asignación Realizada Exitosamente...");
    }

    @PutMapping("/asignarProducto")
    public GeneralOutput asignarProducto(@RequestBody ClienteInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token.trim(),ASIGNAR_PRODUCTO)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        clienteService.asignarProducto(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Asignación Realizada Exitosamente...");
    }

    @GetMapping("/usuario/{username}")
    public GeneralOutput getById(@PathVariable String username, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),ClienteOutputDTO.getDTO (clienteService.clienteXUsuario(username)));
    }

    @GetMapping("/cargo/{idCargo}")
    public GeneralOutput getByCargo(@PathVariable Long idCargo, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),ClienteOutputDTO.getDTO (clienteService.findXCargo(idCargo)));
    }

    @GetMapping("/depto/{idDepto}")
    public GeneralOutput getByDepartamento(@PathVariable Long idDepto, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),ClienteOutputDTO.getDTO (clienteService.findXDepartamento(idDepto)));
    }

    @Override
    public GeneralOutput create(ClienteInputDTO entity, @RequestHeader("Authorization") String token,  @RequestParam("archivo") List<MultipartFile> files) {
        return null;
    }
    @Override
    public GeneralOutput update(ClienteInputDTO entity, @RequestHeader("Authorization") String token,  @RequestParam("archivo") List<MultipartFile> files) {
        return null;
    }

}
