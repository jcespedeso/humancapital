package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.ParametroInputDTO;
import co.com.humancapital.portal.dto.ParametroOutputDTO;
import co.com.humancapital.portal.entity.Parametro;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.ParametroService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 13/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Parametro.
 */
@RestController
@RequestMapping("/parametro")
public class ParametroController extends AbstractController<ParametroInputDTO, GeneralOutput,Long>{

    private final String CREATE = "PARAMETRO_CREAR";
    private final String UPDATE = "PARAMETRO_EDITAR";
    private final String VIEW = "PARAMETRO_VER";
    private final String DELETE = "PARAMETRO_ELIMINAR";

    private final ParametroService parametroService;

    public ParametroController(ParametroService parametroService) {
        this.parametroService = parametroService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), ParametroOutputDTO.getDTO(parametroService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable)throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ParametroOutputDTO> parametroOutputDTOS = new ArrayList<>();
        for (Parametro parametro:parametroService.findAll(pageable)) {
            ParametroOutputDTO parametroOutputDTO = ParametroOutputDTO.getDTO(parametro);
            parametroOutputDTOS.add(parametroOutputDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),parametroOutputDTOS,parametroService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody ParametroInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token.trim(),CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        parametroService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody ParametroInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token.trim(),UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        parametroService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token.trim(),DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        parametroService.delete(id,usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/cliente/{idCliente}")
    public GeneralOutput ObtenerParametrosXCliente(@PathVariable Long idCliente, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable)throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ParametroOutputDTO> parametroOutputDTOS = new ArrayList<>();
        for (Parametro parametro:parametroService.obtenerParametroXCliente(idCliente,pageable)) {
            ParametroOutputDTO parametroOutputDTO = ParametroOutputDTO.getDTO(parametro);
            parametroOutputDTOS.add(parametroOutputDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),parametroOutputDTOS,parametroService.obtenerParametroXCliente(idCliente,pageable));
    }

    @Override
    public GeneralOutput create(ParametroInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(ParametroInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

}
