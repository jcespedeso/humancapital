package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.ContactoEmergenciaInputDTO;
import co.com.humancapital.portal.dto.ContactoEmergenciaOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.ContactoEmergencia;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.ContactoEmergenciaService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad ContactoEmergencia.
 */
@RestController
@RequestMapping("/contactoEmergencia")
public class ContactoEmergenciaController extends AbstractController<ContactoEmergenciaInputDTO, GeneralOutput, Long>{

    private final String CREATE = "CONTEMERGENCIA_CREAR";
    private final String UPDATE = "CONTEMERGENCIA_EDITAR";
    private final String VIEW = "CONTEMERGENCIA_VER";
    private final String DELETE = "CONTEMERGENCIA_ELIMINAR";

    private final ContactoEmergenciaService contactoEmergenciaService;

    public ContactoEmergenciaController(ContactoEmergenciaService contactoEmergenciaService) {
        this.contactoEmergenciaService = contactoEmergenciaService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), ContactoEmergenciaOutputDTO.getDTO(contactoEmergenciaService.findId(id)));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody ContactoEmergenciaInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        contactoEmergenciaService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody ContactoEmergenciaInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        contactoEmergenciaService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        contactoEmergenciaService.delete(id);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/empleado/{idEmpleado}")
    public GeneralOutput contactoEmergenciaXEmpleado(@PathVariable Long idEmpleado, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre","apellido"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ContactoEmergenciaOutputDTO> listDto = new ArrayList<>();
        for (ContactoEmergencia contactoEmergencia:contactoEmergenciaService.contactoEmergenciasXEmpleado(idEmpleado,pageable)) {
            listDto.add(ContactoEmergenciaOutputDTO.getDTO(contactoEmergencia));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),listDto,contactoEmergenciaService.contactoEmergenciasXEmpleado(idEmpleado,pageable));
    }

    @Override
    public GeneralOutput getAll(String token, Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(ContactoEmergenciaInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(ContactoEmergenciaInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
