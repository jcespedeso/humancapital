package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.SalarioFlexibleXProductoInputDTO;
import co.com.humancapital.portal.entity.SalarioFlexibleXProducto;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.SalarioFlexibleXProductoService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 22/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad SalarioFlexibleXProducto.
 */
@RestController
@RequestMapping("/salarioFlexXProducto")
public class SalarioFlexibleXProductoController extends AbstractController<SalarioFlexibleXProductoInputDTO, GeneralOutput,Long>{

    private final String CREATE = "RFI_CREAR";
    private final String UPDATE = "RFI_EDITAR";
    private final String VIEW = "RFI_VER";
    private final String DELETE = "RFI_ELIMINAR";

    private final SalarioFlexibleXProductoService salarioFlexibleXProductoService;

    public SalarioFlexibleXProductoController(SalarioFlexibleXProductoService salarioFlexibleXProductoService) {
        this.salarioFlexibleXProductoService = salarioFlexibleXProductoService;
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody SalarioFlexibleXProductoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        salarioFlexibleXProductoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody SalarioFlexibleXProductoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        salarioFlexibleXProductoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @DeleteMapping("/delete/{id}")
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        salarioFlexibleXProductoService.delete(id);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/empleado/{idEmpleado}")
    public GeneralOutput getByIdEmpleado(@PathVariable Long idEmpleado, @RequestHeader("Authorization") String token, Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<SalarioFlexibleXProductoInputDTO> listEntityDTO = new ArrayList<>();
        for (SalarioFlexibleXProducto list:salarioFlexibleXProductoService.findAllEmpleado(idEmpleado,pageable)) {
            listEntityDTO.add(SalarioFlexibleXProductoInputDTO.getDTO(list));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),listEntityDTO,salarioFlexibleXProductoService.findAllEmpleado(idEmpleado,pageable));
    }

    @Override
    public GeneralOutput getById(Long id, @RequestHeader("Authorization") String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput getAll(String token, Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(SalarioFlexibleXProductoInputDTO entity, String token, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput update(SalarioFlexibleXProductoInputDTO entity, String token, List<MultipartFile> files) throws Exception {
        return null;
    }
}
