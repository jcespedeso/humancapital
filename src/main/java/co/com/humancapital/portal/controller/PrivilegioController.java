package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.PrivilegioInputDTO;
import co.com.humancapital.portal.dto.PrivilegioOutputDTO;
import co.com.humancapital.portal.entity.Privilegio;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.PrivilegioService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 28/12/2019
 * Clase generica de programadores, solo implementa búsquedas por Id y ALL.
 */
@RestController
@RequestMapping("/privilegio")
public class PrivilegioController extends AbstractController<PrivilegioInputDTO, GeneralOutput, String>{

    private final String VIEW = "PRIVILEGIO_VER";

    private final PrivilegioService privilegioService;

    public PrivilegioController(PrivilegioService privilegioService) {
        this.privilegioService = privilegioService;
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token,@PageableDefault(sort = {"categoria","nombrePrivilegio"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<PrivilegioOutputDTO> list = new ArrayList<>();
        for (Privilegio privilegio:privilegioService.findAll(pageable)) {
            list.add(PrivilegioOutputDTO.getDTO(privilegio));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), list, privilegioService.findAll(pageable));
    }

    @GetMapping("/privilegioSesion")
    public GeneralOutput obtenerPrivilegiosUsuarioSesion(@RequestHeader("Authorization") String token, Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<PrivilegioOutputDTO> list = new ArrayList<>();
        for (Privilegio privilegio : privilegioService.findPrivilegiosForUsername(usuarioSesion(token).getUsername(),pageable)) {
            list.add(PrivilegioOutputDTO.getDTO(privilegio));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list, privilegioService.findPrivilegiosForUsername(usuarioSesion(token).getUsername(),pageable));
    }

    @GetMapping("/username/{username}")
    public GeneralOutput obtenerPrivilegiosUsuario(@PathVariable String username, @RequestHeader("Authorization") String token, Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<PrivilegioOutputDTO> list = new ArrayList<>();
        for (Privilegio privilegio : privilegioService.findPrivilegiosForUsername(username,pageable)) {
            list.add(PrivilegioOutputDTO.getDTO(privilegio));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list, privilegioService.findPrivilegiosForUsername(username,pageable));
    }

    @Override
    public GeneralOutput getById(String id, String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(PrivilegioInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput create(PrivilegioInputDTO entity, String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput update(PrivilegioInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(PrivilegioInputDTO entity, String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput delete(String id, String token) throws Exception {
        return null;
    }
}


