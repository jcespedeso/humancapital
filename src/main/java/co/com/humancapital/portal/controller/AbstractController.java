package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.entity.Usuario;
import co.com.humancapital.portal.exception.JWTException;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.JWTService;
import co.com.humancapital.portal.service.UsuarioService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Alejandro Herrera Montilla
 * @project humancapital
 * @date 10/01/2020
 */
@RestController
public abstract class AbstractController<E, EO, PK > implements ControllerInterface<E, EO, PK> {

    @Autowired
    JWTService jwtService;

    @Autowired
    UsuarioService usuarioService;

    /**
     * Metodo que permite obtener el Usuario en sesión.
     * @param headerToken Token enviado en el header de la petición.
     * @return Objeto Usuario.
     * @throws Exception NotFound - Unathorized.
     */
    protected Usuario usuarioSesion(String headerToken) throws Exception{
        try {
            String username = jwtService.validate(jwtService.getToken(headerToken));
            Optional<Usuario> usuarioOp = usuarioService.findByUsernameAndActivoTrue(username);
            if(! usuarioOp.isPresent()){
                throw new ResourceNotFoundException("Usuario no encontrado.");
            } else {
                return usuarioOp.get();
            }
        } catch (JWTException e) {
            e.printStackTrace();
            throw new UnauthorizedException("Sesión finalizada");
        }
    }

    /**
     * Metodo que permite validar si un Usuario tiene asignado el Privilegio enviado.
     * @param codigoPrivilegio Codigo del Privilegio a evaluar.
     * @param usuario Objeto Usuario a consultar.
     * @return @true si tiene el Privilegio asignado y de lo contario @false.
     */
    protected boolean isValidoPrivilegio(String codigoPrivilegio, Usuario usuario) throws Exception{
        return usuarioService.isValidateRol(codigoPrivilegio, usuario);
    }

    /**
     * Metodo que permite evaluar por intermedio del Token si el usuario esta autorizado y si este tiene el Privilegio asignado.
     * @param token Token de autorización que se evalua para obtener el Usuario en sesión.
     * @param privilegio Codigo del Privilegio que se evalua contra el Usuario para determinar si lo tiene o no asignado.
     * @return @true si el Privilegio lo tiene asignado de lo contrario @false.
     * @throws Exception NotFound - Unathorized.
     */
    protected boolean isAuthorized(String token, String privilegio) throws Exception{
        if(token==null){
            throw new UnauthorizedException("Acceso Denegado.");
        }
        if(privilegio==null){
            throw new ResourceNotFoundException("Acceso Denegado.");
        }
        Usuario usuarioSession = usuarioSesion(token);
        if(isValidoPrivilegio("ALL", usuarioSession)){
            return true;
        }
        if(!isValidoPrivilegio(privilegio, usuarioSession)){
            return false;
        }
        return true;
    }

}
