package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.EmpleadoInputDTO;
import co.com.humancapital.portal.dto.EmpleadoOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.Usuario;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.EmpleadoService;
import co.com.humancapital.portal.service.FileSystemService;
import co.com.humancapital.portal.service.SalarioFlexibleService;
import co.com.humancapital.portal.service.UsuarioService;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 13/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Empleado.
 */
@RestController
@RequestMapping("/empleado")
public class EmpleadoController extends AbstractController<EmpleadoInputDTO, GeneralOutput, Long>{

    private final String CREATE = "EMPLEADO_CREAR";
    private final String UPDATE = "EMPLEADO_EDITAR";
    private final String VIEW = "EMPLEADO_VER";
    private final String VIEW_CARGO = "EMPLEADO_VER_X_CARGO";
    private final String VIEW_DEPTO = "EMPLEADO_VER_X_DEPTO";
    private final String VIEW_CLIENTE = "EMPLEADO_VER_X_CLIENTE";
    private final String DELETE = "EMPLEADO_ELIMINAR";

    private final EmpleadoService empleadoService;

    private final UsuarioService usuarioService;

    private final FileSystemService fileSystemService;

    private final SalarioFlexibleService salarioFlexibleService;

    public EmpleadoController(EmpleadoService empleadoService, UsuarioService usuarioService, FileSystemService fileSystemService, SalarioFlexibleService salarioFlexibleService) {
        this.empleadoService = empleadoService;
        this.usuarioService = usuarioService;
        this.fileSystemService = fileSystemService;
        this.salarioFlexibleService = salarioFlexibleService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),EmpleadoOutputDTO.getDTO(empleadoService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token,@PageableDefault(sort = {"nombre","apellido"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<EmpleadoOutputDTO> empleadoOutputDTOS = new ArrayList<>();
        for (Empleado empleado:empleadoService.findAll(pageable)) {
            EmpleadoOutputDTO empleadoDTO = EmpleadoOutputDTO.getDTO(empleado);
            empleadoOutputDTOS.add(empleadoDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),empleadoOutputDTOS,empleadoService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid EmpleadoInputDTO entity, @RequestHeader("Authorization") String token, @RequestParam("archivo") List<MultipartFile> files) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        empleadoService.create(entity,files);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid EmpleadoInputDTO entity,@RequestHeader("Authorization") String token, @RequestParam("archivo") List<MultipartFile> files) throws Exception{
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        empleadoService.update(entity, files);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        empleadoService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/cliente/{id}")
    public GeneralOutput empleadosXCliente(@PathVariable Long id, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre","apellido"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<EmpleadoOutputDTO> empleadoOutputDTOS = new ArrayList<>();
        for (Empleado empleado: empleadoService.empleadosXCliente(id, pageable)) {
            empleadoOutputDTOS.add(EmpleadoOutputDTO.getDTO(empleado));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),empleadoOutputDTOS,empleadoService.empleadosXCliente(id, pageable));
    }

    @GetMapping("/departamento/{id}")
    public GeneralOutput empleadosXDepartamento(@PathVariable Long id,@RequestHeader("Authorization") String token,@PageableDefault(sort = {"nombre","apellido"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW_DEPTO)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<EmpleadoOutputDTO> empleadoOutputDTOS = new ArrayList<>();
        for (Empleado empleado: empleadoService.empleadosXDepartamento(id, pageable)) {
            empleadoOutputDTOS.add(EmpleadoOutputDTO.getDTO(empleado));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),empleadoOutputDTOS, empleadoService.empleadosXDepartamento(id, pageable));
    }

    @GetMapping("/cargo/{id}")
    public GeneralOutput empleadosXCargo(@PathVariable Long id, @RequestHeader("Authorization") String token,@PageableDefault(sort = {"nombre","apellido"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW_CARGO)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<EmpleadoOutputDTO> empleadoOutputDTOS = new ArrayList<>();
        for (Empleado empleado: empleadoService.empleadosXCargo(id,pageable)) {
            empleadoOutputDTOS.add(EmpleadoOutputDTO.getDTO(empleado));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),empleadoOutputDTOS,empleadoService.empleadosXCargo(id,pageable));
    }

    @GetMapping("/jefes/{idCliente}")
    public GeneralOutput empleadosXJefesXClinte(@PathVariable Long idCliente, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre","apellido"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<EmpleadoOutputDTO> empleadoOutputDTOS = new ArrayList<>();
        for (Empleado empleado: empleadoService.empleadosJefes(idCliente, pageable)) {
            empleadoOutputDTOS.add(EmpleadoOutputDTO.getDTO(empleado));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),empleadoOutputDTOS, empleadoService.empleadosJefes(idCliente, pageable));
    }

    @GetMapping("/numDocumento/{documento}")
    public GeneralOutput empleadosXDocumento(@PathVariable String documento,@RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<EmpleadoOutputDTO> empleadoOutputDTOS = new ArrayList<>();
        if(empleadoService.empleadosXDocumento(documento).size()==0) {
            throw new ResourceNotFoundException("El Empleado con numero de documento " + documento +" no existe.");
        }
        for (Empleado empleado: empleadoService.empleadosXDocumento(documento)) {
            empleadoOutputDTOS.add(EmpleadoOutputDTO.getDTO(empleado));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),empleadoOutputDTOS);
    }

    @GetMapping("/codigo/{codigo}")
    public GeneralOutput empleadoXCodigo(@PathVariable String codigo, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        if(empleadoService.empleadosXCodigo(codigo)==null) {
            throw new ResourceNotFoundException("El Empleado con el codigo " + codigo +" no existe.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),EmpleadoOutputDTO.getDTO(empleadoService.empleadosXCodigo(codigo)));
    }

    @GetMapping("/infoFlex/{idCliente}")
    public GeneralOutput empleadoXCodigo(@PathVariable Long idCliente, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        Map<String, Object> countflex = new LinkedHashMap<>();
        countflex.put("totalEmpleados", empleadoService.countEmpleadoXCliente(idCliente));
        countflex.put("empleadosElegibles",empleadoService.countEmpleadoFlex(idCliente)==null?0:empleadoService.countEmpleadoFlex(idCliente).intValue());
        countflex.put("empleadosFlexibilizados", salarioFlexibleService.countEstadoFlex(idCliente,"3")==null?0:salarioFlexibleService.countEstadoFlex(idCliente,"3"));
        countflex.put("empleadosNoElegibles", (int)countflex.get("totalEmpleados")-(int)countflex.get("empleadosElegibles"));
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),countflex);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite descargar la foto del Empleado.
     * @param response Representa la respuesta del servidor
     * @param id del Empleado del cual se desea obtener la foto.
     */
    @RequestMapping(path = "/foto/{id}", method = RequestMethod.GET)
    public void obtenerFoto(HttpServletResponse response, @PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        Empleado empleado = empleadoService.findId(id);
        if(id == null || !empleadoService.existeById(id)){
            throw new UnauthorizedException("El Empleado con id "+id+" no existe.");
        }
        fileSystemService.descargarArchivo(response,new Long(empleado.getFoto()));
    }

    @PutMapping("/activar/{id}")
    public GeneralOutput activarEmpleado(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        if(id == null || !empleadoService.empleadoInactivo(id)) {
            throw new ResourceNotFoundException("El Empleado con id " + id +" no existe o no se encuentra inactivo.");
        }
        Empleado empleado = empleadoService.optenerEmpleadoInactivo(id);
        empleadoService.activarEmpleado(id, usuarioSesion(token).getIdUsuario());
        Usuario usuario = usuarioService.optenerUsuarioInactivoPorEmpleado(empleado.getIdEmpleado());
        usuarioService.activarUsuario(usuario.getIdUsuario(),usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),"Empleado Activado Exitosamente.");
    }

    @GetMapping("/listInactivo")
    public GeneralOutput getAllInactivo(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre","apellido"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<EmpleadoOutputDTO> empleadoDTOList=new ArrayList<>();
        for (Empleado empleado:empleadoService.optenerEmpleadoInactivos(pageable)) {
            EmpleadoOutputDTO empDTO = EmpleadoOutputDTO.getDTO(empleado);
            empleadoDTOList.add(empDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),empleadoDTOList, empleadoService.optenerEmpleadoInactivos(pageable));
    }

    @Override
    public GeneralOutput create(EmpleadoInputDTO entity, String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput update(EmpleadoInputDTO entity, String token) throws Exception {
        return null;
    }
}
