package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.exception.IntegrityViolationException;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.exception.StorageFileNotFoundException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.util.ErrorMensaje;
import java.sql.SQLException;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.AnnotationException;
import org.hibernate.DuplicateMappingException;
import org.hibernate.PropertyNotFoundException;
import org.hibernate.QueryException;
import org.hibernate.boot.InvalidMappingException;
import org.hibernate.boot.MappingNotFoundException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.mapping.MappingException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * @author Alejandro Herrera Montilla
 * @date 20/11/2019 Controla las Excepciones generadas por la aplicación.
 */
@Slf4j
@RestControllerAdvice
public class ApiExceptionHandler {
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({ResourceNotFoundException.class, StorageFileNotFoundException.class})
    @ResponseBody
    public ErrorMensaje notFoundRequest(HttpServletRequest request, Exception exception) {
        log.error(exception.getMessage(), exception);
        return new ErrorMensaje(exception, HttpStatus.NOT_FOUND.value(), request.getRequestURI());
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ErrorMensaje validationException(MethodArgumentNotValidException exception, HttpServletRequest request) {
        log.error(exception.getMessage(), exception);
        return new ErrorMensaje(exception, HttpStatus.BAD_REQUEST.value(),
                exception.getBindingResult().getAllErrors().get(0).getDefaultMessage(), request.getRequestURI());
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NumberFormatException.class)
    @ResponseBody
    public ErrorMensaje formatError(NumberFormatException exception, HttpServletRequest request) {
        log.error(exception.getMessage(), exception);
        return new ErrorMensaje(exception, HttpStatus.BAD_REQUEST.value(),
                "Error al convertir un valor numerico a texto.", request.getRequestURI());
    }
    
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({IntegrityViolationException.class})
    @ResponseBody
    public ErrorMensaje integrityViolation(HttpServletRequest request, Exception exception) {
        log.error(exception.getMessage(), exception);
        return new ErrorMensaje(exception, HttpStatus.UNPROCESSABLE_ENTITY.value(), exception.getMessage(),
                request.getRequestURI());
    }
    
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({DataIntegrityViolationException.class, ConstraintViolationException.class, SQLException.class,
            EntityNotFoundException.class, SQLGrammarException.class, QueryException.class,
            InvalidDataAccessApiUsageException.class})
    @ResponseBody
    public ErrorMensaje integrityViolation(HttpServletRequest request, HttpServletResponse response,
            Exception exception) {
        log.error(exception.getMessage(), exception);
        return new ErrorMensaje(exception, HttpStatus.UNPROCESSABLE_ENTITY.value(),
                "No se pudo ejecutar la sentencia, contacte al administrador.", request.getRequestURI());
    }
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({MappingException.class, AnnotationException.class, DuplicateMappingException.class,
            InvalidMappingException.class, MappingNotFoundException.class, PropertyNotFoundException.class})
    @ResponseBody
    public ErrorMensaje mapping(HttpServletRequest request, Exception exception) {
        log.error(exception.getMessage(), exception);
        return new ErrorMensaje(exception, HttpStatus.NOT_FOUND.value(),
                "El mapeo invalido o no esta generando resultados.", request.getRequestURI());
    }
    
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({UnauthorizedException.class})
    @ResponseBody
    public ErrorMensaje badForbidden(HttpServletRequest request, Exception exception) {
        log.error(exception.getMessage(), exception);
        return new ErrorMensaje(exception, HttpStatus.UNAUTHORIZED.value(), exception.getMessage(),
                request.getRequestURI());
    }
    
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({NullPointerException.class})
    @ResponseBody
    public ErrorMensaje nullPoint(HttpServletRequest request, Exception exception) {
        log.error(exception.getMessage(), exception);
        return new ErrorMensaje(exception, HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Error interno de la aplicación, contacte al administrador.", request.getRequestURI());
    }
    
    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    protected ErrorMensaje genericHandler(HttpServletRequest request, Exception exception) {
        log.error("::: Exception not controlled\n", exception);
        return new ErrorMensaje(exception, HttpStatus.INTERNAL_SERVER_ERROR.value(), request.getRequestURI(), "");
    }
}
