package co.com.humancapital.portal.controller.rfi.loader;

import co.com.humancapital.portal.controller.AbstractController;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.rfi.service.loader.LoaderTemplateService;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import static co.com.humancapital.portal.constant.LoaderConstant.CONTENT_DISPOSITION_HEADER;
import static co.com.humancapital.portal.constant.LoaderConstant.EXCEL_ATTACHMENT_HEADER_VALUE;
import static co.com.humancapital.portal.constant.LoaderConstant.EXCEL_MEDIA_TYPE;

@Slf4j
public abstract class AbstractLoaderTemplateController extends AbstractController {
    
    protected static final String ROOT_PATH = "/loader";
    private static final String DEFAULT_PRIVILEGE = "ALL";
    
    public abstract void downloadTemplate(HttpServletResponse response, String token) throws Exception;
    
    public abstract void loadTemplate(MultipartFile file, Long clientId,
            HttpServletResponse response, String token) throws Exception;
    
    protected abstract String getTemplateFileName();
    
    protected void downloadTemplate(byte[] templateInfo, HttpServletResponse response, String token) throws Exception {
        if(!isAuthorized(token, DEFAULT_PRIVILEGE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        
        try (ByteArrayInputStream bais = new ByteArrayInputStream(templateInfo)) {
            response.setHeader(CONTENT_DISPOSITION_HEADER,
                    String.format(EXCEL_ATTACHMENT_HEADER_VALUE, getTemplateFileName()));
            response.setContentType(EXCEL_MEDIA_TYPE);
            IOUtils.copy(bais, response.getOutputStream());
        }
    }
    
    protected void loadTemplate(MultipartFile file, Long clientId,
            LoaderTemplateService loaderTemplateService,
            HttpServletResponse response, String token) throws Exception {
        log.info("::: Loading file [{}] with contentType [{}] for client [{}]",
                file.getOriginalFilename(), file.getContentType(), clientId);
        
        try (InputStream fis = file.getInputStream()) {
            byte[] fileResponse = loaderTemplateService.loadTemplate(fis, clientId);
            downloadTemplate(fileResponse, response, token);
        }
    }
}
