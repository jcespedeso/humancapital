package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.DatoTributarioInputDTO;
import co.com.humancapital.portal.dto.DatoTributarioOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.DatoTributarioService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 16/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad DatoTributario.
 */
@RestController
@RequestMapping("/datoTributario")
public class DatoTributarioController extends AbstractController<DatoTributarioInputDTO, GeneralOutput, Long>{

    private final String CREATE = "EMPLEADO_CREAR";
    private final String UPDATE = "EMPLEADO_EDITAR";
    private final String VIEW = "EMPLEADO_VER";
    private final String DELETE = "EMPLEADO_ELIMINAR";

    private final DatoTributarioService datoTributarioService;

    public DatoTributarioController(DatoTributarioService datoTributarioService) {
        this.datoTributarioService = datoTributarioService;
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody DatoTributarioInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }

        datoTributarioService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody DatoTributarioInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        datoTributarioService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @DeleteMapping("/delete/{id}")
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        datoTributarioService.delete(id);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/empleados/{id}")
    public GeneralOutput getByIdEmpleado(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),DatoTributarioOutputDTO.getDTO(datoTributarioService.findByEmpleado(id)));
    }

    @Override
    public GeneralOutput getById(Long id, @RequestHeader("Authorization") String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput getAll(String token, Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(DatoTributarioInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(DatoTributarioInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
