package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.DatoAdicionalEmpInputDTO;
import co.com.humancapital.portal.dto.DatoAdicionalEmpOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.DatoAdicionalEmpleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.DatoAdicionalEmpleadoService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 04/02/2020 Clase para la implementación de los diferentes microservicios provistos para la entidad
 * DatoAdicionalEmpleado.
 */
@RestController
@RequestMapping("/datoAdicional")
public class DatoAdicionalEmpleadoController extends AbstractController<DatoAdicionalEmpInputDTO, GeneralOutput, Long> {
    
    private final String CREATE = "DATO_ADICIONAL_CREAR";
    private final String UPDATE = "DATO_ADICIONAL_EDITAR";
    private final String VIEW = "DATO_ADICIONAL_VER";
    private final String DELETE = "DATO_ADICIONAL_ELIMINAR";
    
    private final DatoAdicionalEmpleadoService datoAdicionalEmpleadoService;
    
    public DatoAdicionalEmpleadoController(DatoAdicionalEmpleadoService datoAdicionalEmpleadoService) {
        this.datoAdicionalEmpleadoService = datoAdicionalEmpleadoService;
    }
    
    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        if (!datoAdicionalEmpleadoService.existeById(id)) {
            throw new ResourceNotFoundException("El Dato Adicional con id " + id + " no existe.");
        }
        return new GeneralOutput(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(),
                DatoAdicionalEmpOutputDTO.getDTO(datoAdicionalEmpleadoService.findId(id)));
    }
    
    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token,
            @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<DatoAdicionalEmpOutputDTO> datoAdicionalEmpOutputDTOS = new ArrayList<>();
        for (DatoAdicionalEmpleado datoAdicionalEmpleado : datoAdicionalEmpleadoService.findAll(pageable)) {
            datoAdicionalEmpOutputDTOS.add(DatoAdicionalEmpOutputDTO.getDTO(datoAdicionalEmpleado));
        }
        return new GeneralOutput(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), datoAdicionalEmpOutputDTOS,
                datoAdicionalEmpleadoService.findAll(pageable));
    }
    
    @Override
    public GeneralOutput create(@Valid @RequestBody DatoAdicionalEmpInputDTO entity,
            @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, CREATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        datoAdicionalEmpleadoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(), HttpStatus.CREATED.getReasonPhrase(),
                "Registro Creado Exitosamente...");
    }
    
    @Override
    public GeneralOutput update(@Valid @RequestBody DatoAdicionalEmpInputDTO entity,
            @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, UPDATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        datoAdicionalEmpleadoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(), HttpStatus.ACCEPTED.getReasonPhrase(),
                "Registro Actualizado Exitosamente...");
    }
    
    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, DELETE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        datoAdicionalEmpleadoService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(), HttpStatus.ACCEPTED.getReasonPhrase(),
                "Registro Eliminado Exitosamente...");
    }
    
    @Override
    public GeneralOutput create(DatoAdicionalEmpInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
    
    @Override
    public GeneralOutput update(DatoAdicionalEmpInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
