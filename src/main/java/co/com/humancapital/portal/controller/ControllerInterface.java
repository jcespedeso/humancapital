package co.com.humancapital.portal.controller;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Interface que permite modelar la firma de los metodos basicos que deben tener las clases Controller.
 *
 * @param <E> Clase Entity o InputDTO del Objeto.
 * @param <EO> Clase OutputDTO del Objeto.
 * @param <PK> Tipo de datos de la Primary Key del Objeto.
 */
@RequestMapping("/default")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
        RequestMethod.DELETE}, allowedHeaders = "*")
public interface ControllerInterface<E, EO, PK> {
    
    @ResponseBody
    @GetMapping("/{id}")
    default EO getById(@PathVariable PK id, @RequestHeader("Authorization") String token) throws Exception {
        throw new NotImplementedException();
    }
    
    @ResponseBody
    @GetMapping("/list")
    default EO getAll(@RequestHeader("Authorization") String token, Pageable pageable) throws Exception {
        throw new NotImplementedException();
    }
    
    @ResponseBody
    @PostMapping(path = "/newFile", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    default EO create(E entity, @RequestHeader("Authorization") String token,
            @RequestParam("archivo") List<MultipartFile> files) throws Exception {
        throw new NotImplementedException();
    }
    
    @ResponseBody
    @PostMapping("/new")
    default EO create(@RequestBody E entity, @RequestHeader("Authorization") String token) throws Exception {
        throw new NotImplementedException();
    }
    
    @ResponseBody
    @PutMapping(path = "/updateFile", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    default EO update(E entity, @RequestHeader("Authorization") String token,
            @RequestParam("archivo") List<MultipartFile> files) throws Exception {
        throw new NotImplementedException();
    }
    
    @ResponseBody
    @PutMapping("/update")
    default EO update(@RequestBody E entity, @RequestHeader("Authorization") String token) throws Exception {
        throw new NotImplementedException();
    }
    
    @ResponseBody
    @DeleteMapping("/delete/{id}")
    default EO delete(@PathVariable PK id, @RequestHeader("Authorization") String token) throws Exception {
        throw new NotImplementedException();
    }
}
