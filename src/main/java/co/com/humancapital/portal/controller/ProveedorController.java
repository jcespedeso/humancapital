package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.ProveedorInputDTO;
import co.com.humancapital.portal.dto.ProveedorOutputDTO;
import co.com.humancapital.portal.entity.Proveedor;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.ProveedorService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Proveedor.
 */
@RestController
@RequestMapping("/proveedor")
public class ProveedorController extends AbstractController<ProveedorInputDTO, GeneralOutput, Long>{

    private final String CREATE = "PROVEEDOR_CREAR";
    private final String UPDATE = "PROVEEDOR_EDITAR";
    private final String VIEW = "PROVEEDOR_VER";
    private final String DELETE = "PROVEEDOR_ELIMINAR";

    private final ProveedorService proveedorService;

    public ProveedorController(ProveedorService proveedorService) {
        this.proveedorService = proveedorService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), ProveedorOutputDTO.getDTO(proveedorService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ProveedorOutputDTO> list = new ArrayList<>();
        for (Proveedor proveedor:proveedorService.findAll(pageable)) {
            list.add(ProveedorOutputDTO.getDTO(proveedor));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list, proveedorService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody ProveedorInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        proveedorService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody ProveedorInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        proveedorService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id,@RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        proveedorService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @Override
    public GeneralOutput create(ProveedorInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(ProveedorInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
