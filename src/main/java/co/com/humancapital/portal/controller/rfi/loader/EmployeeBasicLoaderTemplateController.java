package co.com.humancapital.portal.controller.rfi.loader;

import co.com.humancapital.portal.rfi.service.loader.LoaderTemplateService;
import javax.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static co.com.humancapital.portal.constant.LoaderConstant.EXCEL_MEDIA_TYPE;

@RestController
@RequestMapping(EmployeeBasicLoaderTemplateController.DEFAULT_LOADER_PATH)
@Slf4j
public class EmployeeBasicLoaderTemplateController extends AbstractLoaderTemplateController {
    
    public static final String DEFAULT_LOADER_PATH = ROOT_PATH + "/default";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String PARAM_FILE = "file";
    
    @Getter
    @Value("${human.loaders.default.template.name}")
    private String templateFileName;
    
    private final LoaderTemplateService loaderService;
    
    public EmployeeBasicLoaderTemplateController(
            @Qualifier("employeeBasicLoaderTemplateService") LoaderTemplateService loaderService) {
        this.loaderService = loaderService;
    }
    
    @Override
    @GetMapping(produces = EXCEL_MEDIA_TYPE)
    public void downloadTemplate(HttpServletResponse response,
            @RequestHeader(HEADER_AUTHORIZATION) String token) throws Exception {
        downloadTemplate(loaderService.buildTemplate(), response, token);
    }
    
    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void loadTemplate(@RequestParam(PARAM_FILE) MultipartFile file, @RequestParam Long clientId,
            HttpServletResponse response, @RequestHeader(HEADER_AUTHORIZATION) String token) throws Exception {
        loadTemplate(file, clientId, loaderService, response, token);
    }
}