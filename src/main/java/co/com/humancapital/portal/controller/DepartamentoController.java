package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.DepartamentoInputDTO;
import co.com.humancapital.portal.dto.DepartamentoOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.Departamento;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.get.dto.OrganizarJerarquiaInputDTO;
import co.com.humancapital.portal.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.PositiveOrZero;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Departamento.
 */
@RestController
@RequestMapping("/departamento")
@RequiredArgsConstructor
public class DepartamentoController extends AbstractController<DepartamentoInputDTO, GeneralOutput, Long> {

    private final String CREATE = "DEPTO_CREAR";
    private final String UPDATE = "DEPTO_EDITAR";
    private final String VIEW = "DEPTO_VER";
    private final String VIEW_CLIENTE = "DEPTO_VER_X_CLIENTE";
    private final String DELETE = "DEPTO_ELIMINAR";

    private final DepartamentoService departamentoService;

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), DepartamentoOutputDTO.getDTO(departamentoService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<DepartamentoOutputDTO> deptoDTOList=new ArrayList<>();
        for (Departamento depto:departamentoService.findAll(pageable)) {
            DepartamentoOutputDTO departamentoOutputDTO = DepartamentoOutputDTO.getDTO(depto);
            deptoDTOList.add(departamentoOutputDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),deptoDTOList, departamentoService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody DepartamentoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        departamentoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody DepartamentoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        departamentoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        departamentoService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/cliente/{idCliente}")
    public GeneralOutput departamentosXCliente(@PathVariable Long idCliente, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<DepartamentoOutputDTO> departamentoOutputDTOS = new ArrayList<>();
        for (Departamento departamento: departamentoService.departamentoXCliente(idCliente, pageable)) {
            departamentoOutputDTOS.add(DepartamentoOutputDTO.getDTO(departamento));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),departamentoOutputDTOS);
    }

    @GetMapping("/organigrama/{idDepo}/{nivel}")
    public GeneralOutput organigramaDepartamentos(@PathVariable Long idDepo, @PathVariable @PositiveOrZero(message = "El nivel debe ser un numero positivo.") int nivel, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),departamentoService.verEstructuraJerarquica(idDepo.toString(),nivel));
    }

    @GetMapping("/deptoHijo/{idPadre}")
    public GeneralOutput departamentosHijos(@PathVariable Long idPadre, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),departamentoService.findAllDeptoHijo(idPadre));
    }

    @PutMapping("/organizacion")
    public GeneralOutput actualizarOrganizacion(@Valid @RequestBody OrganizarJerarquiaInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        departamentoService.actualizarOrganizacion(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput create(DepartamentoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(DepartamentoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
