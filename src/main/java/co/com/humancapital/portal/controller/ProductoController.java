package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.ProductoInputDTO;
import co.com.humancapital.portal.dto.ProductoOutputDTO;
import co.com.humancapital.portal.entity.Producto;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.ClienteService;
import co.com.humancapital.portal.service.ProductoService;
import co.com.humancapital.portal.service.SalarioFlexibleXProductoService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Céspedes Ortega
 * @date 13/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Producto.
 */
@RestController
@RequestMapping("/producto")
public class ProductoController extends AbstractController<ProductoInputDTO, GeneralOutput, Long>{

    private final String CREATE = "PRODUCTO_CREAR";
    private final String UPDATE = "PRODUCTO_EDITAR";
    private final String VIEW = "PRODUCTO_VER";
    private final String VIEW_PROVEEDOR = "PRODUCTO_VER_X_PROVEEDOR";
    private final String DELETE = "PRODUCTO_ELIMINAR";

    private final ProductoService productoService;

    private final ClienteService clienteService;

    private final SalarioFlexibleXProductoService salarioFlexibleXProductoService;

    public ProductoController(ProductoService productoService, ClienteService clienteService, SalarioFlexibleXProductoService salarioFlexibleXProductoService) {
        this.productoService = productoService;
        this.clienteService = clienteService;
        this.salarioFlexibleXProductoService = salarioFlexibleXProductoService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), ProductoOutputDTO.getDTO(productoService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ProductoOutputDTO> list = new ArrayList<>();
        for (Producto producto:productoService.findAll(pageable)) {
            list.add(ProductoOutputDTO.getDTO(producto));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list, productoService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody ProductoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        productoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody ProductoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        productoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        productoService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite exponer el listado de productos de un proveedor.
     * @param idProveedor Id del Proveedor.
     * @return Lista de ProductoOutputDTO.
     */
    @GetMapping("/proveedor/{idProveedor}")
    public GeneralOutput productoXProveedor(@PathVariable Long idProveedor,@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW_PROVEEDOR)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ProductoOutputDTO> list = new ArrayList<>();
        for (Producto producto:productoService.productoXProveedor(idProveedor,pageable)) {
            list.add(ProductoOutputDTO.getDTO(producto));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list, productoService.productoXProveedor(idProveedor,pageable));
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite exponer el listado de productos asignados a un Modulo.
     * @param idModulo Id del Modulo a consultar.
     * @return Lista de ProductoOutputDTO.
     */
    @GetMapping("/modulo/{idModulo}")
    public GeneralOutput obtenerProductoXModulo(@PathVariable Long idModulo,@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ProductoOutputDTO> list = new ArrayList<>();
        for (Producto producto:productoService.productoXModulo(idModulo,pageable)) {
            list.add(ProductoOutputDTO.getDTO(producto));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list,productoService.productoXModulo(idModulo,pageable));
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite exponer el listado de productos asignados a un Cliente.
     * @param idCliente Id del Cliente a consultar.
     * @return Lista de ProductoOutputDTO.
     */
    @GetMapping("/cliente/{idCliente}")
    public GeneralOutput obtenerProductoXCliente(@PathVariable Long idCliente,@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ProductoOutputDTO> list = new ArrayList<>();
        for (Producto producto:productoService.productoXCliente(idCliente,pageable)) {
            list.add(ProductoOutputDTO.getDTO(producto));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list, productoService.productoXCliente(idCliente,pageable));
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite exponer el listado de productos asignados a un Empleado.
     * @param idEmpleado Id del Cliente a consultar.
     * @return Lista de ProductoOutputDTO.
     */
    @GetMapping("/empleado/{idEmpleado}")
    public GeneralOutput obtenerProductoXEmpleado(@PathVariable Long idEmpleado,@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ProductoOutputDTO> list = new ArrayList<>();
        for (Producto producto:productoService.productoXEmpleado(idEmpleado,pageable)) {
            list.add(ProductoOutputDTO.getDTO(producto));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list, productoService.productoXEmpleado(idEmpleado,pageable));
    }

    @GetMapping("/infoProduto/{idCliente}")
    public GeneralOutput contarProductosUtilizadosCliente(@PathVariable Long idCliente, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe.");
        }
        if(salarioFlexibleXProductoService.contarProductoUtilizadoXCliente(idCliente).size()==0){
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no tiene productos asignados a alguno de sus empleados.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),salarioFlexibleXProductoService.contarProductoUtilizadoXCliente(idCliente));
    }

    @GetMapping("/infoProdutoAll")
    public GeneralOutput contarProductoUtilizado(@RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        if(salarioFlexibleXProductoService.contarProductoUtilizado().size()==0){
            throw new ResourceNotFoundException("El sistema no registra productos asignados a alguno de los empleados.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),salarioFlexibleXProductoService.contarProductoUtilizado());
    }

    @Override
    public GeneralOutput create(ProductoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(ProductoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
