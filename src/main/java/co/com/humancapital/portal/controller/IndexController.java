package co.com.humancapital.portal.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Alejandro Herrera Montilla
 * @date 14/02/2020
 */
@RestController
@RequestMapping("")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class IndexController {

    @RequestMapping("")
    public String inicioApp() {
        return "<h1>Backend Web Portal Human Capital.</h1><br>@Copyright <strong>2020</strong>";
    }

}
