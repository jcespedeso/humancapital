package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.CompensacionInputDTO;
import co.com.humancapital.portal.dto.CompensacionOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.CompensacionService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Compensacion.
 */
@RestController
@RequestMapping("/compensacion")
public class CompensacionController extends AbstractController<CompensacionInputDTO, GeneralOutput, Long> {

    private final String CREATE = "CONTRATO_CREAR";
    private final String UPDATE = "CONTRATO_EDITAR";
    private final String VIEW = "CONTRATO_VER";
    private final String DELETE = "CONTRATO_ELIMINAR";

    private final CompensacionService compensacionService;

    public CompensacionController(CompensacionService compensacionService) {
        this.compensacionService = compensacionService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), CompensacionOutputDTO.getDTO(compensacionService.findId(id)));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody CompensacionInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        compensacionService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody CompensacionInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        compensacionService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @GetMapping("/listCompEmpleado/{idEmpleado}")
    public GeneralOutput getAllByEmpleado(@PathVariable Long idEmpleado, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"empleado"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<CompensacionOutputDTO> compensacionOutputDTOS = new ArrayList<>();
        for (Compensacion list:compensacionService.findAll(idEmpleado, pageable)) {
            compensacionOutputDTOS.add(CompensacionOutputDTO.getDTO(list));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),compensacionOutputDTOS,compensacionService.findAll(idEmpleado, pageable));
    }

    @GetMapping("/compActivaEmpleado/{idEmpleado}")
    public GeneralOutput compensacionActivaEmpleado(@PathVariable Long idEmpleado,@RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),CompensacionOutputDTO.getDTO(compensacionService.obtenerCompensacionActivaEmpleado(idEmpleado)));
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        compensacionService.delete(id,usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @Override
    public GeneralOutput getAll(String token, Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(CompensacionInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(CompensacionInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
