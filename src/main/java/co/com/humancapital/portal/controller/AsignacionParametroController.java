package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.AsignacionParametroInputDTO;
import co.com.humancapital.portal.dto.AsignacionParametroOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.AsignacionParametro;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.AsignacionParametroService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 13/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad AsignacionParametro.
 */
@RestController
@RequestMapping("/asignacionParametro")
public class AsignacionParametroController extends AbstractController<AsignacionParametroInputDTO, GeneralOutput,Long>{

    private final String CREATE = "PARAMETRO_ASIGNAR";
    private final String UPDATE = "ASIGNAR_PARAMETRO_EDITAR";
    private final String VIEW = "ASIGNAR_PARAMETRO_VER";
    private final String DELETE = "ASIGNAR_PARAMETRO_ELIMINAR";

    private final AsignacionParametroService asignacionParametroService;

    public AsignacionParametroController(AsignacionParametroService asignacionParametroService) {
        this.asignacionParametroService = asignacionParametroService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), AsignacionParametroOutputDTO.getDTO(asignacionParametroService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"cliente", "parametro"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<AsignacionParametroOutputDTO> asignacionParametroOutputDTOS=new ArrayList<>();
        for (AsignacionParametro asignacionParametro:asignacionParametroService.findAll(pageable)) {
            AsignacionParametroOutputDTO asignacionParametroOutputDTO = AsignacionParametroOutputDTO.getDTO(asignacionParametro);
            asignacionParametroOutputDTOS.add(asignacionParametroOutputDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),asignacionParametroOutputDTOS,asignacionParametroService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody AsignacionParametroInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token.trim(),CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        asignacionParametroService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody AsignacionParametroInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token.trim(),UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        asignacionParametroService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token.trim(),DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        asignacionParametroService.delete(id,usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/cliente/{idCliente}")
    public GeneralOutput obtenerParametrosAsignadosXCliente(@PathVariable Long idCliente, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"cliente", "parametro"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<AsignacionParametroOutputDTO> asignacionParametroOutputDTOS = new ArrayList<>();
        for (AsignacionParametro asignacionParametro:asignacionParametroService.obtenerAsignacionesXCliente(idCliente, pageable)) {
            AsignacionParametroOutputDTO asignacionParametroOutputDTO = AsignacionParametroOutputDTO.getDTO(asignacionParametro);
            asignacionParametroOutputDTOS.add(asignacionParametroOutputDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),asignacionParametroOutputDTOS,asignacionParametroService.obtenerAsignacionesXCliente(idCliente,pageable));
    }

    @Override
    public GeneralOutput create(AsignacionParametroInputDTO entity, String token, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput update(AsignacionParametroInputDTO entity, String token, List<MultipartFile> files) throws Exception {
        return null;
    }
}
