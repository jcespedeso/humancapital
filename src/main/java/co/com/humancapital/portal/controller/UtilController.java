package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.ClasificacionClienteOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.ClasificacionCliente;
import co.com.humancapital.portal.get.type.*;
import co.com.humancapital.portal.repository.ClasificacionClienteRepository;
import co.com.humancapital.portal.repository.NivelArlRepository;
import co.com.humancapital.portal.repository.TipoOtroIngresoRepository;
import co.com.humancapital.portal.service.ClasificacionClienteService;
import co.com.humancapital.portal.service.TipoOtroIngresoService;
import co.com.humancapital.portal.util.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para clases utilitarias.
 */
@RestController
@RequestMapping("/utilClass")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class UtilController{

    private final NivelArlRepository nivelArlRepository;

    private final ClasificacionClienteRepository clasificacionClienteRepository;

    private final TipoOtroIngresoRepository tipoOtroIngresoRepository;

    public UtilController(NivelArlRepository nivelArlRepository, ClasificacionClienteService clasificacionClienteService, ClasificacionClienteRepository clasificacionClienteRepository, TipoOtroIngresoService tipoOtroIngresoService, TipoOtroIngresoRepository tipoOtroIngresoRepository) {
        this.nivelArlRepository = nivelArlRepository;
        this.clasificacionClienteRepository = clasificacionClienteRepository;
        this.tipoOtroIngresoRepository = tipoOtroIngresoRepository;
    }

    @GetMapping("/listTipoDocumento")
    public GeneralOutput getAllTipoDocumento(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),TipoDocumento.listar());
    }

    @GetMapping("/listEstadoCivil")
    public GeneralOutput getAllEstadoCivil(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),EstadoCivil.listar());
    }

    @GetMapping("/listGenero")
    public GeneralOutput getAllGenero(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),Genero.listar());
    }

    @GetMapping("/listIdioma")
    public GeneralOutput getAllIdioma(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),Idioma.listar());
    }

    @GetMapping("/listSectorEmpresa")
    public GeneralOutput getAllSectorEmpresa(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),SectorEmpresa.listar());
    }

    @GetMapping("/listTamanoEmpresa")
    public GeneralOutput getAllTamanoEmpresa(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),TamanoEmpresa.listar());
    }

    @GetMapping("/listTipoEmpresa")
    public GeneralOutput getAllTipoEmpresa(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),TipoEmpresa.listar());
    }

    @GetMapping("/listTipoRegimen")
    public GeneralOutput getAllTipoRegimen(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),TipoRegimen.listar());
    }

    @GetMapping("/listTipoCompensacion")
    public GeneralOutput getAllTipoCompensacion(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),TipoCompensacion.listar());
    }

    @GetMapping("/listTipoPersona")
    public GeneralOutput getAllTipoPersona(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),TipoPersona.listar());
    }

    @GetMapping("/listNivelArl")
    public GeneralOutput getAllNivelArl(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),nivelArlRepository.findAll());
    }

    @GetMapping("/listTipoContrato")
    public GeneralOutput getAllTipoContrato(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),TipoContrato.listar());
    }

    @GetMapping("/listRegimenPension")
    public GeneralOutput getAllRegimenPension(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),RegimenPension.listar());
    }

    @GetMapping("/listCondicionEmpleado")
    public GeneralOutput getAllCondicionEmpleado(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),CondicionEmpleado.listar());
    }

    @GetMapping("/listMetodoRf")
    public GeneralOutput getAllMetodoRfi(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),MetodoRf.listar());
    }

    @GetMapping("/listEstadoSimulacion")
    public GeneralOutput getAllEstadoSimulacion(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),EstadoSimulacion.listar());
    }

    @GetMapping("/listClasificacionCliente")
    public GeneralOutput getAllClasificacionCliente(){
        List<ClasificacionClienteOutputDTO> listDto = new ArrayList<>();
        for (ClasificacionCliente clasificacion: clasificacionClienteRepository.findAll()) {
            listDto.add(ClasificacionClienteOutputDTO.getDTO(clasificacion));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),listDto);
    }

    @GetMapping("/listTipoOtroIngreso")
    public GeneralOutput getAllTipoOtroIngreso(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),tipoOtroIngresoRepository.findAll());
    }

    @GetMapping("/listIngresoEmpresa")
    public GeneralOutput getAllIngresoEmpresa(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),IngresoEmpresa.listar());
    }

    @GetMapping("/listParentesco")
    public GeneralOutput getAllParentesco(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),Parentesco.listar());
    }

    @GetMapping("/listPresenciaEmpresa")
    public GeneralOutput getAllPresenciaEmpresa(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),PresenciaEmpresa.listar());
    }

    @GetMapping("/listSolucionEmpresa")
    public GeneralOutput getAllSolucionEmpresa(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),SolucionEmpresa.listar());
    }

    @GetMapping("/listTipoCompetencia")
    public GeneralOutput getAllTipoCompetencia(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), TipoCompetencia.values());
    }

    @GetMapping("/listTipoSemaforo")
    public GeneralOutput getAllTipoSemaforo(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), TipoSemaforo.values());
    }

    @GetMapping("/listTipoRespuesta")
    public GeneralOutput getAllTipoRespuesta(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), TipoRespuesta.values());
    }

    @GetMapping("/listConfigPregunta")
    public GeneralOutput getAllConfigPregunta(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), ConfigPregunta.values());
    }

    @GetMapping("/listGradoEvaluacion")
    public GeneralOutput getAllGradoEvaluacion(){
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), GradoEvaluacion.values());
    }

}
