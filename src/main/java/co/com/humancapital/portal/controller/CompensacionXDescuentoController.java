package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.CompensacionXDescuentoInputDTO;
import co.com.humancapital.portal.dto.CompensacionXDescuentoOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.CompensacionXDescuento;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.CompensacionXDescuentoService;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 20/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad CompensacionXDescuento.
 */
@RestController
@RequestMapping("/compXdescuento")
public class CompensacionXDescuentoController extends AbstractController<CompensacionXDescuentoInputDTO, GeneralOutput, Long>{

    private final String CREATE = "RFI_CREAR";
    private final String UPDATE = "RFI_EDITAR";
    private final String VIEW = "RFI_VER";
    private final String DELETE = "RFI_ELIMINAR";

    private final CompensacionXDescuentoService compensacionXDescuentoService;

    public CompensacionXDescuentoController(CompensacionXDescuentoService compensacionXDescuentoService) {
        this.compensacionXDescuentoService = compensacionXDescuentoService;
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody CompensacionXDescuentoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        compensacionXDescuentoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody CompensacionXDescuentoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        compensacionXDescuentoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @DeleteMapping("/delete/{id}")
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        compensacionXDescuentoService.delete(id);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @GetMapping("/empleado/{id}")
    public GeneralOutput getByIdEmpleado(@PathVariable Long id, @RequestHeader("Authorization") String token, Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<CompensacionXDescuentoOutputDTO> listEntityDTO = new ArrayList<>();
        for (CompensacionXDescuento list:compensacionXDescuentoService.descuentosXEmpleado(id,pageable)) {
            listEntityDTO.add(CompensacionXDescuentoOutputDTO.getDTO(list));
        }
        Map<String,Object> resultado = new LinkedHashMap<>();
        resultado.put("descuentosActivos",listEntityDTO);
        resultado.put("totalDescuentos",compensacionXDescuentoService.totalDescuentosXEmpleado(id));
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),resultado, compensacionXDescuentoService.descuentosXEmpleado(id,pageable));
    }

    @Override
    public GeneralOutput getById(Long id, String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput getAll(String token, Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(CompensacionXDescuentoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(CompensacionXDescuentoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
