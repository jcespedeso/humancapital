package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.ModuloInputDTO;
import co.com.humancapital.portal.dto.ModuloOutputDTO;
import co.com.humancapital.portal.entity.Modulo;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.ModuloService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Céspedes Ortega
 * @date 13/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Modulo.
 */
@RestController
@RequestMapping("/modulo")
public class ModuloController extends AbstractController<ModuloInputDTO, GeneralOutput,Long>{

    private final String CREATE = "MODULO_CREAR";
    private final String UPDATE = "MODULO_EDITAR";
    private final String VIEW = "MODULO_VER";
    private final String DELETE = "MODULO_ELIMINAR";
    private final String PRODUCTO_ASIGNAR = "PRODUCTO_ASIGNAR";

    private final ModuloService moduloService;

    public ModuloController(ModuloService moduloService) {
        this.moduloService = moduloService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), ModuloOutputDTO.getDTO(moduloService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<ModuloOutputDTO> moduloOutputDTOS = new ArrayList<>();
        for (Modulo modulo:moduloService.findAll(pageable)) {
            moduloOutputDTOS.add(ModuloOutputDTO.getDTO(modulo));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),moduloOutputDTOS,moduloService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody ModuloInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        moduloService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody ModuloInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        moduloService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if (!isAuthorized(token, DELETE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        moduloService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @PutMapping("/asignarProducto")
    public GeneralOutput asignarProducto(@RequestBody ModuloInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token, PRODUCTO_ASIGNAR)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
       moduloService.asignarProducto(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Productos asignados Exitosamente...");
    }

    @GetMapping("/cliente/{idCliente}")
    public GeneralOutput getModuloByCliente(@PathVariable Long idCliente, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),moduloService.findAllCliente(idCliente));
    }

    @Override
    public GeneralOutput create(ModuloInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(ModuloInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
