package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.CargoInputDTO;
import co.com.humancapital.portal.dto.CargoOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.get.dto.OrganizarJerarquiaInputDTO;
import co.com.humancapital.portal.service.CargoService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.PositiveOrZero;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Cargo.
 */
@RestController
@RequestMapping("/cargo")
public class CargoController extends AbstractController<CargoInputDTO, GeneralOutput, Long> {

    private final String CREATE = "CARGO_CREAR";
    private final String UPDATE = "CARGO_EDITAR";
    private final String VIEW = "CARGO_VER";
    private final String VIEW_DEPTO = "CARGO_VER_X_DEPTO";
    private final String VIEW_CLIENTE = "CARGO_VER_X_CLIENTE";
    private final String DELETE = "CARGO_ELIMINAR";

    private final CargoService cargoService;

    public CargoController(CargoService cargoService) {
        this.cargoService = cargoService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), CargoOutputDTO.getDTO(cargoService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<CargoOutputDTO> cargoOutputDTOS = new ArrayList<>();
        for (Cargo cargo : cargoService.findAll(pageable)) {
            CargoOutputDTO cargoOutputDTO = CargoOutputDTO.getDTO(cargo);
            cargoOutputDTOS.add(cargoOutputDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),cargoOutputDTOS, cargoService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody CargoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, CREATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        cargoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody CargoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, UPDATE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        cargoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if (!isAuthorized(token, DELETE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        cargoService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/depto/{idDepto}")
    public GeneralOutput cargosXDepartamento(@PathVariable Long idDepto, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW_DEPTO)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<CargoOutputDTO> cargoOutputDTOS = new ArrayList<>();
        for (Cargo cargo : cargoService.cargoXDepartamento(idDepto,pageable)) {
            cargoOutputDTOS.add(CargoOutputDTO.getDTO(cargo));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),cargoOutputDTOS,cargoService.cargoXDepartamento(idDepto,pageable));
    }

    @GetMapping("/cliente/{idCliente}")
    public GeneralOutput cargosXCliente(@PathVariable Long idCliente, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if (!isAuthorized(token, VIEW_CLIENTE)) {
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<CargoOutputDTO> cargoOutputDTOS = new ArrayList<>();
        for (Cargo cargo : cargoService.cargosXCliente(idCliente, pageable)) {
            cargoOutputDTOS.add(CargoOutputDTO.getDTO(cargo));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),cargoOutputDTOS, cargoService.cargosXCliente(idCliente, pageable));
    }

    @GetMapping("/organigrama/{idCargo}/{nivel}")
    public GeneralOutput organigramaDepartamentos(@PathVariable Long idCargo, @PathVariable @PositiveOrZero(message = "El nivel debe ser un numero positivo.") int nivel, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),cargoService.verEstructuraJerarquica(idCargo.toString(),nivel));
    }

    @GetMapping("/cargoHijo/{idPadre}")
    public GeneralOutput departamentosHijos(@PathVariable Long idPadre, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),cargoService.findAllCargoHijo(idPadre));
    }

    @PutMapping("/organizacion")
    public GeneralOutput actualizarOrganizacion(@Valid @RequestBody OrganizarJerarquiaInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        cargoService.actualizarOrganizacion(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput create(CargoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(CargoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
