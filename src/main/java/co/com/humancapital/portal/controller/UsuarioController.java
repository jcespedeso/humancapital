package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.UsuarioInputDTO;
import co.com.humancapital.portal.dto.UsuarioOutputDTO;
import co.com.humancapital.portal.entity.Usuario;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.UsuarioService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 02/12/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Usuario.
 */
@RestController
@RequestMapping("/usuario")
public class UsuarioController extends AbstractController<UsuarioInputDTO, GeneralOutput,Long>{

    private final String CREATE = "USUARIO_CREAR";
    private final String UPDATE = "USUARIO_EDITAR";
    private final String VIEW = "USUARIO_VER";
    private final String DELETE = "USUARIO_ELIMINAR";
    private final String RESTPASSWORD = "USUARIO_ELIMINAR";

    private final UsuarioService usuarioService;

    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id,@RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), UsuarioOutputDTO.getDTO (usuarioService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token,@PageableDefault(sort = {"username"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<UsuarioOutputDTO> usuarioDTOList=new ArrayList<>();
        for (Usuario usuario:usuarioService.findAll(pageable)) {
            UsuarioOutputDTO usuDTO = UsuarioOutputDTO.getDTO(usuario);
            usuarioDTOList.add(usuDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),usuarioDTOList,usuarioService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody UsuarioInputDTO entity, @RequestHeader("Authorization") String token ) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        usuarioService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody UsuarioInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        usuarioService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        usuarioService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @PutMapping("/activar/{id}")
    public GeneralOutput activarUsuario(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        Usuario usuario = usuarioService.optenerUsuarioInactivo(id);
        usuarioService.activarUsuario(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),"El usuario "+ usuario.getUsername() +" ha sido Activado Exitosamente...");
    }

    @PutMapping("/resetpass")
    public GeneralOutput restablecerContrasena(@Valid @RequestBody UsuarioInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,RESTPASSWORD)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        Usuario usuario = usuarioService.cambiarContrasena(entity);
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),"El password del Usuario "+ usuario.getUsername() +" ha sido modificado Exitosamente....");
    }

    @GetMapping("/listInactivo")
    public GeneralOutput getAllInactivo(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"username"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<UsuarioOutputDTO> usuarioDTOList=new ArrayList<>();
        for (Usuario usuario:usuarioService.optenerUsuariosInactivos(pageable)) {
            UsuarioOutputDTO usuDTO = UsuarioOutputDTO.getDTO(usuario);
            usuarioDTOList.add(usuDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),usuarioDTOList, usuarioService.optenerUsuariosInactivos(pageable));
    }

    @GetMapping("/empleado/{id}")
    public GeneralOutput getByEmpleadoAndActivo(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),UsuarioOutputDTO.getDTO (usuarioService.optenerUsuarioPorEmpleado(id)));

    }

    @GetMapping("/cliente/{id}")
    public GeneralOutput getAllByClienteAndActivo(@PathVariable Long id, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"username"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<UsuarioOutputDTO> usuarioDTOList=new ArrayList<>();
        for (Usuario usuario:usuarioService.optenerUsuarioPorCliente(id,pageable)) {
            UsuarioOutputDTO usuDTO = UsuarioOutputDTO.getDTO(usuario);
            usuarioDTOList.add(usuDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),usuarioDTOList);
    }

    @GetMapping("/inactivo/cliente/{id}")
    public GeneralOutput getAllByClienteAndFalse(@PathVariable Long id, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"username"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<UsuarioOutputDTO> usuarioDTOList=new ArrayList<>();
        for (Usuario usuario:usuarioService.optenerUsuarioInactivoPorCliente(id,pageable)) {
            UsuarioOutputDTO usuDTO = UsuarioOutputDTO.getDTO(usuario);
            usuarioDTOList.add(usuDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),usuarioDTOList);
    }

    @Override
    public GeneralOutput create(UsuarioInputDTO entity, @RequestHeader("Authorization") String token,  @RequestParam("archivo") List<MultipartFile> files) {
        return null;
    }
    @Override
    public GeneralOutput update(UsuarioInputDTO entity, @RequestHeader("Authorization") String token,  @RequestParam("archivo") List<MultipartFile> files) {
        return null;
    }
}
