package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.SalarioFlexibleInputDTO;
import co.com.humancapital.portal.dto.SalarioFlexibleOutputDTO;
import co.com.humancapital.portal.entity.SalarioFlexible;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.SalarioFlexibleService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad SalarioFlexible.
 */
@RestController
@RequestMapping("/salarioflexible")
public class SalarioFlexibleController extends AbstractController<SalarioFlexibleInputDTO, GeneralOutput, Long>{

    private final String CREATE = "RFI_CREAR";
    private final String UPDATE = "RFI_EDITAR";
    private final String VIEW = "RFI_VER";
    private final String DELETE = "RFI_ELIMINAR";

    private final SalarioFlexibleService salarioFlexibleService;

    public SalarioFlexibleController(SalarioFlexibleService salarioFlexibleService) {
        this.salarioFlexibleService = salarioFlexibleService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), SalarioFlexibleOutputDTO.getDTO(salarioFlexibleService.findId(id)));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody SalarioFlexibleInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        salarioFlexibleService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody SalarioFlexibleInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        salarioFlexibleService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @DeleteMapping("/finalizar/")
    public GeneralOutput finalizacion(@RequestBody SalarioFlexibleInputDTO entity, @RequestHeader("Authorization") String token ) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioInactiva(usuarioSesion(token).getIdUsuario());
        salarioFlexibleService.finalizacion(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/compEstado/{idComp}/{estado}")
    public GeneralOutput listarSalarioFlexibleByCompensacionAndEstado(@PathVariable Long idComp, @PathVariable String estado, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<SalarioFlexibleOutputDTO> list = new ArrayList<>();
        for (SalarioFlexible salario:salarioFlexibleService.salarioFlexibleXCompensacionYEstado(idComp,estado)) {
            list.add(SalarioFlexibleOutputDTO.getDTO(salario));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),list);
    }

    @GetMapping("/infoPorcentajeFlex/{idCliente}")
    public GeneralOutput infoPorcentajeFlex(@PathVariable Long idCliente, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),salarioFlexibleService.obtenerMaxMinAvgPorcentejeFlex(idCliente));
    }

    @Override
    public GeneralOutput getAll(String token, Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(SalarioFlexibleInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(SalarioFlexibleInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
