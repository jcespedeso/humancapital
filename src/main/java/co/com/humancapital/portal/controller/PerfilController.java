package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.PerfilInputDTO;
import co.com.humancapital.portal.dto.PerfilOutputDTO;
import co.com.humancapital.portal.entity.Perfil;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.PerfilService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 22/12/2019
 * Clase para la implementación de los diferentes perfiles del Usuario.
 */
@RestController
@RequestMapping("/perfil")
public class PerfilController extends AbstractController<PerfilInputDTO, GeneralOutput, Long>{

    private final String CREATE = "PERFIL_CREAR";
    private final String UPDATE = "PERFIL_EDITAR";
    private final String VIEW = "PERFIL_VER";
    private final String DELETE = "PERFIL_ELIMINAR";

    private final PerfilService perfilService;

    public PerfilController(PerfilService perfilService) {
        this.perfilService = perfilService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id,@RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), PerfilOutputDTO.getDTO(perfilService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token.trim(),VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<PerfilOutputDTO> perfilOutputDTOS=new ArrayList<>();
        for (Perfil perfil:perfilService.findAll(pageable)) {
            PerfilOutputDTO outputDTO = PerfilOutputDTO.getDTO(perfil);
            perfilOutputDTOS.add(outputDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),perfilOutputDTOS,perfilService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody PerfilInputDTO entity, @RequestHeader("Authorization") String token ) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        perfilService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody PerfilInputDTO entity, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        perfilService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        perfilService.delete(id,usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @Override
    public GeneralOutput create(PerfilInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(PerfilInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }






}


