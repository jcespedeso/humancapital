package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.LoginInput;
import co.com.humancapital.portal.dto.LoginOutput;
import co.com.humancapital.portal.service.LoginService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 */
@RestController
@RequestMapping("")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT,RequestMethod.DELETE})
public class LoginController{

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    @ResponseBody
    public LoginOutput login(@Valid @RequestBody LoginInput loginInput) throws Exception{
        return loginService.login(loginInput);
    }




}
