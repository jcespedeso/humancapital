package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.dto.OtroIngresoInputDTO;
import co.com.humancapital.portal.dto.OtroIngresoOutputDTO;
import co.com.humancapital.portal.entity.OtroIngreso;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.OtroIngresoService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad OtroIngreso.
 */
@RestController
@RequestMapping("/otroIngreso")
public class OtroIngresoController extends AbstractController<OtroIngresoInputDTO, GeneralOutput, Long>{

    private final String CREATE = "OTROINGRESO_CREAR";
    private final String UPDATE = "OTROINGRESO_EDITAR";
    private final String VIEW = "OTROINGRESO_VER";
    private final String VIEW_CLIENTE = "OTROINGRESO_VER_CLIENTE";
    private final String DELETE = "OTROINGRESO_ELIMINAR";

    private final OtroIngresoService otroIngresoService;

    public OtroIngresoController(OtroIngresoService otroIngresoService) {
        this.otroIngresoService = otroIngresoService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), OtroIngresoOutputDTO.getDTO(otroIngresoService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<OtroIngresoOutputDTO> otroIngresoOutputDTOS = new ArrayList<>();
        for (OtroIngreso otroIngreso : otroIngresoService.findAll(pageable)) {
            OtroIngresoOutputDTO otroIngresoOutputDTO = OtroIngresoOutputDTO.getDTO(otroIngreso);
            otroIngresoOutputDTOS.add(otroIngresoOutputDTO);
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),otroIngresoOutputDTOS,otroIngresoService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody OtroIngresoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        otroIngresoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody OtroIngresoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        otroIngresoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        otroIngresoService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/listByCliente/{cliente}")
    public GeneralOutput getAllPorCliente(@PathVariable Long cliente, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<OtroIngresoOutputDTO> otroIngresoOutputDTOS = new ArrayList<>();
        for (OtroIngreso otroIngreso:otroIngresoService.findAllByCliente(cliente, pageable)) {
            otroIngresoOutputDTOS.add(OtroIngresoOutputDTO.getDTO(otroIngreso));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),otroIngresoOutputDTOS, otroIngresoService.findAllByCliente(cliente, pageable));
    }

    @Override
    public GeneralOutput create(OtroIngresoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(OtroIngresoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
