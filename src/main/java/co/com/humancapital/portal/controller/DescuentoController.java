package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.DescuentoInputDTO;
import co.com.humancapital.portal.dto.DescuentoOutputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.Descuento;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.DescuentoService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 13/12/2019
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad Descuento.
 */
@RestController
@RequestMapping("/descuento")
public class DescuentoController extends AbstractController<DescuentoInputDTO, GeneralOutput, Long>{

    private final String CREATE = "DESCUENTO_CREAR";
    private final String UPDATE = "DESCUENTO_EDITAR";
    private final String VIEW = "DESCUENTO_VER";
    private final String VIEW_CLIENTE = "DESCUENTO_VER_CLIENTE";
    private final String DELETE = "DESCUENTO_ELIMINAR";

    private final DescuentoService descuentoService;

    public DescuentoController(DescuentoService descuentoService) {
        this.descuentoService = descuentoService;
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(), DescuentoOutputDTO.getDTO(descuentoService.findId(id)));
    }

    @Override
    public GeneralOutput getAll(@RequestHeader("Authorization") String token,  @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<DescuentoOutputDTO> descuentoOutputDTOS = new ArrayList<>();
        for (Descuento descuento:descuentoService.findAll(pageable)) {
            descuentoOutputDTOS.add(DescuentoOutputDTO.getDTO(descuento));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),descuentoOutputDTOS, descuentoService.findAll(pageable));
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody DescuentoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioCrea(usuarioSesion(token).getIdUsuario());
        descuentoService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody DescuentoInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        entity.setUsuarioModifica(usuarioSesion(token).getIdUsuario());
        descuentoService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @Override
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        descuentoService.delete(id, usuarioSesion(token).getIdUsuario());
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que lista los descuentos aplicables a los Empleado de un Cliente especifico.
     * @param cliente Id del Cliente a consultar.
     * @return Listado de Descuentos asignados a un Empleado.
     */
    @GetMapping("/listByCliente/{cliente}")
    public GeneralOutput getAllPorCliente(@PathVariable Long cliente, @RequestHeader("Authorization") String token, @PageableDefault(sort = {"nombre"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception {
        if(!isAuthorized(token,VIEW_CLIENTE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<DescuentoOutputDTO> descuentoOutputDTOS = new ArrayList<>();
        for (Descuento descuento:descuentoService.findAllByCliente(cliente, pageable)) {
            descuentoOutputDTOS.add(DescuentoOutputDTO.getDTO(descuento));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),descuentoOutputDTOS, descuentoService.findAllByCliente(cliente, pageable));
    }

    @Override
    public GeneralOutput create(DescuentoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(DescuentoInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
