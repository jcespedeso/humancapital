package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.EmpleadoXDatoAdicionalInputDTO;
import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.EmpleadoXDatoAdicional;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.service.EmpleadoXDatoAdicionalService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 04/02/2020
 * Clase para la implementación de los diferentes microservicios provistos
 * para la entidad EmpleadoXDatoAdicional.
 */
@RestController
@RequestMapping("/empXdatoAdicional")
public class EmpleadoXDatoAdicionalController extends AbstractController<EmpleadoXDatoAdicionalInputDTO, GeneralOutput, Long>{

    private final String CREATE = "EMPLEADO_CREAR";
    private final String UPDATE = "EMPLEADO_EDITAR";
    private final String VIEW = "EMPLEADO_VER";
    private final String DELETE = "EMPLEADO_ELIMINAR";

    private final EmpleadoXDatoAdicionalService empleadoXDatoAdicionalService;

    public EmpleadoXDatoAdicionalController(EmpleadoXDatoAdicionalService empleadoXDatoAdicionalService) {
        this.empleadoXDatoAdicionalService = empleadoXDatoAdicionalService;
    }

    @Override
    public GeneralOutput create(@Valid @RequestBody EmpleadoXDatoAdicionalInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,CREATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        empleadoXDatoAdicionalService.create(entity);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Registro Creado Exitosamente...");
    }

    @Override
    public GeneralOutput update(@Valid @RequestBody EmpleadoXDatoAdicionalInputDTO entity, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,UPDATE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        empleadoXDatoAdicionalService.update(entity);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Actualizado Exitosamente...");
    }

    @DeleteMapping("/delete/{id}")
    public GeneralOutput delete(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        if(!isAuthorized(token,DELETE)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        empleadoXDatoAdicionalService.delete(id);
        return new GeneralOutput(HttpStatus.ACCEPTED.value(),HttpStatus.ACCEPTED.getReasonPhrase(),"Registro Eliminado Exitosamente...");
    }

    @GetMapping("/empleado/{id}")
    public GeneralOutput getByIdEmpleado(@PathVariable Long id, @RequestHeader("Authorization") String token,  @PageableDefault(sort = {"empleado"}, direction = Sort.Direction.ASC) Pageable pageable) throws Exception{
        if(!isAuthorized(token,VIEW)){
            throw new UnauthorizedException("El Usuario no esta autorizado para ejecutar este servicio.");
        }
        List<EmpleadoXDatoAdicionalInputDTO> listEntityDTO = new ArrayList<>();
        for (EmpleadoXDatoAdicional list:empleadoXDatoAdicionalService.findAllEmpleado(id,pageable)) {
            listEntityDTO.add(EmpleadoXDatoAdicionalInputDTO.getDTO(list));
        }
        return new GeneralOutput(HttpStatus.OK.value(),HttpStatus.OK.getReasonPhrase(),listEntityDTO, empleadoXDatoAdicionalService.findAllEmpleado(id, pageable));
    }

    @Override
    public GeneralOutput getById(@PathVariable Long id, @RequestHeader("Authorization") String token) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput getAll(String token, Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public GeneralOutput create(EmpleadoXDatoAdicionalInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }

    @Override
    public GeneralOutput update(EmpleadoXDatoAdicionalInputDTO entity, String token, List<MultipartFile> files) {
        return null;
    }
}
