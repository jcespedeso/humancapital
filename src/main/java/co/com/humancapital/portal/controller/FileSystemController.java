package co.com.humancapital.portal.controller;

import co.com.humancapital.portal.dto.GeneralOutput;
import co.com.humancapital.portal.entity.Multimedia;
import co.com.humancapital.portal.service.FileSystemService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/11/2019
 * Clase para la implementación de los diferentes microservicios provistos para la carga y lectura de
 * archivos en el sistema.
 */
@RestController
@RequestMapping("/fileSystem")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequiredArgsConstructor
public class FileSystemController {
    
    private final FileSystemService fileSystemService;
    
    @Value("${co.com.humancapital.portal.multimedia.root}")
    private String directorioRoot;
    
    /**
     * @return Codigo de 13 digitos
     * @author Alejandro Herrera Montilla Método que devuelve la hora actual del sistema en milisegundos, sumado con un
     * numero aleatorio de tres digitos, como codigo para identificar el directorio en donde se almacenan y parte del
     * nombre de los archivos.
     */
    public static BigDecimal generarCodigo() {
        int numeroAleatorio = ThreadLocalRandom.current().nextInt(100, 999);
        BigDecimal codigo = BigDecimal.valueOf(System.currentTimeMillis() + numeroAleatorio);
        return codigo;
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite descargar un archivo multimedia.
     * @param response Representa la respuesta del servidor
     * @param id id que identifica el archivo multimedia a descargar.
     */
    @GetMapping("/descargar/{id}")
    public void descargarArchivo(HttpServletResponse response, @PathVariable("id") Long id) throws Exception {
        fileSystemService.descargarArchivo(response, id);
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Permite guardar en el servidor una archivo multimedia.
     * @param files Archivo a guardar
     * @param procedencia Clase que utiliza el metodo.
     * @param idProcedencia Id del registro.
     * @return Id del archivo multimedia.
     */
    public Long fileSystemSave(List<MultipartFile> files, String procedencia, String idProcedencia) {
        BigDecimal subDirectorio = generarCodigo();
        String directorioFinal = fileSystemService.generarEstructuraRoot(directorioRoot);
        fileSystemService.crearDirectorios(directorioFinal, subDirectorio);
        Multimedia multimedia = fileSystemService.guardarArchivo(
                files.iterator().next(), directorioFinal, subDirectorio, procedencia, idProcedencia);
        return multimedia.getIdMultimedia();
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite guardar en el servidor una archivo multimedia.
     * @param files Archivo a guardar
     * @param nombreArchivo Atributo de referencia del archivo.
     * @param idArchivo Valor que identifica el archivo.
     * @return Objeto GeneralOutput con la confirmación de la operación.
     */
    @PostMapping("fileSave")
    public GeneralOutput saveFile(List<MultipartFile> files, String nombreArchivo, String idArchivo) {
        BigDecimal subDirectorio = generarCodigo();
        String directorioFinal = fileSystemService.generarEstructuraRoot(directorioRoot);
        fileSystemService.crearDirectorios(directorioFinal, subDirectorio);
        fileSystemService.guardarArchivo(
                files.iterator().next(), directorioFinal, subDirectorio, nombreArchivo, idArchivo);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Archivo Guardado Exitosamente...");
    }

    /**
     * @author Alejandro Herrera Montilla
     * Permite guardar en el servidor una archivo multimedia.
     * @param files Archivo a guardar
     * @return Id del archivo multimedia.
     */
    @PostMapping("filesSave")
    public GeneralOutput saveFiles(List<MultipartFile> files) {
        BigDecimal subDirectorio = generarCodigo();
        String directorioFinal = fileSystemService.generarEstructuraRoot(directorioRoot);
        fileSystemService.crearDirectorios(directorioFinal, subDirectorio);
        fileSystemService.guardarMultiplesArchivos(files, directorioFinal, subDirectorio);
        return new GeneralOutput(HttpStatus.CREATED.value(),HttpStatus.CREATED.getReasonPhrase(),"Archivos Guardados Exitosamente...");
    }
}
