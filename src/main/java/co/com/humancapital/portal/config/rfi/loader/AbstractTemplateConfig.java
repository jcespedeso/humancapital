package co.com.humancapital.portal.config.rfi.loader;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
public abstract class AbstractTemplateConfig {
    @Setter(AccessLevel.PROTECTED)
    private Map<Class<?>, List<ExcelColumnHeaderDefinition>> fields =
            new TreeMap<>(Comparator.comparing(x -> x.getName()));
    
    protected void addField(Class<?> entity, ExcelColumnHeaderDefinition columnHeaderDefinition) {
        if (!fields.containsKey(entity)) {
            fields.put(entity, new ArrayList<>());
        }
        
        this.fields.get(entity).add(columnHeaderDefinition);
    }
    
    public abstract String getSheetName();
}