package co.com.humancapital.portal.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CharacterEncodingFilter;


/**
 * @author Johan Miguel Céspedes Ortega
 * @date 19/12/2019
 * Configuracion del servicio de autenticación por diferentes medios.
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter, CsrfFilter.class);

        http.csrf()
                .disable()
                .authorizeRequests()
                //Esto no debe mantenerse habilitado ya que es necesario pasar por la autenticación y esto hace que la omita para usuario y los metodos dependientes de usuario
                //.antMatchers("/usuario/**").permitAll()
                // Unico path que se permite sin autenticación, para que se permita la obtención del Token.
                .antMatchers("/login/**").permitAll()
                .antMatchers("/").permitAll();
                 /*Cualquier solicitud debe ser autenticada, No se puede activar ya que se esta aplicando Autenticacion JWT*/
//                .anyRequest().authenticated();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new
                UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

//    @Override
//    public void configure(WebSecurity web)  {
//    /** Excluye los recursos asociados a servicios REST que manejan otro tipo
//    * de autenticación.*/
//        web.ignoring().antMatchers("/integrationActivity/**");
//    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//
//        if (autenticationSource.equals("bd")) {
//            auth
//                    .jdbcAuthentication()
//                    .dataSource(dataSource).passwordEncoder(new BCryptPasswordEncoder())
//                    .usersByUsernameQuery("SELECT \"USERNAME\", \"PASSWORD\", \"ACTIVO\" " +
//                            "FROM \"humanCapitalSch\".\"USUARIO\" " +
//                            "WHERE \"USERNAME\" = ? and \"ACTIVO\" = true")
//                    .authoritiesByUsernameQuery("SELECT DISTINCT\n" +
//                            "US.\"USERNAME\",\n" +
//                            "PRI.\"CODIGO_PRIVILEGIO\"\n" +
//                            "FROM\n" +
//                            "\"humanCapitalSch\".\"USUARIO\" as \"us\"\n" +
//                            "LEFT JOIN \"humanCapitalSch\".\"USUARIO_X_PERFIL\" uxp ON (\"uxp\".\"USUARIO\" = \"us\".\"ID_USUARIO\" )\n" +
//                            "LEFT JOIN \"humanCapitalSch\".\"PERFIL\" pro ON ( \"pro\".\"ID_PERFIL\" = \"uxp\".\"PERFIL\")\n" +
//                            "LEFT JOIN \"humanCapitalSch\".\"PERFIL_X_PRIVILEGIO\" pxp ON ( \"pxp\".\"PERFIL\" = \"pro\".\"ID_PERFIL\" )\n" +
//                            "LEFT JOIN \"humanCapitalSch\".\"PRIVILEGIO\" pri ON ( \"pri\".\"CODIGO_PRIVILEGIO\" = \"pxp\".\"PRIVILEGIO\" )\n" +
//                            "WHERE\n" +
//                            "US.\"USERNAME\" = ?");
//        } else {
//            auth.inMemoryAuthentication()
//                    .withUser("admin")
//                    .password("$2a$10$i6Wbz2qRIcZGsWKSeOE80Oqx1YxUdIA6OLnpPPJodmOXC.cOOMnwq")
//                    .roles("ALL").authorities("ALL");
//        }
//
//    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
