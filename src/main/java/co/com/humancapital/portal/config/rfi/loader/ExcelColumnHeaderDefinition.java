package co.com.humancapital.portal.config.rfi.loader;

import co.com.humancapital.portal.rfi.type.ExcelColumnType;
import java.util.Collection;
import lombok.Data;

@Data
public class ExcelColumnHeaderDefinition {
    
    private final int columnIndex;
    private final String name;
    private final ExcelColumnType fieldType;
    private Collection<String> items;
    private ExcelColumnType itemType;
    
    public ExcelColumnHeaderDefinition(int columnIndex, String name, ExcelColumnType fieldType) {
        this.columnIndex = columnIndex;
        this.name = name;
        this.fieldType = fieldType;
    }
    
    public ExcelColumnHeaderDefinition(int columnIndex, String name, ExcelColumnType fieldType,
            Collection<String> items, ExcelColumnType itemType) {
        this(columnIndex, name, fieldType);
        this.items = items;
        this.itemType = itemType;
        validateColumnDefinition();
    }
    
    private void validateColumnDefinition() {
        if (fieldType == ExcelColumnType.ITEMS) {
            if (items == null) {
                throw new IllegalArgumentException("::: Column config ITEMS requires values to populate the list");
            } else if (itemType == null) {
                throw new IllegalArgumentException("::: Column config ITEMS requires a ITEM TYPE");
            }
        }
    }
}
