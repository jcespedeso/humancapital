package co.com.humancapital.portal.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

@Configuration
@RequiredArgsConstructor
public class TransactionConfig {
    private final PlatformTransactionManager transactionManager;
    
    @Bean
    public TransactionTemplate getTransactionTemplate() {
        return new TransactionTemplate(transactionManager);
    }
}