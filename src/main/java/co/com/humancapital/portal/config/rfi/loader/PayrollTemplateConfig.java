package co.com.humancapital.portal.config.rfi.loader;

import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.rfi.type.ExcelColumnType;

public final class PayrollTemplateConfig extends AbstractTemplateConfig {
    private static final String DEFAULT_SHEET_NAME = "1. Payroll Employee Info";
    private static final PayrollTemplateConfig instance = new PayrollTemplateConfig();
    
    private PayrollTemplateConfig() {
        int columnIndex = 1;
        addEmployeeField(new ExcelColumnHeaderDefinition(columnIndex++, "codigo", ExcelColumnType.TEXT));
    }
    
    private void addEmployeeField(ExcelColumnHeaderDefinition headerDefinition) {
        addField(Empleado.class, headerDefinition);
    }
    
    public static PayrollTemplateConfig getInstance() {
        return instance;
    }
    
    @Override
    public String getSheetName() {
        return DEFAULT_SHEET_NAME;
    }
}
