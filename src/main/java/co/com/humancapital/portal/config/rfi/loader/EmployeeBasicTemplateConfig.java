package co.com.humancapital.portal.config.rfi.loader;

import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.DatoTributario;
import co.com.humancapital.portal.entity.Departamento;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.rfi.type.ExcelColumnType;
import co.com.humancapital.portal.util.CategoriaCargo;
import co.com.humancapital.portal.util.CondicionEmpleado;
import co.com.humancapital.portal.util.EstadoCivil;
import co.com.humancapital.portal.util.Genero;
import co.com.humancapital.portal.util.RegimenPension;
import co.com.humancapital.portal.util.TipoCompensacion;
import co.com.humancapital.portal.util.TipoContrato;
import co.com.humancapital.portal.util.TipoDocumento;
import co.com.humancapital.portal.util.TipoRegimen;
import java.util.Arrays;
import java.util.Collection;

public final class EmployeeBasicTemplateConfig extends AbstractTemplateConfig {
    
    private static final String DEFAULT_SHEET_NAME = "1. Default Employee Info";
    private static final EmployeeBasicTemplateConfig instance = new EmployeeBasicTemplateConfig();
    
    public static EmployeeBasicTemplateConfig getInstance() {
        return instance;
    }
    
    private EmployeeBasicTemplateConfig() {
        int columnIndex = 1;
        columnIndex = addEmployeeFields(columnIndex);
        columnIndex = addDepartmentFields(columnIndex);
        columnIndex = addJobFields(columnIndex);
        columnIndex = addCompensationFields(columnIndex);
        addDatoTributario(columnIndex);
    }
    
    private int addEmployeeFields(int columnIndex) {
        addEmployeeField(columnIndex++, "codigo", ExcelColumnType.TEXT);
        addEmployeeFieldFromItems(columnIndex++, "tipoDocumento", TipoDocumento.listar().values());
        addEmployeeField(columnIndex++, "numeroDocumento", ExcelColumnType.TEXT);
        addEmployeeField(columnIndex++, "nombre", ExcelColumnType.TEXT);
        addEmployeeField(columnIndex++, "apellido", ExcelColumnType.TEXT);
        addEmployeeFieldFromItems(columnIndex++, "estadoCivil", EstadoCivil.listar().values());
        addEmployeeField(columnIndex++, "email", ExcelColumnType.EMAIL);
        addEmployeeField(columnIndex++, "ciudad", ExcelColumnType.TEXT);
        addEmployeeField(columnIndex++, "nacionalidad", ExcelColumnType.TEXT);
        addEmployeeFieldFromItems(columnIndex++, "genero", Genero.listar().values());
        addEmployeeFieldFromItems(columnIndex++, "regimenPension", RegimenPension.listar().values());
        addEmployeeFieldFromItems(columnIndex++, "dependientes",
                Arrays.asList("Si", "No"), ExcelColumnType.BOOLEAN);
        return columnIndex;
    }
    
    private void addEmployeeField(int columnIndex, String columnName, ExcelColumnType type) {
        addField(Empleado.class, new ExcelColumnHeaderDefinition(columnIndex, columnName, type));
    }
    
    private void addEmployeeFieldFromItems(int columnIndex, String columnName, Collection<String> items) {
        addFieldFromItems(Empleado.class, columnIndex, columnName, items, ExcelColumnType.TEXT);
    }
    
    private void addEmployeeFieldFromItems(int columnIndex, String columnName,
            Collection<String> items, ExcelColumnType itemsType) {
        addFieldFromItems(Empleado.class, columnIndex, columnName, items, itemsType);
    }
    
    private int addJobFields(int columnIndex) {
        addField(Cargo.class, new ExcelColumnHeaderDefinition(columnIndex++, "nombre", ExcelColumnType.TEXT));
        addFieldFromItems(Cargo.class, columnIndex++, "categoria",
                CategoriaCargo.listar().values(), ExcelColumnType.TEXT);
        return columnIndex;
    }
    
    private int addDepartmentFields(int columnIndex) {
        addField(Departamento.class, new ExcelColumnHeaderDefinition(columnIndex++, "nombre", ExcelColumnType.TEXT));
        return columnIndex;
    }
    
    private int addCompensationFields(int columnIndex) {
        addCompensationFieldFromItems(columnIndex++, "tipoCompensacion", TipoCompensacion.listar().values());
        addCompensationFieldFromItems(columnIndex++, "regimenLaboral", TipoRegimen.listar().values());
        addCompensationFieldFromItems(columnIndex++, "condicionLaboral", CondicionEmpleado.listar().values());
        addCompensationFieldFromItems(columnIndex++, "tipoContrato", TipoContrato.listar().values());
        addCompensationField(columnIndex++, "fechaIngreso", ExcelColumnType.DATE);
        addCompensationField(columnIndex++, "salario", ExcelColumnType.MONEY);
        return columnIndex;
    }
    
    private void addCompensationField(int columnIndex, String columnName, ExcelColumnType type) {
        addField(Compensacion.class, new ExcelColumnHeaderDefinition(columnIndex, columnName, type));
    }
    
    private void addCompensationFieldFromItems(int columnIndex, String columnName, Collection<String> items) {
        addFieldFromItems(Compensacion.class, columnIndex, columnName, items, ExcelColumnType.TEXT);
    }
    
    private int addDatoTributario(int columnIndex) {
        addDatoTributarioField(columnIndex++, "metodoRf", ExcelColumnType.NUMBER_INTEGER);
        addDatoTributarioField(columnIndex++, "porcentajeRf", ExcelColumnType.NUMBER);
        addDatoTributarioField(columnIndex++, "aporteVoluntarioPension", ExcelColumnType.NUMBER);
        addDatoTributarioField(columnIndex++, "aporteCuentaAfc", ExcelColumnType.NUMBER);
        addDatoTributarioField(columnIndex++, "aporteCuentaAfc", ExcelColumnType.NUMBER);
        addDatoTributarioField(columnIndex++, "deducibleVivienda", ExcelColumnType.NUMBER);
        return columnIndex;
    }
    
    private void addDatoTributarioField(int columnIndex, String columnName, ExcelColumnType type) {
        addField(DatoTributario.class, new ExcelColumnHeaderDefinition(columnIndex, columnName, type));
    }
    
    private void addFieldFromItems(Class<?> clazz, int columnIndex, String columnName,
            Collection<String> items, ExcelColumnType itemsType) {
        addField(clazz, new ExcelColumnHeaderDefinition(columnIndex, columnName,
                ExcelColumnType.ITEMS, items, itemsType));
    }
    
    @Override
    public String getSheetName() {
        return DEFAULT_SHEET_NAME;
    }
}