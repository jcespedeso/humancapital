package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.PerfilInputDTO;
import co.com.humancapital.portal.entity.Perfil;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.repository.PerfilRepository;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Céspedes Ortega
 * @Project humancapital
 * @date 20/12/2019
 */
@Service
public class PerfilService implements ServiceInterfaceDto<Perfil, Long, PerfilInputDTO>{

    private final PerfilRepository perfilRepository;

    public PerfilService(PerfilRepository perfilRepository) {
        this.perfilRepository = perfilRepository;
    }

    @Override
    public Perfil findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Perfil con id "+key+" no existe.");
        }
        return perfilRepository.findByIdPerfilAndActivoTrue(key);
    }

    @Override
    public Page<Perfil> findAll(Pageable pageable) throws Exception {
        if(perfilRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Perfiles creados en el sistema.");
        }
        return perfilRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Perfil create(PerfilInputDTO entity) throws Exception {
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El nombre del perfil no debe estar vacio.");
        }
        if(entity.getDescripcion() == null || entity.getDescripcion().trim().isEmpty()){
            throw new ResourceNotFoundException("El campo descripción no debe estar vacio.");
        }
        if(existeByNombre(entity.getNombre().toUpperCase().trim())){
            throw new ResourceNotFoundException("El nombre de perfil "+ entity.getNombre() +" ya existe, seleccione uno diferente");
        }

        Perfil perfil = new Perfil();
        perfil.setActivo(true);
        perfil.setNombre(entity.getNombre().toUpperCase().trim());
        perfil.setDescripcion(entity.getDescripcion());
        perfil.setFechaCreacion(new Date());
        perfil.setUsuarioCrea(entity.getUsuarioCrea());
        perfil.setFechaModifica(new Date());
        perfil.setUsuarioModifica(entity.getUsuarioCrea());
        if (entity.getPrivilegioCollection().size()> 0){
            perfil.setPrivilegioCollection(entity.getPrivilegioCollection());
        }
        return perfilRepository.saveAndFlush(perfil);
    }

    @Override
    public Perfil update(PerfilInputDTO entity) throws Exception {
        if(entity.getIdPerfil() == null || !existeById(entity.getIdPerfil())) {
            throw new ResourceNotFoundException("El Perfil id " + entity.getIdPerfil() +" no existe.");
        }
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El nombre del perfil no debe estar vacio.");
        }
        if(entity.getDescripcion() == null || entity.getDescripcion().trim().isEmpty()){
            throw new ResourceNotFoundException("El campo descripción No debe estar vacio.");
        }

        Perfil perfil = perfilRepository.findByIdPerfilAndActivoTrue(entity.getIdPerfil());
        perfil.setNombre(entity.getNombre().toUpperCase().trim());
        perfil.setDescripcion(entity.getDescripcion());
        perfil.setActivo(perfil.getActivo());
        perfil.setFechaCreacion(perfil.getFechaCreacion());
        perfil.setUsuarioCrea(perfil.getUsuarioCrea());
        perfil.setFechaModifica(new Date());
        perfil.setUsuarioModifica(entity.getUsuarioModifica());
        if (entity.getPrivilegioCollection().size()> 0){
            perfil.setPrivilegioCollection(entity.getPrivilegioCollection());
        }
        return perfilRepository.saveAndFlush(perfil);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El perfil con id " + key +" no existe.");
        }
        Perfil perfil = perfilRepository.findByIdPerfilAndActivoTrue(key);
        perfil.setActivo(false);
        perfil.setFechaInactiva(new Date());
        perfil.setUsuarioInactiva(usuarioInactiva);
        perfil.setPrivilegioCollection(null);
        //Todo validar como se comporta esta coleccion al eleminar el perfi, debe borrar las asignaciones que tenga a los usuarios.
        perfil.setUsuarioCollection(null);
        perfilRepository.save(perfil);
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(perfilRepository.findByIdPerfilAndActivoTrue(key)==null){
            return false;
        }
        return perfilRepository.existsById(key);
    }

    /**
     * Metodo que permite validar si existe un Perfil mediante su nombre especifico.
     * @param nombre Nombre a buscar.
     * @return @{true} Si el Perfil existe por el nombre indicado de lo contrario @{false}
     */
    public Boolean existeByNombre (String nombre) throws Exception {
        if (nombre == null){
            throw new ResourceNotFoundException("El nombre a validar, no puede estar vacio.");
        }
        return perfilRepository.findByNombreAndActivoTrue(nombre.trim().toUpperCase()) != null;
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite obtener el perfil apartir de su nombre especifico.
     * @param nombre Nombre del perfil a consultar.
     * @return Objeto Perfil
     */
    public Perfil findByNombre (String nombre) {
        if (nombre == null){
            throw new ResourceNotFoundException("El nombre a validar, no puede estar vacio.");
        }
        if (perfilRepository.findByNombreAndActivoTrue(nombre.trim().toUpperCase())==null) {
            try {
                throw new UnauthorizedException("El nombre del perfil no genero resultados, dicho perfil no existe.");
            } catch (UnauthorizedException e) {
                e.printStackTrace();
            }
        }
        return perfilRepository.findByNombreAndActivoTrue(nombre.trim().toUpperCase());
    }

    @Override
    public Perfil create(PerfilInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public Perfil update(PerfilInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
