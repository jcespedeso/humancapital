package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.DatoTributarioInputDTO;
import co.com.humancapital.portal.entity.DatoTributario;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.DatoTributarioRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import co.com.humancapital.portal.util.MetodoRf;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 16/12/2019 Clase para la implementación de las diferentes reglas de negocio que se generen alrededor de los
 * DatoTributario.
 */
@Service
public class DatoTributarioService implements ServiceInterfaceDto<DatoTributario, Long, DatoTributarioInputDTO> {
    
    private final DatoTributarioRepository datoTributarioRepository;
    
    private final ClienteRepository clienteRepository;
    
    private final EmpleadoRepository empleadoRepository;
    
    private final EmpleadoService empleadoService;
    
    public DatoTributarioService(DatoTributarioRepository datoTributarioRepository, ClienteRepository clienteRepository,
            EmpleadoRepository empleadoRepository, EmpleadoService empleadoService) {
        this.datoTributarioRepository = datoTributarioRepository;
        this.clienteRepository = clienteRepository;
        this.empleadoRepository = empleadoRepository;
        this.empleadoService = empleadoService;
    }
    
    @Override
    public DatoTributario findId(Long key) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Dato Tributario con id " + key + " no existe.");
        }
        return datoTributarioRepository.findByIdDatoTributario(key);
    }
    
    @Override
    public Page<DatoTributario> findAll(Pageable pageable) {
        if (datoTributarioRepository.findAll(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Datos Tributarios creados en el sistema.");
        }
        return datoTributarioRepository.findAll(pageable);
    }
    
    @Override
    public DatoTributario create(DatoTributarioInputDTO entity) {
        if (entity.getEmpleado() == null || !empleadoService.existeById(entity.getEmpleado())) {
            throw new ResourceNotFoundException("El Empleado con id " + entity.getEmpleado() + ", no existe.");
        }
        if (empleadoTieneDatoTributario(entity.getEmpleado())) {
            throw new ResourceNotFoundException(
                    "El empleado seleccionado, ya tiene un conjunto de datos tributarios creados en el sistema.");
        }
        if (entity.getMetodoRf() == null || !MetodoRf.existeKey(entity.getMetodoRf())) {
            throw new ResourceNotFoundException(
                    "El Metodo seleccionado para calcular la Retención en la Fuente no puede estar vacio o no corresponde con los metodos permitidos en el sistema.");
        }
        if (entity.getPorcentajeRf() == null) {
            entity.setPorcentajeRf(0.0);
        }
        if (entity.getPorcentajeRf() < 0) {
            throw new ResourceNotFoundException("El valor porcentual de Retefuente no puede ser negativo.");
        }
        if (entity.getAporteVoluntarioPension() == null) {
            entity.setAporteVoluntarioPension(0.0);
        }
        if (entity.getAporteVoluntarioPension() < 0) {
            throw new ResourceNotFoundException("El valor de aporte voluntario a pension no puede ser negativo.");
        }
        if (entity.getAporteCuentaAfc() == null) {
            entity.setAporteCuentaAfc(0.0);
        }
        if (entity.getAporteCuentaAfc() < 0) {
            throw new ResourceNotFoundException("El valor de aporte a cuentas AFC no puede ser negativo.");
        }
        if (entity.getDeducibleSalud() == null) {
            entity.setDeducibleSalud(0.0);
        }
        if (entity.getDeducibleSalud() < 0) {
            throw new ResourceNotFoundException(
                    "El valor deducible en otros servicios de salud no puede ser negativo.");
        }
        if (entity.getDeducibleVivienda() == null) {
            entity.setDeducibleVivienda(0.0);
        }
        if (entity.getDeducibleVivienda() < 0) {
            throw new ResourceNotFoundException("El valor deducible por vivienda no puede ser negativo.");
        }
        
        DatoTributario datoTributario = new DatoTributario();
        datoTributario.setMetodoRf(entity.getMetodoRf());
        datoTributario.setPorcentajeRf(entity.getPorcentajeRf());
        datoTributario.setAporteVoluntarioPension(entity.getAporteVoluntarioPension());
        datoTributario.setAporteCuentaAfc(entity.getAporteCuentaAfc());
        datoTributario.setDeducibleSalud(entity.getDeducibleSalud());
        datoTributario.setDeducibleVivienda(entity.getDeducibleVivienda());
        datoTributario.setEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()));
        return datoTributarioRepository.saveAndFlush(datoTributario);
    }
    
    @Override
    public DatoTributario update(DatoTributarioInputDTO entity) {
        if (!existeById(entity.getIdDatoTributario())) {
            throw new ResourceNotFoundException(
                    "El Dato Tributario con id No." + entity.getIdDatoTributario() + " no existe.");
        }
        if (entity.getMetodoRf() == null || !MetodoRf.existeKey(entity.getMetodoRf())) {
            throw new ResourceNotFoundException(
                    "El Metodo seleccionado para calcular la Retención en la Fuente no puede estar vacio o no corresponde con los metodos permitidos en el sistema.");
        }
        if (entity.getPorcentajeRf() == null) {
            entity.setPorcentajeRf(0.0);
        }
        if (entity.getPorcentajeRf() < 0) {
            throw new ResourceNotFoundException("El valor porcentual de Retefuente no puede ser negativo.");
        }
        if (entity.getAporteVoluntarioPension() == null) {
            entity.setAporteVoluntarioPension(0.0);
        }
        if (entity.getAporteVoluntarioPension() < 0) {
            throw new ResourceNotFoundException("El valor de aporte voluntario a pension no puede ser negativo.");
        }
        if (entity.getAporteCuentaAfc() == null) {
            entity.setAporteCuentaAfc(0.0);
        }
        if (entity.getAporteCuentaAfc() < 0) {
            throw new ResourceNotFoundException("El valor de aporte a cuentas AFC no puede ser negativo.");
        }
        if (entity.getDeducibleSalud() == null) {
            entity.setDeducibleSalud(0.0);
        }
        if (entity.getDeducibleSalud() < 0) {
            throw new ResourceNotFoundException(
                    "El valor deducible en otros servicios de salud no puede ser negativo.");
        }
        if (entity.getDeducibleVivienda() == null) {
            entity.setDeducibleVivienda(0.0);
        }
        if (entity.getDeducibleVivienda() < 0) {
            throw new ResourceNotFoundException("El valor deducible por vivienda no puede ser negativo.");
        }
        
        DatoTributario datoTributario = findId(entity.getIdDatoTributario());
        datoTributario.setMetodoRf(entity.getMetodoRf());
        datoTributario.setPorcentajeRf(entity.getPorcentajeRf());
        datoTributario.setAporteVoluntarioPension(entity.getAporteVoluntarioPension());
        datoTributario.setAporteCuentaAfc(entity.getAporteCuentaAfc());
        datoTributario.setDeducibleSalud(entity.getDeducibleSalud());
        datoTributario.setDeducibleVivienda(entity.getDeducibleVivienda());
        return datoTributarioRepository.saveAndFlush(datoTributario);
    }
    
    public void delete(Long key) {
        if (key == null || findId(key) == null) {
            throw new ResourceNotFoundException("El dato tributario con id " + key + " no existe o esta vacio.");
        }
        datoTributarioRepository.delete(findId(key));
    }
    
    @Override
    public Boolean existeById(Long key) {
        if (key == null) {
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if (datoTributarioRepository.findByIdDatoTributario(key) == null) {
            return false;
        }
        return datoTributarioRepository.existsById(key);
    }
    
    /**
     * @param key Id del Empleado a Consultar.
     * @return Objeto DatoTributario.
     * @author Alejandro Herrera Montilla - alhemoasde@gmail.com Metodo que permite obtener el registro de los Datos
     * Tributarios de un Empleado especifico.
     */
    public DatoTributario findByEmpleado(Long key) {
        if (!empleadoService.existeById(key)) {
            throw new ResourceNotFoundException("El Empleado con id " + key + " no existe.");
        }
        Empleado empleado = empleadoService.findId(key);
        if (datoTributarioRepository.findAllByEmpleado(empleado) == null) {
            throw new ResourceNotFoundException("El Empleado con id " + key + " no tiene Datos Tributarios asociados.");
        }
        return datoTributarioRepository.findAllByEmpleado(empleado);
    }
    
    /**
     * @param key Id del Empleado a validar.
     * @return {@code true} Si el Dato Tributario Existe, {@code false} Si no Existe el Dato Tributario.
     * @author Alejandro Herrera Montilla - alhemoasde@gmail.com Metodo que permite validar si un empleado tiene o no un
     * registro de Dato Tributario activo en el sistema.
     */
    public Boolean empleadoTieneDatoTributario(Long key) {
        Empleado empleado = empleadoRepository.findByIdEmpleadoAndActivoTrue(key);
        if (datoTributarioRepository.findAllByEmpleado(empleado) == null) {
            return false;
        }
        return true;
    }
    
    @Override
    public DatoTributario create(DatoTributarioInputDTO entity, List<MultipartFile> files) {
        return null;
    }
    
    @Override
    public DatoTributario update(DatoTributarioInputDTO entity, List<MultipartFile> files) {
        return null;
    }
    
    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
    
    }
}
