package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.ParametroInputDTO;
import co.com.humancapital.portal.entity.AsignacionParametro;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Parametro;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.repository.AsignacionParametroRepository;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.ParametroRepository;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor del Parametro.
 */
@Service
public class ParametroService implements ServiceInterfaceDto<Parametro, Long, ParametroInputDTO>{

    private final ParametroRepository parametroRepository;

    private final ClienteRepository clienteRepository;

    private final AsignacionParametroRepository asignacionParametroRepository;

    private final ClienteService clienteService;

    public ParametroService(ParametroRepository parametroRepository, ClienteRepository clienteRepository, AsignacionParametroRepository asignacionParametroRepository, ClienteService clienteService) {
        this.parametroRepository = parametroRepository;
        this.clienteRepository = clienteRepository;
        this.asignacionParametroRepository = asignacionParametroRepository;
        this.clienteService = clienteService;
    }

    @Override
    public Parametro findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Parametro con id "+key+" no existe.");
        }
        return parametroRepository.findByIdParametroAndActivoTrue(key);
    }

    @Override
    public Page<Parametro> findAll(Pageable pageable) throws Exception {
        if(parametroRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Parametros creados en el sistema.");
        }
        return parametroRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Parametro create(ParametroInputDTO entity) throws Exception {
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("Por favor ingrese un nombre de parametro.");
        }
        if(entity.getValor() == null || entity.getValor().trim().isEmpty()){
            throw new ResourceNotFoundException("El valor del parametro no puede estar vacio.");
        }
        if(entity.getUnidadMedida() == null || entity.getUnidadMedida().trim().isEmpty()){
            throw new ResourceNotFoundException("La Unidad de medidad no puede estar vaciA.");
        }

        Parametro parametro = new Parametro();
        parametro.setNombre(entity.getNombre().toUpperCase());
        parametro.setValor(entity.getValor().toUpperCase());
        parametro.setUnidadMedida(entity.getUnidadMedida().toUpperCase());
        parametro.setNormativo(entity.getNormativo());

        parametro.setActivo(true);
        parametro.setFechaCreacion(new Date());
        parametro.setUsuarioCrea(entity.getUsuarioCrea());
        parametro.setFechaModifica(new Date());
        parametro.setUsuarioModifica(entity.getUsuarioCrea());
        return parametroRepository.saveAndFlush(parametro);
    }

    @Override
    public Parametro update(ParametroInputDTO entity) throws Exception {
        if(entity.getIdParametro() == null || !existeById(entity.getIdParametro())){
            throw new UnauthorizedException("El Parametro con id "+entity.getIdParametro()+" no existe o no se envio el dato.");
        }
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("Por favor ingrese un nombre de parametro.");
        }
        if(entity.getValor() == null || entity.getValor().trim().isEmpty()){
            throw new ResourceNotFoundException("El valor del parametro no puede estar vacio.");
        }
        if(entity.getUnidadMedida() == null || entity.getUnidadMedida().trim().isEmpty()){
            throw new ResourceNotFoundException("La Unidad de medidad no puede estar vacio.");
        }

        Parametro parametro = findId(entity.getIdParametro());
        parametro.setNombre(entity.getNombre().toUpperCase());
        parametro.setValor(entity.getValor().toUpperCase());
        parametro.setUnidadMedida(entity.getUnidadMedida().toUpperCase());
        parametro.setNormativo(entity.getNormativo());

        parametro.setFechaModifica(new Date());
        parametro.setUsuarioModifica(entity.getUsuarioModifica());
        return parametroRepository.saveAndFlush(parametro);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        if(key == null || !existeById(key)){
            throw new UnauthorizedException("El Parametro con id "+key+" no existe o no se envio el dato.");
        }
        for (Cliente cliente:clienteRepository.findAllByActivoTrue()) {
            for (AsignacionParametro asigParamer :asignacionParametroRepository.findAllByClienteAndActivoTrue(cliente)) {
                if(asigParamer.getParametro().getIdParametro().equals(key)){
                    throw new ResourceNotFoundException("El Parametro no puede ser eliminado, por que tiene asociado un Cliente activo.");
                }
            }
        }
        Parametro parametro = parametroRepository.findByIdParametroAndActivoTrue(key);
        parametro.setActivo(false);
        parametro.setFechaInactivacion(new Date());
        parametro.setUsuarioInactiva(usuarioInactiva);
        parametroRepository.save(parametro);
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(parametroRepository.findByIdParametroAndActivoTrue(key)==null){
            return false;
        }
        return parametroRepository.existsById(key);
    }

    /**
     * @autor Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de Parametros asignados a un Cliente especifico.
     * @param idCliente Id del Cliente a consultar.
     * @return List de Objetos Parametro.
     */
    public Page<Parametro> obtenerParametroXCliente(Long idCliente, Pageable  pageable) throws Exception {
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new UnauthorizedException("El Cliente con id "+idCliente+" no existe o no se envio el dato.");
        }
        if(parametroRepository.obtenerParametroXCliente(idCliente,pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Parametros asignados al Cliente indicado.");
        }
        return parametroRepository.obtenerParametroXCliente(idCliente, pageable);
    }

    @Override
    public Parametro create(ParametroInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public Parametro update(ParametroInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
