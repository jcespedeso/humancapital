package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.ContactoEmergenciaInputDTO;
import co.com.humancapital.portal.entity.ContactoEmergencia;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ContactoEmergenciaRepository;
import co.com.humancapital.portal.util.Parentesco;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de las diferentes reglas de negocio que se generen
 * alrededor de los contactos de emergencia que puede tener un empleado.
 */
@Service
public class ContactoEmergenciaService implements ServiceInterfaceDto<ContactoEmergencia, Long, ContactoEmergenciaInputDTO> {

    private final ContactoEmergenciaRepository contactoEmergenciaRepository;

    private final EmpleadoService empleadoService;

    public ContactoEmergenciaService(ContactoEmergenciaRepository contactoEmergenciaRepository, EmpleadoService empleadoService) {
        this.contactoEmergenciaRepository = contactoEmergenciaRepository;
        this.empleadoService = empleadoService;
    }

    @Override
    public ContactoEmergencia findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Contacto de Emergencia con id "+key+" no existe.");
        }
        return contactoEmergenciaRepository.findByIdContacto(key);
    }

    @Override
    public Page<ContactoEmergencia> findAll(Pageable pageable) throws Exception {
        if(contactoEmergenciaRepository.findAll(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Cargos creados en el sistema.");
        }
        return contactoEmergenciaRepository.findAll(pageable);
    }

    @Override
    public ContactoEmergencia create(ContactoEmergenciaInputDTO entity) throws Exception {
        if (entity.getNombre() == null || entity.getNombre().trim().isEmpty()) {
            throw new ResourceNotFoundException("El nombre del contacto no puede estar vacio.");
        }
        if (entity.getApellido() == null || entity.getApellido().trim().isEmpty()) {
            throw new ResourceNotFoundException("El apellido del contacto no puede estar vacio.");
        }
        if (entity.getParentesco() == null || !Parentesco.existeKey(entity.getParentesco())) {
            throw new ResourceNotFoundException("El Parentesco no puede estar vacio o no es un valor permitido en el sistema.");
        }
        if (entity.getTelefono() == null || entity.getTelefono().trim().isEmpty()) {
            throw new ResourceNotFoundException("El numero telefonico del contacto no puede estar vacio.");
        }
        if (entity.getEmpleado() == null || !empleadoService.existeById(entity.getEmpleado())){
            throw new ResourceNotFoundException("El Empleado con id "+ entity.getEmpleado() +" no existe.");
        }

        ContactoEmergencia contactoEmergencia = new ContactoEmergencia();
        contactoEmergencia.setNombre(entity.getNombre().toUpperCase());
        contactoEmergencia.setApellido(entity.getApellido().toUpperCase());
        contactoEmergencia.setParentesco(entity.getParentesco());
        contactoEmergencia.setTelefono(entity.getTelefono());
        if(entity.getEmail()!=null){
            contactoEmergencia.setEmail(entity.getEmail().toLowerCase());
        }
        contactoEmergencia.setEmpleado(empleadoService.findId(entity.getEmpleado()));
        return contactoEmergenciaRepository.saveAndFlush(contactoEmergencia);
    }

    @Override
    public ContactoEmergencia update(ContactoEmergenciaInputDTO entity) throws Exception {
        if (entity.getIdContacto() == null || !existeById(entity.getIdContacto())) {
            throw new ResourceNotFoundException("El ContactoEmergencia con id "+ entity.getIdContacto()+" no existe o no se envio el dato.");
        }
        if (entity.getNombre() == null || entity.getNombre().trim().isEmpty()) {
            throw new ResourceNotFoundException("El nombre del contacto no puede estar vacio.");
        }
        if (entity.getApellido() == null || entity.getApellido().trim().isEmpty()) {
            throw new ResourceNotFoundException("El apellido del contacto no puede estar vacio.");
        }
        if (entity.getParentesco() == null || !Parentesco.existeKey(entity.getParentesco())) {
            throw new ResourceNotFoundException("El Parentesco no puede estar vacio o no es un valor permitido en el sistema.");
        }
        if (entity.getTelefono() == null || entity.getTelefono().trim().isEmpty()) {
            throw new ResourceNotFoundException("El numero telefonico del contacto no puede estar vacio.");
        }
        if (entity.getEmpleado() == null || !empleadoService.existeById(entity.getEmpleado())){
            throw new ResourceNotFoundException("El Empleado con id "+ entity.getEmpleado() +" no existe.");
        }

        ContactoEmergencia contactoEmergencia = findId(entity.getIdContacto());
        contactoEmergencia.setNombre(entity.getNombre().toUpperCase());
        contactoEmergencia.setApellido(entity.getApellido().toUpperCase());
        contactoEmergencia.setParentesco(entity.getParentesco().toUpperCase());
        contactoEmergencia.setTelefono(entity.getTelefono());
        if(entity.getEmail()!=null){
            contactoEmergencia.setEmail(entity.getEmail().toLowerCase());
        }
        contactoEmergencia.setEmpleado(empleadoService.findId(entity.getEmpleado()));
        return contactoEmergenciaRepository.saveAndFlush(contactoEmergencia);
    }

    public void delete(Long key) throws Exception {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Contacto de Emergencia con id " + key +" no existe o no se envio el dato.");
        }
        contactoEmergenciaRepository.delete(findId(key));
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite validar si el ContactoEmergencia se encuentra en el sistema.
     * @param key Id del ContactoEmergencia a validar.
     * @return {@code true} Si el ContactoEmergencia Existe, {@code false} Si no Existe el ContactoEmergencia.
     */
    @Override
    public Boolean existeById(Long key) throws Exception{
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(contactoEmergenciaRepository.findByIdContacto(key)==null){
            return false;
        }
        return contactoEmergenciaRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar todas los Contactos de Emergencia que registra un Empleado.
     * @param idEmpleado Id del Empleado a consultar.
     * @return Lista de ContactoEmergencia que pueda tener un Empleado.
     */
    public Page<ContactoEmergencia> contactoEmergenciasXEmpleado(Long idEmpleado, Pageable pageable) throws Exception {
        if(idEmpleado == null || !empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no existe o no se envio el dato.");
        }
        Empleado empleado = empleadoService.findId(idEmpleado);
        if(contactoEmergenciaRepository.findAllByEmpleado(empleado, pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no registra contacto de emergencia.");
        }
        return contactoEmergenciaRepository.findAllByEmpleado(empleado, pageable);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
    }

    @Override
    public ContactoEmergencia create(ContactoEmergenciaInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public ContactoEmergencia update(ContactoEmergenciaInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
