package co.com.humancapital.portal.service;

import co.com.humancapital.portal.entity.TipoOtroIngreso;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.TipoOtroIngresoRepository;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de TipoOtroIngreso.
 */
@Service
public class TipoOtroIngresoService implements ServiceInterfaceDto<TipoOtroIngreso, Long, TipoOtroIngreso> {

    private final TipoOtroIngresoRepository tipoOtroIngresoRepository;

    public TipoOtroIngresoService(TipoOtroIngresoRepository tipoOtroIngresoRepository) {
        this.tipoOtroIngresoRepository = tipoOtroIngresoRepository;
    }

    @Override
    public TipoOtroIngreso findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Tipo Ingreso con id "+key+" no existe.");
        }
        return tipoOtroIngresoRepository.findAllByIdTipoOtroIngresoAndActivoTrue(key);
    }

    @Override
    public Page<TipoOtroIngreso> findAll(Pageable pageable) throws Exception {
        if(tipoOtroIngresoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Tipos de Ingreso creados en el sistema.");
        }
        return tipoOtroIngresoRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public TipoOtroIngreso create(TipoOtroIngreso entity) throws Exception {
        TipoOtroIngreso tipoOtroIngreso = new TipoOtroIngreso();
        tipoOtroIngreso.setTipoIngreso(entity.getTipoIngreso().toUpperCase());
        tipoOtroIngreso.setActivo(true);
        tipoOtroIngreso.setFechaCreacion(new Date());
        tipoOtroIngreso.setUsuarioCrea(entity.getUsuarioCrea());
        tipoOtroIngreso.setFechaModifica(new Date());
        tipoOtroIngreso.setUsuarioModifica(entity.getUsuarioCrea());
        return tipoOtroIngresoRepository.saveAndFlush(tipoOtroIngreso);
    }

    @Override
    public TipoOtroIngreso update(TipoOtroIngreso entity) throws Exception {
        TipoOtroIngreso tipoOtroIngreso = findId(entity.getIdTipoOtroIngreso());
        tipoOtroIngreso.setTipoIngreso(entity.getTipoIngreso().toUpperCase());
        tipoOtroIngreso.setFechaModifica(new Date());
        tipoOtroIngreso.setUsuarioModifica(entity.getUsuarioModifica());
        return tipoOtroIngresoRepository.saveAndFlush(tipoOtroIngreso);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception{
        TipoOtroIngreso tipoOtroIngreso = findId(key);
        tipoOtroIngreso.setActivo(false);
        tipoOtroIngreso.setFechaInactivacion(new Date());
        tipoOtroIngreso.setUsuarioInactiva(usuarioInactiva);
        tipoOtroIngresoRepository.save(tipoOtroIngreso);
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(tipoOtroIngresoRepository.findAllByIdTipoOtroIngresoAndActivoTrue(key)==null){
            return false;
        }
        return tipoOtroIngresoRepository.existsById(key);
    }

    @Override
    public TipoOtroIngreso create(TipoOtroIngreso entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public TipoOtroIngreso update(TipoOtroIngreso entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
