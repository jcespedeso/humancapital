package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.UsuarioInputDTO;
import co.com.humancapital.portal.entity.*;
import co.com.humancapital.portal.exception.IntegrityViolationException;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import co.com.humancapital.portal.repository.PrivilegioRepository;
import co.com.humancapital.portal.repository.UsuarioRepository;
import co.com.humancapital.portal.util.EncoderUtil;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * @author Alejandro Herrera Montilla
 * @date 20/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor del usuario asignado a un empleado.
 */
@Service
public class UsuarioService implements ServiceInterfaceDto<Usuario, Long, UsuarioInputDTO> {

    private final EmpleadoRepository empleadoRepository;
    private final UsuarioRepository usuarioRepository;
    private final PrivilegioRepository privilegioRepository;
    private final EmpleadoService empleadoService;
    private final ClienteService clienteService;
    private final ClienteRepository clienteRepository;
    private final PerfilService perfilService;

    public UsuarioService(EmpleadoRepository empleadoRepository, UsuarioRepository usuarioRepository, PrivilegioRepository privilegioRepository, @Lazy EmpleadoService empleadoService, ClienteService clienteService, ClienteRepository clienteRepository, PerfilService perfilService) {
        this.empleadoRepository = empleadoRepository;
        this.usuarioRepository = usuarioRepository;
        this.privilegioRepository = privilegioRepository;
        this.empleadoService = empleadoService;
        this.clienteService = clienteService;
        this.clienteRepository = clienteRepository;
        this.perfilService = perfilService;
    }

    @Override
    public Usuario findId(Long key) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Usuario con id " + key + " no existe.");
        }
        return usuarioRepository.findByIdUsuarioAndActivoTrue(key);
    }

    @Override
    public Page<Usuario> findAll(Pageable pageable) {
        if (usuarioRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Usuarios creados en el sistema.");
        }
        return usuarioRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Usuario create(UsuarioInputDTO entity) {
        if (entity.getUsername() == null || entity.getUsername().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Username del empleado no puede estar vacio.");
        }
        if (entity.getPassword() == null || entity.getPassword().trim().isEmpty()) {
            throw new ResourceNotFoundException("Debe asignar un Password, este campo no puede estar vacio.");
        }
        if(entity.getCliente() == null && entity.getEmpleado() == null){
            throw new ResourceNotFoundException("El usuario debe ser vinculado a un Cliente o a un Empleado, falta uno de estos datos.");
        }
        if (existeByUsername(entity.getUsername())) {
            throw new ResourceNotFoundException("El username " + entity.getUsername() + " ya existe y esta activo, seleccione otro nombre de Usuario");
        }
        if (entity.getCliente() != null) {
            if (!clienteService.existeById(entity.getCliente())) {
                throw new ResourceNotFoundException("El Cliente con id " + entity.getCliente() + " no existe.");
            }
        }
        if (entity.getEmpleado() != null) {
            if (!empleadoService.existeById(entity.getEmpleado())) {
                throw new ResourceNotFoundException("El Empleado con id " + entity.getEmpleado() + " no existe.");
            }
            if (entity.getCliente() != null && !empleadoRepository.obtenerEmpleadoXCliente(entity.getCliente()).contains(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()))) {
                throw new ResourceNotFoundException("El Empleado con id " + entity.getEmpleado() + " no pertenece al Cliente con id  "+entity.getCliente()+".");
            }
            if (optenerUsuarioPorEmpleado(entity.getEmpleado()) != null) {
                throw new IntegrityViolationException("El Empleado con id " + entity.getEmpleado() + " ya tiene un usuario Activo asociado.");
            }
            if (optenerUsuarioInactivoPorEmpleado(entity.getEmpleado()) != null) {
                throw new IntegrityViolationException("El Empleado con id " + entity.getEmpleado() + " ya tiene un usuario Inactivo asociado.");
            }
        }
        Usuario usuario = new Usuario();
        usuario.setUsername(entity.getUsername().trim().toLowerCase());
        usuario.setPassword(EncoderUtil.encode(entity.getPassword()));
        if (entity.getEmpleado() != null) {
            usuario.setEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()));
        }
        if (entity.getCliente() != null) {
            usuario.setCliente(clienteService.findId(entity.getCliente()));
        }
        if (entity.getPerfilCollection() != null && entity.getPerfilCollection().size() > 0) {
            for (Perfil perfil:entity.getPerfilCollection()) {
                if (!perfilService.existeById(perfil.getIdPerfil())) {
                    throw new ResourceNotFoundException("El Perfil con id " + perfil.getIdPerfil() + " no existe.");
                }
            }
            usuario.setPerfilCollection(entity.getPerfilCollection());
        }
        usuario.setActivo(true);
        usuario.setFechaCreacion(new Date());
        usuario.setUsuarioCrea(entity.getUsuarioCrea());
        usuario.setFechaModifica(new Date());
        usuario.setUsuarioModifica(entity.getUsuarioCrea());
        return usuarioRepository.saveAndFlush(usuario);
    }

    @Override
    public Usuario update (UsuarioInputDTO entity) {
        if (entity.getIdUsuario() == null || !existeById(entity.getIdUsuario())) {
            throw new ResourceNotFoundException("El usuario con ID " + entity.getIdUsuario() + " no existe o no se envio el dato.");
        }
        if (entity.getUsername() == null || entity.getUsername().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Username del empleado no puede estar vacio.");
        }
        if(entity.getCliente() == null && entity.getEmpleado() == null){
            throw new ResourceNotFoundException("El usuario debe ser vinculado a un Cliente o a un Empleado, falta uno de estos datos.");
        }
        if (entity.getPassword() != null) {
            throw new ResourceNotFoundException("No se puede cambiar la contraseña del usuario por esta funcionalidad.");
        }
        if (entity.getCliente() != null) {
            if (!clienteService.existeById(entity.getCliente())) {
                throw new ResourceNotFoundException("El Cliente con id " + entity.getCliente() + " no existe.");
            }
        }
        if (entity.getEmpleado() != null) {
            if (!empleadoService.existeById(entity.getEmpleado())) {
                throw new ResourceNotFoundException("El Empleado con id " + entity.getEmpleado() + " no existe.");
            }
            if (entity.getCliente() != null && !empleadoRepository.obtenerEmpleadoXCliente(entity.getCliente()).contains(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()))) {
                throw new ResourceNotFoundException("El Empleado con id " + entity.getEmpleado() + " no pertenece al Cliente con id  "+entity.getCliente()+".");
            }
            Usuario usuario = usuarioRepository.findByIdUsuarioAndActivoTrue(entity.getIdUsuario());
            Empleado empleadoAsignadoAntiguo = empleadoService.findId(usuario.getEmpleado().getIdEmpleado());
            if (!entity.getEmpleado().equals(empleadoAsignadoAntiguo.getIdEmpleado())) {
                if (optenerUsuarioPorEmpleado(entity.getEmpleado()) != null) {
                    throw new ResourceNotFoundException("El empleado seleccionado ya cuenta con una cuenta de usuario asignada.");
                }
            }
        }
        Usuario usuario = findId(entity.getIdUsuario());
        usuario.setUsername(entity.getUsername().trim().toLowerCase());
        if (entity.getEmpleado() != null) {
            usuario.setEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()));
        }
        if (entity.getCliente() != null) {
            usuario.setCliente(clienteService.findId(entity.getCliente()));
        }
        if (entity.getPerfilCollection() != null && entity.getPerfilCollection().size() > 0) {
            for (Perfil perfil:entity.getPerfilCollection()) {
                if (!perfilService.existeById(perfil.getIdPerfil())) {
                    throw new ResourceNotFoundException("El Perfil con id " + perfil.getIdPerfil() + " no existe.");
                }
            }
            usuario.setPerfilCollection(entity.getPerfilCollection());
        }
        usuario.setUsuarioModifica(entity.getUsuarioModifica());
        usuario.setFechaModifica(new Date());
        return usuarioRepository.saveAndFlush(usuario);
    }

    @Override
    public void delete (Long key, Long usuarioInactiva) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Usuario con id " + key + " no existe.");
        }
        Usuario usuario = usuarioRepository.findByIdUsuarioAndActivoTrue(key);
        usuario.setActivo(false);
        usuario.setFechaInactivacion(new Date());
        usuario.setUsuarioInactiva(usuarioInactiva);
        usuario.setPerfilCollection(null);
        usuarioRepository.save(usuario);
    }

    @Override
    public Boolean existeById (Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if (usuarioRepository.findByIdUsuarioAndActivoTrue(key) == null) {
            return false;
        }
        return usuarioRepository.existsById(key);
    }

    /**
     * Metodo que permite validar si ya existe un username especifico.
     * @param username Nombre de usuario a validar.
     * @return @{true} Si ya existe de lo contrario @{false}
     */
    public Boolean existeByUsername (String username) {
        return usuarioRepository.findByUsernameAndActivoTrue(username.toLowerCase()).isPresent();
    }

    /**
     * Metodo que permite obtener el usuario apartir de su nombre de usuario.
     * @param username Nombre de usuario a validar.
     * @return Objeto Usuario.
     */
    public Optional<Usuario> findByUsernameAndActivoTrue (String username) {
        if(username == null){
            throw new ResourceNotFoundException("El Username a validar, no puede estar vacio.");
        }
        return usuarioRepository.findByUsernameAndActivoTrue(username.trim().toLowerCase());
    }

    /**
     * Metodo que permite obtener los privilegios especificos de un usuario apartir de su nombre
     * de usuario.
     * @param username Nombre de usuario a validar.
     * @return Listado de String con los codigos de los privilegios.
     */
    public List<String> obtenerPrivilegiosUsuario (String username) {
        if (username == null || !existeByUsername(username)) {
            throw new ResourceNotFoundException("El Usuario con username " + username + " no existe o no se encuentra activo.");
        }
        LinkedList<String> codigos = new LinkedList<>();
        for (Privilegio privilegio : privilegioRepository.findPrivilegiosForUsername(username)) {
            codigos.add(privilegio.getCodigoPrivilegio());
        }
        return codigos;
    }

    /**
     * Metodo que permite validar si un usuario tiene asignado o no un privilegio especifico.
     * @param codigoPrivilegio Codigo del privilegio.
     * @param usuario Objeto Usuario a validar.
     * @return @{true} Si el usuario tiene el privilegio asignado, de lo contrario @{false}
     */
    public boolean isValidateRol(String codigoPrivilegio, Usuario usuario) {
        if (codigoPrivilegio == null || privilegioRepository.findByCodigoPrivilegioEquals(codigoPrivilegio) == null) {
            throw new ResourceNotFoundException("El codigo de Privilegio " + codigoPrivilegio + " no existe.");
        }
        existeById(usuario.getIdUsuario());
        boolean success = false;
        if(obtenerPrivilegiosUsuario(usuario.getUsername()).contains(codigoPrivilegio)){
            success=true;
        }
        return success;
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite obtener el usuario activo de un Empleado.
     * @param idEmpleado Id del Empleado a consultar.
     * @return Objeto Usuario.
     */
    public Usuario optenerUsuarioPorEmpleado (Long idEmpleado) {
        if (idEmpleado == null) {
            throw new ResourceNotFoundException("El id del Empleado no puede estar vacio.");
        }
        empleadoService.existeById(idEmpleado);
        return usuarioRepository.findByEmpleadoAndActivoTrue(empleadoRepository.findByIdEmpleadoAndActivoTrue(idEmpleado));
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite obtener el usuario inactivo de un Empleado.
     * @param idEmpleado Id del Empleado a consultar.
     * @return Objeto Usuario.
     */
    public Usuario optenerUsuarioInactivoPorEmpleado (Long idEmpleado) {
        if (idEmpleado == null) {
            throw new ResourceNotFoundException("El id del Empleado no puede estar vacio.");
        }
        empleadoService.existeById(idEmpleado);
        return usuarioRepository.findByEmpleadoAndActivoFalse(empleadoRepository.findByIdEmpleadoAndActivoTrue(idEmpleado));
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite obtener el usuario inactivo apartir de su Id.
     * @param key Id del Usuario a consultar.
     * @return Objeto Usuario.
     */
    public Usuario optenerUsuarioInactivo (Long key) {
        if (key == null) {
            throw new ResourceNotFoundException("El id del Usuario no puede estar vacio.");
        }
        existeById(key);
        return usuarioRepository.findByIdUsuarioAndActivoFalse(key);
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite Listar todos los usuarios inactivos del sistema.
     * @return Objetos Usuario.
     */
    public Page<Usuario> optenerUsuariosInactivos (Pageable pageable) {
        if (usuarioRepository.findAllByActivoFalse(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen usuarios inactivos en el sistema.");
        }
        return usuarioRepository.findAllByActivoFalse(pageable);
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite validar si un usuario esta inactivo.
     * @param key Id del Usuario a validar
     * @return {@true} Si esta Inactivo o {@false} Si esta Activo.
     */
    public Boolean usuarioInactivo (Long key) {
        if (usuarioRepository.existsById(key)) {
            if (usuarioRepository.findByIdUsuarioAndActivoFalse(key) != null) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite activar un usuario.
     * @param key Id del usuario a activar
     * @param usuarioActiva Id del usuario que realiza la modificación.
     */
    public void activarUsuario (Long key, Long usuarioActiva) {
        if (key == null || !usuarioInactivo(key)) {
            throw new ResourceNotFoundException("El Usuario con id " + key + " no existe o no se encuentra inactivo.");
        }
        Usuario usuario = usuarioRepository.findByIdUsuarioAndActivoFalse(key);
        if (usuario.getEmpleado() != null && empleadoRepository.findByIdEmpleadoAndActivoTrue(usuario.getEmpleado().getIdEmpleado()) == null) {
            throw new ResourceNotFoundException("El Usuario no puede ser Activado, por que tiene un empleado asociado, active el Empleado y automaticamente se activa su Usuario.");
        }
        usuario.setActivo(true);
        usuario.setFechaModifica(new Date());
        usuario.setUsuarioModifica(usuarioActiva);
        usuario.setFechaInactivacion(null);
        usuario.setUsuarioInactiva(null);
        //Todo: Enviar notificacion push o correo electronico, notificando el cambio de contrasena, si tiene vinculado un empleado.
        usuarioRepository.save(usuario);
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite realizar el cambio de contrasena a un usuario.
     * @param entity Id del usuario a activar
     * @return Objeto Usuario.
     */
    public Usuario cambiarContrasena (UsuarioInputDTO entity) {
        if (entity.getIdUsuario() == null || !existeById(entity.getIdUsuario())) {
            throw new ResourceNotFoundException("El Usuario con id " + entity.getIdUsuario() + " no existe o se encuentra inactivo.");
        }
        if (entity.getPassword() == null || entity.getPassword().trim().isEmpty()) {
            throw new ResourceNotFoundException("Debe asignar un Password, este campo no puede estar vacio.");
        }
        if (entity.getPassword().trim().length() < 6) {
            throw new ResourceNotFoundException("El Password asignado, debe ser igual o mayor a 6 caracteres.");
        }
        Usuario usuario = findId(entity.getIdUsuario());
        usuario.setPassword(EncoderUtil.encode(entity.getPassword()));
        usuario.setUsuarioModifica(entity.getUsuarioModifica());
        usuario.setFechaModifica(new Date());
        //Todo: Enviar notificacion push o correo electronico, notificando la activacion del usuario, si tiene vinculado un empleado.
        return usuarioRepository.saveAndFlush(usuario);
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite obtener el listado de usuario activos de un Cliente.
     * @param idCliente Id del Cliente a consultar.
     * @return Lista Objeto Usuario.
     */
    public Page<Usuario> optenerUsuarioPorCliente (Long idCliente, Pageable pageable) {
        if (idCliente == null) {
            throw new ResourceNotFoundException("El id del Cliente no puede estar vacio.");
        }
        if (clienteRepository.findByIdClienteAndActivoTrue(idCliente) == null) {
            throw new ResourceNotFoundException("El Cliente con id " + idCliente + " no existe o esta inactivo.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if (usuarioRepository.findAllByClienteAndActivoTrue(cliente, pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Cliente no tiene usuarios activos en el sistema");
        }
        return usuarioRepository.findAllByClienteAndActivoTrue(cliente, pageable);
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite obtener el listado de usuario inactivos de un Cliente.
     * @param idCliente Id del Cliente a consultar.
     * @return Lista Objeto Usuario.
     */
    public Page<Usuario> optenerUsuarioInactivoPorCliente(Long idCliente, Pageable pageable) {
        if(idCliente == null){
            throw new ResourceNotFoundException("El id del Cliente no puede estar vacio.");
        }
        if(clienteRepository.findByIdClienteAndActivoTrue(idCliente)==null){
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe o no esta inactivo.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if(usuarioRepository.findAllByClienteAndActivoFalse(cliente,pageable).isEmpty()){
            throw new ResourceNotFoundException("El Cliente no tiene usuarios inactivos en el sistema");
        }
        return usuarioRepository.findAllByClienteAndActivoFalse(cliente,pageable);
    }

    /**
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com
     * Metodo que permite obtener el listado de usuario activos de un Cliente.
     * @param idCliente Id del Cliente a consultar.
     * @return Lista Objeto Usuario.
     */
    public List<Usuario> optenerUsuarioPorCliente (Long idCliente) {
        if (idCliente == null) {
            throw new ResourceNotFoundException("El id del Cliente no puede estar vacio.");
        }
        if (clienteRepository.findByIdClienteAndActivoTrue(idCliente) == null) {
            throw new ResourceNotFoundException("El Cliente con id " + idCliente + " no existe o esta inactivo.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if (usuarioRepository.findAllByClienteAndActivoTrue(cliente).isEmpty()) {
            throw new ResourceNotFoundException("El Cliente no tiene usuarios activos en el sistema");
        }
        return usuarioRepository.findAllByClienteAndActivoTrue(cliente);
    }

    @Override
    public Usuario create (UsuarioInputDTO entity, List <MultipartFile> files) {
        return null;
    }

    @Override
    public Usuario update (UsuarioInputDTO entity, List <MultipartFile> files) {
        return null;
    }
}