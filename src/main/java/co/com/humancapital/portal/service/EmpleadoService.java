package co.com.humancapital.portal.service;

import co.com.humancapital.portal.controller.FileSystemController;
import co.com.humancapital.portal.dto.EmpleadoInputDTO;
import co.com.humancapital.portal.dto.UsuarioInputDTO;
import co.com.humancapital.portal.entity.*;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.CargoRepository;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import co.com.humancapital.portal.util.*;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.nio.file.Files;
import java.util.*;

/**
 * @author Alejandro Herrera Montilla
 * @date 13/11/2019 Clase para la implementación de las diferentes reglas de negocio que se generen alrededor del
 * Emplado.
 */
@Service
public class EmpleadoService implements ServiceInterfaceDto<Empleado, Long, EmpleadoInputDTO> {
    
    private final EmpleadoRepository empleadoRepository;
    private final ClienteRepository clienteRepository;
    private final CargoRepository cargoRepository;
    private final CargoService cargoService;
    private final UsuarioService usuarioService;
    private final FileSystemController fileSystemController;
    private final FileSystemService fileSystemService;
    private final CompensacionService compensacionService;
    private final ClienteService clienteService;
    private final DepartamentoService departamentoService;
    private final PerfilService perfilService;

    public EmpleadoService(EmpleadoRepository empleadoRepository, ClienteRepository clienteRepository, CargoRepository cargoRepository, CargoService cargoService, UsuarioService usuarioService, FileSystemController fileSystemController, FileSystemService fileSystemService, @Lazy CompensacionService compensacionService, ClienteService clienteService, DepartamentoService departamentoService, PerfilService perfilService) {
        this.empleadoRepository = empleadoRepository;
        this.clienteRepository = clienteRepository;
        this.cargoRepository = cargoRepository;
        this.cargoService = cargoService;
        this.usuarioService = usuarioService;
        this.fileSystemController = fileSystemController;
        this.fileSystemService = fileSystemService;
        this.compensacionService = compensacionService;
        this.clienteService = clienteService;
        this.departamentoService = departamentoService;
        this.perfilService = perfilService;
    }

    @Override
    public Empleado findId(Long key) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Empleado con id " + key + " no existe.");
        }
        return empleadoRepository.findByIdEmpleadoAndActivoTrue(key);
    }
    
    @Override
    public Page<Empleado> findAll(Pageable pageable) {
        if (empleadoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Empleados creado en el sistema.");
        }
        return empleadoRepository.findAllByActivoTrue(pageable);
    }
    
    @Transactional
    @Override
    public Empleado create(EmpleadoInputDTO entity, List<MultipartFile> files) {
        
        if (entity.getTipoDocumento() == null || !TipoDocumento.existeKey(entity.getTipoDocumento())) {
            throw new ResourceNotFoundException(
                    "El Tipo de Documento del empleado a crear, no coincide con los permitidos por el sistema.");
        }
        if (entity.getNumeroDocumento() == null || entity.getNumeroDocumento().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Numero de Documento del empleado a Crear no puede estar vacio.");
        }
        if (documentoDuplicado(entity.getNumeroDocumento(), entity.getCargo())) {
            throw new ResourceNotFoundException("El Empleado con numero de documento " + entity.getNumeroDocumento()
                    + " que intenta Crear, ya existe como empleado de un mismo cliente.");
        }
        if (entity.getNombre() == null || entity.getNombre().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Nombre(s) del empleado a Crear no puede estar vacio.");
        }
        if (entity.getApellido() == null || entity.getApellido().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Apellido(s) del empleado a Crear no puede estar vacio.");
        }
        if (entity.getEstadoCivil() == null || !EstadoCivil.existeKey(entity.getEstadoCivil())) {
            throw new ResourceNotFoundException(
                    "El Estado Civil del empleado a crear, no coincide con los permitidos por el sistema.");
        }
        if (entity.getEmail() == null || entity.getEmail().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Email del empleado a Crear no puede estar vacio.");
        }
        if (entity.getCiudad() == null || entity.getCiudad().trim().isEmpty()) {
            throw new ResourceNotFoundException("La Ciudad donde radica el empleado a Crear no puede estar vacio.");
        }
        if (entity.getNacionalidad() == null || entity.getNacionalidad().trim().isEmpty()) {
            throw new ResourceNotFoundException(
                    "El nombre del País que define la nacionalidad del empleado a Crear no puede estar vacio.");
        }
        if (entity.getGenero() == null || !Genero.existeKey(entity.getGenero())) {
            throw new ResourceNotFoundException(
                    "El Genero del empleado a crear, no coincide con los permitidos por el sistema.");
        }
        if (entity.getIdioma() == null || !Idioma.existeKey(entity.getIdioma())) {
            throw new ResourceNotFoundException(
                    "El Idioma principal del empleado a crear, no coincide con los permitidos por el sistema.");
        }
        if (entity.getRegimenPension() == null || !RegimenPension.existeKey(entity.getRegimenPension())) {
            throw new ResourceNotFoundException(
                    "El Regimen Pensional del empleado a crear, no coincide con los permitidos por el sistema.");
        }
        if (entity.getCargo() == null || !cargoService.existeById(entity.getCargo())) {
            throw new ResourceNotFoundException("El Cargo con id " + entity.getCargo() + " no existe.");
        }
        
        Cliente cliente = clienteRepository.obtenerClienteXCargo(entity.getCargo());
        Cargo cargo = cargoRepository.findAllByIdCargoAndActivoTrue(entity.getCargo());
        
        Empleado empleado = new Empleado();
        empleado.setCodigo(cliente.getIdCliente() + "-" + entity.getNumeroDocumento());
        
        if (entity.getCodigoNomina() != null) {
            empleado.setCodigoNomina(entity.getCodigoNomina().trim().isEmpty() ? cliente.getIdCliente() + "-" + entity
                    .getNumeroDocumento() : entity.getCodigoNomina());
        }
        
        if (entity.getCentroCosto() != null) {
            empleado.setCentroCosto(entity.getCentroCosto().trim().isEmpty() ? cliente.getIdCliente() + "-" + entity
                    .getNumeroDocumento() : entity.getCentroCosto());
        }
        
        empleado.setTipoDocumento(entity.getTipoDocumento());
        empleado.setNumeroDocumento(entity.getNumeroDocumento());
        empleado.setNombre(entity.getNombre().toUpperCase());
        empleado.setApellido(entity.getApellido().toUpperCase());
        if (entity.getFechaNacimiento() != null) {
            empleado.setFechaNacimiento(entity.getFechaNacimiento());
        }
        empleado.setGenero(entity.getGenero());
        empleado.setEstadoCivil(entity.getEstadoCivil());
        empleado.setEmail(entity.getEmail().toLowerCase());
        empleado.setTelefono(entity.getTelefono() != null ? entity.getTelefono() : "0");
        
        if (entity.getCiudad() != null) {
            empleado.setCiudad(entity.getCiudad().toUpperCase());
        }
        
        if (entity.getNacionalidad() != null) {
            empleado.setNacionalidad(entity.getNacionalidad().toUpperCase());
        }
        
        if (entity.getIdioma() != null) {
            empleado.setIdioma(entity.getIdioma());
        }
        empleado.setFoto(entity.getFoto());
        empleado.setCategoria(cargo.getCategoria().getIdCategoria());
        empleado.setRegimenPension(entity.getRegimenPension());
        empleado.setDependientes(entity.isDependientes());
        empleado.setCambiarContrasena(entity.isCambiarContrasena());
        empleado.setPuedeSimular(entity.isPuedeSimular());
        empleado.setCargo(cargoRepository.findAllByIdCargoAndActivoTrue(entity.getCargo()));
        if (entity.getDatoAdicionalCollection() != null && entity.getDatoAdicionalCollection().size() > 0) {
            empleado.setDatoAdicionalCollection(entity.getDatoAdicionalCollection());
        }
        empleado.setActivo(true);
        empleado.setFechaCreacion(new Date());
        empleado.setUsuarioCrea(entity.getUsuarioCrea());
        empleado.setFechaModifica(new Date());
        empleado.setUsuarioModifica(entity.getUsuarioCrea());
        empleadoRepository.saveAndFlush(empleado);
        saveFoto(empleado,files);
        if (entity.getUsername() != null) {
            saveUsuarioAndPerfil(entity,empleado);
        }
        return empleadoRepository.saveAndFlush(empleado);
    }
    
    @Transactional
    @Override
    public Empleado update(EmpleadoInputDTO entity, List<MultipartFile> files) {
        if (entity.getIdEmpleado() == null || !existeById(entity.getIdEmpleado())) {
            throw new ResourceNotFoundException("El Empleado con Id " + entity.getIdEmpleado() + " no existe.");
        }
        if (entity.getTipoDocumento() == null || !TipoDocumento.existeKey(entity.getTipoDocumento())) {
            throw new ResourceNotFoundException(
                    "El Tipo de Documento del empleado a Actualizar, no coincide con los permitidos por el sistema.");
        }
        if (entity.getNumeroDocumento() == null || entity.getNumeroDocumento().trim().isEmpty()) {
            throw new ResourceNotFoundException(
                    "El Numero de Documento del empleado a Actualizar no puede estar vacio.");
        }
        if (!findId(entity.getIdEmpleado()).getNumeroDocumento().equals(entity.getNumeroDocumento())) {
            if (documentoDuplicado(entity.getNumeroDocumento(), entity.getCargo())) {
                throw new ResourceNotFoundException("El Empleado con numero de documento " + entity.getNumeroDocumento()
                        + " que intenta Actualizar, ya existe como empleado de un mismo cliente.");
            }
        }
        if (entity.getNombre() == null || entity.getNombre().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Nombre(s) del empleado a Actualizar no puede estar vacio.");
        }
        if (entity.getApellido() == null || entity.getApellido().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Apellido(s) del empleado a Actualizar no puede estar vacio.");
        }
        if (entity.getEstadoCivil() == null || !EstadoCivil.existeKey(entity.getEstadoCivil())) {
            throw new ResourceNotFoundException(
                    "El Estado Civil del empleado a Actualizar, no coincide con los permitidos por el sistema.");
        }
        if (entity.getEmail() == null || entity.getEmail().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Email del empleado a Actualizar no puede estar vacio.");
        }
        if (entity.getCiudad() == null || entity.getCiudad().trim().isEmpty()) {
            throw new ResourceNotFoundException(
                    "La Ciudad donde radica el empleado a Actualizar no puede estar vacio.");
        }
        if (entity.getNacionalidad() == null || entity.getNacionalidad().trim().isEmpty()) {
            throw new ResourceNotFoundException(
                    "El nombre del País que define la nacionalidad del empleado a Actualizar no puede estar vacio.");
        }
        if (entity.getGenero() == null || !Genero.existeKey(entity.getGenero())) {
            throw new ResourceNotFoundException(
                    "El Genero del empleado a actualizar, no coincide con los permitidos por el sistema.");
        }
        if (entity.getIdioma() == null || !Idioma.existeKey(entity.getIdioma())) {
            throw new ResourceNotFoundException(
                    "El Idioma principal del empleado a crear, no coincide con los permitidos por el sistema.");
        }
        if (entity.getRegimenPension() == null || !RegimenPension.existeKey(entity.getRegimenPension())) {
            throw new ResourceNotFoundException(
                    "El Regimen Pensional del empleado a crear, no coincide con los permitidos por el sistema.");
        }
        if (entity.getCargo() == null || !cargoService.existeById(entity.getCargo())) {
            throw new ResourceNotFoundException("El Cargo con id " + entity.getCargo() + " no existe.");
        }
        Empleado empleado = empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getIdEmpleado());
        
        if (!empleado.getNumeroDocumento().equals(entity.getNumeroDocumento())) {
            Cliente cliente = clienteRepository.obtenerClienteXCargo(entity.getCargo());
            empleado.setCodigo(cliente.getIdCliente() + "-" + entity.getNumeroDocumento());
        }
        if (!empleado.getCargo().getIdCargo().equals(entity.getCargo())) {
            Cargo cargo = cargoRepository.findAllByIdCargoAndActivoTrue(entity.getCargo());
            empleado.setCategoria(cargo.getCategoria().getIdCategoria());
            empleado.setCargo(cargo);
        }
        empleado.setTipoDocumento(entity.getTipoDocumento());
        empleado.setNumeroDocumento(entity.getNumeroDocumento());
        empleado.setNombre(entity.getNombre().toUpperCase());
        empleado.setApellido(entity.getApellido().toUpperCase());
        if (entity.getFechaNacimiento() != null) {
            empleado.setFechaNacimiento(entity.getFechaNacimiento());
        }
        empleado.setEstadoCivil(entity.getEstadoCivil());
        empleado.setGenero(entity.getGenero());
        empleado.setEmail(entity.getEmail().toLowerCase());
        if (entity.getTelefono() != null) {
            empleado.setTelefono(entity.getTelefono());
        }
        empleado.setCiudad(entity.getCiudad().toUpperCase());
        empleado.setNacionalidad(entity.getNacionalidad().toUpperCase());
        empleado.setRegimenPension(entity.getRegimenPension());
        empleado.setDependientes(entity.isDependientes());
        empleado.setCambiarContrasena(entity.isCambiarContrasena());
        empleado.setPuedeSimular(entity.isPuedeSimular());
        if (entity.getDatoAdicionalCollection() != null && entity.getDatoAdicionalCollection().size() > 0) {
            empleado.setDatoAdicionalCollection(entity.getDatoAdicionalCollection());
        }
        empleado.setUsuarioModifica(entity.getUsuarioModifica());
        empleado.setFechaModifica(new Date());
        empleadoRepository.saveAndFlush(empleado);
        saveFoto(empleado,files);
        return empleadoRepository.saveAndFlush(empleado);
    }
    
    @Override
    public void delete(Long key, Long usuarioInactiva) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Empleado con id " + key + " no existe.");
        }
        Empleado empleado = empleadoRepository.findByIdEmpleadoAndActivoTrue(key);
        //Todo revalidar esta regla de negocio con la nueva organizacion jerarquica.
//        if (esEmpleadoJefe(empleado.getCargo().getDepartamento().getCliente().getIdCliente(),
//                empleado.getIdEmpleado())) {
//            throw new ResourceNotFoundException("El empleado con id " + key
//                    + " no puede ser eliminado, por que registra como Jefe de un Departamento existe.");
//        }
        if (compensacionService.obtenerCompensacionActivaEmpleado(empleado.getIdEmpleado()) != null) {
            throw new ResourceNotFoundException("El empleado con id " + key
                    + " no puede ser eliminado, por que aun tiene un registro de Compensación activo.");
        }
        if (!empleado.getFoto().equals("Sin Foto")) {
            fileSystemService.borrarArchivo(new Long(empleado.getFoto()));
        }
        if (usuarioService.optenerUsuarioPorEmpleado(empleado.getIdEmpleado()) != null) {
            Usuario usuarioAsociado = usuarioService.optenerUsuarioPorEmpleado(empleado.getIdEmpleado());
            usuarioService.delete(usuarioAsociado.getIdUsuario(), usuarioInactiva);
        }
        empleado.setActivo(false);
        empleado.setFechaInactivacion(new Date());
        empleado.setUsuarioInactiva(usuarioInactiva);
        empleado.setFoto("Sin Foto");
        empleadoRepository.save(empleado);
    }
    
    @Override
    public Boolean existeById(Long key) {
        if (key == null) {
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if (empleadoRepository.findByIdEmpleadoAndActivoTrue(key) == null) {
            return false;
        }
        return empleadoRepository.existsById(key);
    }

    /**
     * @author Alendro Herrera Montilla
     * @date 04/04/2020
     * Metodo que permite guardar y validar el archivo que es cargado como foto del Empleado.
     * @param empleado Objeto Empleado.
     * @param files Objeto File con el archivo cargado.
     * @return Objeto empleado con el atributo foto cargado.
     */
    public Empleado saveFoto(Empleado empleado, List<MultipartFile> files){
        if (!CollectionUtils.isEmpty(Collections.singleton(files))) {
            if (!validarExtencionFoto(files)) {
                throw new ResourceNotFoundException("Tipo de archivo no permitido, solo son admitidos (png, jpeg y jpg).");
            }
            if (empleado.getFoto() != null && !empleado.getFoto().equals("Sin Foto")) {
                fileSystemService.borrarArchivo(new Long(empleado.getFoto()));
            }
            empleado.setFoto(fileSystemController
                    .fileSystemSave(files, Empleado.class.getSimpleName(), empleado.getIdEmpleado().toString())
                    .toString());
            if (empleado.getFoto()==null) {
                throw new ResourceNotFoundException("No ha sido posible guardar el archivo en el FileSystem del sistema.");
            }
        }else {
            empleado.setFoto("Sin Foto");
        }
        return empleadoRepository.saveAndFlush(empleado);
    }

    /**
     * @author Alendro Herrera Montilla
     * @date 04/04/2020
     * Metodo que permite asignar el usuario y el perfil por defecto <EMPLEADO> a un Empleado.
     * @param empleado Objeto Empleado.
     * @param entity Objeto EmpleadoInputDTO con la información del usuario a crear.
     */
    public void saveUsuarioAndPerfil(EmpleadoInputDTO entity, Empleado empleado){
        UsuarioInputDTO usuario = new UsuarioInputDTO();
        usuario.setUsername(entity.getUsername());
        usuario.setPassword(entity.getPassword());
        usuario.setEmpleado(empleado.getIdEmpleado());
        usuario.setCliente(empleado.getCargo().getDepartamento().getCliente().getIdCliente());
        usuario.setUsuarioCrea(entity.getUsuarioCrea());
        //Todo pefil por defecto para el Usuario Empleado del Cliente...
        usuario.setPerfilCollection(Collections.singleton(perfilService.findByNombre("EMPLEADO")));
        usuarioService.create(usuario);
    }

    /**
     * * @author Alejandro Herrera Montilla Metodo que permite validar si ya existeKey un empleado a partir de su numero
     * de documento de identificación dentro del conjunto de empleados de un Cliente.
     *
     * @param documento Numero de documento del empleado a validar.
     * @param idCargo Id cargo del empleado.
     * @return {@code true} Si el empleado Existe, {@code false} Si no Existe el empleado.
     */
    public Boolean documentoDuplicado(String documento, Long idCargo) {
        if (cargoRepository.findAllByIdCargoAndActivoTrue(idCargo) == null) {
            throw new ResourceNotFoundException("El Cargo indicado no existe o esta inactivo.");
        }
        if (documento == null || documento.isEmpty()) {
            throw new ResourceNotFoundException("El numero de documento no puede estar vacio.");
        }
        Cliente cliente = clienteRepository.obtenerClienteXCargo(idCargo);
        if (!clienteRepository.existsById(cliente.getIdCliente())) {
            return false;
        }
        List<Empleado> empleados = empleadoRepository.obtenerEmpleadoXCliente(cliente.getIdCliente());
        if (empleados.size() == 0) {
            return false;
        }
        List<Empleado> empXDocumento = empleadoRepository.findAllByNumeroDocumento(documento.trim());
        if (empXDocumento.size() == 0) {
            return false;
        }
        for (Empleado emp : empXDocumento) {
            if (empleados.contains(emp)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param files Archivo a evaluar.
     * @return String que identifica la extencion del archivo que recibio por parametro.
     * @author Alejandro Herrera Montilla Metodo que identifica la metadata de la extension de un archivo.
     */
    public Boolean validarExtencionFoto(List<MultipartFile> files) {
        Map<String, String> listContentType = new HashMap<>();
        listContentType.put("image/jpeg", "jpeg");
        listContentType.put("image/jpg", "jpg");
        listContentType.put("image/png", "png");
        return listContentType.containsKey(files.iterator().next().getContentType());
    }
    
    /**
     * @param idCargo Id del Cargo a validar.
     * @return Listado de Empleado pertenecientes al cargo.
     * @author Alejandro Herrera Montilla Metodo que permite listar los empleado que pertenecen a un Cargo.
     */
    public Page<Empleado> empleadosXCargo(Long idCargo, Pageable pageable) {
        if (idCargo == null || !cargoService.existeById(idCargo)) {
            throw new ResourceNotFoundException("El Cargo con id " + idCargo + " no existe o esta vacio.");
        }
        Cargo cargo = cargoService.findId(idCargo);
        if (empleadoRepository.findAllByCargoAndActivoTrue(cargo).size() == 0) {
            throw new ResourceNotFoundException("El cargo " + cargo.getNombre() + " no tiene empleados activos.");
        }
        return empleadoRepository.findAllByCargoAndActivoTrue(cargo, pageable);
    }
    
    /**
     * @param idDepto Id del Departamento a validar.
     * @return Listado de Empleados pertenecientes al departamento.
     * @author Alejandro Herrera Montilla Metodo que permite listar los empleado que pertenecen a un Departamento.
     */
    public Page<Empleado> empleadosXDepartamento(Long idDepto, Pageable pageable) {
        if (idDepto == null || !departamentoService.existeById(idDepto)) {
            throw new ResourceNotFoundException("El Departamento con id " + idDepto + " no existe o esta vacio.");
        }
        Departamento departamento = departamentoService.findId(idDepto);
        if (empleadoRepository.obtenerEmpleadoXDepartamento(departamento.getIdDepartamento(), pageable).isEmpty()) {
            throw new ResourceNotFoundException(
                    "El Departamento " + departamento.getNombre() + " no tiene empleados activos.");
        }
        return empleadoRepository.obtenerEmpleadoXDepartamento(idDepto, pageable);
    }
    
    /**
     * @param idCliente Id Cliente a validar.
     * @return Listado de Empleados pertenecientes al cliente.
     * @author Alejandro Herrera Montilla Metodo que permite listar los empleado que pertenecen a un Cliente.
     */
    public Page<Empleado> empleadosXCliente(Long idCliente, Pageable pageable) {
        if (idCliente == null || !clienteService.existeById(idCliente)) {
            throw new ResourceNotFoundException("El Cliente con id " + idCliente + " no existe o esta vacio.");
        }
        ;
        Cliente cliente = clienteService.findId(idCliente);
        if (empleadoRepository.obtenerEmpleadoXCliente(idCliente, pageable).isEmpty()) {
            throw new ResourceNotFoundException(
                    "El Cliente " + cliente.getRazonSocial() + " no tiene empleados activos.");
        }
        return empleadoRepository.obtenerEmpleadoXCliente(idCliente, pageable);
    }
    
    /**
     * @param idCliente Id Cliente a validar.
     * @return Listado de Empleados nombrados como Jefe en los diferentes Departamentos de un Cliente.
     * @author Alejandro Herrera Montilla Metodo que permite listar los empleado que ya se encuentran asignados como
     * Jefes de un Departamento.
     */
    @Deprecated
    public Page<Empleado> empleadosJefes(Long idCliente, Pageable pageable) {
        if (idCliente == null || !clienteService.existeById(idCliente)) {
            throw new ResourceNotFoundException("El Cliente con id " + idCliente + " no existe o esta vacio.");
        }
        Cliente cliente = clienteService.findId(idCliente);
        if (empleadoRepository.obtenerEmpleadoJefes(cliente.getIdCliente(), pageable).isEmpty()) {
            throw new ResourceNotFoundException(
                    "El Cliente " + cliente.getRazonSocial() + " no tiene empleados jefes activos.");
        }
        return empleadoRepository.obtenerEmpleadoJefes(idCliente, pageable);
    }
    
    /**
     * @param cliente Id del Cliente al cual se asigna el Departamento.
     * @param jefe Id del Empleado que se pretende asignar como Jefe del Departamento.
     * @return {@code true} Si el Usuario pertenece al Cliente, {@code false} Si no pertenece al Cliente.
     * @author Alejandro Herrera Montilla Metodo que permite validar si el Usuario puede ser Jefe del departamento
     * teniendo en cuenta que pertences al conjunto de empleado del Cliente al cual se esta asignando el Departamento.
     */
    @Deprecated
    public Boolean empleadoJefeValido(Long cliente, Long jefe) {
        if (jefe == null || !existeById(jefe)) {
            throw new ResourceNotFoundException("El Empleado con Id " + jefe + " no existe.");
        }
        if (cliente == null || !clienteService.existeById(cliente)) {
            throw new ResourceNotFoundException("El Cliente con Id " + cliente + " no existe.");
        }
        List<Empleado> empleadoList = empleadoRepository.obtenerEmpleadoXCliente(cliente);
        Empleado empleado = findId(jefe);
        if (empleadoList != null && empleadoList.contains(empleado)) {
            return true;
        }
        return false;
    }
    
    /**
     * @param cliente Id del Cliente al cual se asigna el Departamento.
     * @param jefe Id del Empleado que se pretende asignar como Jefe del Departamento.
     * @return {@code true} Si el Usuario es Jefe, {@code false} Si no es Jefe.
     * @author Alejandro Herrera Montilla Metodo que permite saber si un Empleado es Jefe de algun Departamento.
     */
    public Boolean esEmpleadoJefe(Long cliente, Long jefe) {
        if (jefe == null || !existeById(jefe)) {
            throw new ResourceNotFoundException("El Empleado con Id " + jefe + " no existe.");
        }
        if (cliente == null || !clienteService.existeById(cliente)) {
            throw new ResourceNotFoundException("El Cliente con Id " + cliente + " no existe.");
        }
        Empleado empleado = findId(jefe);
        List<Empleado> empleadoJefe = empleadoRepository.obtenerEmpleadoJefes(cliente);
        if (empleadoJefe.contains(empleado)) {
            return true;
        }
        return false;
    }
    
    /**
     * @param numDocumento Numero de documento del Empleado.
     * @return Registros del Empleado Buscado.
     * @author Alejandro Herrera Montilla Metodo que permite listar los registros de un empleado por su numero de
     * documento.
     */
    public List<Empleado> empleadosXDocumento(String numDocumento) {
        if (numDocumento == null) {
            throw new ResourceNotFoundException("El numero de documento no puede estar vacio.");
        }
        if (empleadoRepository.findAllByNumeroDocumento(numDocumento) == null) {
            throw new ResourceNotFoundException(
                    "El numero de documento indicado no pertenece a Empleados registrados en el sistema.");
        }
        return empleadoRepository.findAllByNumeroDocumento(numDocumento);
    }
    
    /**
     * @param codigo Codigo del Empleado.
     * @return Objeto Empleado.
     * @author Alejandro Herrera Montilla Metodo que permite buscar un empleado por su codigo.
     */
    public Empleado empleadosXCodigo(String codigo) {
        if (codigo == null) {
            throw new ResourceNotFoundException("El numero de codigo no puede estar vacio.");
        }
        if (empleadoRepository.findByCodigoAndActivoTrue(codigo) == null) {
            throw new ResourceNotFoundException(
                    "El numero de codigo indicado no coincide con algún Empleado registrado en el sistema.");
        }
        return empleadoRepository.findByCodigoAndActivoTrue(codigo);
    }
    
    /**
     * @param idCliente Codigo del Empleado.
     * @return Long Cantidad de empleados con el atributo PUEDE_SIMULAR = {@true}.
     * @author Alejandro Herrera Montilla Metodo que permite obtener la cantidad de Empleados actos para flexibizar.
     */
    public Long countEmpleadoFlex(Long idCliente) {
        if (idCliente == null) {
            throw new ResourceNotFoundException("El Cliente con id " + idCliente + " no puede estar vacio.");
        }
        clienteService.existeById(idCliente);
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        List<Cargo> cargoList = cargoRepository.obtenerCargoXCliente(cliente.getIdCliente());
        return empleadoRepository.countEmpleadoByActivoTrueAndPuedeSimularTrueAndCargo(cargoList.iterator().next());
    }
    
    /**
     * @param idCliente Codigo del Empleado.
     * @return Integer Cantidad de empleados Activos.
     * @author Alejandro Herrera Montilla Metodo que permite obtener la cantidad de Empleados Activos que tiene un
     * Cliente.
     */
    public Integer countEmpleadoXCliente(Long idCliente) {
        if (idCliente == null || !clienteService.existeById(idCliente)) {
            throw new ResourceNotFoundException("El Cliente con id " + idCliente + " no existe.");
        }
        return empleadoRepository.obtenerEmpleadoXCliente(idCliente).size();
    }
    
    /**
     * @param key Id del Empleado a validar
     * @return {@true} Si esta Inactivo o {@false} Si esta Activo.
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com Metodo que permite validar si un Empleado esta
     * inactivo.
     */
    public Boolean empleadoInactivo(Long key) {
        if (!empleadoRepository.existsById(key)) {
            throw new ResourceNotFoundException(
                    "El Empleado con id " + key + " no existe como usuario inactivo o activo.");
        }
        return empleadoRepository.findByIdEmpleadoAndActivoFalse(key) != null;
    }
    
    /**
     * @param key Id del Empleado a consultar.
     * @return Objeto Empleado.
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com Metodo que permite obtener el Empleado inactivo apartir
     * de su Id.
     */
    public Empleado optenerEmpleadoInactivo(Long key) {
        if (!empleadoRepository.existsById(key)) {
            throw new ResourceNotFoundException(
                    "El Empleado con id " + key + " no existe como usuario inactivo o activo.");
        }
        return empleadoRepository.findByIdEmpleadoAndActivoFalse(key);
    }
    
    /**
     * @return Objetos Empleado.
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com Metodo que permite Listar todos los Empleados inactivos
     * del sistema.
     */
    public Page<Empleado> optenerEmpleadoInactivos(Pageable pageable) {
        if (empleadoRepository.findAllByActivoFalse(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Empleados inactivos en el sistema.");
        }
        return empleadoRepository.findAllByActivoFalse(pageable);
    }
    
    /**
     * @param key Id del Empleado a activar
     * @param usuarioActiva Id del usuario que realiza la modificación.
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com Metodo que permite activar un Empleado.
     */
    public void activarEmpleado(Long key, Long usuarioActiva) {
        if (!empleadoRepository.existsById(key)) {
            throw new ResourceNotFoundException(
                    "El Empleado con id " + key + " no existe como usuario inactivo o activo.");
        }
        if (!usuarioService.existeById(usuarioActiva)) {
            throw new ResourceNotFoundException(
                    "El Usuario con id " + usuarioActiva + " que realiza la accion de activación no existe.");
        }
        Empleado empleado = empleadoRepository.findByIdEmpleadoAndActivoFalse(key);
        empleado.setActivo(true);
        empleado.setFechaModifica(new Date());
        empleado.setUsuarioModifica(usuarioActiva);
        empleado.setFechaInactivacion(null);
        empleado.setUsuarioInactiva(null);
        //Todo: Enviar notificacion push o correo electronico, notificando el cambio de contrasena, si tiene vinculado un empleado.
        empleadoRepository.save(empleado);
    }
    
    public void save(Empleado empleado) {
        empleadoRepository.save(empleado);
    }
    
    @Override
    public Empleado create(EmpleadoInputDTO entity) throws Exception {
        return create(entity, Collections.EMPTY_LIST);
    }
    
    @Override
    public Empleado update(EmpleadoInputDTO entity) {
        return null;
    }
}
