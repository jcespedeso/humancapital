package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.EmpleadoXDatoAdicionalInputDTO;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.EmpleadoXDatoAdicional;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.DatoAdicionalEmpleadoRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import co.com.humancapital.portal.repository.EmpleadoXDatoAdicionalRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 04/02/2020
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los asignaciones de Datos Adicionales por Empleado.
 */
@Service
public class EmpleadoXDatoAdicionalService implements ServiceInterfaceDto<EmpleadoXDatoAdicional, Long, EmpleadoXDatoAdicionalInputDTO> {

    private final EmpleadoXDatoAdicionalRepository empleadoXDatoAdicionalRepository;

    private final EmpleadoRepository empleadoRepository;

    private final DatoAdicionalEmpleadoRepository datoAdicionalEmpleadoRepository;

    private final EmpleadoService empleadoService;

    private final DatoAdicionalEmpleadoService datoAdicionalEmpleadoService;

    public EmpleadoXDatoAdicionalService(EmpleadoXDatoAdicionalRepository empleadoXDatoAdicionalRepository, EmpleadoRepository empleadoRepository, DatoAdicionalEmpleadoRepository datoAdicionalEmpleadoRepository, EmpleadoService empleadoService, DatoAdicionalEmpleadoService datoAdicionalEmpleadoService) {
        this.empleadoXDatoAdicionalRepository = empleadoXDatoAdicionalRepository;
        this.empleadoRepository = empleadoRepository;
        this.datoAdicionalEmpleadoRepository = datoAdicionalEmpleadoRepository;
        this.empleadoService = empleadoService;
        this.datoAdicionalEmpleadoService = datoAdicionalEmpleadoService;
    }

    @Override
    public EmpleadoXDatoAdicional findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El registro de EmpleadoXDatoAdicional con id "+key+" no existe.");
        }
        return empleadoXDatoAdicionalRepository.findByIdEmpleadoDato(key);
    }

    @Override
    public EmpleadoXDatoAdicional create(EmpleadoXDatoAdicionalInputDTO entity) throws Exception {
        if(entity.getValue() == null) {
            throw new ResourceNotFoundException("El valor del Dato Adicional no puede estar vacio.");
        }
        if(entity.getEmpleado() == null || !empleadoService.existeById(entity.getEmpleado())) {
            throw new ResourceNotFoundException("El empleado no existe o esta vacio.");
        }
        if(entity.getDatoAdicionalEmpleado() == null || !existeById(entity.getDatoAdicionalEmpleado())) {
            throw new ResourceNotFoundException("El Dato Adicional no existe o esta vacio.");
        }

        EmpleadoXDatoAdicional empleadoXDatoAdicional = new EmpleadoXDatoAdicional();
        empleadoXDatoAdicional.setValue(entity.getValue());
        empleadoXDatoAdicional.setEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()));
        empleadoXDatoAdicional.setDatoAdicionalEmpleado(datoAdicionalEmpleadoRepository.findByIdDatoEmpleadoAndActivoTrue(entity.getDatoAdicionalEmpleado()));
        return empleadoXDatoAdicionalRepository.saveAndFlush(empleadoXDatoAdicional);
    }

    @Override
    public EmpleadoXDatoAdicional update(EmpleadoXDatoAdicionalInputDTO entity) throws Exception {
        if(entity.getIdEmpleadoDato() == null || !existeById(entity.getIdEmpleadoDato())) {
            throw new ResourceNotFoundException("El registro de dato adicional por empleado con id No."+entity.getIdEmpleadoDato()+" no existe o esta vacio");
        }
        if(entity.getValue() == null) {
            throw new ResourceNotFoundException("El valor del Dato Adicional no puede estar vacio.");
        }
        if(entity.getEmpleado() == null || !empleadoService.existeById(entity.getEmpleado())) {
            throw new ResourceNotFoundException("El empleado no existe o esta vacio.");
        }
        if(entity.getDatoAdicionalEmpleado() == null || !datoAdicionalEmpleadoService.existeById(entity.getDatoAdicionalEmpleado())) {
            throw new ResourceNotFoundException("El Dato Adicional no existe o esta vacio.");
        }

        EmpleadoXDatoAdicional empleadoXDatoAdicional = findId(entity.getIdEmpleadoDato());
        empleadoXDatoAdicional.setValue(entity.getValue());
        empleadoXDatoAdicional.setEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()));
        empleadoXDatoAdicional.setDatoAdicionalEmpleado(datoAdicionalEmpleadoRepository.findByIdDatoEmpleadoAndActivoTrue(entity.getDatoAdicionalEmpleado()));
        return empleadoXDatoAdicionalRepository.saveAndFlush(empleadoXDatoAdicional);
    }

    public void delete(Long key) throws Exception {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El registro de dato adicional por empleado con id No."+key+" no existe o esta vacio");
        }
        empleadoXDatoAdicionalRepository.delete(findId(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        return empleadoXDatoAdicionalRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de EmpleadoXDatoAdicional por empleado.
     * @param id del Empleado a consultar.
     * @return Lista de EmpleadoXDatoAdicional.
     */
    public Page<EmpleadoXDatoAdicional> findAllEmpleado(Long id, Pageable pageable) throws Exception {
        if(!empleadoService.existeById(id)) {
            throw new ResourceNotFoundException("El Empleado con id " + id +" no existe.");
        }
        Empleado empleado = empleadoRepository.findByIdEmpleadoAndActivoTrue(id);
        if(empleadoXDatoAdicionalRepository.findAllByEmpleado(empleado,pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Empleado con " + id +" no tiene Datos Adicionales Asociados.");
        }
        return empleadoXDatoAdicionalRepository.findAllByEmpleado(empleado,pageable);
    }

    @Override
    public Page<EmpleadoXDatoAdicional> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public EmpleadoXDatoAdicional create(EmpleadoXDatoAdicionalInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public EmpleadoXDatoAdicional update(EmpleadoXDatoAdicionalInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}
