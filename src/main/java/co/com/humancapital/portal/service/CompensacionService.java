package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.CompensacionInputDTO;
import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.CompensacionRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import co.com.humancapital.portal.repository.NivelArlRepository;
import co.com.humancapital.portal.util.CondicionEmpleado;
import co.com.humancapital.portal.util.TipoContrato;
import co.com.humancapital.portal.util.TipoRegimen;
import java.util.Date;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 *
 * que se generen alrededor de la entidad Compensacion.
 */
@Service
@RequiredArgsConstructor
public class CompensacionService implements ServiceInterfaceDto<Compensacion, Long, CompensacionInputDTO> {

    private final CompensacionRepository compensacionRepository;
    private final EmpleadoService empleadoService;
    private final EmpleadoRepository empleadoRepository;
    private final NivelArlRepository nivelArlRepository;
    private final NivelArlService nivelArlService;

    @Override
    public Compensacion findId(Long key) {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Compensación con id "+key+" no existe.");
        }
        return compensacionRepository.findByIdCompensacionAndActivoTrue(key);
    }

    @Override
    public Page<Compensacion> findAll(Pageable pageable) {
        if(compensacionRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Clientes creados en el sistema.");
        }
        return compensacionRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Compensacion create(CompensacionInputDTO entity) {
        //Todo Un Empleado solo puede tener un registro de compensacion (Contrato) activo.
        if (obtenerCompensacionActivaEmpleado(entity.getEmpleado()) != null) {
            throw new ResourceNotFoundException("El Empleado con id "+entity.getEmpleado()+", ya tiene un registro de Compensacion activo.");
        }
        if (entity.getRegimenLaboral() == null || !TipoRegimen.existeKey(entity.getRegimenLaboral())) {
            throw new ResourceNotFoundException("El Regimen Laboral no puede estar vacio o no es un valor permitido en el sistema.");
        }
        if (entity.getCondicionLaboral() == null || !CondicionEmpleado.existeKey(entity.getCondicionLaboral())) {
            throw new ResourceNotFoundException("La Condición Laboral no puede estar vacia o no es un valor permitido en el sistema.");
        }
        if (entity.getTipoContrato() == null || !TipoContrato.existeKey(entity.getTipoContrato())) {
            throw new ResourceNotFoundException("El Tipo de Contrato no puede estar vacio o no es un valor permitido en el sistema.");
        }
        if (entity.getFechaIngreso() == null || entity.getFechaIngreso().toString().trim().isEmpty()) {
            throw new ResourceNotFoundException("La fecha de ingreso de la Compensación a Crear no puede estar vacio.");
        }
        if (TipoContrato.listar().get(new Integer(entity.getTipoContrato())).equals(TipoContrato.TERMINO_FIJO)) {
            if (entity.getFechaTermino() == null || entity.getFechaTermino().toString().trim().isEmpty()) {
                throw new ResourceNotFoundException("La fecha de termino de la Compensación a Crear no puede estar vacia, en razon a que el tipo de contrato es a termino definido.");
            }
            if(entity.getFechaTermino()!= null && entity.getFechaTermino().before(entity.getFechaIngreso()) ){
                throw new ResourceNotFoundException("La fecha de terminación de la Compensación a Crear no puede ser inferior a la de Ingreso.");
            }
        }
        if(entity.getSalario()==null || entity.getSalario()<=0){
            throw new ResourceNotFoundException("El valor Salarial asignado al Empleado, no puede estar vacio, ser igual a cero o menor que cero.");
        }
        if(entity.getEmpleado() == null || !empleadoService.existeById(entity.getEmpleado())){
            throw new ResourceNotFoundException("El Empleado con id "+ entity.getEmpleado() +" al que se quiere asignar el registro, no existe.");
        }
        if(entity.getNivelArl() == null || !nivelArlService.existeById(entity.getNivelArl())){
            throw new ResourceNotFoundException("El Nivel de ARL con id "+ entity.getNivelArl() +" que se quiere asignar al registro, no existe.");
        }
        Compensacion compensacion = new Compensacion();
        //Todo Resgistro de Compensacion  por defecto asume el Tipo de Compensacion "1=TRADICIONAL"
        compensacion.setTipoCompensacion("1");
        compensacion.setRegimenLaboral(entity.getRegimenLaboral());
        compensacion.setCondicionLaboral(entity.getCondicionLaboral());
        compensacion.setTipoContrato(entity.getTipoContrato());
        compensacion.setFechaIngreso(entity.getFechaIngreso());
        if(entity.getFechaTermino() != null){
            compensacion.setFechaTermino(entity.getFechaTermino());
        }
        compensacion.setSalario(entity.getSalario());
        compensacion.setEmpleado(empleadoRepository.findByIdEmpleadoAndActivoTrue(entity.getEmpleado()));
        compensacion.setNivelArl(nivelArlRepository.findByCodigo(entity.getNivelArl()));

        compensacion.setActivo(true);
        compensacion.setFechaCreacion(new Date());
        compensacion.setUsuarioCrea(entity.getUsuarioCrea());
        compensacion.setFechaModifica(new Date());
        compensacion.setUsuarioModifica(entity.getUsuarioCrea());
        return compensacionRepository.saveAndFlush(compensacion);
    }

    @Override
    public Compensacion update(CompensacionInputDTO entity) {
        if(entity.getIdCompensacion() == null || !existeById(entity.getIdCompensacion())) {
            throw new ResourceNotFoundException("La Compensación con id " + entity.getIdCompensacion() +" no existe o esta vacia.");
        }
        if (entity.getRegimenLaboral() == null || !TipoRegimen.existeKey(entity.getRegimenLaboral())) {
            throw new ResourceNotFoundException("El Regimen Laboral no puede estar vacio o no es un valor permitido en el sistema.");
        }
        if (entity.getCondicionLaboral() == null || !CondicionEmpleado.existeKey(entity.getCondicionLaboral())) {
            throw new ResourceNotFoundException("La Condición Laboral no puede estar vacia o no es un valor permitido en el sistema.");
        }
        if (entity.getTipoContrato() == null || !TipoContrato.existeKey(entity.getTipoContrato())) {
            throw new ResourceNotFoundException("El Tipo de Contrato no puede estar vacio o no es un valor permitido en el sistema.");
        }
        if (entity.getFechaIngreso() == null || entity.getFechaIngreso().toString().trim().isEmpty()) {
            throw new ResourceNotFoundException("La fecha de ingreso de la Compensación a Actualizar no puede estar vacia.");
        }
        if(entity.getFechaTermino()!= null && entity.getFechaTermino().before(entity.getFechaIngreso()) ){
            throw new ResourceNotFoundException("La fecha de terminación de la Compensación a Actualizar no puede ser inferior a la de Ingreso.");
        }
        if(entity.getSalario()==null || entity.getSalario()<=0){
            throw new ResourceNotFoundException("El valor Salarial asignado al Empleado, no puede estar vacio, ser igual a cero o menor que cero.");
        }
        if(entity.getNivelArl() == null || !nivelArlService.existeById(entity.getNivelArl())){
            throw new ResourceNotFoundException("El Nivel de ARL con id "+ entity.getNivelArl() +" que se quiere asignar el registro, no existe.");
        }

        Compensacion compensacion = findId(entity.getIdCompensacion());
        compensacion.setRegimenLaboral(entity.getRegimenLaboral());
        compensacion.setCondicionLaboral(entity.getCondicionLaboral());
        compensacion.setTipoContrato(entity.getTipoContrato());
        compensacion.setFechaIngreso(entity.getFechaIngreso());
        if(entity.getFechaTermino() != null){
            compensacion.setFechaTermino(entity.getFechaTermino());
        }
        compensacion.setSalario(entity.getSalario());
        compensacion.setNivelArl(nivelArlRepository.findByCodigo(entity.getNivelArl()));

        compensacion.setFechaModifica(new Date());
        compensacion.setUsuarioModifica(entity.getUsuarioModifica());
        return compensacionRepository.saveAndFlush(compensacion);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("La Compensación con id " + key +" no existe.");
        }
        Compensacion compensacion = findId(key);
        compensacion.setActivo(false);
        compensacion.setFechaInactivacion(new Date());
        compensacion.setUsuarioInactiva(usuarioInactiva);
        //Todo Implementar Regla de Negocio
        //No se puede permitir la inactivacion de una compensacion que tenga un registro de SalarioFlexible en estado diferente de "FINALIZADO"
        compensacionRepository.save(compensacion);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite validar si una Compensacion se encuentra activo en el sistema.
     * @param key Id de la Compensacion a validar.
     * @return {@code true} Si el Compensacion Existe, {@code false} Si no Existe el Compensacion.
     */
    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(compensacionRepository.findByIdCompensacionAndActivoTrue(key)==null){
            return false;
        }
        return compensacionRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar todas las compensaciones laborales que registra un Empleado
     * sin importar si esta activa o no.
     * @param idEmpleado Id del Empleado a consultar.
     * @return Lista de Compensaciones que pueda tener un empleado.
     */
    public Page<Compensacion> findAll(Long idEmpleado,Pageable pageable) {
        if (idEmpleado == null || !empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado a consultar no existe o esta vacio.");
        }
        Empleado empleado = empleadoService.findId(idEmpleado);
        if (compensacionRepository.findAllByEmpleado(empleado,pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Empleado a consultar no tiene compensaciones registradas.");
        }
        return compensacionRepository.findAllByEmpleado(empleado,pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener la Compensación Activa asociada a un Empleado.
     * @param idEmpleado Id del Empleado a validar.
     * @return Objeto Compensación Activo.
     */
    public Compensacion obtenerCompensacionActivaEmpleado(Long idEmpleado) {
        if (idEmpleado == null || !empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado a consultar no existe o esta vacio.");
        }
        Empleado empleado = empleadoService.findId(idEmpleado);
//        if (compensacionRepository.findByEmpleadoAndActivoTrue(empleado) == null) {
//            throw new ResourceNotFoundException("El Empleado a consultar no tiene una compensación salarial Activa.");
//        }
        return compensacionRepository.findByEmpleadoAndActivoTrue(empleado);
    }

    @Override
    public Compensacion create(CompensacionInputDTO entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public Compensacion update(CompensacionInputDTO entity, List<MultipartFile> files) {
        return null;
    }
}
