package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.CargoInputDTO;
import co.com.humancapital.portal.dto.CargoOutputDTO;
import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Departamento;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.OrganizarJerarquiaInputDTO;
import co.com.humancapital.portal.get.repository.CategoriaRepository;
import co.com.humancapital.portal.get.service.CategoriaService;
import co.com.humancapital.portal.repository.CargoRepository;
import co.com.humancapital.portal.repository.DepartamentoRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor del Cargo.
 */
@Service
@RequiredArgsConstructor
public class CargoService implements ServiceInterfaceDto<Cargo, Long, CargoInputDTO>{

    private final CargoRepository cargoRepository;
    private final DepartamentoRepository departamentoRepository;
    private final EmpleadoRepository empleadoRepository;
    private final ClienteService clienteService;
    private final CategoriaService categoriaService;
    private final CategoriaRepository categoriaRepository;
    private final DepartamentoService departamentoService;

    @Override
    public Cargo findId(Long key) {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Cargo con id "+key+" no existe.");
        }
        return cargoRepository.findAllByIdCargoAndActivoTrue(key);
    }

    @Override
    public Page<Cargo> findAll(Pageable pageable) {
        if(cargoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Cargos creados en el sistema.");
        }
        return cargoRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Cargo create(CargoInputDTO entity) {
        if (entity.getCategoria() == null || !categoriaService.existeById(entity.getCategoria())) {
            throw new ResourceNotFoundException("La Categoria asignada al cargo que desea crear, no coincide con las permitidos por el sistema o esta vacio.");
        }
        if (entity.getDepartamento() == null || !departamentoService.existeById(entity.getDepartamento())) {
            throw new ResourceNotFoundException("El Departamento seleccionado no existe.");
        }
        Departamento departamento = departamentoRepository.findByIdDepartamentoAndActivoTrue(entity.getDepartamento());
        for (Cargo cargo: cargoRepository.obtenerCargoXCliente(departamento.getIdDepartamento())) {
            if(cargo.getNombre().trim().toLowerCase().equals(entity.getNombre().trim().toLowerCase())){
                throw new ResourceNotFoundException("El Cargo a Crear, ya existe en el Departamento seleccionado.");
            }
        }
        Cargo cargo = new Cargo();
        cargo.setNombre(entity.getNombre().toUpperCase());
        cargo.setCategoria(categoriaRepository.findByIdCategoriaAndActivoTrue(entity.getCategoria()));
        cargo.setDepartamento(departamento);
        //Todo no debe ser contra el departamento si no contra el cargo padre.
        cargo.setPesoOrden(departamento.getCargoCollection()==null?1:departamento.getCargoCollection().size()+1);
        if(entity.getCargoPadre() != null){
            if(!existeById(entity.getCargoPadre())){
                throw new ResourceNotFoundException("El Cargo padre a asignar no existe.");
            }
            Cargo cargoPadre = cargoRepository.findAllByIdCargoAndActivoTrue(entity.getCargoPadre());
            if (!cargoRepository.findAllByDepartamentoAndActivoTrue(departamento).contains(cargoPadre)) {
                throw new ResourceNotFoundException("El Cargo "+cargoPadre.getIdCargo()+" seleccionado como padre no pertences al Departamento "+entity.getDepartamento());
            }
            cargo.setCargoPadre(cargoPadre);
        }else{
            cargo.setCargoPadre(cargoRepository.findAllByIdCargoAndActivoTrue(departamento.getJefe()));
        }
        cargo.setCargoJefe(entity.isCargoJefe());
        cargo.setActivo(true);
        cargo.setFechaCreacion(new Date());
        cargo.setUsuarioCrea(entity.getUsuarioCrea());
        cargo.setFechaModifica(new Date());
        cargo.setUsuarioModifica(entity.getUsuarioCrea());
        return cargoRepository.saveAndFlush(cargo);
    }

    @Override
    public Cargo update(CargoInputDTO entity) {
        if (entity.getIdCargo() == null || !existeById(entity.getIdCargo())) {
            throw new ResourceNotFoundException("El Cargo con id "+entity.getIdCargo()+" no existe o esta vacio.");
        }
        if (entity.getCategoria() == null || !categoriaService.existeById(entity.getCategoria())) {
            throw new ResourceNotFoundException("La Categoria asignada al Cargo que desea crear, no coincide con las permitidas por el sistema o esta vacio.");
        }
        if (entity.getDepartamento() == null || !departamentoService.existeById(entity.getDepartamento())) {
            throw new ResourceNotFoundException("El Departamento seleccionado no existe.");
        }
        Departamento departamento = departamentoRepository.findByIdDepartamentoAndActivoTrue(entity.getDepartamento());
        Cargo cargo = findId(entity.getIdCargo());
        cargo.setNombre(entity.getNombre().toUpperCase());
        cargo.setCategoria(categoriaRepository.findByIdCategoriaAndActivoTrue(entity.getCategoria()));
        cargo.setDepartamento(departamento);
        if (entity.getPesoOrden()!=null) {
            cargo.setPesoOrden(entity.getPesoOrden());
        }
        if (entity.getCargoPadre()!=null) {
            if(!existeById(entity.getCargoPadre())){
                throw new ResourceNotFoundException("El Cargo padre a asignar no existe.");
            }
            Cargo cargoPadre = cargoRepository.findAllByIdCargoAndActivoTrue(entity.getCargoPadre());
            if (!cargoRepository.findAllByDepartamentoAndActivoTrue(departamento).contains(cargoPadre)) {
                throw new ResourceNotFoundException("El Cargo "+cargoPadre.getIdCargo()+" seleccionado como padre no pertences al Departamento "+entity.getDepartamento());
            }
            cargo.setCargoPadre(cargoPadre);
        }
        cargo.setFechaModifica(new Date());
        cargo.setUsuarioModifica(entity.getUsuarioModifica());
        return cargoRepository.saveAndFlush(cargo);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Cargo con id "+key+" no existe o esta vacio.");
        }
        Cargo cargo = findId(key);
        if (empleadoRepository.findAllByCargoAndActivoTrue(cargo).size()>0) {
            throw new ResourceNotFoundException("El Cargo no puede ser eliminado por que tiene Empleados activos asignados.");
        }
        cargo.setActivo(false);
        cargo.setFechaInactivacion(new Date());
        cargo.setUsuarioInactiva(usuarioInactiva);
        cargoRepository.save(cargo);
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(cargoRepository.findAllByIdCargoAndActivoTrue(key)==null){
            return false;
        }
        return cargoRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los cargo que pertenecen a un Departamento.
     * @param depto Id del Departamento a validar.
     * @return Listado de Cargos pertenecientes al Departamento.
     */
    public Page<Cargo> cargoXDepartamento(Long depto, Pageable pageable) {
        Departamento departamento = departamentoRepository.findByIdDepartamentoAndActivoTrue(depto);
        return cargoRepository.findAllByDepartamentoAndActivoTrue(departamento, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los cargos que pertenecen a un Cliente.
     * @param idCliente Id del Cliente.
     * @return Listado de Cargos pertenecientes al Cliente.
     */
    public Page<Cargo> cargosXCliente(Long idCliente, Pageable pageable) {
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El id del Cliente no puede estar vacio.");
        }
        return cargoRepository.obtenerCargoXCliente(idCliente, pageable);
    }

    public List<Cargo> cargosXCliente(Long idCliente) {
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El id del Cliente no puede estar vacio.");
        }
        return cargoRepository.obtenerCargoXCliente(idCliente);
    }

    /**
     * Metodo que permite validar si el cargo pertenece a un Cliente.
     * @param cliente Id del Cliente.
     * @param idCargo Id del Cargo a validar.
     * @return {@code true} Si el cargo pertenece al Cliente, {@code false} Si no pertenece al Cliente.
     */
    public Boolean validarCargo(Long cliente, Long idCargo) {
        List<Cargo> cargoList = cargoRepository.obtenerCargoXCliente(cliente);
        Cargo cargo = findId(idCargo);
        if(cargoList.contains(cargo)){
            return true;
        }
        return false;
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 28/03/2020
     * Metodo que permite modelar la estructura jerarquica de la organizacion a nivel de los Cargos.
     * @param inicio Id del Cargo desde el cual se desea desplegar la organización jerarquica.
     * @param nivel Determina el nivel maximo al caul se desea visualizar donde cero permite visualizar todo.
     * @return Lista de datos de los cargos organizados jerarquicamente.
     */
    public List<Map<String, Map<String,Object>>> verEstructuraJerarquica(String inicio, int nivel){
        if (inicio == null || !existeById(new Long(inicio))) {
            throw new ResourceNotFoundException("El Cargo con id " + inicio + " no exite.");
        }
        List<Map<String,Map<String,Object>>> jerarquia = new ArrayList<>();
        for (Object[] list:cargoRepository.organigramaCargos(inicio,nivel)) {
            if(cargoRepository.findAllByIdCargoAndActivoTrue(new Long(list[0].toString()))!=null){
                Map<String, Map<String,Object>> data = new LinkedHashMap<>();
                Map<String,Object> dataCargo = new LinkedHashMap<>();
                Cargo cargo = cargoRepository.findAllByIdCargoAndActivoTrue(new Long(list[0].toString()));
                dataCargo.put("idCargo",cargo.getIdCargo());
                dataCargo.put("nombre",cargo.getNombre());
                dataCargo.put("nivel",list[2]);
                dataCargo.put("branch",list[3]);
                data.put("cargo",dataCargo);
                Map<String,Object> dataCargoPadre = new LinkedHashMap<>();
                Cargo cargoPadre = cargoRepository.findAllByIdCargoAndActivoTrue(new Long(String.valueOf(list[1]!=null?list[1].toString():0)));
                if(cargoPadre != null){
                    dataCargoPadre.put("idCargo",cargoPadre.getIdCargo());
                    dataCargoPadre.put("nombre",cargoPadre.getNombre());
                } else if(cargo.getCargoPadre()!=null){
                    dataCargoPadre.put("idCargo",cargo.getCargoPadre().getIdCargo());
                    dataCargoPadre.put("nombre",cargo.getCargoPadre().getNombre());
                }
                data.put("padre",dataCargoPadre);
                jerarquia.add(data);
            }
        }
        return jerarquia;
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 28/03/2020
     * Metodo que permite obtener los departamentos hijos de primer nivel de un Departamento.
     * @param idPadre Id del Departamento desde el cual se desea desplegar la organización jerarquica.
     * @return Lista de DepartamentoOutputDTO con la data de los Departamentos Hijos.
     */
    public List<CargoOutputDTO> findAllCargoHijo(Long idPadre) {
        List<CargoOutputDTO> cargoOutputDTOS = new ArrayList<>();
        for (Cargo cargo: cargoRepository.obtenerCargoHijo(idPadre)) {
            cargoOutputDTOS.add(CargoOutputDTO.getDTO(cargo));
        }
        return cargoOutputDTOS;
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 29/03/2020
     * Metodo que permite actualizar la organización jerarquica del Cargo.
     * @param entity Objeto OrganizarJerarquiaInputDTO con la data a actualizar.
     */
    public void actualizarOrganizacion(OrganizarJerarquiaInputDTO entity){
        if (entity.getIdEntiy() == null || !existeById(entity.getIdEntiy())) {
            throw new ResourceNotFoundException("El Cargo con id " + entity.getIdEntiy() + " no exite.");
        }
        if (entity.getIdEntityPadre() == null || !existeById(entity.getIdEntityPadre())) {
            throw new ResourceNotFoundException("El Cargo padre con id " + entity.getIdEntityPadre() + " no exite.");
        }
        Cargo cargo = cargoRepository.findAllByIdCargoAndActivoTrue(entity.getIdEntiy());
        cargo.setCargoPadre(cargoRepository.findAllByIdCargoAndActivoTrue(entity.getIdEntityPadre()));
        cargo.setPesoOrden(entity.getPesoOrden());
        cargo.setFechaModifica(new Date());
        cargo.setUsuarioModifica(entity.getUsuarioModifica());
        cargoRepository.save(cargo);
    }

    @Override
    public Cargo create(CargoInputDTO entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public Cargo update(CargoInputDTO entity, List<MultipartFile> files) {
        return null;
    }
}