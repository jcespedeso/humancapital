package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.ProductoInputDTO;
import co.com.humancapital.portal.entity.*;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ProductoRepository;
import co.com.humancapital.portal.repository.ProveedorRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor del Producto.
 */
@Service
public class ProductoService implements ServiceInterfaceDto<Producto, Long, ProductoInputDTO> {

    private final ProductoRepository productoRepository;
    private final ProveedorRepository proveedorRepository;
    private final ProveedorService proveedorService;
    private final ModuloService moduloService;
    private final ClienteService clienteService;
    private final EmpleadoService empleadoService;
    private final SalarioFlexibleXProductoService salarioFlexibleXProductoService;

    public ProductoService(ProductoRepository productoRepository, ProveedorRepository proveedorRepository, ProveedorService proveedorService, ModuloService moduloService, @Lazy ClienteService clienteService, EmpleadoService empleadoService, @Lazy SalarioFlexibleXProductoService salarioFlexibleXProductoService) {
        this.productoRepository = productoRepository;
        this.proveedorRepository = proveedorRepository;
        this.proveedorService = proveedorService;
        this.moduloService = moduloService;
        this.clienteService = clienteService;
        this.empleadoService = empleadoService;
        this.salarioFlexibleXProductoService = salarioFlexibleXProductoService;
    }

    @Override
    public Producto findId(Long key) {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Producto con id "+key+" no existe.");
        }
        return productoRepository.findByIdProductoAndActivoTrue(key);
    }

    @Override
    public Page<Producto> findAll(Pageable pageable) {
        if(productoRepository.findAll(pageable).isEmpty()){
            throw new ResourceNotFoundException("No existen productos creados en el sistema.");
        }
        return productoRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Producto create(ProductoInputDTO entity) {
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El nombre del producto a crear, no puede estar vacio.");
        }
        if(entity.getNaturaleza() == null || entity.getNaturaleza().trim().isEmpty()){
            throw new ResourceNotFoundException("La naturaleza del producto a crear, no puede estar vacia.");
        }
        if(entity.getParametro() == null || entity.getParametro().trim().isEmpty()){
            throw new ResourceNotFoundException("El parametro del producto a crear, no puede estar vacio.");
        }
        if(entity.getBeneficioTributario() == null || entity.getBeneficioTributario().trim().isEmpty()){
            throw new ResourceNotFoundException("El beneficio tributario del producto a crear, no puede estar vacio.");
        }
        if(entity.getParametroBt() == null || entity.getParametroBt().trim().isEmpty()){
            throw new ResourceNotFoundException("El parametro de beneficio tributario del producto a crear, no puede estar vacio.");
        }
        if(entity.getMonto() == null || entity.getMonto().intValue()<0){
            throw new ResourceNotFoundException("El monto no puede ser negativo o estar vacio.");
        }
        if(entity.getProveedor() == null || !proveedorService.existeById(entity.getProveedor())){
            throw new ResourceNotFoundException("El Proveedor con id "+ entity.getProveedor()+" no existe.");
        }
        Producto producto = new Producto();
        producto.setNombre(entity.getNombre().toUpperCase());
        producto.setNaturaleza(entity.getNaturaleza().toUpperCase());
        producto.setSeguridadSocial(entity.getSeguridadSocial());
        producto.setIngresoFiscal(entity.getIngresoFiscal());
        producto.setParametro(entity.getParametro().toUpperCase());
        producto.setBeneficioTributario(entity.getBeneficioTributario().toUpperCase());
        producto.setParametroBt(entity.getParametroBt().toUpperCase());
        producto.setMonto(entity.getMonto());
        if(!entity.getDescripcion().trim().isEmpty()){
            producto.setDescripcion(entity.getDescripcion());
        }
        if(entity.getProveedor()!=null){
            producto.setProveedor(proveedorRepository.findByIdProveedorAndActivoTrue(entity.getProveedor()));
        }

        producto.setActivo(true);
        producto.setFechaCreacion(new Date());
        producto.setFechaModifica(new Date());
        producto.setUsuarioCrea(entity.getUsuarioCrea());
        producto.setUsuarioModifica(entity.getUsuarioCrea());
        return productoRepository.saveAndFlush(producto);
    }

    @Override
    public Producto update(ProductoInputDTO entity) {
        if(entity.getIdProducto() == null || existeById(entity.getIdProducto())) {
            throw new ResourceNotFoundException("El Producto con Id "+entity.getIdProducto()+" no existe.");
        }
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El nombre del producto a Actualizar, no puede estar vacio.");
        }
        if(entity.getNaturaleza() == null || entity.getNaturaleza().trim().isEmpty()){
            throw new ResourceNotFoundException("La naturaleza del producto a Actualizar, no puede estar vacia.");
        }
        if(entity.getParametro() == null || entity.getParametro().trim().isEmpty()){
            throw new ResourceNotFoundException("El parametro del producto a Actualizar, no puede estar vacio.");
        }
        if(entity.getBeneficioTributario() == null || entity.getBeneficioTributario().trim().isEmpty()){
            throw new ResourceNotFoundException("El beneficio tributario del producto a Actualizar, no puede estar vacio.");
        }
        if(entity.getParametroBt() == null || entity.getParametroBt().trim().isEmpty()){
            throw new ResourceNotFoundException("El parametro de beneficio tributario del producto a Actualizar, no puede estar vacio.");
        }
        if(entity.getMonto() == null || entity.getMonto().intValue()<0){
            throw new ResourceNotFoundException("El monto no puede ser negativo o estar vacio.");
        }
        if(entity.getProveedor() == null || !proveedorService.existeById(entity.getProveedor())){
            throw new ResourceNotFoundException("El Proveedor con id "+ entity.getProveedor()+" no existe.");
        }

        Producto producto = findId(entity.getIdProducto());
        producto.setNombre(entity.getNombre().toUpperCase());
        producto.setNaturaleza(entity.getNaturaleza().toUpperCase());
        producto.setSeguridadSocial(entity.getSeguridadSocial());
        producto.setIngresoFiscal(entity.getIngresoFiscal());
        producto.setParametro(entity.getParametro().toUpperCase());
        producto.setBeneficioTributario(entity.getBeneficioTributario().toUpperCase());
        producto.setParametroBt(entity.getParametroBt().toUpperCase());
        producto.setMonto(entity.getMonto());
        if(!entity.getDescripcion().trim().isEmpty()){
            producto.setDescripcion(entity.getDescripcion());
        }
        if(entity.getProveedor()!=null){
            producto.setProveedor(proveedorRepository.findByIdProveedorAndActivoTrue(entity.getProveedor()));
        }

        producto.setFechaModifica(new Date());
        producto.setUsuarioModifica(entity.getUsuarioModifica());
        return productoRepository.saveAndFlush(producto);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) {
        if(key == null || existeById(key)) {
            throw new ResourceNotFoundException("El Producto con Id "+key+" no existe.");
        }
        Producto producto = findId(key);
        producto.setActivo(false);
        producto.setFechaInactivacion(new Date());
        producto.setUsuarioInactiva(usuarioInactiva);
        productoRepository.save(producto);
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(productoRepository.findByIdProductoAndActivoTrue(key)==null){
            return false;
        }
        return productoRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los productos de un proveedor especifico.
     * @param idProveedor Id del proveedor a consultar.
     * @return Lista de Productos del Proveedor.
     */
    public Page<Producto> productoXProveedor(Long idProveedor, Pageable pageable) {
        if(idProveedor == null || !proveedorService.existeById(idProveedor)){
            throw new ResourceNotFoundException("El Proveedor con id "+ idProveedor +" no existe.");
        }
        Proveedor proveedor = proveedorRepository.findByIdProveedorAndActivoTrue(idProveedor);
        if(productoRepository.findAllByProveedorAndActivoTrue(proveedor,pageable).isEmpty()){
            throw new ResourceNotFoundException("El Proveedor "+ proveedor.getRazonSocial() +" no cuenta con productos asignados.");
        }
        return productoRepository.findAllByProveedorAndActivoTrue(proveedor,pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los productos asignados a un Medulo especifico.
     * @param idModulo Id del Modulo a consultar.
     * @return Lista de Productos.
     */
    public Page<Producto> productoXModulo(Long idModulo, Pageable pageable) throws Exception {
        if(idModulo == null || !moduloService.existeById(idModulo)){
            throw new ResourceNotFoundException("El Modulo con id "+ idModulo +" no existe.");
        }
        Modulo modulo = moduloService.findId(idModulo);
        if(productoRepository.findAllByModuloCollectionAndActivoTrue(modulo,pageable).isEmpty()){
            throw new ResourceNotFoundException("El Modulo con id "+ idModulo +" no cuenta con productos asignados.");
        }
        return productoRepository.findAllByModuloCollectionAndActivoTrue(modulo,pageable);
    }

    public List<Producto> productoXModulo(Long idModulo) {
        if(idModulo == null || !moduloService.existeById(idModulo)){
            throw new ResourceNotFoundException("El Modulo con id "+ idModulo +" no existe.");
        }
        Modulo modulo = moduloService.findId(idModulo);
        if(productoRepository.obtenerProductoXModulo(modulo.getIdModulo()).isEmpty()){
            throw new ResourceNotFoundException("El Modulo "+ modulo.getNombre() +" no cuenta con productos asignados.");
        }
        return productoRepository.obtenerProductoXModulo(modulo.getIdModulo());
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los productos asignados a un Cliente especifico.
     * @param idCliente Id del Cliente a consultar.
     * @return Lista de Productos.
     */
    public Page<Producto> productoXCliente(Long idCliente, Pageable pageable) {
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Cliente con id "+ idCliente +" no existe.");
        }
        Cliente cliente = clienteService.findId(idCliente);
        if(productoRepository.findAllByClienteCollectionAndActivoTrue(cliente,pageable).isEmpty()){
            throw new ResourceNotFoundException("El Cliente "+ cliente.getRazonSocial() +" no cuenta con productos asignados.");
        }
        return productoRepository.findAllByClienteCollectionAndActivoTrue(clienteService.findId(idCliente),pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los productos asignados a un Empleado especifico.
     * @param idEmpleado Id del Empleado a consultar.
     * @return Lista de Productos.
     */
    public Page<Producto> productoXEmpleado(Long idEmpleado, Pageable pageable) throws Exception {
        Empleado empleado = empleadoService.findId(idEmpleado);
        List<Producto> productos = new ArrayList<>();
        for (SalarioFlexibleXProducto list:salarioFlexibleXProductoService.findAllEmpleado(empleado.getIdEmpleado()) ) {
                productos.add(findId(list.getProducto().getIdProducto()));
        }
        return new PageImpl<>(productos);
    }

    @Override
    public Producto create(ProductoInputDTO entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public Producto update(ProductoInputDTO entity, List<MultipartFile> files) {
        return null;
    }
}
