package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.DescuentoInputDTO;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.CompensacionXDescuento;
import co.com.humancapital.portal.entity.Descuento;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.CompensacionXDescuentoRepository;
import co.com.humancapital.portal.repository.DescuentoRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 13/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los Descuentos que se le pueden aplicar a un empleado.
 */
@Service
public class DescuentoService implements ServiceInterfaceDto<Descuento, Long, DescuentoInputDTO>{

    private final DescuentoRepository descuentoRepository;

    private final ClienteRepository clienteRepository;

    private final ClienteService clienteService;

    private final EmpleadoRepository empleadoRepository;

    private final CompensacionXDescuentoRepository compensacionXDescuentoRepository;

    public DescuentoService(DescuentoRepository descuentoRepository, ClienteRepository clienteRepository, ClienteService clienteService, EmpleadoRepository empleadoRepository, CompensacionXDescuentoRepository compensacionXDescuentoRepository) {
        this.descuentoRepository = descuentoRepository;
        this.clienteRepository = clienteRepository;
        this.clienteService = clienteService;
        this.empleadoRepository = empleadoRepository;
        this.compensacionXDescuentoRepository = compensacionXDescuentoRepository;
    }

    @Override
    public Descuento findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Descuento con id "+key+" no existe.");
        }
        return descuentoRepository.findByIdDescuentoAndActivoTrue(key);
    }

    @Override
    public Page<Descuento> findAll(Pageable pageable) throws Exception {
        if(descuentoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Descuentos creados en el sistema.");
        }
        return descuentoRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Descuento create(DescuentoInputDTO entity) throws Exception {
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El Nombre(s) del Descuento a Crear no puede estar vacio.");
        }
        if(entity.getValorPorDefecto() == null){
            entity.setValorPorDefecto(0.0);
        }
        if(entity.getValorPorDefecto()<0){
            throw new ResourceNotFoundException("El valor por defecto no puede ser negativo.");
        }
        if(entity.getCliente() == null || !clienteService.existeById(entity.getCliente())){
            throw new ResourceNotFoundException("El Cliente seleccionado no existe o no se envio el dato.");
        }

        Descuento descuento = new Descuento();
        descuento.setNombre(entity.getNombre().toUpperCase());
        descuento.setValorPorDefecto(entity.getValorPorDefecto()==null?0:entity.getValorPorDefecto());
        descuento.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        descuento.setActivo(true);
        descuento.setFechaCreacion(new Date());
        descuento.setUsuarioCrea(entity.getUsuarioCrea());
        descuento.setFechaModifica(new Date());
        descuento.setUsuarioModifica(entity.getUsuarioCrea());
        return descuentoRepository.saveAndFlush(descuento);
    }

    @Override
    public Descuento update(DescuentoInputDTO entity) throws Exception {
        if(entity.getIdDescuento() == null || !existeById(entity.getIdDescuento())){
            throw new ResourceNotFoundException("El Descuento a Actualizar no existe.");
        }
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El Nombre(s) del Descuento a Actualizar no puede estar vacio.");
        }
        if(entity.getValorPorDefecto() == null){
            entity.setValorPorDefecto(0.0);
        }
        if(entity.getValorPorDefecto()<0){
            throw new ResourceNotFoundException("El valor por defecto no puede ser negativo.");
        }
        if(entity.getCliente() == null || !clienteService.existeById(entity.getCliente())){
            throw new ResourceNotFoundException("El Cliente seleccionado no existe o no se envio el dato.");
        }

        Descuento descuento = findId(entity.getIdDescuento());
        descuento.setNombre(entity.getNombre().toUpperCase());
        descuento.setValorPorDefecto(entity.getValorPorDefecto());
        descuento.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        descuento.setFechaModifica(new Date());
        descuento.setUsuarioModifica(entity.getUsuarioModifica());
        return descuentoRepository.saveAndFlush(descuento);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Descuento con id " + key +" no existe.");
        }
        for (Empleado empleado:empleadoRepository.findAllByActivoTrue()) {
            for (CompensacionXDescuento compDesc :compensacionXDescuentoRepository.obtenerDescuentoXEmpleado(empleado.getIdEmpleado())) {
                if(compDesc.getDescuento().getIdDescuento().equals(key)){
                    throw new ResourceNotFoundException("El Descuento no puede ser eliminado, por que tiene asociado un Empleado activo.");
                }
            }
        }

        Descuento descuento= descuentoRepository.findByIdDescuentoAndActivoTrue(key);
        descuento.setActivo(false);
        descuento.setFechaInactivacion(new Date());
        descuento.setUsuarioInactiva(usuarioInactiva);
        descuentoRepository.save(descuento);
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(descuentoRepository.findByIdDescuentoAndActivoTrue(key)==null){
            return false;
        }
        return descuentoRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar todos los descuentos activos que tiene un Cliente para sus empleados.
     * @param idCliente Id del cliente a validar.
     * @return List de Descuentos disponibles.
     */
    public Page<Descuento> findAllByCliente(Long idCliente, Pageable pageable) throws Exception{
        if(clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El cliente con id "+idCliente+" no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if(descuentoRepository.findAllByClienteAndActivoTrue(cliente, pageable).isEmpty()){
            throw new ResourceNotFoundException("El cliente "+cliente.getRazonSocial()+" no tiene descuentos disponibles.");
        }
        return descuentoRepository.findAllByClienteAndActivoTrue(cliente, pageable);
    }

    /**
     * Metodo que permite validar si el Descuento pertenece a un Cliente.
     * @param idCliente Id del Cliente.
     * @param idDescuento Id del Descuento a validar.
     * @return {@code true} Si el Descuento pertenece al Cliente, {@code false} Si no pertenece al Cliente.
     */
    public Boolean validarDescuentoCliente(Long idCliente, Long idDescuento) throws Exception{
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe.");
        }
        if(idDescuento == null || !existeById(idDescuento)){
            throw new ResourceNotFoundException("El Descuento con id "+idDescuento+" no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        List<Descuento> descuentoList = descuentoRepository.findAllByClienteAndActivoTrue(cliente);
        Descuento descuento = findId(idDescuento);
        if(descuentoList.contains(descuento)){
            return true;
        }
        return false;
    }

    @Override
    public Descuento create(DescuentoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public Descuento update(DescuentoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
