package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.SalarioFlexibleInputDTO;
import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.SalarioFlexible;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.CompensacionRepository;
import co.com.humancapital.portal.repository.SalarioFlexibleRepository;
import co.com.humancapital.portal.util.EstadoSimulacion;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de SalarioFlexible.
 */
@Service
public class SalarioFlexibleService implements ServiceInterfaceDto<SalarioFlexible, Long, SalarioFlexibleInputDTO> {

    DecimalFormat formDecimal = new DecimalFormat("#.##");

    private final SalarioFlexibleRepository salarioFlexibleRepository;

    private final CompensacionRepository compensacionRepository;

    private final CompensacionService compensacionService;

    private final ClienteService clienteService;

    public SalarioFlexibleService(SalarioFlexibleRepository salarioFlexibleRepository, CompensacionRepository compensacionRepository, CompensacionService compensacionService, ClienteService clienteService) {
        this.salarioFlexibleRepository = salarioFlexibleRepository;
        this.compensacionRepository = compensacionRepository;
        this.compensacionService = compensacionService;
        this.clienteService = clienteService;
    }

    @Override
    public SalarioFlexible findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El registro de SalarioFlexible con id "+key+" no existe.");
        }
        return salarioFlexibleRepository.findByIdSalarioFlexible(key);
    }

    @Override
    public SalarioFlexible create(SalarioFlexibleInputDTO entity) throws Exception {
        if(entity.getCompensacion() == null || !compensacionService.existeById(entity.getCompensacion())){
            throw new ResourceNotFoundException("La Compensación a asociar, no puede estar vacia o no existe.");
        }
        SalarioFlexible salarioFlexibleActivo = salarioFlexibleXCompensacion(entity.getCompensacion());
        if(salarioFlexibleActivo !=null){
            throw new ResourceNotFoundException("El Empleado con compensación No."+ entity.getCompensacion() +", ya tiene un registro de flexibilización en estado "+
                    EstadoSimulacion.listar().get(new Integer(salarioFlexibleActivo.getEstadoSimulacion())));
        }
        if(entity.getPorcentajeFlexible()==null || entity.getPorcentajeFlexible()<=0){
            throw new ResourceNotFoundException("El porcentaje de flexibilización, no puede estar vacio, ser igual a cero o menor que cero.");
        }
        if(entity.getPorcentajeFlexible()>40){
            throw new ResourceNotFoundException("El porcentaje de flexibilización, no puede ser superior al 40%.");
        }
        if(entity.getValorPlus()==null || entity.getValorPlus()<=0){
            throw new ResourceNotFoundException("El valor plus, no puede estar vacio, ni ser menor que cero.");
        }
        if(entity.getAhorroEmpleado()==null){
            throw new ResourceNotFoundException("No se indico la cantidad de ahorro del empleado.");
        }
        if(entity.getAhorroEmpresa()==null){
            throw new ResourceNotFoundException("No se indico la cantidad de ahorro de la empresa.");
        }
        if(entity.getEstadoSimulacion()==null || !EstadoSimulacion.existeKey(entity.getEstadoSimulacion())){
            throw new ResourceNotFoundException("El estado de la flexibilización, esta vacio o no corresponde con los permitidos por el sistema.");
        }

        Compensacion compensacion = compensacionRepository.findByIdCompensacion(entity.getCompensacion());
        SalarioFlexible salarioFlexible = new SalarioFlexible();
        salarioFlexible.setPorcentajeFlexible(entity.getPorcentajeFlexible());
        salarioFlexible.setValorPlus(entity.getValorPlus());
        salarioFlexible.setValorSalarioFlex(((compensacion.getSalario()*entity.getPorcentajeFlexible())/100));
        salarioFlexible.setAhorroEmpleado(entity.getAhorroEmpleado());
        salarioFlexible.setAhorroEmpresa(entity.getAhorroEmpresa());
        salarioFlexible.setEstadoSimulacion(entity.getEstadoSimulacion());
        if(entity.getObservacion() != null){
            salarioFlexible.setObservacion(entity.getObservacion());
        }else{
            salarioFlexible.setObservacion("No aplica");
        }
        salarioFlexible.setCompensacion(compensacion);
        if(EstadoSimulacion.listar().get(new Integer(entity.getEstadoSimulacion())).equals(EstadoSimulacion.SIMULADA)){
            compensacion.setTipoCompensacion("2");
            compensacionRepository.save(compensacion);
        }
        if(EstadoSimulacion.listar().get(new Integer(entity.getEstadoSimulacion())).equals(EstadoSimulacion.CONFIRMADA)){
            compensacion.setTipoCompensacion("2");
            compensacionRepository.save(compensacion);
        }

        salarioFlexible.setFechaCreacion(new Date());
        salarioFlexible.setUsuarioCrea(entity.getUsuarioCrea());
        salarioFlexible.setFechaModifica(new Date());
        salarioFlexible.setUsuarioModifica(entity.getUsuarioCrea());
        return salarioFlexibleRepository.saveAndFlush(salarioFlexible);
    }

    @Override
    public SalarioFlexible update(SalarioFlexibleInputDTO entity) throws Exception {
        if(entity.getIdSalarioFlexible()==null || !existeById(entity.getIdSalarioFlexible())) {
            throw new ResourceNotFoundException("El SalarioFlexible con id " + entity.getIdSalarioFlexible() +" no existe o esta vacio.");
        }
        if(entity.getPorcentajeFlexible()==null || entity.getPorcentajeFlexible()<=0){
            throw new ResourceNotFoundException("El porcentaje de flexibilización, no puede estar vacio, ser igual a cero o menor que cero.");
        }
        if(entity.getPorcentajeFlexible()>40){
            throw new ResourceNotFoundException("El porcentaje de flexibilización, no puede ser superior al 40%.");
        }
        if(entity.getValorPlus()==null || entity.getValorPlus()<=0){
            throw new ResourceNotFoundException("El valor plus, no puede estar vacio, ni ser menor que cero.");
        }
        if(entity.getAhorroEmpleado()==null){
            throw new ResourceNotFoundException("No se indico la cantidad de ahorro del empleado.");
        }
        if(entity.getAhorroEmpresa()==null){
            throw new ResourceNotFoundException("No se indico la cantidad de ahorro de la empresa.");
        }
        if(entity.getEstadoSimulacion()==null || !EstadoSimulacion.existeKey(entity.getEstadoSimulacion())){
            throw new ResourceNotFoundException("El estado de la flexibilización, esta vacio o no corresponde con los permitidos por el sistema.");
        }

        SalarioFlexible salarioFlexible = findId(entity.getIdSalarioFlexible());
        Compensacion compensacion = compensacionRepository.findByIdCompensacion(salarioFlexible.getCompensacion().getIdCompensacion());
        salarioFlexible.setPorcentajeFlexible(entity.getPorcentajeFlexible());
        salarioFlexible.setValorPlus(entity.getValorPlus());
        salarioFlexible.setValorSalarioFlex(((compensacion.getSalario()*entity.getPorcentajeFlexible())/100));
        salarioFlexible.setAhorroEmpleado(entity.getAhorroEmpleado());
        salarioFlexible.setAhorroEmpresa(entity.getAhorroEmpresa());
        salarioFlexible.setEstadoSimulacion(entity.getEstadoSimulacion());
        if(entity.getObservacion() != null){
            salarioFlexible.setObservacion(entity.getObservacion());
        }
        if(EstadoSimulacion.listar().get(new Integer(entity.getEstadoSimulacion())).equals(EstadoSimulacion.CONFIRMADA)){
            compensacion.setTipoCompensacion("2");
            compensacionRepository.save(compensacion);
        }
        if(EstadoSimulacion.listar().get(new Integer(entity.getEstadoSimulacion())).equals(EstadoSimulacion.VALIDADA)){
            compensacion.setTipoCompensacion("2");
            compensacionRepository.save(compensacion);
        }
        if(EstadoSimulacion.listar().get(new Integer(entity.getEstadoSimulacion())).equals(EstadoSimulacion.DESCARTADA)){
            compensacion.setTipoCompensacion("1");
            compensacionRepository.save(compensacion);
        }
        if(EstadoSimulacion.listar().get(new Integer(entity.getEstadoSimulacion())).equals(EstadoSimulacion.RECHAZADA_AUDITORIA)){
            compensacion.setTipoCompensacion("1");
            compensacionRepository.save(compensacion);
        }
        if(EstadoSimulacion.listar().get(new Integer(entity.getEstadoSimulacion())).equals(EstadoSimulacion.FINALIZADA)){
            compensacion.setTipoCompensacion("1");
            compensacionRepository.save(compensacion);
        }

        salarioFlexible.setFechaModifica(new Date());
        salarioFlexible.setUsuarioModifica(entity.getUsuarioModifica());
        return salarioFlexibleRepository.saveAndFlush(salarioFlexible);
    }

    public void finalizacion(SalarioFlexibleInputDTO entity) throws Exception {
        if(entity.getIdSalarioFlexible() == null || !existeById(entity.getIdSalarioFlexible())) {
            throw new ResourceNotFoundException("El SalarioFlexible con id " + entity.getIdSalarioFlexible() +" no existe.");
        }
        if(entity.getObservacion() == null || entity.getObservacion().trim().isEmpty()) {
            throw new ResourceNotFoundException("La finalización de la flexibilización salarial debe tener una justificación y/o observacion que la sustente.");
        }

        SalarioFlexible salarioFlexible = findId(entity.getIdSalarioFlexible());
        salarioFlexible.setEstadoSimulacion("6");
        salarioFlexible.setObservacion(entity.getObservacion());

        Compensacion compensacion = compensacionRepository.findByIdCompensacion(salarioFlexible.getCompensacion().getIdCompensacion());
        compensacion.setTipoCompensacion("1");
        compensacionRepository.save(compensacion);

        salarioFlexible.setFechaInactivacion(new Date());
        salarioFlexible.setUsuarioInactiva(entity.getUsuarioInactiva());
        salarioFlexibleRepository.save(salarioFlexible);
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(salarioFlexibleRepository.findByIdSalarioFlexible(key)==null){
            return false;
        }
        return salarioFlexibleRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite buscar un registro de SalarioFlexible por Compensación que se encuentre en estado 1=Simulada o 2=Confirmada o 3=Validada.
     * @param idCompensacion Id de la compensacion a validar.
     * @return Objeto SalarioFlexible.
     */
    public SalarioFlexible salarioFlexibleXCompensacion(Long idCompensacion) throws Exception {
        if(idCompensacion == null || !compensacionService.existeById(idCompensacion)){
            throw new ResourceNotFoundException("La Compensación con Id."+ idCompensacion +", no existe.");
        }
        return salarioFlexibleRepository.findByCompensacionEstadosIniciales(compensacionRepository.findByIdCompensacion(idCompensacion).getIdCompensacion());
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los registro de SalarioFlexible por Compensación y un estado especifico.
     * @param key Id de la compensacion a validar.
     * @param estado Id del estado de la simulación a validar.
     * @return Objeto SalarioFlexible.
     */
    public List<SalarioFlexible> salarioFlexibleXCompensacionYEstado(Long key, String estado) throws Exception {
        if(key == null || !compensacionService.existeById(key)){
            throw new ResourceNotFoundException("La Compensación con Id."+ key +", no existe.");
        }
        if(estado == null || !EstadoSimulacion.existeKey(estado)){
            throw new ResourceNotFoundException("El estado de la flexibilización, esta vacio o no corresponde con los permitidos por el sistema.");
        }
        Compensacion compensacion = compensacionRepository.findByIdCompensacion(key);
        if(salarioFlexibleRepository.findAllByCompensacionAndEstadoSimulacion(compensacion,estado).isEmpty()) {
            throw new ResourceNotFoundException("No hay registros de flexibilizaciones salariales por los criterios de busqueda suministrados.");
        }
        return salarioFlexibleRepository.findAllByCompensacionAndEstadoSimulacion(compensacion, estado);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite contar los registros de SalarioFlexible en un estado especifico.
     * @param idCliente Id del cliente a validar.
     * @param estado Id del estado de la simulación a validar.
     * @return Objeto SalarioFlexible.
     */
    public BigInteger countEstadoFlex(Long idCliente, String estado) throws Exception {
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe.");
        }
        if(estado == null || !EstadoSimulacion.existeKey(estado)){
            throw new ResourceNotFoundException("El estado de la flexibilización, esta vacio o no corresponde con los permitidos por el sistema.");
        }
        Map<String,BigInteger> countSal = salarioFlexibleRepository.contarFlexibilizacionesPorEstado(idCliente,estado);
        for (Map.Entry<String,BigInteger> entry : countSal.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
        return countSal.get("count");
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el valor Maximo, Minimo y el Promedio del porcentaje de flexibilización aplicado por los empleado de un Cliente.
     * Reglas: El Empleado.Activo={@true}, SalarioFlexible.Estado={@3-VALIDADA}
     * @param idCliente del Cliente a consultar.
     * @return List de Valor con key maximo, minimo y promedio.
     */
    public List<?> obtenerMaxMinAvgPorcentejeFlex(Long idCliente) throws Exception {
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe.");
        }
        if(salarioFlexibleRepository.obtenerMaxMinAvgPorcentajeFlex(idCliente).isEmpty()){
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no tiene empleados con datos de flexibilización, para realizar los calculos.");
        }
        List<Map<String,Object>> cuentaProducto = new ArrayList<>();
        for (Object[] datos : salarioFlexibleRepository.obtenerMaxMinAvgPorcentajeFlex(idCliente)) {
            Map<String, Object> data = new LinkedHashMap<>();
            data.put("maximo",datos[0]==null?0:datos[0]);
            data.put("minimo",datos[1]==null?0:datos[1]);
            data.put("promedio",datos[2]==null?0:formDecimal.format(new Double(datos[2].toString())));
            cuentaProducto.add(data);
        }
        return cuentaProducto;
    }

    @Override
    public Page<SalarioFlexible> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public SalarioFlexible create(SalarioFlexibleInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public SalarioFlexible update(SalarioFlexibleInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }
}
