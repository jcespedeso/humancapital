package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.DatoAdicionalEmpInputDTO;
import co.com.humancapital.portal.entity.DatoAdicionalEmpleado;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.DatoAdicionalEmpleadoRepository;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 04/02/2020
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los Datos Adicionales del Empleado.
 */
@Service
public class DatoAdicionalEmpleadoService implements ServiceInterfaceDto<DatoAdicionalEmpleado, Long, DatoAdicionalEmpInputDTO> {

    private final DatoAdicionalEmpleadoRepository datoAdicionalEmpleadoRepository;

    public DatoAdicionalEmpleadoService(DatoAdicionalEmpleadoRepository datoAdicionalEmpleadoRepository) {
        this.datoAdicionalEmpleadoRepository = datoAdicionalEmpleadoRepository;
    }

    @Override
    public DatoAdicionalEmpleado findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Dato Adicional con id "+key+" no existe.");
        }
        return datoAdicionalEmpleadoRepository.findByIdDatoEmpleadoAndActivoTrue(key);
    }

    @Override
    public Page<DatoAdicionalEmpleado> findAll(Pageable pageable) throws Exception {
        if(datoAdicionalEmpleadoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Datos Adicionales creados en el sistema.");
        }
        return datoAdicionalEmpleadoRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public DatoAdicionalEmpleado create(DatoAdicionalEmpInputDTO entity) throws Exception {
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El Nombre(s) del Dato Adicional a Crear no puede estar vacio.");
        }

        DatoAdicionalEmpleado datoAdicionalEmpleado = new DatoAdicionalEmpleado();
        datoAdicionalEmpleado.setNombre(entity.getNombre().toUpperCase());
        datoAdicionalEmpleado.setActivo(true);
        datoAdicionalEmpleado.setFechaCreacion(new Date());
        datoAdicionalEmpleado.setUsuarioCrea(entity.getUsuarioCrea());
        datoAdicionalEmpleado.setFechaModifica(new Date());
        datoAdicionalEmpleado.setUsuarioModifica(entity.getUsuarioCrea());
        return datoAdicionalEmpleadoRepository.saveAndFlush(datoAdicionalEmpleado);
    }

    @Override
    public DatoAdicionalEmpleado update(DatoAdicionalEmpInputDTO entity) throws Exception {
        if(entity.getIdDatoEmpleado() == null || !existeById(entity.getIdDatoEmpleado())){
            throw new ResourceNotFoundException("El Dato Adicional a Actualizar no existe.");
        }
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El Nombre(s) del Dato Adicional a Actualizar no puede estar vacio.");
        }

        DatoAdicionalEmpleado datoAdicionalEmpleado = findId(entity.getIdDatoEmpleado());
        datoAdicionalEmpleado.setNombre(entity.getNombre().toUpperCase());
        datoAdicionalEmpleado.setFechaModifica(new Date());
        datoAdicionalEmpleado.setUsuarioModifica(entity.getUsuarioModifica());
        return datoAdicionalEmpleadoRepository.saveAndFlush(datoAdicionalEmpleado);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Dato Adicional con id " + key +" no existe.");
        }
        DatoAdicionalEmpleado datoAdicionalEmpleado = datoAdicionalEmpleadoRepository.findByIdDatoEmpleadoAndActivoTrue(key);
        datoAdicionalEmpleado.setActivo(false);
        datoAdicionalEmpleado.setFechaInactivacion(new Date());
        datoAdicionalEmpleado.setUsuarioInactiva(usuarioInactiva);
        datoAdicionalEmpleadoRepository.save(datoAdicionalEmpleado);
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(datoAdicionalEmpleadoRepository.findByIdDatoEmpleadoAndActivoTrue(key)==null){
            return false;
        }
        return datoAdicionalEmpleadoRepository.existsById(key);
    }

    @Override
    public DatoAdicionalEmpleado create(DatoAdicionalEmpInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public DatoAdicionalEmpleado update(DatoAdicionalEmpInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
