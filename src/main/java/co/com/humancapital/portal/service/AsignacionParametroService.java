package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.AsignacionParametroInputDTO;
import co.com.humancapital.portal.entity.AsignacionParametro;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Parametro;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.AsignacionParametroRepository;
import co.com.humancapital.portal.repository.ParametroRepository;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de la AsignacionParametro.
 */
@Service
public class AsignacionParametroService implements ServiceInterfaceDto<AsignacionParametro, Long, AsignacionParametroInputDTO>{

    private final AsignacionParametroRepository asignacionParametroRepository;

    private final ParametroRepository parametroRepository;

    private final ClienteService clienteService;

    private final ParametroService parametroService;

    public AsignacionParametroService(AsignacionParametroRepository asignacionParametroRepository, ParametroRepository parametroRepository, ClienteService clienteService, ParametroService parametroService) {
        this.asignacionParametroRepository = asignacionParametroRepository;
        this.parametroRepository = parametroRepository;
        this.clienteService = clienteService;
        this.parametroService = parametroService;
    }

    @Override
    public AsignacionParametro findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El registro de asignacion de parametro con id "+key+" no existe.");
        }
        return asignacionParametroRepository.findByIdAsignacionAndActivoTrue(key);
    }

    @Override
    public Page<AsignacionParametro> findAll(Pageable pageable) throws Exception {
        if(asignacionParametroRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Asignaciones de Parametros creados en el sistema.");
        }
        return asignacionParametroRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public AsignacionParametro create(AsignacionParametroInputDTO entity) throws Exception {
        if(entity.getCliente() == null || !clienteService.existeById(entity.getCliente())){
            throw new ResourceNotFoundException("El cliente con id"+entity.getCliente()+" no existe.");
        }
        if(entity.getParametro() == null || !parametroService.existeById(entity.getParametro())){
            throw new ResourceNotFoundException("El Parametro con id"+entity.getParametro()+" no existe.");
        }
        for (AsignacionParametro asignacion : asignacionParametroRepository.findAllByClienteAndActivoTrue(clienteService.findId(entity.getCliente()))) {
            if(asignacion.getParametro().getIdParametro().equals(entity.getParametro())){
                throw new ResourceNotFoundException("El Parametro con id "+entity.getParametro()+" ya se encuentra asignado al cliente seleccionado.");
            }
        }
        Parametro parametro = parametroService.findId(entity.getParametro());
        if(parametro.getNormativo()){
            entity.setValor(parametro.getValor());
            entity.setUnidadMedida(parametro.getUnidadMedida());
        }else{
            if(entity.getValor() == null || entity.getValor().trim().isEmpty()){
                throw new ResourceNotFoundException("El valor no puede estar vacio.");
            }
            if(entity.getUnidadMedida() == null || entity.getUnidadMedida().trim().isEmpty()){
                throw new ResourceNotFoundException("La Unidad de medidad no puede estar vacia.");
            }
        }

        AsignacionParametro asignacionParametro = new AsignacionParametro();
        asignacionParametro.setValor(entity.getValor().toUpperCase());
        asignacionParametro.setUnidadMedida(entity.getUnidadMedida().toUpperCase());
        asignacionParametro.setCliente(clienteService.findId(entity.getCliente()));
        asignacionParametro.setParametro(parametroRepository.findByIdParametroAndActivoTrue(entity.getParametro()));

        asignacionParametro.setActivo(true);
        asignacionParametro.setFechaCreacion(new Date());
        asignacionParametro.setUsuarioCrea(entity.getUsuarioCrea());
        asignacionParametro.setFechaModifica(new Date());
        asignacionParametro.setUsuarioModifica(entity.getUsuarioCrea());
        return asignacionParametroRepository.saveAndFlush(asignacionParametro);
    }

    @Override
    public AsignacionParametro update(AsignacionParametroInputDTO entity) throws Exception {
        if(entity.getIdAsignacion() == null || !existeById(entity.getIdAsignacion())){
            throw new ResourceNotFoundException("La asignación con id "+entity.getIdAsignacion()+" no existe.");
        }
        if(entity.getCliente() == null || !clienteService.existeById(entity.getCliente())){
            throw new ResourceNotFoundException("El cliente con id"+entity.getCliente()+" no existe.");
        }
        if(entity.getParametro() == null || !parametroService.existeById(entity.getParametro())){
            throw new ResourceNotFoundException("El Parametro con id"+entity.getParametro()+" no existe.");
        }
        Parametro parametro = parametroService.findId(entity.getParametro());
        if(parametro.getNormativo()){
            entity.setValor(parametro.getValor());
            entity.setUnidadMedida(parametro.getUnidadMedida());
        }else{
            if(entity.getValor() == null || entity.getValor().trim().isEmpty()){
                throw new ResourceNotFoundException("El valor no puede estar vacio.");
            }
            if(entity.getUnidadMedida() == null || entity.getUnidadMedida().trim().isEmpty()){
                throw new ResourceNotFoundException("La Unidad de medidad no puede estar vacia.");
            }
        }

        AsignacionParametro asignacionParametro = asignacionParametroRepository.findByIdAsignacionAndActivoTrue(entity.getIdAsignacion());
        asignacionParametro.setValor(entity.getValor().toUpperCase());
        asignacionParametro.setUnidadMedida(entity.getUnidadMedida().toUpperCase());
        asignacionParametro.setCliente(clienteService.findId(entity.getCliente()));
        asignacionParametro.setParametro(parametroRepository.findByIdParametroAndActivoTrue(entity.getParametro()));

        asignacionParametro.setFechaModifica(new Date());
        asignacionParametro.setUsuarioModifica(entity.getUsuarioModifica());
        return asignacionParametroRepository.saveAndFlush(asignacionParametro);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La asignación con id "+key+" no existe.");
        }
        AsignacionParametro asignaParametro = asignacionParametroRepository.findByIdAsignacionAndActivoTrue(key);
        asignaParametro.setActivo(false);
        asignaParametro.setFechaInactivacion(new Date());
        asignaParametro.setUsuarioInactiva(usuarioInactiva);
        asignacionParametroRepository.saveAndFlush(asignaParametro);

    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(asignacionParametroRepository.findByIdAsignacionAndActivoTrue(key)==null){
            return false;
        }
        return asignacionParametroRepository.existsById(key);
    }

    /**
     * @autor Alejandor Herrera Montilla
     * Metodo que permite obtener los Parametros asignados a un Cliente especifico.
     * @param idCliente Id del Cliente a validar.
     * @return List Objeto AsignacionParametro.
     */
    public Page<AsignacionParametro> obtenerAsignacionesXCliente(Long idCliente, Pageable pageable) throws Exception {
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El cliente con id"+idCliente+" no existe.");
        }
        Cliente cliente = clienteService.findId(idCliente);
        if(asignacionParametroRepository.findAllByClienteAndActivoTrue(cliente,pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Asignaciones de Parametros creados para el cliente indicado.");
        }
        return asignacionParametroRepository.findAllByClienteAndActivoTrue(cliente,pageable);
    }

    @Override
    public AsignacionParametro create(AsignacionParametroInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public AsignacionParametro update(AsignacionParametroInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
