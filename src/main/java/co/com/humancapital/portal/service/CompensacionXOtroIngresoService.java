package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.CompensacionXOtroIngresoInputDTO;
import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.CompensacionXOtroIngreso;
import co.com.humancapital.portal.entity.OtroIngreso;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.CompensacionRepository;
import co.com.humancapital.portal.repository.CompensacionXOtroIngresoRepository;
import co.com.humancapital.portal.repository.OtroIngresoRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 19/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los CompensacionXOtroIngreso.
 */
@Service
public class CompensacionXOtroIngresoService implements ServiceInterfaceDto<CompensacionXOtroIngreso,Long, CompensacionXOtroIngresoInputDTO>{

    private final CompensacionXOtroIngresoRepository compensacionXOtroIngresoRepository;

    private final OtroIngresoRepository otroIngresoRepository;

    private final CompensacionRepository compensacionRepository;

    private final CompensacionService compensacionService;

    private final OtroIngresoService otroIngresoService;

    private final EmpleadoService empleadoService;

    public CompensacionXOtroIngresoService(CompensacionXOtroIngresoRepository compensacionXOtroIngresoRepository, OtroIngresoRepository otroIngresoRepository, CompensacionRepository compensacionRepository, CompensacionService compensacionService, OtroIngresoService otroIngresoService, EmpleadoService empleadoService) {
        this.compensacionXOtroIngresoRepository = compensacionXOtroIngresoRepository;
        this.otroIngresoRepository = otroIngresoRepository;
        this.compensacionRepository = compensacionRepository;
        this.compensacionService = compensacionService;
        this.otroIngresoService = otroIngresoService;
        this.empleadoService = empleadoService;
    }

    @Override
    public CompensacionXOtroIngreso findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El registro de CompensacionXOtroIngreso con id "+key+" no existe.");
        }
        return compensacionXOtroIngresoRepository.findByIdCompOtroIngreso(key);
    }

    @Override
    public CompensacionXOtroIngreso create(CompensacionXOtroIngresoInputDTO entity) throws Exception {
        if(entity.getValor() == null) {
            throw new ResourceNotFoundException("El valor del ingreso no puede estar vacio.");
        }
        if(entity.getValor()<0){
            throw new ResourceNotFoundException("El valor del ingreso no puede ser negativo.");
        }
        if(entity.getCompensacion() == null || !compensacionService.existeById(entity.getCompensacion())) {
            throw new ResourceNotFoundException("La compensación no existe o esta vacia.");
        }
        if(entity.getOtroIngreso() == null || !otroIngresoService.existeById(entity.getOtroIngreso())) {
            throw new ResourceNotFoundException("El ingreso no existe o esta vacio.");
        }
        CompensacionXOtroIngreso compOtroIngreso = new CompensacionXOtroIngreso();
        compOtroIngreso.setValor(entity.getValor());
        compOtroIngreso.setCompensacion(compensacionRepository.findByIdCompensacion(entity.getCompensacion()));
        compOtroIngreso.setOtroIngreso(otroIngresoRepository.findAllByIdOtroIngresoAndActivoTrue(entity.getOtroIngreso()));
        return compensacionXOtroIngresoRepository.saveAndFlush(compOtroIngreso);
    }

    @Override
    public CompensacionXOtroIngreso update(CompensacionXOtroIngresoInputDTO entity) throws Exception {
        if(entity.getIdCompOtroIngreso() == null || !existeById(entity.getIdCompOtroIngreso())) {
            throw new ResourceNotFoundException("El registro de compensacion por ingreso con id No. ."+entity.getIdCompOtroIngreso()+" no existe o esta vacio");
        }
        if(entity.getValor() == null) {
            throw new ResourceNotFoundException("El valor del ingreso no puede estar vacio.");
        }
        if(entity.getValor()<0){
            throw new ResourceNotFoundException("El valor del ingreso no puede ser negativo.");
        }
        if(entity.getCompensacion() == null || !compensacionService.existeById(entity.getCompensacion())) {
            throw new ResourceNotFoundException("La compensación no existe o esta vacia.");
        }
        if(entity.getOtroIngreso() == null || !otroIngresoService.existeById(entity.getOtroIngreso())) {
            throw new ResourceNotFoundException("El ingreso no existe o esta vacio.");
        }
        CompensacionXOtroIngreso compOtroIngreso = findId(entity.getIdCompOtroIngreso());
        compOtroIngreso.setValor(entity.getValor());
        compOtroIngreso.setCompensacion(compensacionRepository.findByIdCompensacion(entity.getCompensacion()));
        compOtroIngreso.setOtroIngreso(otroIngresoRepository.findAllByIdOtroIngresoAndActivoTrue(entity.getOtroIngreso()));
        return compensacionXOtroIngresoRepository.saveAndFlush(compOtroIngreso);
    }

    public void delete(Long key) throws Exception{
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El registro de compensacion por ingreso con id No. ."+key+" no existe o esta vacio");
        }
        compensacionXOtroIngresoRepository.delete(findId(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        return otroIngresoRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de CompensacionXOtroIngreso asociados a un empleado, por intermedio de su respectiva compensacion.
     * @param idCompensacion de la Compensación activa del empleado.
     * @return Lista de CompensacionXOtroIngreso del Empleado.
     */
    public Page<CompensacionXOtroIngreso> findAllCompensacion(Long idCompensacion, Pageable pageable) throws Exception {
        if(idCompensacion == null || !compensacionService.existeById(idCompensacion)) {
            throw new ResourceNotFoundException("La Compensación con id " + idCompensacion +" no existe.");
        }
        Compensacion compensacion = compensacionRepository.findByIdCompensacion(idCompensacion);
        if(compensacionXOtroIngresoRepository.findAllByCompensacion(compensacion, pageable).isEmpty()){
            throw new ResourceNotFoundException("No existen registros de Otros Ingresos por la compensación indicada.");
        }
        return compensacionXOtroIngresoRepository.findAllByCompensacion(compensacion, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de CompensacionXOtroIngreso por Otros ingresos.
     * @param idOtroIngreso del Otro Ingreso.
     * @return Lista de CompensacionXOtroIngreso.
     */
    public Page<CompensacionXOtroIngreso> findAllOtroIngreso(Long idOtroIngreso, Pageable pageable) throws Exception {
        if(idOtroIngreso == null || !otroIngresoService.existeById(idOtroIngreso)){
            throw new ResourceNotFoundException("El Ingreso con id No."+idOtroIngreso+" no existe.");
        }
        OtroIngreso otroIngreso = otroIngresoRepository.findAllByIdOtroIngresoAndActivoTrue(idOtroIngreso);
        if(compensacionXOtroIngresoRepository.findAllByOtroIngreso(otroIngreso, pageable).isEmpty()){
            throw new ResourceNotFoundException("No existen registros de Otros Ingresos por el descuento indicado.");
        }
        return compensacionXOtroIngresoRepository.findAllByOtroIngreso(otroIngreso, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de los OtrosIngreso asignados a un Empleado.
     * @param idEmpleado del Empleado a consultar.
     * @return Lista de CompensacionXOtroIngreso.
     */
    public Page<CompensacionXOtroIngreso> otroIngresoXEmpleado(Long idEmpleado, Pageable pageable) throws Exception {
        if(!empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no existe.");
        }
        if(compensacionXOtroIngresoRepository.obtenerOtroIngresoXEmpleado(idEmpleado, pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no tiene Otros Ingresos Asociados.");
        }
        return compensacionXOtroIngresoRepository.obtenerOtroIngresoXEmpleado(idEmpleado, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite calcular el valor total de los OtroIngreso que tiene asignado un Empleado.
     * @param idEmpleado Id del Empleado a validar.
     * @return Valor Total OtroIngreso asignados al Empleado.
     */
    public Double totalOtroIngresoXEmpleado(Long idEmpleado) throws Exception {
        if(!empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no existe.");
        }
        Double totalOtroIngreso = compensacionXOtroIngresoRepository.obtenerTotalOtroIngresoXEmpleado(idEmpleado);
        if(totalOtroIngreso == null || totalOtroIngreso <=0){
            totalOtroIngreso = (double) 0;
        }
        return totalOtroIngreso;
    }

    @Override
    public Page<CompensacionXOtroIngreso> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }

    @Override
    public CompensacionXOtroIngreso create(CompensacionXOtroIngresoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public CompensacionXOtroIngreso update(CompensacionXOtroIngresoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
