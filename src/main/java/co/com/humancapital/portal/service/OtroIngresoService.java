package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.OtroIngresoInputDTO;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.CompensacionXOtroIngreso;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.OtroIngreso;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.CompensacionXOtroIngresoRepository;
import co.com.humancapital.portal.repository.EmpleadoRepository;
import co.com.humancapital.portal.repository.OtroIngresoRepository;
import co.com.humancapital.portal.repository.TipoOtroIngresoRepository;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los OtroIngreso.
 */
@Service
public class OtroIngresoService implements ServiceInterfaceDto<OtroIngreso, Long, OtroIngresoInputDTO> {

    private final OtroIngresoRepository otroIngresoRepository;

    private final ClienteRepository clienteRepository;

    private final EmpleadoRepository empleadoRepository;

    private final TipoOtroIngresoRepository tipoOtroIngresoRepository;

    private final ClienteService clienteService;

    private final TipoOtroIngresoService tipoOtroIngresoService;

    private final CompensacionXOtroIngresoRepository compensacionXOtroIngresoRepository;

    public OtroIngresoService(OtroIngresoRepository otroIngresoRepository, ClienteRepository clienteRepository, EmpleadoRepository empleadoRepository, TipoOtroIngresoRepository tipoOtroIngresoRepository, ClienteService clienteService, TipoOtroIngresoService tipoOtroIngresoService, CompensacionXOtroIngresoRepository compensacionXOtroIngresoRepository) {
        this.otroIngresoRepository = otroIngresoRepository;
        this.clienteRepository = clienteRepository;
        this.empleadoRepository = empleadoRepository;
        this.tipoOtroIngresoRepository = tipoOtroIngresoRepository;
        this.clienteService = clienteService;
        this.tipoOtroIngresoService = tipoOtroIngresoService;
        this.compensacionXOtroIngresoRepository = compensacionXOtroIngresoRepository;
    }

    @Override
    public OtroIngreso findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El OtroIngreso con id "+key+" no existe.");
        }
        return otroIngresoRepository.findAllByIdOtroIngresoAndActivoTrue(key);
    }

    @Override
    public Page<OtroIngreso> findAll(Pageable pageable) throws Exception {
        if(otroIngresoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen OtrosIngresos creados en el sistema.");
        }
        return otroIngresoRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public OtroIngreso create(OtroIngresoInputDTO entity) throws Exception {
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El Nombre(s) del Ingreso a Crear no puede estar vacio.");
        }
        if(entity.getValorPorDefecto()<0){
            throw new ResourceNotFoundException("El valor por defecto no puede ser negativo.");
        }
        if(entity.getCliente() == null || !clienteService.existeById(entity.getCliente())){
            throw new ResourceNotFoundException("El Cliente asignado al Ingreso a Crear, no puede estar vacio o no existe.");
        }
        if(entity.getTipoOtroIngreso() == null || !tipoOtroIngresoService.existeById(entity.getTipoOtroIngreso())){
            throw new ResourceNotFoundException("El Tipo de Ingreso asignada al Ingreso a crear, no puede estar vacio o no existe.");
        }

        OtroIngreso otroIngreso = new OtroIngreso();
        otroIngreso.setNombre(entity.getNombre().toUpperCase());
        otroIngreso.setValorPorDefecto(entity.getValorPorDefecto());
        otroIngreso.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        otroIngreso.setTipoOtroIngreso(tipoOtroIngresoRepository.findAllByIdTipoOtroIngresoAndActivoTrue(entity.getTipoOtroIngreso()));
        otroIngreso.setActivo(true);
        otroIngreso.setFechaCreacion(new Date());
        otroIngreso.setUsuarioCrea(entity.getUsuarioCrea());
        otroIngreso.setFechaModifica(new Date());
        otroIngreso.setUsuarioModifica(entity.getUsuarioCrea());
        return otroIngresoRepository.saveAndFlush(otroIngreso);
    }

    @Override
    public OtroIngreso update(OtroIngresoInputDTO entity) throws Exception {
        if(entity.getIdOtroIngreso() == null ||!existeById(entity.getIdOtroIngreso())) {
            throw new ResourceNotFoundException("El Ingreso con id " + entity.getIdOtroIngreso() +" no existe.");
        }
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El Nombre(s) del Ingreso a Actualizar no puede estar vacio.");
        }
        if(entity.getValorPorDefecto()<0){
            throw new ResourceNotFoundException("El valor por defecto no puede ser negativo.");
        }
        if(entity.getCliente() == null || !clienteService.existeById(entity.getCliente())){
            throw new ResourceNotFoundException("El Cliente asignado al Ingreso a Actualizar, no puede estar vacio o no existe.");
        }
        if(entity.getTipoOtroIngreso() == null || !tipoOtroIngresoService.existeById(entity.getTipoOtroIngreso())){
            throw new ResourceNotFoundException("El Tipo de Ingreso asignada al Ingreso a Actualizar, no puede estar vacio o no existe.");
        }

        OtroIngreso otroIngreso = findId(entity.getIdOtroIngreso());
        otroIngreso.setNombre(entity.getNombre().toUpperCase());
        otroIngreso.setValorPorDefecto(entity.getValorPorDefecto());
        otroIngreso.setCliente(clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente()));
        otroIngreso.setTipoOtroIngreso(tipoOtroIngresoRepository.findAllByIdTipoOtroIngresoAndActivoTrue(entity.getTipoOtroIngreso()));
        otroIngreso.setFechaModifica(new Date());
        otroIngreso.setUsuarioModifica(entity.getUsuarioModifica());
        return otroIngresoRepository.saveAndFlush(otroIngreso);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception{
        if(key==null || !existeById(key)){
            throw new ResourceNotFoundException("El OtroIngreso con Id "+key+" no existe.");
        }
        for (Empleado empleado:empleadoRepository.findAllByActivoTrue()) {
            for (CompensacionXOtroIngreso compIng :compensacionXOtroIngresoRepository.obtenerOtroIngresoXEmpleado(empleado.getIdEmpleado())) {
                if(compIng.getOtroIngreso().getIdOtroIngreso().equals(key)){
                    throw new ResourceNotFoundException("El Descuento no puede ser eliminado, por que tiene asociado un Empleado activo.");
                }
            }
        }

        OtroIngreso otroIngreso = findId(key);
        otroIngreso.setActivo(false);
        otroIngreso.setFechaInactivacion(new Date());
        otroIngreso.setUsuarioInactiva(usuarioInactiva);
        otroIngresoRepository.save(otroIngreso);
    }

    @Override
    public Boolean existeById(Long key) throws Exception{
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(otroIngresoRepository.findAllByIdOtroIngresoAndActivoTrue(key)==null){
            return false;
        }
        return otroIngresoRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los OtroIngresos que pertenecen a un Cliente.
     * @param idCliente Objeto Cliente.
     * @return Listado de OtroIngreso pertenecientes al Cliente.
     */
    public Page<OtroIngreso> findAllByCliente(Long idCliente, Pageable pageable) throws Exception {
        if(clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El cliente con id "+idCliente+" no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if(otroIngresoRepository.findAllByClienteAndActivoTrue(cliente, pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen OtrosIngresos creados en el sistema para este cliente.");
        }
        return otroIngresoRepository.findAllByClienteAndActivoTrue(cliente, pageable);
    }

    /**
     * Metodo que permite validar si el OtroIngreso pertenece a un Cliente.
     * @param idCliente Id del Cliente.
     * @param idIgreso Id del OtroIngreso a validar.
     * @return {@code true} Si el OtroIngreso pertenece al Cliente, {@code false} Si no pertenece al Cliente.
     */
    public Boolean validarOtroIngresoCliente(Long idCliente, Long idIgreso) throws Exception{
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Empleado con id "+idCliente+" no existe.");
        }
        if(idIgreso == null || !existeById(idIgreso)){
            throw new ResourceNotFoundException("El OtroIngreso con id "+idIgreso+" no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        List<OtroIngreso> otroIngresoList = otroIngresoRepository.findAllByClienteAndActivoTrue(cliente);
        OtroIngreso otroIngreso = findId(idIgreso);
        if(otroIngresoList.contains(otroIngreso)){
            return true;
        }
        return false;
    }

    @Override
    public OtroIngreso create(OtroIngresoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public OtroIngreso update(OtroIngresoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
