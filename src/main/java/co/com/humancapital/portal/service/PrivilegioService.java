package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.PrivilegioInputDTO;
import co.com.humancapital.portal.entity.Privilegio;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.PrivilegioRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 26/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de Privilegios de un Usuario.
 */
@Service
public class PrivilegioService implements ServiceInterfaceDto<Privilegio, String, PrivilegioInputDTO>{

    private final PrivilegioRepository privilegioRepository;

    private  final UsuarioService usuarioService;

    public PrivilegioService(PrivilegioRepository privilegioRepository, UsuarioService usuarioService) {
        this.privilegioRepository = privilegioRepository;
        this.usuarioService = usuarioService;
    }

    @Override
    public Privilegio findId(String key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Privilegio con id "+key+" no existe.");
        }
        return privilegioRepository.findByCodigoPrivilegioEquals(key);
    }

    @Override
    public Page<Privilegio> findAll(Pageable pageable) throws Exception {
        if(privilegioRepository.findByOrderByCategoriaAscNombrePrivilegioAsc(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Privilegios creados en el sistema.");
        }
        return privilegioRepository.findByOrderByCategoriaAscNombrePrivilegioAsc(pageable);
    }

    @Override
    public Boolean existeById(String key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(privilegioRepository.findByCodigoPrivilegioEquals(key)==null){
            return false;
        }
        return privilegioRepository.existsById(key);
    }

    /**
     * Metodo que permite obtener los Privilegios de un Usuario mediante su Username.
     * @param username Nombre de Usuario a consultar.
     * @return Listado Objeto Privilegio
     */
    public Page<Privilegio> findPrivilegiosForUsername(String username, Pageable pageable) throws Exception{
        if(!usuarioService.existeByUsername(username)){
            throw new ResourceNotFoundException("No existen un Usuario asociado al username indicado.");
        }
        return privilegioRepository.findPrivilegiosForUsername(username,pageable);
    }

    @Override
    public Privilegio create(PrivilegioInputDTO entity) throws Exception {
        return null;
    }

    @Override
    public Privilegio create(PrivilegioInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public Privilegio update(PrivilegioInputDTO entity) throws Exception {
        return null;
    }

    @Override
    public Privilegio update(PrivilegioInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public void delete(String key, Long usuarioInactiva) throws Exception {

    }
}
