package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.ModuloInputDTO;
import co.com.humancapital.portal.dto.ModuloOutputDTO;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Modulo;
import co.com.humancapital.portal.entity.Producto;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.ModuloRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.humancapital.portal.repository.ProductoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor del Modulo.
 */
@Service
@RequiredArgsConstructor
public class ModuloService implements ServiceInterfaceDto<Modulo, Long, ModuloInputDTO>{

    private final ModuloRepository moduloRepository;
    private final ProductoRepository productoRepository;
    private final ClienteRepository clienteRepository;

    @Override
    public Modulo findId(Long key) {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Modulo con id " + key +", no existe.");
        }
        return moduloRepository.findByIdModuloAndActivoTrue(key);
    }

    @Override
    public Page<Modulo> findAll(Pageable pageable) {
        if(moduloRepository.findAll(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Modulos creados en el sistema.");
        }
        return moduloRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Modulo create(ModuloInputDTO entity) {
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El Nombre del Modulo a Crear, no puede estar vacio.");
        }
        if(entity.getDescripcion() == null || entity.getDescripcion().trim().isEmpty()){
            throw new ResourceNotFoundException("La Descripción del Modulo a Crear, no puede estar vacia.");
        }

        Modulo modulo = new Modulo();
        modulo.setNombre(entity.getNombre().toUpperCase());
        modulo.setDescripcion(entity.getDescripcion());

        modulo.setActivo(true);
        modulo.setFechaCreacion(new Date());
        modulo.setUsuarioCrea(entity.getUsuarioCrea());
        modulo.setFechaModifica(new Date());
        modulo.setUsuarioModifica(entity.getUsuarioCrea());
        return moduloRepository.saveAndFlush(modulo);
    }

    @Override
    public Modulo update(ModuloInputDTO entity) {
        if(entity.getIdModulo() == null || !existeById(entity.getIdModulo())) {
            throw new ResourceNotFoundException("El Modulo con id " + entity.getIdModulo() +", no existe o no se envio el dato.");
        }
        existeById(entity.getIdModulo());
        if(entity.getNombre() == null || entity.getNombre().trim().isEmpty()){
            throw new ResourceNotFoundException("El Nombre del Modulo a Crear, no puede estar vacio.");
        }
        if(entity.getDescripcion() == null || entity.getDescripcion().trim().isEmpty()){
            throw new ResourceNotFoundException("La Descripción del Modulo a Crear, no puede estar vacia.");
        }
        Modulo modulo = findId(entity.getIdModulo());
        modulo.setNombre(entity.getNombre().toUpperCase());
        modulo.setDescripcion(entity.getDescripcion());

        modulo.setFechaModifica(new Date());
        modulo.setUsuarioModifica(entity.getUsuarioModifica());
        return moduloRepository.saveAndFlush(modulo);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Modulo con id " + key +", no existe o no se envio el dato.");
        }
        Modulo modulo = findId(key);
        modulo.setActivo(false);
        modulo.setFechaInactivacion(new Date());
        modulo.setUsuarioInactiva(usuarioInactiva);
        moduloRepository.save(modulo);
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(moduloRepository.findByIdModuloAndActivoTrue(key)==null){
            return false;
        }
        return moduloRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite asignar uno o muchos Productos a un Modulo.
     * @param entity Objeto Modulo
     * @return Objeto Modulo
     */
    public Modulo asignarProducto(ModuloInputDTO entity) {
        if(entity.getIdModulo() == null ) {
            throw new ResourceNotFoundException("El Modulo con id " + entity.getIdModulo() +", no existe o no se envio el dato.");
        }
        existeById(entity.getIdModulo());
        if(entity.getProductoCollection() == null || entity.getProductoCollection().size()==0) {
            throw new ResourceNotFoundException("No se han seleccionado Productos para asignar al Modulo.");
        }
        for (Producto producto:entity.getProductoCollection()) {
            if(producto == null || !productoRepository.existsByIdProductoAndActivoTrue(producto.getIdProducto())) {
                throw new ResourceNotFoundException("El Producto con id " + producto +", no existe.");
            }
        }
        Modulo modulo = moduloRepository.findByIdModuloAndActivoTrue(entity.getIdModulo());
        modulo.setProductoCollection(entity.getProductoCollection());
        return moduloRepository.saveAndFlush(modulo);
    }

    public Page<ModuloOutputDTO> findAllCliente(Long idCliente) {
        if(idCliente == null || !clienteRepository.existsByIdClienteAndActivoTrue(idCliente)) {
            throw new ResourceNotFoundException("El Cliente con id "+idCliente+" no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(idCliente);
        if(cliente.getModuloCollection().isEmpty()){
            throw  new ResourceNotFoundException("El cliente "+cliente.getRazonSocial()+" no tiene asignado modulos.");
        }
        List<ModuloOutputDTO> moduloOutputDTOS = new ArrayList<>();
        for (Modulo modulo:cliente.getModuloCollection()) {
            moduloOutputDTOS.add(ModuloOutputDTO.getDTO(modulo));
        }
        return new PageImpl<>(moduloOutputDTOS);
    }

    @Override
    public Modulo create(ModuloInputDTO entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public Modulo update(ModuloInputDTO entity, List<MultipartFile> files) {
        return null;
    }
}
