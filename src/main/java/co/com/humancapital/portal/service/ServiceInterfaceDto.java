package co.com.humancapital.portal.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 13/11/2019
 * @param <E> Entidad (Tabla)
 * @param <PK> Tipo de dato del Id de la entidad.
 * @param <DT> Clase DT de la entidad.
 * Metodos minimos que deben ser implementados en las clases de servicio.
 */
public interface ServiceInterfaceDto<E, PK, DT> {

    E findId(PK key) throws Exception;

    Page<E> findAll(Pageable pageable) throws Exception;

    E create(DT entity) throws Exception;
    E create(DT entity, List<MultipartFile> files) throws Exception;

    E update(DT entity) throws Exception;
    E update(DT entity, List<MultipartFile> files) throws Exception;

    void delete(PK key, Long usuarioInactiva) throws Exception;

    Boolean existeById(PK key) throws Exception;

}
