package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.LoginInput;
import co.com.humancapital.portal.dto.LoginOutput;
import co.com.humancapital.portal.dto.UsuarioOutputDTO;
import co.com.humancapital.portal.entity.Usuario;
import co.com.humancapital.portal.exception.JWTException;
import co.com.humancapital.portal.exception.ServerErrorException;
import co.com.humancapital.portal.exception.UnauthorizedException;
import co.com.humancapital.portal.util.EncoderUtil;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Alejandro Herrera Montilla
 * @project humancapital
 * @date 10/01/2020
 */
@Service
public class LoginService {

    private final UsuarioService usuarioService;

    private final JWTService jwtService;

    public LoginService(UsuarioService usuarioService, JWTService jwtService) {
        this.usuarioService = usuarioService;
        this.jwtService = jwtService;
    }

    public LoginOutput login(LoginInput loginInput) throws Exception {
        LoginOutput loginOutput = new LoginOutput();
        Optional<Usuario> usuarioOp = usuarioService.findByUsernameAndActivoTrue(loginInput.getUsername());
        if(! usuarioOp.isPresent())
            throw new UnauthorizedException("Credenciales erroneas.");
        else {
            Usuario usuario = usuarioOp.get();
            if(! validatePassword(loginInput, usuario))
                throw new UnauthorizedException("Credenciales erroneas.");
            else {
                try {
                    String token = jwtService.generateToken(usuario.getUsername());
                    loginOutput.setStatus(200);
                    loginOutput.setMessage("Bienvenido");
                    loginOutput.setToken(token);
                    loginOutput.setUsuario(UsuarioOutputDTO.getDTO(usuario));
                } catch (JWTException e) {
                    e.printStackTrace();
                    throw new ServerErrorException("Descripción del Error:"+e.getMessage());
                }
            }
        }
        return loginOutput;
    }

    private boolean validatePassword(LoginInput input, Usuario usuario){
        return EncoderUtil.validate(input.getPassword(), usuario.getPassword());
    }

}
