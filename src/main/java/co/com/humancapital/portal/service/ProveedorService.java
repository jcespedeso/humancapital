package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.ProveedorInputDTO;
import co.com.humancapital.portal.entity.Proveedor;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ProductoRepository;
import co.com.humancapital.portal.repository.ProveedorRepository;
import co.com.humancapital.portal.util.SectorEmpresa;
import co.com.humancapital.portal.util.TipoPersona;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los proveedores de productos para RFI.
 */
@Service
public class ProveedorService implements ServiceInterfaceDto<Proveedor, Long, ProveedorInputDTO>{

    private final ProveedorRepository proveedorRepository;

    private final ProductoRepository productoRepository;

    public ProveedorService(ProveedorRepository proveedorRepository, ProductoRepository productoRepository) {
        this.proveedorRepository = proveedorRepository;
        this.productoRepository = productoRepository;
    }

    @Override
    public Proveedor findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Proveedor con id "+key+" no existe.");
        }
        return proveedorRepository.findByIdProveedorAndActivoTrue(key);
    }

    @Override
    public Page<Proveedor> findAll(Pageable pageable) throws Exception {
        if(proveedorRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Proveedores creados en el sistema.");
        }
        return proveedorRepository.findAllByActivoTrue(pageable);
    }

    @Override
    public Proveedor create(ProveedorInputDTO entity) throws Exception {
        if(entity.getNit() == null || entity.getNit().trim().isEmpty()){
            throw new ResourceNotFoundException("El NIT del Proveedor a crear, no puede estar vacio.");
        }
        if(buscarXNit(entity.getNit())!=null) {
            throw new ResourceNotFoundException("El Proveedor con Nit "+ entity.getNit() +", Ya se encuentra registrado.");
        }
        if(entity.getRazonSocial() == null || entity.getRazonSocial().trim().isEmpty()){
            throw new ResourceNotFoundException("El nombre del producto a crear, no puede estar vacio.");
        }
        if(buscarXRazonSocial(entity.getRazonSocial())!=null) {
            throw new ResourceNotFoundException("El Proveedor con Razón Social  "+ entity.getRazonSocial() +", Ya se encuentra registrado.");
        }
        if(entity.getTipoPersona() == null || !TipoPersona.existeKey(entity.getTipoPersona())){
            throw new ResourceNotFoundException("El Tipo de Persona Juridica indicado para el Proveedor, no coincide con los permitidos por el sistema o esta vacio.");
        }
        if(entity.getSector() == null || !SectorEmpresa.existeKey(entity.getSector())){
            throw new ResourceNotFoundException("El Sector economico indicado para el Proveedor, no coincide con los permitidos por el sistema o esta vacio.");
        }
        if(entity.getContactoComercial() == null){
            throw new ResourceNotFoundException("Se requiere el contacto comercial con el Proveedor.");
        }
        if(entity.getTelefono() == null){
            throw new ResourceNotFoundException("Se requiere el numero telefonico del contacto comercial del Proveedor.");
        }
        if(entity.getEmail() == null){
            throw new ResourceNotFoundException("Se requiere el correo electronico del contacto comercial del Proveedor.");
        }

        Proveedor proveedor = new Proveedor();
        proveedor.setNit(entity.getNit().trim());
        proveedor.setRazonSocial(entity.getRazonSocial().toUpperCase());
        proveedor.setTipoPersona(entity.getTipoPersona());
        proveedor.setSector(entity.getSector());
        proveedor.setContactoComercial(entity.getContactoComercial().toUpperCase());
        if(entity.getDireccion()!=null){
            proveedor.setDireccion(entity.getDireccion().toUpperCase());
        }
        proveedor.setTelefono(entity.getTelefono());
        proveedor.setEmail(entity.getEmail().toLowerCase());

        proveedor.setActivo(true);
        proveedor.setFechaCreacion(new Date());
        proveedor.setFechaModifica(new Date());
        proveedor.setUsuarioCrea(entity.getUsuarioCrea());
        proveedor.setUsuarioModifica(entity.getUsuarioCrea());
        return proveedorRepository.saveAndFlush(proveedor);
    }

    @Override
    public Proveedor update(ProveedorInputDTO entity) throws Exception {
        if(entity.getIdProveedor() == null || !existeById(entity.getIdProveedor())) {
            throw new ResourceNotFoundException("El Proveedor con id " + entity.getIdProveedor() +", no existe.");
        }
        if(entity.getNit() == null || entity.getNit().trim().isEmpty()){
            throw new ResourceNotFoundException("El NIT del Proveedor a Actualizar, no puede estar vacio.");
        }
        if(entity.getRazonSocial() == null || entity.getRazonSocial().trim().isEmpty()){
            throw new ResourceNotFoundException("El nombre del producto a Actualizar, no puede estar vacio.");
        }
        if(entity.getTipoPersona() == null || !TipoPersona.existeKey(entity.getTipoPersona())){
            throw new ResourceNotFoundException("El Tipo de Persona Juridica indicado para el Proveedor, no coincide con los permitidos por el sistema o esta vacio.");
        }
        if(entity.getSector() == null || !SectorEmpresa.existeKey(entity.getSector())){
            throw new ResourceNotFoundException("El Sector economico indicado para el Proveedor, no coincide con los permitidos por el sistema o esta vacio.");
        }
        if(entity.getContactoComercial() == null){
            throw new ResourceNotFoundException("Se requiere el contacto comercial con el Proveedor.");
        }
        if(entity.getTelefono() == null){
            throw new ResourceNotFoundException("Se requiere el numero telefonico del contacto comercial del Proveedor.");
        }
        if(entity.getEmail() == null){
            throw new ResourceNotFoundException("Se requiere el correo electronico del contacto comercial del Proveedor.");
        }

        Proveedor proveedor = findId(entity.getIdProveedor());
        proveedor.setNit(entity.getNit().trim());
        proveedor.setRazonSocial(entity.getRazonSocial().toUpperCase());
        proveedor.setTipoPersona(entity.getTipoPersona());
        proveedor.setSector(entity.getSector());
        proveedor.setContactoComercial(entity.getContactoComercial().toUpperCase());
        if(entity.getDireccion()!=null){
            proveedor.setDireccion(entity.getDireccion().toUpperCase());
        }
        proveedor.setTelefono(entity.getTelefono());
        proveedor.setEmail(entity.getEmail().toLowerCase());

        proveedor.setFechaModifica(new Date());
        proveedor.setUsuarioModifica(entity.getUsuarioModifica());
        return proveedorRepository.saveAndFlush(proveedor);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Proveedor con Id " + key +", no existe.");
        }
        if(!productoRepository.findAllByProveedorAndActivoTrue(findId(key)).isEmpty()) {
            throw new ResourceNotFoundException("No se puede borrar el Proveedor en razon a que tiene Productos activos.");
        }
        Proveedor proveedor = proveedorRepository.findByIdProveedorAndActivoTrue(key);
        proveedor.setActivo(false);
        proveedor.setFechaInactivacion(new Date());
        proveedor.setUsuarioInactiva(usuarioInactiva);
        proveedorRepository.save(proveedor);
    }

    @Override
    public Boolean existeById(Long key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(proveedorRepository.findByIdProveedorAndActivoTrue(key) == null){
            return false;
        }
        return proveedorRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener un proveedor por el numero de nit.
     * @param nit Nit del Proveedor a validar.
     * @return Objeto Proveedor.
     */
    public Proveedor buscarXNit(String nit) throws Exception {
        if(nit == null){
            throw new ResourceNotFoundException("El NIT a validar, no puede estar vacio.");
        }
        return proveedorRepository.findByNitAndActivoTrue(nit.toUpperCase());
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener un proveedor por su razon social.
     * @param nombre Nit del Proveedor a validar.
     * @return Objeto Proveedor.
     */
    public Proveedor buscarXRazonSocial(String nombre) {
        if(nombre == null){
            throw new ResourceNotFoundException("El Nombre a validar, no puede estar vacio.");
        }
        return proveedorRepository.findByRazonSocialAndActivoTrue(nombre.toUpperCase());
    }

    @Override
    public Proveedor create(ProveedorInputDTO entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public Proveedor update(ProveedorInputDTO entity, List<MultipartFile> files) {
        return null;
    }
}
