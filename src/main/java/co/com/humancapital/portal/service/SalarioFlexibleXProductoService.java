package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.SalarioFlexibleXProductoInputDTO;
import co.com.humancapital.portal.entity.*;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Alejandro Herrera Montilla
 * @date 22/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los SalarioFlexibleXProducto.
 */
@Service
public class SalarioFlexibleXProductoService implements ServiceInterfaceDto<SalarioFlexibleXProducto, Long, SalarioFlexibleXProductoInputDTO> {

    private final SalarioFlexibleXProductoRepository salarioFlexibleXProductoRepository;

    private final ProductoRepository productoRepository;

    private final SalarioFlexibleRepository salarioFlexibleRepository;

    private final EmpleadoRepository empleadoRepository;

    private final CompensacionRepository compensacionRepository;

    private final SalarioFlexibleService salarioFlexibleService;

    private final ProductoService productoService;

    private final ClienteService clienteService;

    private final EmpleadoService empleadoService;

    public SalarioFlexibleXProductoService(SalarioFlexibleXProductoRepository salarioFlexibleXProductoRepository, ProductoRepository productoRepository, SalarioFlexibleRepository salarioFlexibleRepository, EmpleadoRepository empleadoRepository, CompensacionRepository compensacionRepository, SalarioFlexibleService salarioFlexibleService, ProductoService productoService, ClienteService clienteService, EmpleadoService empleadoService) {
        this.salarioFlexibleXProductoRepository = salarioFlexibleXProductoRepository;
        this.productoRepository = productoRepository;
        this.salarioFlexibleRepository = salarioFlexibleRepository;
        this.empleadoRepository = empleadoRepository;
        this.compensacionRepository = compensacionRepository;
        this.salarioFlexibleService = salarioFlexibleService;
        this.productoService = productoService;
        this.clienteService = clienteService;
        this.empleadoService = empleadoService;
    }

    @Override
    public SalarioFlexibleXProducto findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El registro de SalarioFlexible por Producto con id "+key+" no existe.");
        }
        return salarioFlexibleXProductoRepository.findByIdSalarioProducto(key);
    }

    @Override
    public SalarioFlexibleXProducto create(SalarioFlexibleXProductoInputDTO entity) throws Exception {
        if(entity.getValorProducto() == null) {
            throw new ResourceNotFoundException("El valor del producto no puede estar vacio.");
        }
        if(entity.getValorProducto()<0){
            throw new ResourceNotFoundException("El valor del producto no puede ser negativo.");
        }
        if(entity.getProducto() == null || !productoService.existeById(entity.getProducto())) {
            throw new ResourceNotFoundException("El Producto con id "+ entity.getProducto() +" no existe o esta vacio.");
        }
        if(entity.getSalarioFlexible() == null || !salarioFlexibleService.existeById(entity.getSalarioFlexible())) {
            throw new ResourceNotFoundException("El registro de SalarioFlexible con id "+ entity.getSalarioFlexible() +" no existe o esta vacio.");
        }
        SalarioFlexible salarioFlexActivo = salarioFlexibleService.findId(entity.getSalarioFlexible());
        if(salarioFlexibleService.salarioFlexibleXCompensacion(salarioFlexActivo.getCompensacion().getIdCompensacion()) == null) {
            throw new ResourceNotFoundException("El SalarioFlexible con id. "+ entity.getSalarioFlexible() +", no se encuentra en un estado valido para la asignación productos.");
        }
        Cliente cliente = clienteService.findId(clienteService.findXCargo(salarioFlexibleService.findId(entity.getSalarioFlexible()).getCompensacion().getEmpleado().getCargo().getIdCargo()).getIdCliente());
        if(!productoRepository.obtenerProductoXCliente(cliente.getIdCliente()).contains(productoService.findId(entity.getProducto()))){
            throw new ResourceNotFoundException("El Producto con id " + entity.getProducto() +" no esta asignado a alguno de los modulos del Cliente al que pertence el Empleado.");
        }
        if((entity.getValorProducto() + obtenerCupoUtilizado(salarioFlexibleService.findId(entity.getSalarioFlexible()).getCompensacion().getEmpleado().getIdEmpleado()))>salarioFlexibleService.findId(entity.getSalarioFlexible()).getValorSalarioFlex()){
            throw new ResourceNotFoundException("No se puede adicionar el Producto, cupo dispononible insuficiente");
        }

        SalarioFlexibleXProducto salarioFlexibleXProducto = new SalarioFlexibleXProducto();
        salarioFlexibleXProducto.setValorProducto(entity.getValorProducto());
        salarioFlexibleXProducto.setProducto(productoRepository.findByIdProductoAndActivoTrue(entity.getProducto()));
        salarioFlexibleXProducto.setSalarioFlexible(salarioFlexibleRepository.findByIdSalarioFlexible(entity.getSalarioFlexible()));
        return salarioFlexibleXProductoRepository.saveAndFlush(salarioFlexibleXProducto);
    }

    @Override
    public SalarioFlexibleXProducto update(SalarioFlexibleXProductoInputDTO entity) throws Exception {
        if(entity.getIdSalarioProducto() == null || !existeById(entity.getIdSalarioProducto())) {
            throw new ResourceNotFoundException("El registro de SalarioFlexible por producto con id No."+entity.getIdSalarioProducto()+" no existe o esta vacio");
        }
        if(entity.getValorProducto() == null) {
            throw new ResourceNotFoundException("El valor del producto no puede estar vacio.");
        }
        if(entity.getValorProducto()<0){
            throw new ResourceNotFoundException("El valor del producto no puede ser negativo.");
        }
        if(entity.getProducto() == null || !productoService.existeById(entity.getProducto())) {
            throw new ResourceNotFoundException("El Producto con id "+ entity.getProducto() +" no existe o esta vacio.");
        }
        if(entity.getSalarioFlexible() == null || !salarioFlexibleService.existeById(entity.getSalarioFlexible())) {
            throw new ResourceNotFoundException("El registro de SalarioFlexible con id "+ entity.getSalarioFlexible() +" no existe o esta vacio.");
        }
        SalarioFlexible salarioFlexActivo = salarioFlexibleService.findId(entity.getSalarioFlexible());
        if(salarioFlexibleService.salarioFlexibleXCompensacion(salarioFlexActivo.getCompensacion().getIdCompensacion()) == null) {
            throw new ResourceNotFoundException("El SalarioFlexible con id. "+ entity.getSalarioFlexible() +", no se encuentra en un estado valido para la asignación productos.");
        }
        Cliente cliente = clienteService.findId(clienteService.findXCargo(salarioFlexibleService.findId(entity.getSalarioFlexible()).getCompensacion().getEmpleado().getCargo().getIdCargo()).getIdCliente());
        if(!productoRepository.obtenerProductoXCliente(cliente.getIdCliente()).contains(productoService.findId(entity.getProducto()))){
            throw new ResourceNotFoundException("El Producto con id " + entity.getProducto() +" no esta asignado a alguno de los modulos del Cliente al que pertence el Empleado.");
        }
        SalarioFlexibleXProducto valorProductoAnterior = findId(entity.getIdSalarioProducto());
        if(((entity.getValorProducto() + obtenerCupoUtilizado(salarioFlexibleService.findId(entity.getSalarioFlexible()).getCompensacion().getEmpleado().getIdEmpleado()))-valorProductoAnterior.getValorProducto())
                > salarioFlexibleService.findId(entity.getSalarioFlexible()).getValorSalarioFlex()){
            throw new ResourceNotFoundException("No se puede adicionar el Producto, cupo dispononible insuficiente");
        }

        SalarioFlexibleXProducto salarioFlexibleXProducto = findId(entity.getIdSalarioProducto());
        salarioFlexibleXProducto.setValorProducto(entity.getValorProducto());
        salarioFlexibleXProducto.setProducto(productoRepository.findByIdProductoAndActivoTrue(entity.getProducto()));
        salarioFlexibleXProducto.setSalarioFlexible(salarioFlexibleRepository.findByIdSalarioFlexible(entity.getSalarioFlexible()));
        return salarioFlexibleXProductoRepository.saveAndFlush(salarioFlexibleXProducto);
    }

    public void delete(Long key) throws Exception {
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El registro de SalarioFlexibleXProducto con id No. "+key+", no existe o esta vacio");
        }
        SalarioFlexibleXProducto salarioFlexibleXProducto = findId(key);
        salarioFlexibleXProductoRepository.delete(salarioFlexibleXProducto);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de SalarioFlexibleXProducto asociados a un producto.
     * @param idProducto del Producto.
     * @return Lista de SalarioFlexibleXProducto.
     */
    public Page<SalarioFlexibleXProducto> findAllProducto(Long idProducto, Pageable pageable) throws Exception {
        if(idProducto == null || !productoService.existeById(idProducto)){
            throw new ResourceNotFoundException("El Producto con Id "+idProducto+" no existe.");
        }
        Producto producto = productoRepository.findByIdProductoAndActivoTrue(idProducto);
        if(salarioFlexibleXProductoRepository.findAllByProducto(producto, pageable).isEmpty() ){
            throw new ResourceNotFoundException("No se registran productos asignados.");
        }
        return salarioFlexibleXProductoRepository.findAllByProducto(producto, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de SalarioFlexibleXProducto por SalarioFlexible.
     * @param idSalFlex del SalarioFlexible.
     * @return Lista de SalarioFlexibleXProducto.
     */
    public Page<SalarioFlexibleXProducto> findAllSalarioFlexible(Long idSalFlex, Pageable pageable) throws Exception {
        if(idSalFlex == null || !salarioFlexibleService.existeById(idSalFlex)){
            throw new ResourceNotFoundException("El registro de Salario Flexible con Id "+idSalFlex+" no existe.");
        }
        SalarioFlexible salarioFlexible = salarioFlexibleRepository.findByIdSalarioFlexible(idSalFlex);
        if(salarioFlexibleXProductoRepository.findAllBySalarioFlexible(salarioFlexible, pageable).isEmpty() ){
            throw new ResourceNotFoundException("No se registran asignaciones de productos al SalarioFlexible indicado.");
        }
        return salarioFlexibleXProductoRepository.findAllBySalarioFlexible(salarioFlexible, pageable);
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        return salarioFlexibleXProductoRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de SalarioFlexibleXProducto por Empleado.
     * @param idEmpleado del Empleado a consultar.
     * @return Lista de SalarioFlexibleXProducto.
     */
    public Page<SalarioFlexibleXProducto> findAllEmpleado(Long idEmpleado, Pageable pageable) throws Exception {
        if(!empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no existe.");
        }
        Compensacion compensacion = compensacionRepository.findByEmpleadoAndActivoTrue(empleadoRepository.findByIdEmpleadoAndActivoTrue(idEmpleado));
        if(compensacion==null){
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no tiene registros de Compensación activo.");
        }
        SalarioFlexible salarioFlexible = salarioFlexibleService.salarioFlexibleXCompensacion(compensacion.getIdCompensacion());
        if(salarioFlexibleXProductoRepository.findAllBySalarioFlexible(salarioFlexible, pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no tiene Productos Asociados.");
        }
        return salarioFlexibleXProductoRepository.findAllBySalarioFlexible(salarioFlexible,pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de SalarioFlexibleXProducto por Empleado.
     * @param idEmpleado del Empleado a consultar.
     * @return Lista de SalarioFlexibleXProducto.
     */
    public List<SalarioFlexibleXProducto> findAllEmpleado(Long idEmpleado) throws Exception {
        if(!empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no existe.");
        }
        Compensacion compensacion = compensacionRepository.findByEmpleadoAndActivoTrue(empleadoRepository.findByIdEmpleadoAndActivoTrue(idEmpleado));
        if(compensacion==null){
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no tiene registros de Compensación activo.");
        }
        SalarioFlexible salarioFlexible = salarioFlexibleService.salarioFlexibleXCompensacion(compensacion.getIdCompensacion());
        if(salarioFlexibleXProductoRepository.findAllBySalarioFlexible(salarioFlexible).isEmpty()) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no tiene Productos Asociados.");
        }
        return salarioFlexibleXProductoRepository.findAllBySalarioFlexible(salarioFlexible);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el cupo disponible de flexibilización en productos.
     * @param idEmpleado del Empleado a consultar.
     * @return Valor entereso del cupo disponible.
     */
    public Integer obtenerCupoUtilizado(Long idEmpleado) throws Exception {
        if(idEmpleado == null || !empleadoService.existeById(idEmpleado)){
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no exite.");
        }
        Empleado empleado = empleadoRepository.findByIdEmpleadoAndActivoTrue(idEmpleado);
        Compensacion compensacion = compensacionRepository.findByEmpleadoAndActivoTrue(empleado);
        SalarioFlexible salarioFlexible = salarioFlexibleRepository.findByCompensacionEstadosIniciales(compensacion.getIdCompensacion());
        Integer cupoUtilizado = 0;
            for (SalarioFlexibleXProducto list:salarioFlexibleXProductoRepository.findAllBySalarioFlexible(salarioFlexible)) {
                cupoUtilizado = cupoUtilizado + list.getValorProducto();
            }
        return cupoUtilizado;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de productos con la cantidad de empleados que lo han utilizado por Cliente.
     * Reglas: El Producto.Activo={@true}, El Empleado.Activo={@true}, SalarioFlexible.Estado={@3-VALIDADA}
     * @param idCliente del Cliente a consultar.
     * @return List de Valor con key idProducto, nombreProducto y Value countEmpleados.
     */
    public List<?> contarProductoUtilizadoXCliente(Long idCliente) throws Exception {
        if(idCliente == null || !clienteService.existeById(idCliente)){
            throw new ResourceNotFoundException("El Cliente con id " + idCliente +" no exite.");
        }
        List<Map<String,Object>> cuentaProducto = new ArrayList<>();
        for (Object[] datos : salarioFlexibleXProductoRepository.contarProductosUtilizadosXCliente(idCliente)) {
            Map<String, Object> data = new LinkedHashMap<>();
            data.put("idProducto",datos[1]);
            data.put("Nombre",datos[0]);
            data.put("count",datos[2]);
            cuentaProducto.add(data);
        }
        return cuentaProducto;
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de productos con la cantidad de empleados que lo han utilizado.
     * Reglas: El Producto.Activo={@true}, El Empleado.Activo={@true}, SalarioFlexible.Estado={@3-VALIDADA}
     * @return List de Valor con key idProducto, nombreProducto y Value countEmpleados.
     */
    public List<?> contarProductoUtilizado() throws Exception {
        List<Map<String,Object>> cuentaProducto = new ArrayList<>();
        for (Object[] datos : salarioFlexibleXProductoRepository.contarProductosUtilizados()) {
            Map<String, Object> data = new LinkedHashMap<>();
            data.put("idProducto",datos[1]);
            data.put("Nombre",datos[0]);
            data.put("count",datos[2]);
            cuentaProducto.add(data);
        }
        return cuentaProducto;
    }

    @Override
    public Page<SalarioFlexibleXProducto> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public SalarioFlexibleXProducto create(SalarioFlexibleXProductoInputDTO entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public SalarioFlexibleXProducto update(SalarioFlexibleXProductoInputDTO entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) {

    }
}
