package co.com.humancapital.portal.service;

import co.com.humancapital.portal.entity.Multimedia;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.MultimediaRepository;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import javax.servlet.http.HttpServletResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 21/11/2019
 * Clase para la implementación de las diferentes reglas de negocio que se generen alrededor de la
 * carga y lectura de archivos en el sistema.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class FileSystemService {
    
    private final MultimediaRepository multimediaRepository;
    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite modelar la estructura del directorio raiz donde se
     * @param directorioRoot Directorio raiz definido en el application.properties
     * @return Estructura definitiva del directorio en el cual se guardan los archivos.
     * almacenaran aleatoriamente los archivos multimedia.
     */
    public String generarEstructuraRoot(String directorioRoot) {
        LocalDate actual = LocalDate.now();
        int directorioA = ThreadLocalRandom.current().nextInt(1, 12);
        int directorioB = ThreadLocalRandom.current().nextInt(1, 12);
        int directorioC = ThreadLocalRandom.current().nextInt(1, 12);
        String directorio =
                directorioRoot + File.separator + actual.getYear() + File.separator + directorioA + File.separator
                        + directorioB + File.separator + directorioC;
        return directorio;
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Crea el directorio root y los subdirectorios validando previamente si
     * @param directorioRoot Directorio principal.
     * @param subDirectorio Subdirectorio secundario.
     * existeKey o no.
     */
    public void crearDirectorios(String directorioRoot, BigDecimal subDirectorio) {
        boolean existeDirectorioRoot = validarExistenciaDirectorio(directorioRoot);
        boolean existeDirectorio = validarExistenciaDirectorio(directorioRoot + File.separator + subDirectorio);
        Path dirPathObj = Paths.get(directorioRoot);
        Path sudDirPathObj = Paths.get(directorioRoot + File.separator + subDirectorio.toString());
        
        try {
            if (!existeDirectorioRoot) {
                Files.createDirectories(dirPathObj);
                Files.createDirectories(sudDirPathObj);
            }
            if (!existeDirectorio) {
                Files.createDirectories(sudDirPathObj);
            }
        } catch (IOException ioExceptionObj) {
            log.info("::: Se genero un problema al crear el directorio = " + ioExceptionObj.getMessage());
        }
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Crea un directorio validando previamente si existeKey o no.
     * @param directorio Nombre del Directorio a crear.
     */
    public void crearDirectorio(Object directorio) {
        boolean existeDirectorio = validarExistenciaDirectorio(directorio);
        Path directorioPath = Paths.get(directorio.toString());
        
        try {
            if (!existeDirectorio) {
                Files.createDirectories(directorioPath);
            }
            
        } catch (IOException ioExceptionObj) {
            log.info("::: Se genero un problema al crear el directorio = " + ioExceptionObj.getMessage());
        }
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Valida si un directorio existeKey.
     * @param directorioValidar Directorio a evaluar.
     * @return {@code true} si el directorio existeKey o {@code false} si no existeKey.
     */
    public boolean validarExistenciaDirectorio(Object directorioValidar) {
        return Files.exists(Paths.get(directorioValidar.toString()));
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo para guardar un archivo en un directorios predeterminado.
     * @param file Archivo a guardar
     * @param directorioRoot Directorio Principal.
     * @param subDirectorio Directorio Secundario.
     * @param procedencia Clase que utiliza el metodo.
     * @param idProcedencia Id del registro.
     * @throws IOException Se presenta en caso de errores de acceso.
     */
    @Transactional
    public Multimedia guardarArchivo(MultipartFile file, String directorioRoot, BigDecimal subDirectorio,
            String procedencia, String idProcedencia) {
        Multimedia multimedia = new Multimedia();
        //Valida si hay archivo seleccionado o si el seleccionado esta vacio.
        if (file.isEmpty()) {
            throw new ResourceNotFoundException("El archivo esta vacio.");
        }
        //Valida si el Archivo seleccionado tiene un formato valido segun logica @code isValidoContentType.
        if (!isValidoContentType(file.getContentType())) {
            throw new ResourceNotFoundException("El formato del archivo no esta permitido.");
        }
        multimedia.setFechaRegistro(new Date());
        multimedia.setProcedencia(procedencia);
        multimedia.setIdProcedencia(idProcedencia);
        multimedia.setContentType(file.getContentType());
        multimedia.setNombreOriginal(file.getOriginalFilename());
        /*
         * Numero Aleatorio para renombrar el archivo formato (subDirectorio+numeroAleatorio+extension)
         * El archivo es almacenado en el directorio de destino sin extencion
         */
        try {
            int numeroAleatorio = ThreadLocalRandom.current().nextInt(1, 1000);
            String nuevoNombre = subDirectorio + "" + numeroAleatorio;
            byte[] bytes = file.getBytes();
            Path path = Paths.get(directorioRoot + File.separator + subDirectorio + File.separator
                    + file.getOriginalFilename().replace(file.getOriginalFilename(), nuevoNombre));
            Files.write(path, bytes);
            multimedia.setNombreFinal(nuevoNombre);
            multimedia.setUbicacionDisco(directorioRoot + File.separator + subDirectorio);
            return multimediaRepository.saveAndFlush(multimedia);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Permite agregar varios documentos adjuntos, segun contentType permitidos
     * @param files Archivos seleccionados.
     * @param directorioRoot Directorio Principal.
     * @param subDirectorio Directorio Secundario.
     */
    @Transactional
    public void guardarMultiplesArchivos(List<MultipartFile> files, String directorioRoot, BigDecimal subDirectorio) {
        String procedencia=""+FileSystemService.class.getSimpleName();
        String idProcedencia=""+FileSystemService.class.getSimpleName().length();
        for (MultipartFile file : files) {
            guardarArchivo(file, directorioRoot, subDirectorio, procedencia, idProcedencia);
        }
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo que identifica la metadata de la extension de un archivo.
     * @param file Archivo a evaluar.
     * @return String que identifica la extencion del archivo que recibio por parametro.
     */
    public String identificarExtencion(MultipartFile file) {
        Map<String, String> listContentType = new HashMap<>();
        listContentType.put("application/pdf", "pdf");
        listContentType.put("application/msword", "doc");
        listContentType.put("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx");
        listContentType.put("application/vnd.ms-powerpoint", "ppt");
        listContentType.put("application/vnd.openxmlformats-officedocument.presentationml.presentation", "pptx");
        listContentType.put("application/vnd.ms-excel", "xls");
        listContentType.put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx");
        listContentType.put("image/jpeg", "jpeg");
        listContentType.put("image/jpg", "jpg");
        listContentType.put("image/png", "png");
        return listContentType.get(file.getContentType());
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Permite validar el formato del archivo que se esta adjuntando.
     * @param contentType Tipo de contenido.
     * @return {@code true} Si el tipo de contenido es válido (PDF, WORD, POWERPOINT, EXCEL, JPG, JPEG, PNG); de lo
     * contrario, {@code false}.
     */
    private boolean isValidoContentType(String contentType) {
        //Mejorar metodo validando su valides contra el metodo identificarExtencion
        String[] validContentTypes = {"application/pdf",
                "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/vnd.ms-powerpoint",
                "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "image/jpeg", "image/jpg", "image/png"};
        for (String validContentType : validContentTypes) {
            if (contentType.equals(validContentType)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite descargar un archivo multimedia.
     * @param response Representa la respuesta del servidor
     * @param id id que identifica el archivo multimedia a descargar.
     */
    public void descargarArchivo(HttpServletResponse response, Long id) {
        Multimedia multimedia = multimediaRepository.findByIdMultimedia(id);
        String nombreFichero = multimedia.getNombreFinal();
        String subDirectorio = multimedia.getUbicacionDisco();
        response.setContentType(multimedia.getContentType());
        response.setHeader("Content-Disposition", "attachment; filename=\""
                + nombreFichero.replace(nombreFichero, multimedia.getNombreOriginal()) + "\"");
        
        try {
            InputStream archivoLeido = new FileInputStream(subDirectorio + File.separator + nombreFichero);
            IOUtils.copy(archivoLeido, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Permite borrar tanto de la base de datos como del disco un archivo multimedia.
     * @param id Identificador del archivo multimedia.
     */
    public void borrarArchivo(Long id) {
        Multimedia multimedia = multimediaRepository.findByIdMultimedia(id);
        Path path = Paths.get(multimedia.getUbicacionDisco() + File.separator + multimedia.getNombreFinal());
        
        try {
            if (Files.deleteIfExists(path)) {
                delete(multimedia.getIdMultimedia());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Elimina un registro de en Multimedia.
     * @param key Id del archivo Multimedia.
     */
    public void delete(Long key) {
        multimediaRepository.deleteById(key);
    }
    
}
