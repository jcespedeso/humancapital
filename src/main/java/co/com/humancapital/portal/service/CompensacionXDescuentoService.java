package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.CompensacionXDescuentoInputDTO;
import co.com.humancapital.portal.entity.Compensacion;
import co.com.humancapital.portal.entity.CompensacionXDescuento;
import co.com.humancapital.portal.entity.Descuento;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.CompensacionRepository;
import co.com.humancapital.portal.repository.CompensacionXDescuentoRepository;
import co.com.humancapital.portal.repository.DescuentoRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 19/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los CompensacionXDescuento.
 */
@Service
public class CompensacionXDescuentoService implements ServiceInterfaceDto<CompensacionXDescuento,Long,CompensacionXDescuentoInputDTO>{

    private final CompensacionXDescuentoRepository compensacionXDescuentoRepository;

    private final CompensacionRepository compensacionRepository;

    private final DescuentoRepository descuentoRepository;

    private final CompensacionService compensacionService;

    private final DescuentoService descuentoService;

    private final EmpleadoService empleadoService;

    public CompensacionXDescuentoService(CompensacionXDescuentoRepository compensacionXDescuentoRepository, CompensacionRepository compensacionRepository, DescuentoRepository descuentoRepository, CompensacionService compensacionService, DescuentoService descuentoService, EmpleadoService empleadoService) {
        this.compensacionXDescuentoRepository = compensacionXDescuentoRepository;
        this.compensacionRepository = compensacionRepository;
        this.descuentoRepository = descuentoRepository;
        this.compensacionService = compensacionService;
        this.descuentoService = descuentoService;
        this.empleadoService = empleadoService;
    }

    @Override
    public CompensacionXDescuento findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El registro de asignacion de CompensaciónXDescuento con id "+key+" no existe.");
        }
        return compensacionXDescuentoRepository.findByIdCompDescuento(key);
    }

    @Override
    public CompensacionXDescuento create(CompensacionXDescuentoInputDTO entity) throws Exception {
        if(entity.getValor() == null) {
            throw new ResourceNotFoundException("El valor del descuento no puede estar vacio.");
        }
        if(entity.getValor() < 0){
            throw new ResourceNotFoundException("El valor del descuento no puede ser negativo.");
        }
        if(entity.getCompensacion() == null || !compensacionService.existeById(entity.getCompensacion())) {
            throw new ResourceNotFoundException("La compensación no existe o esta vacia.");
        }
        if(entity.getDescuento() == null || !descuentoService.existeById(entity.getDescuento())) {
            throw new ResourceNotFoundException("El Descuento no existe o esta vacio.");
        }

        CompensacionXDescuento compeXDescuento = new CompensacionXDescuento();
        compeXDescuento.setValor(entity.getValor());
        compeXDescuento.setCompensacion(compensacionRepository.findByIdCompensacion(entity.getCompensacion()));
        compeXDescuento.setDescuento(descuentoRepository.findByIdDescuentoAndActivoTrue(entity.getDescuento()));
        return compensacionXDescuentoRepository.saveAndFlush(compeXDescuento);
    }

    @Override
    public CompensacionXDescuento update(CompensacionXDescuentoInputDTO entity) throws Exception {
        if(entity.getIdCompDescuento() == null || !existeById(entity.getIdCompDescuento())) {
            throw new ResourceNotFoundException("El registro de compensacion por descuento con id No."+entity.getIdCompDescuento()+" no existe o esta vacio");
        }
        if(entity.getValor() == null) {
            throw new ResourceNotFoundException("El valor del descuento no puede estar vacio.");
        }
        if(entity.getValor()<0){
            throw new ResourceNotFoundException("El valor del descuento no puede ser negativo.");
        }
        if(entity.getCompensacion() == null || !compensacionService.existeById(entity.getCompensacion())) {
            throw new ResourceNotFoundException("La compensación no existe o esta vacia.");
        }
        if(entity.getDescuento() == null || !descuentoService.existeById(entity.getDescuento())) {
            throw new ResourceNotFoundException("El ingreso no existe o esta vacio.");
        }

        CompensacionXDescuento compDescuento = findId(entity.getIdCompDescuento());
        compDescuento.setValor(entity.getValor());
        compDescuento.setCompensacion(compensacionRepository.findByIdCompensacion(entity.getCompensacion()));
        compDescuento.setDescuento(descuentoRepository.findByIdDescuentoAndActivoTrue(entity.getDescuento()));
        return compensacionXDescuentoRepository.saveAndFlush(compDescuento);
    }

    public void delete(Long key) throws Exception{
        if(key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El registro de compensacion por descuento con id No."+key+" no existe o esta vacio");
        }
        compensacionXDescuentoRepository.delete(compensacionXDescuentoRepository.findByIdCompDescuento(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        return compensacionXDescuentoRepository.existsById(key);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de CompensacionXDescuento asociados a un empleado,
     * por intermedio de su respectiva compensacion.
     * @param idCompensacion de la Compensación activa del empleado.
     * @return Lista de CompensacionXDescuento del Empleado.
     */
    public Page<CompensacionXDescuento> findACompensacion(Long idCompensacion, Pageable pageable) throws Exception {
        if(idCompensacion == null || !compensacionService.existeById(idCompensacion)){
            throw new ResourceNotFoundException("La Compensación con id No."+idCompensacion+" no existe.");
        }
        Compensacion compensacion = compensacionRepository.findByIdCompensacion(idCompensacion);
        if(compensacionXDescuentoRepository.findAllByCompensacion(compensacion, pageable).isEmpty()){
            throw new ResourceNotFoundException("No existen registros de descuentos por la compensación indicada.");
        }
        return compensacionXDescuentoRepository.findAllByCompensacion(compensacion, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite obtener el listado de CompensacionXDescuento por Descuento.
     * @param idDescuento del Descuento.
     * @return Lista de CompensacionXDescuento.
     */
    public Page<CompensacionXDescuento> findAllDescuento(Long idDescuento, Pageable pageable) throws Exception {
        if(idDescuento == null || !descuentoService.existeById(idDescuento)){
            throw new ResourceNotFoundException("El Descuento con id No."+idDescuento+" no existe.");
        }
        Descuento descuento = descuentoRepository.findByIdDescuentoAndActivoTrue(idDescuento);
        if(compensacionXDescuentoRepository.findAllByDescuento(descuento, pageable).isEmpty()){
            throw new ResourceNotFoundException("No existen registros de descuentos por el descuento indicado.");
        }
        return compensacionXDescuentoRepository.findAllByDescuento(descuento, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar todos los descuentos que tiene un empleado.
     * @param idEmpleado Id del Empleado a validar.
     * @return List de Descuentos asignados al Empleado.
     */
    public Page<CompensacionXDescuento> descuentosXEmpleado(Long idEmpleado, Pageable pageable) throws Exception{
        if(!empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no existe.");
        }
        if(compensacionXDescuentoRepository.obtenerDescuentoXEmpleado(idEmpleado, pageable).isEmpty()) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no tiene Descuentos Asociados.");
        }
        return compensacionXDescuentoRepository.obtenerDescuentoXEmpleado(idEmpleado, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite calcular el valor total de los descuentos que tiene asignado un Empleado.
     * @param idEmpleado Id del Empleado a validar.
     * @return Valor Total Descuentos asignados al Empleado.
     */
    public Double totalDescuentosXEmpleado(Long idEmpleado) throws Exception{
        if(!empleadoService.existeById(idEmpleado)) {
            throw new ResourceNotFoundException("El Empleado con id " + idEmpleado +" no existe.");
        }
        Double totalDescuento = compensacionXDescuentoRepository.obtenerTotalDescuentoXEmpleado(idEmpleado);
        if(totalDescuento == null || totalDescuento <=0){
            totalDescuento = (double) 0;
        }
        return totalDescuento;
    }

    @Override
    public Page<CompensacionXDescuento> findAll(Pageable pageable) throws Exception {
        return null;
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {

    }

    @Override
    public CompensacionXDescuento create(CompensacionXDescuentoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public CompensacionXDescuento update(CompensacionXDescuentoInputDTO entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
