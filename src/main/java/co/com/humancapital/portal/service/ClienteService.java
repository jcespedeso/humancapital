package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.ClienteInputDTO;
import co.com.humancapital.portal.dto.EmpleadoInputDTO;
import co.com.humancapital.portal.dto.UsuarioInputDTO;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Empleado;
import co.com.humancapital.portal.entity.Modulo;
import co.com.humancapital.portal.entity.Producto;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.*;
import co.com.humancapital.portal.util.*;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019 Clase para la implementación de las diferentes reglas de negocio que se generen alrededor del
 * Cliente.
 */
@Service
public class ClienteService implements ServiceInterfaceDto<Cliente, Long, ClienteInputDTO> {
    
    private final ClienteRepository clienteRepository;
    private final ClasificacionClienteService clasificacionClienteService;
    private final EmpleadoRepository empleadoRepository;
    private final CargoRepository cargoRepository;
    private final DepartamentoRepository departamentoRepository;
    private final ProductoService productoService;
    private final ModuloService moduloService;
    private final UsuarioService usuarioService;
    private final PerfilService perfilService;
    private final UsuarioRepository usuarioRepository;
    private final ClasificacionClienteRepository clasificacionClienteRepository;

    public ClienteService(ClienteRepository clienteRepository, ClasificacionClienteService clasificacionClienteService, EmpleadoRepository empleadoRepository, CargoRepository cargoRepository, DepartamentoRepository departamentoRepository, @Lazy ProductoService productoService, ModuloService moduloService, @Lazy UsuarioService usuarioService, PerfilService perfilService, UsuarioRepository usuarioRepository, ClasificacionClienteRepository clasificacionClienteRepository) {
        this.clienteRepository = clienteRepository;
        this.clasificacionClienteService = clasificacionClienteService;
        this.empleadoRepository = empleadoRepository;
        this.cargoRepository = cargoRepository;
        this.departamentoRepository = departamentoRepository;
        this.productoService = productoService;
        this.moduloService = moduloService;
        this.usuarioService = usuarioService;
        this.perfilService = perfilService;
        this.usuarioRepository = usuarioRepository;
        this.clasificacionClienteRepository = clasificacionClienteRepository;
    }

    @Override
    public Cliente findId(Long key) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Cliente con id " + key + " no existe.");
        }
        return clienteRepository.findByIdClienteAndActivoTrue(key);
    }
    
    @Override
    public Page<Cliente> findAll(Pageable pageable) throws Exception {
        if (clienteRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Clientes creados en el sistema.");
        }
        return clienteRepository.findAllByActivoTrue(pageable);
    }

    @Transactional
    @Override
    public Cliente create(ClienteInputDTO entity) throws Exception {
        if (existeByNit(entity.getNit())) {
            throw new ResourceNotFoundException("El Cliente con Nit " + entity.getNit() + ", ya Existe.");
        }
        if (existeByRazonSocial(entity.getRazonSocial())) {
            throw new ResourceNotFoundException("La Razón Social  " + entity.getRazonSocial() + ", ya Existe.");
        }
        if (entity.getTipoPersona() != null ) {
            if(!TipoPersona.existeKey(entity.getTipoPersona())){
                throw new ResourceNotFoundException(
                        "El Tipo de Persona Juridica indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacia.");
            }
        }
        if (entity.getSector() != null) {
            if (!SectorEmpresa.existeKey(entity.getSector())) {
                throw new ResourceNotFoundException(
                        "El Sector economico indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacia.");
            }
        }
        if (entity.getTipo() == null || !TipoEmpresa.existeKey(entity.getTipo())) {
            throw new ResourceNotFoundException(
                    "La Tipologia de empresa indicada para el Cliente, no coincide con los permitidos por el sistema o esta vacia.");
        }
        if (entity.getSolucionImplementada() != null) {
            if(!SolucionEmpresa.existeKey(entity.getSolucionImplementada())){
                throw new ResourceNotFoundException(
                        "El tipo de solucion indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacio.");
            }
        }
        if (entity.getTamanoEmpresa() != null ) {
            if(!TamanoEmpresa.existeKey(entity.getTamanoEmpresa())){
                throw new ResourceNotFoundException(
                        "El Tamaño de Empresa indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacio.");
            }
        }
        if (entity.getPresencia() != null) {
            if(!PresenciaEmpresa.existeKey(entity.getPresencia())){
                throw new ResourceNotFoundException(
                        "El nivel de presencia o alcance operacional indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacio.");
            }
        }
        if (entity.getIngreso() != null ) {
            if(!IngresoEmpresa.existeKey(entity.getIngreso())){
                throw new ResourceNotFoundException(
                        "El rango de Ingresos definidos para el Cliente, no coincide con los permitidos por el sistema o esta vacio.");
            }
        }
        if (entity.getClasificacionCliente() != null) {
            if(!clasificacionClienteService.existeById(entity.getClasificacionCliente())){
                throw new ResourceNotFoundException(
                        "La Clasificación indicada para el Cliente, no coincide con los permitidos por el sistema o esta vacia.");
            }
        }
        Cliente cliente = new Cliente();
        cliente.setNit(entity.getNit().trim());
        cliente.setRazonSocial(entity.getRazonSocial().toUpperCase());
        cliente.setTipoPersona(entity.getTipoPersona());
        cliente.setSector(entity.getSector());
        if (entity.getContactoComercial() != null) {
            cliente.setContactoComercial(entity.getContactoComercial().toUpperCase());
        }
        cliente.setDireccion(entity.getDireccion());
        cliente.setTelefono(entity.getTelefono());
        if (entity.getEmail() != null) {
            cliente.setEmail(entity.getEmail().toLowerCase());
        }
        cliente.setPrincipal(entity.getNit().trim().equals("830098590-6"));
        cliente.setTipo(entity.getTipo());
        cliente.setSolucionImplementada(entity.getSolucionImplementada());
        cliente.setTamanoEmpresa(entity.getTamanoEmpresa());
        cliente.setPresencia(entity.getPresencia());
        cliente.setIngreso(entity.getIngreso());
        
        //Auditoria
        cliente.setActivo(true);
        cliente.setFechaCreacion(new Date());
        cliente.setUsuarioCrea(entity.getUsuarioCrea());
        cliente.setFechaModifica(new Date());
        cliente.setUsuarioModifica(entity.getUsuarioCrea());
        if (entity.getClasificacionCliente() != null) {
            cliente.setClasificacionCliente(clasificacionClienteService.findId(entity.getClasificacionCliente()));
        }else{
            cliente.setClasificacionCliente(clasificacionClienteRepository.findClasificacionClienteByTipoEspecifico("Sin Clasificacion"));
        }
        clienteRepository.saveAndFlush(cliente);
        if(entity.getUsername() != null){
            saveUsuarioAndPerfil(entity,cliente);
        }
        return cliente;
    }
    
    @Override
    public Cliente update(ClienteInputDTO entity) throws Exception {
        if (entity.getIdCliente() == null || !existeById(entity.getIdCliente())) {
            throw new ResourceNotFoundException("El Cliente con Id " + entity.getIdCliente() + " no existe.");
        }
        if (existeByNit(entity.getNit()) && !findNit(entity.getNit()).getIdCliente().equals(entity.getIdCliente())) {
            throw new ResourceNotFoundException("El Nit Suministrado ya esta registrado.");
        }
        if (existeByRazonSocial(entity.getRazonSocial()) && !findXRazonSocial(entity.getRazonSocial()).getIdCliente()
                .equals(entity.getIdCliente())) {
            throw new ResourceNotFoundException("la Razón Social Suministrada pertenece a otro Cliente");
        }
        if (entity.getEmail() == null || entity.getEmail().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Email del Cliente no puede estar vacio.");
        }
        if (entity.getTipoPersona() == null || !TipoPersona.existeKey(entity.getTipoPersona())) {
            throw new ResourceNotFoundException(
                    "El Tipo de Persona Juridica indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacia.");
        }
        if (entity.getSector() == null || !SectorEmpresa.existeKey(entity.getSector())) {
            throw new ResourceNotFoundException(
                    "El Sector Economico indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacia.");
        }
        if (entity.getTipo() == null || !TipoEmpresa.existeKey(entity.getTipo())) {
            throw new ResourceNotFoundException(
                    "La Tipologia de empresa indicada para el Cliente, no coincide con los permitidos por el sistema o esta vacia.");
        }
        if (entity.getSolucionImplementada() == null || !SolucionEmpresa.existeKey(entity.getSolucionImplementada())) {
            throw new ResourceNotFoundException(
                    "El tipo de solucion indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacio.");
        }
        if (entity.getTamanoEmpresa() == null || !TamanoEmpresa.existeKey(entity.getTamanoEmpresa())) {
            throw new ResourceNotFoundException(
                    "El Tamaño de Empresa indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacio.");
        }
        if (entity.getPresencia() == null || !PresenciaEmpresa.existeKey(entity.getPresencia())) {
            throw new ResourceNotFoundException(
                    "El nivel de presencia o alcance operacional indicado para el Cliente, no coincide con los permitidos por el sistema o esta vacio.");
        }
        if (entity.getIngreso() == null || !IngresoEmpresa.existeKey(entity.getIngreso())) {
            throw new ResourceNotFoundException(
                    "El rango de Ingresos definidos para el Cliente, no coincide con los permitidos por el sistema o esta vacio.");
        }
        if (entity.getClasificacionCliente() == null || !clasificacionClienteService.existeById(entity.getClasificacionCliente())) {
            throw new ResourceNotFoundException(
                    "La Clasificación indicada para el Cliente, no coincide con los permitidos por el sistema o esta vacia.");
        }
        
        Cliente cliente = findId(entity.getIdCliente());
        cliente.setIdCliente(cliente.getIdCliente());
        cliente.setNit(entity.getNit().trim());
        cliente.setRazonSocial(entity.getRazonSocial().toUpperCase());
        cliente.setTipoPersona(entity.getTipoPersona());
        cliente.setSector(entity.getSector());
        if (entity.getContactoComercial() != null) {
            cliente.setContactoComercial(entity.getContactoComercial().toUpperCase());
        }
        cliente.setDireccion(entity.getDireccion());
        cliente.setTelefono(entity.getTelefono());
        if (entity.getEmail() != null) {
            cliente.setEmail(entity.getEmail().toLowerCase());
        }
        cliente.setPrincipal(entity.getNit().trim().equals("830098590-6"));
        cliente.setTipo(entity.getTipo());
        cliente.setSolucionImplementada(entity.getSolucionImplementada());
        cliente.setTamanoEmpresa(entity.getTamanoEmpresa());
        cliente.setPresencia(entity.getPresencia());
        cliente.setIngreso(entity.getIngreso());
        
        //Auditoria
        cliente.setActivo(cliente.isActivo());
        cliente.setFechaCreacion(cliente.getFechaCreacion());
        cliente.setUsuarioCrea(cliente.getUsuarioCrea());
        cliente.setFechaModifica(new Date());
        cliente.setUsuarioModifica(entity.getUsuarioModifica());
        if (entity.getClasificacionCliente() != null) {
            cliente.setClasificacionCliente(clasificacionClienteService.findId(entity.getClasificacionCliente()));
        }
        return clienteRepository.saveAndFlush(cliente);
    }
    
    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Cliente con Id " + key + " no existe.");
        }
        Cliente cliente = findId(key);
        if (empleadoRepository.obtenerEmpleadoXCliente(key).size() > 0) {
            throw new ResourceNotFoundException("El Cliente " + cliente.getRazonSocial()
                    + " registra Empleados activos en el sistema, lo que no permite su eliminación.");
        }
        if (cargoRepository.obtenerCargoXCliente(key).size() > 0) {
            throw new ResourceNotFoundException("El Cliente " + cliente.getRazonSocial()
                    + " registra Cargos activos en el sistema, lo que no permite su eliminación.");
        }
        if (departamentoRepository.findAllByClienteAndActivoTrue(cliente).size() > 0) {
            throw new ResourceNotFoundException("El Cliente " + cliente.getRazonSocial()
                    + " registra Departamentos activos en el sistema, lo que no permite su eliminación.");
        }
        if(!usuarioRepository.findAllByClienteAndActivoTrue(cliente).isEmpty()){
            throw new ResourceNotFoundException("El Cliente no puede ser eliminado por que tiene usuarios activos.");
        }
        cliente.setActivo(false);
        cliente.setFechaInactivacion(new Date());
        cliente.setUsuarioInactiva(usuarioInactiva);
        clienteRepository.save(cliente);
    }
    
    @Override
    public Boolean existeById(Long key) {
        if (key == null) {
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if (clienteRepository.findByIdClienteAndActivoTrue(key) == null) {
            return false;
        }
        return clienteRepository.existsById(key);
    }

    /**
     * @author Alendro Herrera Montilla
     * @date 04/04/2020
     * Metodo que permite asignar el usuario y el perfil por defecto <ADMINCLIENTE> a un Cliente.
     * @param cliente Objeto Cliente.
     * @param entity Objeto ClienteInputDTO con la información del usuario a crear.
     */
    public void saveUsuarioAndPerfil(ClienteInputDTO entity, Cliente cliente){
        UsuarioInputDTO usuario = new UsuarioInputDTO();
        usuario.setUsername(entity.getUsername());
        usuario.setPassword(entity.getPassword());
        usuario.setCliente(cliente.getIdCliente());
        usuario.setUsuarioCrea(entity.getUsuarioCrea());
        //Todo pefil por defecto para el Usuario administrativo del Cliente...
        usuario.setPerfilCollection(Collections.singleton(perfilService.findByNombre("ADMINCLIENTE")));
        usuarioService.create(usuario);
    }
    
    /**
     * Metodo que permite obtener el Cliente mediante su NIT.
     *
     * @param nit Nit a validar.
     * @return Objeto Cliente.
     */
    public Cliente findNit(String nit) {
        if (clienteRepository.findByNitAndActivoTrue(nit) == null) {
            throw new ResourceNotFoundException("El Cliente con NIT " + nit + ", no existe o esta inactivo.");
        }
        return clienteRepository.findByNitAndActivoTrue(nit);
    }
    
    /**
     * Metodo que permite validar si un numero de Nit existe en el conjunto de Clientes Activos.
     *
     * @param nit Nit a validar.
     * @return @{true} Si el Nit suministrado existe de lo contrario devuelve @{false}
     */
    public boolean existeByNit(String nit) {
        if (nit == null) {
            throw new ResourceNotFoundException("El numero de NIT no puede estar vacio.");
        }
        return clienteRepository.findByNit(nit) != null;
    }
    
    /**
     * Metodo que permite obtener el Cliente mediante su Razon Social.
     *
     * @param rSocial Nit a validar.
     * @return Objeto Cliente.
     */
    public Cliente findXRazonSocial(String rSocial) {
        if (clienteRepository.findByRazonSocial(rSocial) == null) {
            throw new ResourceNotFoundException(
                    "El Cliente con Razon Social " + rSocial + ", no existe o esta inactivo.");
        }
        return clienteRepository.findByRazonSocial(rSocial);
    }
    
    /**
     * Metodo que permite validar si una RazonSocial existe en el conjunto de Clientes Activos.
     *
     * @param rSocial Razon Social a validar.
     * @return @{true} Si la Razon Social suministrada existe de lo contrario devuelve @{false}
     */
    public boolean existeByRazonSocial(String rSocial) {
        if (rSocial == null) {
            throw new ResourceNotFoundException("La Razon Social no puede estar vacia.");
        }
        return clienteRepository.findByRazonSocial(rSocial) != null;
    }
    
    /**
     * @param entity Objeto ClienteInputDTO
     * @return Objeto Cliente
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com Metodo que permite asignar uno o muchos Modulos a un
     * Cliente.
     */
    public Cliente asignarMudulo(ClienteInputDTO entity) {
        if (entity.getIdCliente() == null || !existeById(entity.getIdCliente())) {
            throw new ResourceNotFoundException("El Cliente con Id " + entity.getIdCliente() + " no existe.");
        }
        if (entity.getModuloCollection() == null || entity.getModuloCollection().size() <= 0) {
            throw new ResourceNotFoundException("No se han seleccionado Modulo(s) para asignar al Cliente.");
        }
        for (Modulo modulo : entity.getModuloCollection()) {
            if (!moduloService.existeById(modulo.getIdModulo())) {
                throw new ResourceNotFoundException("El Modulo con id " + modulo.getIdModulo() + " no existe.");
            }
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(entity.getIdCliente());
        cliente.setModuloCollection(entity.getModuloCollection());
        return clienteRepository.saveAndFlush(cliente);
    }
    
    /**
     * @param entity Objeto ClienteInputDTO
     * @return Objeto Cliente
     * @autor Alejandro Herrera Montilla - alhemoasde@gmail.com Metodo que permite asignar uno o muchos Productos a un
     * Cliente.
     */
    public Cliente asignarProducto(ClienteInputDTO entity) {
        if (entity.getIdCliente() == null || !existeById(entity.getIdCliente())) {
            throw new ResourceNotFoundException("El Cliente con Id " + entity.getIdCliente() + " no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(entity.getIdCliente());
        if (cliente.getModuloCollection() == null || cliente.getModuloCollection().size() <= 0) {
            throw new ResourceNotFoundException("El Cliente no tiene Modulo(s) asignados.");
        }
        if (entity.getProductoCollection() == null || entity.getProductoCollection().size() <= 0) {
            throw new ResourceNotFoundException("No se han seleccionado Producto(s) para asignar al Cliente.");
        }
        for (Producto producto : entity.getProductoCollection()) {
            if (!productoService.existeById(producto.getIdProducto())) {
                throw new ResourceNotFoundException("El Producto con id " + producto.getIdProducto() + " no existe.");
            }
            //Todo Validar funcionamiento correcto de este codigo .next()
            if (!productoService.productoXModulo(cliente.getModuloCollection().iterator().next().getIdModulo())
                    .contains(producto)) {
                throw new ResourceNotFoundException("El Producto con id " + producto.getIdProducto()
                        + " no esta asignado a alguno de los modulos del Cliente.");
            }
        }
        cliente.setProductoCollection(entity.getProductoCollection());
        return clienteRepository.saveAndFlush(cliente);
    }
    
    /**
     * @param idCargo Id del Cargo a validar.
     * @return Objeto Cliente.
     * @autor Alejandro Herrera Montilla Metodo que permite obtener el Cliente mediante uno de sus Cargos.
     */
    public Cliente findXCargo(Long idCargo) {
        if (idCargo == null) {
            throw new ResourceNotFoundException("El Cargo no puede estar vacio.");
        }
        if (clienteRepository.obtenerClienteXCargo(idCargo) == null) {
            throw new ResourceNotFoundException("El Cargo indicado no registra un Cliente activo del sistema.");
        }
        return clienteRepository.obtenerClienteXCargo(idCargo);
    }
    
    /**
     * @param username Nombre de Usuario a validar.
     * @return Objeto Cliente.
     * @autor Alejandro Herrera Montilla Metodo que permite obtener el Cliente mediante el username de uno de sus
     * Usuarios.
     */
    public Cliente clienteXUsuario(String username) {
        if (username == null) {
            throw new ResourceNotFoundException("El nombre de Usuario no puede estar vacio.");
        }
        if (!usuarioService.existeByUsername(username)) {
            throw new ResourceNotFoundException(
                    "El nombre de usuario indicado no pertenece a algún Usuario activo del sistema.");
        }
        if (clienteRepository.obtenerClienteXUsuario(username.trim().toLowerCase()) == null) {
            throw new ResourceNotFoundException(
                    "El nombre de usuario indicado no pertenece a algún Cliente activo del sistema.");
        }
        return clienteRepository.obtenerClienteXUsuario(username.trim().toLowerCase());
    }
    
    /**
     * @param idDepto Id del Departamento a validar.
     * @return Objeto Cliente.
     * @autor Alejandro Herrera Montilla Metodo que permite obtener el Cliente mediante uno de sus Departamentos.
     */
    public Cliente findXDepartamento(Long idDepto) {
        if (idDepto == null) {
            throw new ResourceNotFoundException("El Departamento no puede estar vacio.");
        }
        if (clienteRepository.obtenerClienteXDepartamento(idDepto) == null) {
            throw new ResourceNotFoundException("El Departamento indicado no registra un Cliente activo del sistema.");
        }
        return clienteRepository.obtenerClienteXDepartamento(idDepto);
    }
    
    @Override
    public Cliente create(ClienteInputDTO entity, List<MultipartFile> files) {
        return null;
    }
    
    @Override
    public Cliente update(ClienteInputDTO entity, List<MultipartFile> files) {
        return null;
    }
}
