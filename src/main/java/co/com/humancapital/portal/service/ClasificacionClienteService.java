package co.com.humancapital.portal.service;

import co.com.humancapital.portal.entity.ClasificacionCliente;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.ClasificacionClienteRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Johan Miguel Céspedes Ortega
 * @date 14/11/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de la ClasificacionCliente.
 */
@Service
public class ClasificacionClienteService implements ServiceInterfaceDto<ClasificacionCliente, Long, ClasificacionCliente> {

    private final ClasificacionClienteRepository clasificacionClienteRepository;

    public ClasificacionClienteService(ClasificacionClienteRepository clasificacionClienteRepository) {
        this.clasificacionClienteRepository = clasificacionClienteRepository;
    }

    @Override
    public ClasificacionCliente findId(Long key) throws Exception {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("La Clasificación con id "+key+" no existe.");
        }
        return clasificacionClienteRepository.findByIdClasificacion(key);
    }

    @Override
    public Page<ClasificacionCliente> findAll(Pageable pageable) throws Exception {
        if(clasificacionClienteRepository.findAll(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Clasificaciones para los Clientes creados en el sistema.");
        }
        return clasificacionClienteRepository.findAll(pageable);
    }

    @Override
    public ClasificacionCliente create(ClasificacionCliente entity) throws Exception {
        return clasificacionClienteRepository.saveAndFlush(entity);
    }

    @Override
    public ClasificacionCliente update(ClasificacionCliente entity) throws Exception {
        return clasificacionClienteRepository.saveAndFlush(entity);
    }

    @Override
    public void delete(Long key, Long usuarioInactiva) throws Exception {
        clasificacionClienteRepository.delete(findId(key));
    }

    @Override
    public Boolean existeById(Long key) throws Exception {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(clasificacionClienteRepository.findByIdClasificacion(key)==null){
            return false;
        }
        return clasificacionClienteRepository.existsById(key);
    }

    @Override
    public ClasificacionCliente create(ClasificacionCliente entity, List<MultipartFile> files) throws Exception {
        return null;
    }

    @Override
    public ClasificacionCliente update(ClasificacionCliente entity, List<MultipartFile> files) throws Exception {
        return null;
    }
}
