package co.com.humancapital.portal.service;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailNotificationService {
    private final JavaMailSender emailSender;
    
    @Async
    public void sendSimpleMessage(List<String> to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to.toArray(new String[to.size()]));
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }
    
    @Async
    public void sendHtmlMessage(List<String> to, String subject, String text, String fromAddress, String fromName) {
        MimeMessagePreparator message = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(fromAddress, fromName);
            messageHelper.setTo(to.toArray(new String[to.size()]));
            messageHelper.setSubject(subject);
            messageHelper.setText(text, true);
        };
        
        emailSender.send(message);
    }
    
    @Async
    public void sendHtmlMessage(String to, String subject, String text, String fromAddress, String fromName) {
        MimeMessagePreparator message = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(fromAddress, fromName);
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(text, true);
        };
        
        log.info("::: Enviando correo a {} de {}", to, fromAddress);
        emailSender.send(message);
        log.info("::: Correo enviado a {} de {}", to, fromAddress);
    }
}
