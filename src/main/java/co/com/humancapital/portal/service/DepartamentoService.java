package co.com.humancapital.portal.service;

import co.com.humancapital.portal.dto.CargoInputDTO;
import co.com.humancapital.portal.dto.DepartamentoInputDTO;
import co.com.humancapital.portal.dto.DepartamentoOutputDTO;
import co.com.humancapital.portal.entity.Cargo;
import co.com.humancapital.portal.entity.Cliente;
import co.com.humancapital.portal.entity.Departamento;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.get.dto.OrganizarJerarquiaInputDTO;
import co.com.humancapital.portal.get.repository.CategoriaRepository;
import co.com.humancapital.portal.repository.CargoRepository;
import co.com.humancapital.portal.repository.ClienteRepository;
import co.com.humancapital.portal.repository.DepartamentoRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * @author Alejandro Herrera Montilla
 * @date 15/11/2019
 * Clase para la implementación de las diferentes reglas de negocio que se generen alrededor del
 * Departamento.
 */
@Service
public class DepartamentoService implements ServiceInterfaceDto<Departamento, Long, DepartamentoInputDTO> {
    
    private final DepartamentoRepository departamentoRepository;
    private final ClienteService clienteService;
    private final CargoRepository cargoRepository;
    private final CategoriaRepository categoriaRepository;
    private final CargoService cargoService;
    private final ClienteRepository clienteRepository;

    public DepartamentoService(DepartamentoRepository departamentoRepository, ClienteService clienteService, CargoRepository cargoRepository, CategoriaRepository categoriaRepository, @Lazy CargoService cargoService, ClienteRepository clienteRepository) {
        this.departamentoRepository = departamentoRepository;
        this.clienteService = clienteService;
        this.cargoRepository = cargoRepository;
        this.categoriaRepository = categoriaRepository;
        this.cargoService = cargoService;
        this.clienteRepository = clienteRepository;
    }


    @Override
    public Departamento findId(Long key) {
        if (key == null || !existeById(key)) {
            throw new ResourceNotFoundException("El Departamento con id " + key + " no existe.");
        }
        existeById(key);
        return departamentoRepository.findByIdDepartamentoAndActivoTrue(key);
    }
    
    @Override
    public Page<Departamento> findAll(Pageable pageable) {
        if (departamentoRepository.findAllByActivoTrue(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Departamentos creados en el sistema.");
        }
        return departamentoRepository.findAllByActivoTrue(pageable);
    }
    
    @Override
    @Transactional
    public Departamento create(DepartamentoInputDTO entity) {
        if (entity.getNombre() == null || entity.getNombre().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Nombre(s) del Departamento a Crear no puede estar vacio.");
        }
        if (entity.getCliente() == null || !clienteService.existeById(entity.getCliente())) {
            throw new ResourceNotFoundException("El Cliente seleccionado no existe o no se envio el dato.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente());
        for (Departamento departamento : departamentoRepository
                .findAllByClienteAndActivoTrue(cliente)){
            if (departamento.getNombre().trim().toLowerCase().equals(entity.getNombre().trim().toLowerCase())) {
                throw new ResourceNotFoundException("El Departamento a Crear, ya existe en el Cliente seleccionado.");
            }
        }
        Departamento departamento = new Departamento();
        departamento.setNombre(entity.getNombre().toUpperCase());
        departamento.setCliente(cliente);
        if(entity.getPesoOrden() != null){
            departamento.setPesoOrden(entity.getPesoOrden());
        }
        if(entity.getDeptoPadre() != null){
            if(!existeById(entity.getDeptoPadre())){
                throw new ResourceNotFoundException("El Departamento padre a asignar no existe.");
            }
            Departamento deptoPadre = departamentoRepository.findByIdDepartamentoAndActivoTrue(entity.getDeptoPadre());
            if (!departamentoRepository.findAllByClienteAndActivoTrue(cliente).contains(deptoPadre)) {
                throw new ResourceNotFoundException("El Departamento  "+deptoPadre.getIdDepartamento()+" seleccionado como padre no pertences al cliente "+entity.getCliente());
            }
            departamento.setDeptoPadre(deptoPadre);
            List<Departamento> deptoHijos = departamentoRepository.obtenerDeptoHijo(deptoPadre.getIdDepartamento());
            departamento.setPesoOrden(deptoHijos==null?1:deptoHijos.size()+1);
        }
        departamento.setActivo(true);
        departamento.setFechaCreacion(new Date());
        departamento.setUsuarioCrea(entity.getUsuarioCrea());
        departamento.setFechaModifica(new Date());
        departamento.setUsuarioModifica(entity.getUsuarioCrea());
        departamento.setJefe(asignarCargoJefe(departamentoRepository.saveAndFlush(departamento)).getIdCargo());
        return departamentoRepository.saveAndFlush(departamento);
    }
    
    @Override
    public Departamento update(DepartamentoInputDTO entity) {
        existeById(entity.getIdDepartamento());
        if (entity.getNombre() == null || entity.getNombre().trim().isEmpty()) {
            throw new ResourceNotFoundException("El Nombre(s) del Departamento a Actualizar no puede estar vacio.");
        }
        if (entity.getCliente() == null || !clienteService.existeById(entity.getCliente())) {
            throw new ResourceNotFoundException("El Cliente seleccionado no existe.");
        }
        Cliente cliente = clienteRepository.findByIdClienteAndActivoTrue(entity.getCliente());
        Departamento departamento = findId(entity.getIdDepartamento());
        departamento.setNombre(entity.getNombre().toUpperCase());
        departamento.setCliente(cliente);
        if(entity.getPesoOrden() != null){
            departamento.setPesoOrden(entity.getPesoOrden());
        }
        if(entity.getDeptoPadre() != null){
            if(!existeById(entity.getDeptoPadre())){
                throw new ResourceNotFoundException("El Departamento padre a asignar no existe.");
            }
            Departamento deptoPadre = departamentoRepository.findByIdDepartamentoAndActivoTrue(entity.getDeptoPadre());
            if (!departamentoRepository.findAllByClienteAndActivoTrue(cliente).contains(deptoPadre)) {
                throw new ResourceNotFoundException("El Departamento  "+departamento.getIdDepartamento()+" seleccionado como padre no pertences al cliente."+entity.getCliente());
            }
            departamento.setDeptoPadre(deptoPadre);
        }
        departamento.setFechaModifica(new Date());
        departamento.setUsuarioModifica(entity.getUsuarioModifica());
        return departamentoRepository.saveAndFlush(departamento);
    }
    
    @Override
    public void delete(Long key, Long usuarioInactiva) {
        if (key == null) {
            throw new ResourceNotFoundException("El Departamento con id no puede estar vacio.");
        }
        existeById(key);
        if (cargoRepository.obtenerCargoXCliente(key).size() > 0) {
            throw new ResourceNotFoundException(
                    "El Departamento no puede ser eliminado por que tiene Cargos activos asignados.");
        }
        Departamento departamento = findId(key);
        departamento.setJefe(0L);
        departamento.setActivo(false);
        departamento.setFechaInactivacion(new Date());
        departamento.setUsuarioInactiva(usuarioInactiva);
        departamentoRepository.save(departamento);
    }
    
    @Override
    public Boolean existeById(Long key) {
        if (departamentoRepository.findByIdDepartamentoAndActivoTrue(key) == null) {
            throw new ResourceNotFoundException("El departamento con id " + key + " no existe.");
        }
        return departamentoRepository.existsById(key);
    }
    
    /**
     * @author Alejandro Herrera Montilla
     * Metodo que permite listar los Departamentos que pertenecen a un Cliente.
     * @param idCliente Objeto Cliente.
     * @return Listado de Departamentos pertenecientes a un Cliente.
     */
    public Page<Departamento> departamentoXCliente(Long idCliente, Pageable pageable) {
        if (idCliente == null || !clienteService.existeById(idCliente)) {
            throw new ResourceNotFoundException("El Cliente con id " + idCliente + " no exite.");
        }
        Cliente cliente = clienteService.findId(idCliente);
        return departamentoRepository.findAllByClienteAndActivoTrue(cliente, pageable);
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 29/03/2020
     * Metodo que permite crear el cargo principal del jefe del Departamento.
     * @param entity Objeto Departamento, al cual se le asignara el Cargo creado.
     * @return Objeto Cargo creado.
     */
    public Cargo asignarCargoJefe(Departamento entity){
        CargoInputDTO cargo = new CargoInputDTO();
        cargo.setNombre("Jefe "+entity.getNombre());
        cargo.setCategoria(categoriaRepository.findByNombreAndActivoTrue("ESTRATEGICO").getIdCategoria());
        cargo.setDepartamento(entity.getIdDepartamento());
        cargo.setCargoJefe(true);
        cargo.setUsuarioCrea(entity.getUsuarioCrea());
        cargo.setUsuarioModifica(entity.getUsuarioCrea());
        return cargoService.create(cargo);
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 27/03/2020
     * Metodo que permite modelar la estructura jerarquica de la organizacion a nivel de sus Departamentos.
     * @param inicio Id del Departamento desde el cual se desea desplegar la organización jerarquica.
     * @param nivel Determina el nivel maximo al caul se desea visualizar donde cero permite visualizar todo.
     * @return Lista de datos de los departamentos organizados jerarquicamente.
     */
    public List<Map<String,Map<String,Object>>> verEstructuraJerarquica(String inicio, int nivel){
        if (inicio == null || !existeById(new Long(inicio))) {
            throw new ResourceNotFoundException("El Departamento con id " + inicio + " no exite.");
        }
        List<Map<String,Map<String,Object>>> jerarquia = new ArrayList<>();
        for (Object[] list:departamentoRepository.organigramaDepartamentos(inicio,nivel)) {
            if(departamentoRepository.findByIdDepartamentoAndActivoTrue(new Long(list[0].toString()))!=null){
                Map<String, Map<String,Object>> data = new LinkedHashMap<>();
                Map<String,Object> dataDepto = new LinkedHashMap<>();
                Departamento departamento = departamentoRepository.findByIdDepartamentoAndActivoTrue(new Long(list[0].toString()));
                dataDepto.put("idDepartamento",departamento.getIdDepartamento());
                dataDepto.put("nombre",departamento.getNombre());
                dataDepto.put("nivel",list[2]);
                dataDepto.put("branch",list[3]);
                data.put("departamento",dataDepto);
                Map<String,Object> dataDeptoPadre = new LinkedHashMap<>();
                Departamento deptoPadre = departamentoRepository.findByIdDepartamentoAndActivoTrue(new Long(String.valueOf(list[1]!=null?list[1].toString():0)));
                if(deptoPadre != null){
                    dataDeptoPadre.put("idDepartamento",deptoPadre.getIdDepartamento());
                    dataDeptoPadre.put("nombre",deptoPadre.getNombre());
                } else if(departamento.getDeptoPadre()!=null){
                    dataDeptoPadre.put("idDepartamento",departamento.getDeptoPadre().getIdDepartamento());
                    dataDeptoPadre.put("nombre",departamento.getDeptoPadre().getNombre());
                }
                data.put("padre",dataDeptoPadre);
                jerarquia.add(data);
            }
        }
        return jerarquia;
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 27/03/2020
     * Metodo que permite obtener los departamentos hijos de primer nivel de un Departamento.
     * @param idPadre Id del Departamento desde el cual se desea desplegar la organización jerarquica.
     * @return Lista de DepartamentoOutputDTO con la data de los Departamentos Hijos.
     */
    public List<DepartamentoOutputDTO> findAllDeptoHijo(Long idPadre) {
        List<DepartamentoOutputDTO> departamentoOutputDTOS = new ArrayList<>();
        for (Departamento departamento: departamentoRepository.obtenerDeptoHijo(idPadre)) {
            departamentoOutputDTOS.add(DepartamentoOutputDTO.getDTO(departamento));
        }
        return departamentoOutputDTOS;
    }

    /**
     * @author Alejandro Herrera Montilla
     * @date 29/03/2020
     * Metodo que permite actualizar la organización jerarquica del Departamento.
     * @param entity Objeto OrganizarJerarquiaInputDTO con la data a actualizar.
     */
    public void actualizarOrganizacion(OrganizarJerarquiaInputDTO entity){
        if (entity.getIdEntiy() == null || !existeById(entity.getIdEntiy())) {
            throw new ResourceNotFoundException("El Departamento con id " + entity.getIdEntiy() + " no exite.");
        }
        if (entity.getIdEntityPadre() == null || !existeById(entity.getIdEntityPadre())) {
            throw new ResourceNotFoundException("El Departamento padre con id " + entity.getIdEntityPadre() + " no exite.");
        }
        Departamento departamento = departamentoRepository.findByIdDepartamentoAndActivoTrue(entity.getIdEntiy());
        departamento.setDeptoPadre(departamentoRepository.findByIdDepartamentoAndActivoTrue(entity.getIdEntityPadre()));
        departamento.setPesoOrden(entity.getPesoOrden());
        departamento.setFechaModifica(new Date());
        departamento.setUsuarioModifica(entity.getUsuarioModifica());
        departamentoRepository.save(departamento);
    }

    @Override
    public Departamento create(DepartamentoInputDTO entity, List<MultipartFile> files) {
        return null;
    }
    
    @Override
    public Departamento update(DepartamentoInputDTO entity, List<MultipartFile> files) {
        return null;
    }
}
