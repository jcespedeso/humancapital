package co.com.humancapital.portal.service;

import co.com.humancapital.portal.entity.NivelArl;
import co.com.humancapital.portal.exception.ResourceNotFoundException;
import co.com.humancapital.portal.repository.NivelArlRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Alejandro Herrera Montilla
 * @date 11/12/2019
 * Clase para la implementación de las diferentes reglas de negocio
 * que se generen alrededor de los niveles de riesgo de la ARL.
 */
@Service
@RequiredArgsConstructor
public class NivelArlService implements ServiceInterfaceDto<NivelArl, Integer, NivelArl> {

    private final NivelArlRepository nivelArlRepository;

    @Override
    public NivelArl findId(Integer key) {
        if(key == null || !existeById(key)){
            throw new ResourceNotFoundException("El Nivel de ARL con id "+key+" no existe.");
        }
        return nivelArlRepository.findByCodigo(key);
    }

    @Override
    public Page<NivelArl> findAll(Pageable pageable) {
        if(nivelArlRepository.findAll(pageable).isEmpty()) {
            throw new ResourceNotFoundException("No existen Niveles de ARL creados en el sistema.");
        }
        return nivelArlRepository.findAll(pageable);
    }

    @Override
    public NivelArl create(NivelArl entity) {
        return nivelArlRepository.saveAndFlush(entity);
    }

    @Override
    public NivelArl update(NivelArl entity) {
        return nivelArlRepository.saveAndFlush(entity);
    }

    @Override
    public void delete(Integer key, Long usuarioInactiva) {
        NivelArl nivelArl= nivelArlRepository.findByCodigo(key);
        nivelArlRepository.delete(nivelArl);
    }

    @Override
    public Boolean existeById(Integer key) {
        if(key==null){
            throw new ResourceNotFoundException("El ID a validar, no puede estar vacio.");
        }
        if(nivelArlRepository.findByCodigo(key)==null){
            return false;
        }
        return true;
    }

    @Override
    public NivelArl create(NivelArl entity, List<MultipartFile> files) {
        return null;
    }

    @Override
    public NivelArl update(NivelArl entity, List<MultipartFile> files) {
        return null;
    }
}
